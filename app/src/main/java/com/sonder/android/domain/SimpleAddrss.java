package com.sonder.android.domain;

import android.location.Address;
import android.renderscript.Double2;

import com.baidu.location.BDLocation;
import com.common.util.StringUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wangzy on 2016/11/23.
 */

public class SimpleAddrss {


    private String id;

    @SerializedName("student_id")
    private String studentId;

    private String address;


    @SerializedName("location_lat")
    private String lat;

    @SerializedName("location_lon")
    private String lng;


    public String getAddress() {
        return address;
    }


    public LatLng getLatLng(){

        LatLng latLng=new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        return latLng;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public static SimpleAddrss frombdLocation(BDLocation bdLocation) {

        SimpleAddrss simpleAddrss = new SimpleAddrss();


        simpleAddrss.setAddress(bdLocation.getAddrStr());

        simpleAddrss.setLat(String.valueOf(bdLocation.getLatitude()));
        simpleAddrss.setLng(String.valueOf(bdLocation.getLongitude()));


        return simpleAddrss;

    }

    public static SimpleAddrss fromGoogleAddress(Address bdLocation) {

        SimpleAddrss simpleAddrss = new SimpleAddrss();

        simpleAddrss.setAddress(getResultStress(bdLocation));

        simpleAddrss.setLat(String.valueOf(bdLocation.getLatitude()));
        simpleAddrss.setLng(String.valueOf(bdLocation.getLongitude()));

        return simpleAddrss;

    }


    public static String getResultStress(Address addres) {

        String l1 = addres.getAddressLine(0);
        String l2 = addres.getAddressLine(1);
        String l3 = addres.getAddressLine(2);

        String addressLine = StringUtils.isEmpty(l1) ? "" : l1 + ", " + (StringUtils.isEmpty(l2) ? "" : l2 + ", ") + (StringUtils.isEmpty(l3) ? "" : l3);

        try {
            if (addressLine.endsWith(", ")) {
                addressLine = addressLine.substring(0, addressLine.lastIndexOf(", "));
            }
        } catch (Exception e) {

        }

        return addressLine;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public boolean equals(Object obj) {

        SimpleAddrss sa = (SimpleAddrss) obj;

        return sa.getAddress().equals(address) ;//&& sa.getLat().equals(getLat()) && sa.getLng().equals(getLng());

    }

    @Override
    public String toString() {

        return address+"("+lat+","+lng+")";
    }
}
