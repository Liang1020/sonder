package com.sonder.android.domain;

import java.util.Date;

/**
 * Created by leocx on 2016/10/28.
 */

public class Student {
    private int studentId;
    private String email;
    private int status;
    private String fullName;
    private String preferredName;
    private Date dob;
    private String address;
    private long lastKnownLat;
    private long lastKnownLon;
    private String lastKnownAddress;
    private int planLevel;
    private Date planExpiry;
    private String avatar;
    private String phoneNumber;
    private int gender;
    private int profileCompletion;
    private Date lastLoginTime;
    private String registerCity;
    private String educationAgent;
    private Date createdAt;
    private Date updateAt;
    private Profile profile;


    public Student() {

    }

    public Student(Student student) {
        setStudentId(student.getStudentId());
        setEmail(student.getEmail());
        setStatus(student.getStatus());
        setFullName(student.getFullName());
        setPreferredName(student.getPreferredName());
        setDob(student.getDob());
        setAddress(student.getAddress());
        setLastKnownLat(student.getLastKnownLat());
        setLastKnownLon(student.getLastKnownLon());
        setLastKnownAddress(student.getLastKnownAddress());
        setPlanLevel(student.getPlanLevel());
        setPlanExpiry(student.getPlanExpiry());
        setAvatar(student.getAvatar());
        setPhoneNumber(student.getPhoneNumber());
        setGender(student.getGender());
        setProfileCompletion(student.getProfileCompletion());
        setLastLoginTime(student.getLastLoginTime());
        setRegisterCity(student.getRegisterCity());
        setEducationAgent(student.getEducationAgent());
        setCreatedAt(student.getCreatedAt());
        setUpdateAt(student.getUpdateAt());
        setProfile(new Profile(student.getProfile()));
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getLastKnownLat() {
        return lastKnownLat;
    }

    public void setLastKnownLat(long lastKnownLat) {
        this.lastKnownLat = lastKnownLat;
    }

    public long getLastKnownLon() {
        return lastKnownLon;
    }

    public void setLastKnownLon(long lastKnownLon) {
        this.lastKnownLon = lastKnownLon;
    }

    public String getLastKnownAddress() {
        return lastKnownAddress;
    }

    public void setLastKnownAddress(String lastKnownAddress) {
        this.lastKnownAddress = lastKnownAddress;
    }

    public int getPlanLevel() {
        return planLevel;
    }

    public void setPlanLevel(int planLevel) {
        this.planLevel = planLevel;
    }

    public Date getPlanExpiry() {
        return planExpiry;
    }

    public void setPlanExpiry(Date planExpiry) {
        this.planExpiry = planExpiry;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getProfileCompletion() {
        return profileCompletion;
    }

    public void setProfileCompletion(int profileCompletion) {
        this.profileCompletion = profileCompletion;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getRegisterCity() {
        return registerCity;
    }

    public void setRegisterCity(String registerCity) {
        this.registerCity = registerCity;
    }

    public String getEducationAgent() {
        return educationAgent;
    }

    public void setEducationAgent(String educationAgent) {
        this.educationAgent = educationAgent;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "Student{" +
                "\n    studentId=" + studentId +
                ",\n    email='" + email + '\'' +
                ",\n    status=" + status +
                ",\n    fullName='" + fullName + '\'' +
                ",\n    preferredName='" + preferredName + '\'' +
                ",\n    dob=" + dob +
                ",\n    address='" + address + '\'' +
                ",\n    lastKnownLat=" + lastKnownLat +
                ",\n    lastKnownLon=" + lastKnownLon +
                ",\n    lastKnownAddress='" + lastKnownAddress + '\'' +
                ",\n    planLevel=" + planLevel +
                ",\n    planExpiry=" + planExpiry +
                ",\n    avatar='" + avatar + '\'' +
                ",\n    phoneNumber='" + phoneNumber + '\'' +
                ",\n    gender=" + gender +
                ",\n    profileCompletion=" + profileCompletion +
                ",\n    lastLoginTime=" + lastLoginTime +
                ",\n    registerCity='" + registerCity + '\'' +
                ",\n    educationAgent='" + educationAgent + '\'' +
                ",\n    createdAt=" + createdAt +
                ",\n    updateAt=" + updateAt +
                ",\n    profile=" + profile.toString() +
                "\n}";
    }
}
