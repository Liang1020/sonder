package com.sonder.android.domain;

import com.sonder.android.App;
import com.sonder.android.R;

import java.io.Serializable;
import java.util.Locale;

/**
 * Created by wangzy on 2016/12/29.
 */

public class Angel implements Serializable {

    private String angleId;
    private String studentId;
    private String status;
    private String lat;
    private String lon;
    private String address;
    private String duration;
    private String checkInTime;
    private String checkOutTime;
    private String notification_every;
    private String crateAt;


    public String getAddressText() {
        return address;
    }

    public String getDurationText() {

        int d = Integer.parseInt(duration);

        int h = d / 60;
        int m = d % 60;

        String hour = App.getApp().getString(R.string.hour);
        String min = App.getApp().getString(R.string.minute);


        String lang = Locale.getDefault().getLanguage().toLowerCase();
        String ret = "";
        if (lang.equalsIgnoreCase("zh")) {
            ret = ((0 == h) ? "" : String.valueOf(h) + (h > 1 ? hour : hour) + " " + (String.valueOf(m) + m));
        } else {
            ret = ((0 == h ? "" : (String.valueOf(h) + " " + (h > 1 ? (hour + "s") : hour))) + " " + (String.valueOf(m) + " " + (m > 1 ? (min + "s") : min)));
        }
        return ret;
    }


    public String getNotificationEvey() {

        int d = Integer.parseInt(notification_every);

        if (d == 0) {
            return App.getApp().getResources().getString(R.string.never);
        }

        int h = d / 60;
        int m = d % 60;


        String hour = App.getApp().getString(R.string.hour);
        String min = App.getApp().getString(R.string.minute);

        String every = App.getApp().getString(R.string.every);
        String lang = Locale.getDefault().getLanguage().toLowerCase();

        String ret = "";

        if (lang.equalsIgnoreCase("zh")) {

            ret = every + (((0 == h) ? "" : String.valueOf(h) + hour) + (m > 0 ? (m + min) : ""));

        } else {
            if (h > 0) {
                ret = every + h + (h >= 1 ? hour + "s" : hour);
            } else {
                ret = every;
            }
            ret += " " + m + " " + (m >= 1 ? min : (min + "s"));

        }

        return ret;
    }


    public String getAngleId() {
        return angleId;
    }

    public void setAngleId(String angleId) {
        this.angleId = angleId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getNotification_every() {
        return notification_every;
    }

    public void setNotification_every(String notification_every) {
        this.notification_every = notification_every;
    }

    public String getCrateAt() {
        return crateAt;
    }

    public void setCrateAt(String crateAt) {
        this.crateAt = crateAt;
    }
}
