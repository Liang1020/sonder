package com.sonder.android.domain;

import java.util.Date;

/**
 * Created by leocx on 2016/11/1.
 */

public class Profile {
    private int profileId;
    private String studentId;
    private String educationStudentId;
    private String educationInstitutionName;
    private String educationInstitutionCampusName;
    private String educationInstitutionCampusPhoneNumber;
    private String educationInstitutionCampusAddress;
    private String primaryContactName;
    private String primaryContactPhone;
    private String secondaryContactName;
    private String secondaryContactPhone;
    private String medicalInsuranceProvider;
    private String medicalInsuranceMemberNumber;
    private String medicalExistingConditions;
    private String medicalHistory;
    private String medicalAllergies;
    private String medicalCurrentMedication;
    private String travelInsuranceProvider;
    private String travelInsuranceMemberNumber;
    private String travelPassportPhotoUrl;
    private String travelDriverLicenseNumber;
    private String travelVisaType;
    private Date travelVisaStartDate;
    private Date travelVisaEndDate;
    private Date createdAt;
    private Date updateAt;

    public Profile() {

    }

    public Profile(Profile pro) {
        profileId = pro.getProfileId();
        studentId = pro.getStudentId();
        educationStudentId = pro.getEducationStudentId();
        educationInstitutionName = pro.getEducationInstitutionName();
        educationInstitutionCampusName = pro.getEducationInstitutionCampusName();
        educationInstitutionCampusPhoneNumber = pro.getEducationInstitutionCampusPhoneNumber();
        educationInstitutionCampusAddress = pro.getEducationInstitutionCampusAddress();
        primaryContactName = pro.getPrimaryContactName();
        primaryContactPhone = pro.getPrimaryContactPhone();
        secondaryContactName = pro.getSecondaryContactName();
        secondaryContactPhone = pro.getSecondaryContactPhone();
        medicalInsuranceProvider = pro.getMedicalInsuranceProvider();
        medicalInsuranceMemberNumber = pro.getMedicalInsuranceMemberNumber();
        medicalExistingConditions = pro.getMedicalExistingConditions();
        medicalHistory = pro.getMedicalHistory();
        medicalAllergies = pro.getMedicalAllergies();
        medicalCurrentMedication = pro.getMedicalCurrentMedication();
        travelInsuranceProvider = pro.getTravelInsuranceProvider();
        travelInsuranceMemberNumber = pro.getTravelInsuranceMemberNumber();
        travelPassportPhotoUrl = pro.getTravelPassportPhotoUrl();
        travelDriverLicenseNumber = pro.getTravelDriverLicenseNumber();
        travelVisaType = pro.getTravelVisaType();
        travelVisaStartDate = pro.getTravelVisaStartDate();
        travelVisaEndDate = pro.getTravelVisaEndDate();
        createdAt = pro.getCreatedAt();
        updateAt = pro.getUpdateAt();
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getEducationStudentId() {
        return educationStudentId;
    }

    public void setEducationStudentId(String educationStudentId) {
        this.educationStudentId = educationStudentId;
    }

    public String getEducationInstitutionName() {
        return educationInstitutionName;
    }

    public void setEducationInstitutionName(String educationInstitutionName) {
        this.educationInstitutionName = educationInstitutionName;
    }

    public String getEducationInstitutionCampusName() {
        return educationInstitutionCampusName;
    }

    public void setEducationInstitutionCampusName(String educationInstitutionCampusName) {
        this.educationInstitutionCampusName = educationInstitutionCampusName;
    }

    public String getEducationInstitutionCampusPhoneNumber() {
        return educationInstitutionCampusPhoneNumber;
    }

    public void setEducationInstitutionCampusPhoneNumber(String educationInstitutionCampusPhoneNumber) {
        this.educationInstitutionCampusPhoneNumber = educationInstitutionCampusPhoneNumber;
    }

    public String getEducationInstitutionCampusAddress() {
        return educationInstitutionCampusAddress;
    }

    public void setEducationInstitutionCampusAddress(String educationInstitutionCampusAddress) {
        this.educationInstitutionCampusAddress = educationInstitutionCampusAddress;
    }

    public String getPrimaryContactName() {
        return primaryContactName;
    }

    public void setPrimaryContactName(String primaryContactName) {
        this.primaryContactName = primaryContactName;
    }

    public String getPrimaryContactPhone() {
        return primaryContactPhone;
    }

    public void setPrimaryContactPhone(String primaryContactPhone) {
        this.primaryContactPhone = primaryContactPhone;
    }

    public String getSecondaryContactName() {
        return secondaryContactName;
    }

    public void setSecondaryContactName(String secondaryContactName) {
        this.secondaryContactName = secondaryContactName;
    }

    public String getSecondaryContactPhone() {
        return secondaryContactPhone;
    }

    public void setSecondaryContactPhone(String secondaryContactPhone) {
        this.secondaryContactPhone = secondaryContactPhone;
    }

    public String getMedicalInsuranceProvider() {
        return medicalInsuranceProvider;
    }

    public void setMedicalInsuranceProvider(String medicalInsuranceProvider) {
        this.medicalInsuranceProvider = medicalInsuranceProvider;
    }

    public String getMedicalInsuranceMemberNumber() {
        return medicalInsuranceMemberNumber;
    }

    public void setMedicalInsuranceMemberNumber(String medicalInsuranceMemberNumber) {
        this.medicalInsuranceMemberNumber = medicalInsuranceMemberNumber;
    }

    public String getMedicalExistingConditions() {
        return medicalExistingConditions;
    }

    public void setMedicalExistingConditions(String medicalExistingConditions) {
        this.medicalExistingConditions = medicalExistingConditions;
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public String getMedicalAllergies() {
        return medicalAllergies;
    }

    public void setMedicalAllergies(String medicalAllergies) {
        this.medicalAllergies = medicalAllergies;
    }

    public String getMedicalCurrentMedication() {
        return medicalCurrentMedication;
    }

    public void setMedicalCurrentMedication(String medicalCurrentMedication) {
        this.medicalCurrentMedication = medicalCurrentMedication;
    }

    public String getTravelInsuranceProvider() {
        return travelInsuranceProvider;
    }

    public void setTravelInsuranceProvider(String travelInsuranceProvider) {
        this.travelInsuranceProvider = travelInsuranceProvider;
    }

    public String getTravelInsuranceMemberNumber() {
        return travelInsuranceMemberNumber;
    }

    public void setTravelInsuranceMemberNumber(String travelInsuranceMemberNumber) {
        this.travelInsuranceMemberNumber = travelInsuranceMemberNumber;
    }

    public String getTravelPassportPhotoUrl() {
        return travelPassportPhotoUrl;
    }

    public void setTravelPassportPhotoUrl(String travelPassportPhotoUrl) {
        this.travelPassportPhotoUrl = travelPassportPhotoUrl;
    }

    public String getTravelDriverLicenseNumber() {
        return travelDriverLicenseNumber;
    }

    public void setTravelDriverLicenseNumber(String travelDriverLicenseNumber) {
        this.travelDriverLicenseNumber = travelDriverLicenseNumber;
    }

    public String getTravelVisaType() {
        return travelVisaType;
    }

    public void setTravelVisaType(String travelVisaType) {
        this.travelVisaType = travelVisaType;
    }

    public Date getTravelVisaStartDate() {
        return travelVisaStartDate;
    }

    public void setTravelVisaStartDate(Date travelVisaStartDate) {
        this.travelVisaStartDate = travelVisaStartDate;
    }

    public Date getTravelVisaEndDate() {
        return travelVisaEndDate;
    }

    public void setTravelVisaEndDate(Date travelVisaEndDate) {
        this.travelVisaEndDate = travelVisaEndDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "\n    profileId=" + profileId +
                ",\n    studentId=" + studentId +
                ",\n    educationStudentId=" + educationStudentId +
                ",\n    educationInstitutionName='" + educationInstitutionName + '\'' +
                ",\n    educationInstitutionCampusName='" + educationInstitutionCampusName + '\'' +
                ",\n    educationInstitutionCampusPhoneNumber='" + educationInstitutionCampusPhoneNumber + '\'' +
                ",\n    educationInstitutionCampusAddress='" + educationInstitutionCampusAddress + '\'' +
                ",\n    primaryContactName='" + primaryContactName + '\'' +
                ",\n    primaryContactPhone='" + primaryContactPhone + '\'' +
                ",\n    secondaryContactName='" + secondaryContactName + '\'' +
                ",\n    secondaryContactPhone='" + secondaryContactPhone + '\'' +
                ",\n    medicalInsuranceProvider='" + medicalInsuranceProvider + '\'' +
                ",\n    medicalInsuranceMemberNumber='" + medicalInsuranceMemberNumber + '\'' +
                ",\n    medicalExistingConditions='" + medicalExistingConditions + '\'' +
                ",\n    medicalHistory='" + medicalHistory + '\'' +
                ",\n    medicalAllergies='" + medicalAllergies + '\'' +
                ",\n    medicalCurrentMedication='" + medicalCurrentMedication + '\'' +
                ",\n    travelInsuranceProvider='" + travelInsuranceProvider + '\'' +
                ",\n    travelInsuranceMemberNumber='" + travelInsuranceMemberNumber + '\'' +
                ",\n    travelPassportPhotoUrl='" + travelPassportPhotoUrl + '\'' +
                ",\n    travelDriverLicenseNumber='" + travelDriverLicenseNumber + '\'' +
                ",\n    travelVisaType='" + travelVisaType + '\'' +
                ",\n    travelVisaStartDate='" + travelVisaStartDate + '\'' +
                ",\n    travelVisaEndDate='" + travelVisaEndDate + '\'' +
                ",\n    createdAt=" + createdAt +
                ",\n    updateAt=" + updateAt +
                "\n}";
    }
}
