package com.sonder.android.domain;

import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wangzy on 2017/1/12.
 */

public class Walk {


    @SerializedName("wwm_id")
    private String wwmId;

    @SerializedName("student_id")
    private String studentId;

    @SerializedName("location_address")
    private String locaitonAddress;

    @SerializedName("location_lon")
    private String locationLon;

    @SerializedName("location_lat")
    private String locationLat;


    @SerializedName("destination_address")
    private String destinationAddress;

    @SerializedName("destination_lon")
    private String destinationLon;

    @SerializedName("destination_lat")
    private String destinationLat;


    public String getWwmId() {
        return wwmId;
    }

    public void setWwmId(String wwmId) {
        this.wwmId = wwmId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getLocaitonAddress() {
        return locaitonAddress;
    }

    public void setLocaitonAddress(String locaitonAddress) {
        this.locaitonAddress = locaitonAddress;
    }

    public String getLocationLon() {
        return locationLon;
    }

    public void setLocationLon(String locationLon) {
        this.locationLon = locationLon;
    }

    public String getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(String locationLat) {
        this.locationLat = locationLat;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getDestinationLon() {
        return destinationLon;
    }

    public void setDestinationLon(String destinationLon) {
        this.destinationLon = destinationLon;
    }

    public String getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(String destinationLat) {
        this.destinationLat = destinationLat;
    }

    public SimpleAddrss getStart() {

        SimpleAddrss simpleAddrss = new SimpleAddrss();

        simpleAddrss.setAddress(locaitonAddress);
        simpleAddrss.setLat(locationLat);
        simpleAddrss.setLng(locationLon);

        return simpleAddrss;

    }

    public SimpleAddrss getEnd() {


        SimpleAddrss simpleAddrss = new SimpleAddrss();

        simpleAddrss.setAddress(destinationAddress);
        simpleAddrss.setLat(destinationLat);
        simpleAddrss.setLng(destinationLon);

        return simpleAddrss;
    }

}
