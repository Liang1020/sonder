package com.sonder.android.domain;

/**
 * Created by wangzy on 2016/11/10.
 */

public class RequestStatus {

    public final static int TYPE_TEXT = 1;
    public final static int TYPE_IMG = 2;
    public final static int TYPE_PDF = 3;
    public final static int TYPE_LO_ASSIGNED = 4;
    public final static int TYPE_STU_CRATE=5;
    public final static int TYPE_CLOSE=6;

//    "1 = Free Text , 2 = Image , 3 = PDF , 4 = Incident created , 5 = Incident triage , 6 = Incident updated , 7 = Incident closed "

    private long size;
    private int type;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
