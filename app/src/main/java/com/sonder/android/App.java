package com.sonder.android;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.callback.CwiActivityLifeCycleCallback;
import com.common.BaseApp;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.sonder.android.activity.MainActivity;
import com.sonder.android.activity.SupportDetailActivity;
import com.sonder.android.domain.Angel;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Student;
import com.sonder.android.net.NetInterface;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.relex.okhttpmanager.BaseCallback;
import me.relex.okhttpmanager.OkHttpClientManager;
import me.relex.okhttpmanager.RequestParams;


/**
 * Created by leocx on 2016/9/1.
 */
public class App extends BaseApp {


    public final static String TAG = "sonder";

    private static App app;
    private static Activity activity;
    private static Service service;

    private static final String USERNAME_TAG = "lkjhgfdsa";
    private static final String PASSWORD_TAG = "mnbvcxz";
    public static final String KEY_GUILD = "keyguild";


    private Student student;
    private BDLocation bdLocation;
    private SimpleAddrss currenteSimpleAddrss;
    private LatLng currentLatlng;


    public static final String KEY_TOUCH_ID = "ascasd";
    public static final String KEY_FIST_USE = "acascaaad";
    public static final String KEY_ANGLE = "angle";

    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();
    public int PIC_W = 300, PIC_H = 300;
    public GoogleApiClient apiClient;
    private boolean isLastVisible = false;

    public static final boolean isParseLocaitonUseGoogle = false;//define parse locaiton user here or google

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        registerActivityLifecycleCallbacks(new CwiActivityLifeCycleCallback() {
            @Override
            public void onActivityResumed(Activity activity) {
                App.activity = activity;
                ++resumed;
                if (isAppVisible() && null == App.getApp().getCurrentLatlng()) {
                    startLocation();
                    conenctionApiClient();
                }

                if (isAppVisible() && !isLastVisible && null != App.getApp().getStudent()) {

                    LogUtil.i(App.TAG, "updateLocation:registerActivityLifecycleCallbacks");
                    updateLocation();
                }
            }

            @Override
            public void onActivityStopped(Activity activity) {
                ++stopped;
                if (!isAppVisible()) {
                    stopLocation();
                    disconnectApiClient();
                }
                isLastVisible = isAppVisible();
            }
        });
//        initLocation();
        initOneSignal();
        initGoogleApiClientForLocationService();

    }


    /**
     * update user location
     */
    public void updateLocation() {


        LogUtil.i(App.TAG, "updateLocation------ call in Application");
        if (null != App.getApp().getCurrenteSimpleAddrss()) {

            LogUtil.i(App.TAG, "SimpleAddress updateLocation------" + currenteSimpleAddrss.getLat()
                    + ", " + currenteSimpleAddrss.getLng() + " " + currenteSimpleAddrss.getAddress());

            new Thread(new Runnable() {
                @Override
                public void run() {
                    uplodateLocationWithAddress(getCurrenteSimpleAddrss());
                }
            }).start();

        } else if (null != currentLatlng) {

            LogUtil.i(App.TAG, " current updateLocation------ " + currentLatlng.longitude + "," + currentLatlng.latitude);
            parseLocationFromlatLng(App.getApp(), currentLatlng, new NetCallBack() {
                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {

                    if (null != result.getTag()) {

                        uplodateLocationWithAddress((SimpleAddrss) result.getTag());

                    } else {
                        SimpleAddrss simpleAddrss = new SimpleAddrss();
                        simpleAddrss.setLat(String.valueOf(currentLatlng.latitude));
                        simpleAddrss.setLng(String.valueOf(currentLatlng.longitude));

                        uplodateLocationWithAddress(simpleAddrss);
                    }
                }
            });

        } else {
            LogUtil.e(App.TAG, "UpdateLocation call fail,beacase of no location found");
        }
    }


    private void uplodateLocationWithAddress(SimpleAddrss simpleAddrss) {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("address", simpleAddrss.getAddress());
            jsonObject.put("location_lon", simpleAddrss.getLng());
            jsonObject.put("location_lat", simpleAddrss.getLat());

            NetInterface.updateLocaiton(jsonObject);

        } catch (Exception e) {
            LogUtil.e(App.TAG, "uplodateLocationWithAddress error:" + e.getLocalizedMessage());
        }

    }


    public void saveAngle(Angel angel) {
        SharePersistent.setObjectValue(this, KEY_ANGLE, angel);
    }

    public Angel getAngle() {
        return (Angel) SharePersistent.getObjectValue(this, KEY_ANGLE);
    }


    public boolean isAppVisible() {
        return resumed > stopped;
    }


    private void initOneSignal() {

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.WARN, OneSignal.LOG_LEVEL.WARN);

        OneSignal.startInit(this).autoPromptLocation(false).setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
            @Override
            public void notificationOpened(OSNotificationOpenResult result) {

                LogUtil.i(App.TAG, "notificationOpened");

                if (App.getApp().isAppVisible() && null != App.getApp().getActivity() && (App.getApp().getActivity() instanceof SupportDetailActivity)) {

                    SupportDetailActivity sa = (SupportDetailActivity) App.getApp().getActivity();
                    sa.refreshSupportDetails();
                } else if (App.getApp().isAppVisible() && null != App.getApp().getActivity() && (App.getApp().getActivity() instanceof MainActivity)) {

                }

            }

        }).setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
            @Override
            public void notificationReceived(OSNotification notification) {
                LogUtil.i(App.TAG, "notificationReceived");

            }
        }).init();

    }

    private void initLocation() {

        mLocationClient = new LocationClient(getApplicationContext());     //声明LocationClient类
        mLocationClient.registerLocationListener(myListener);

        LocationClientOption option = new LocationClientOption();

        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setScanSpan(1000 * 30);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setIsNeedLocationDescribe(true);//可选，设置是否需要地址描述
        option.setNeedDeviceDirect(false);//可选，设置是否需要设备方向结果
        option.setLocationNotify(false);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集

        option.setIsNeedAltitude(false);//可选，默认false，设置定位时是否需要海拔信息，默认不需要，除基础定位版本都可用
        mLocationClient.setLocOption(option);
    }


    public void initGoogleApiClientForLocationService() {

        apiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

            @Override
            public void onConnected(@Nullable Bundle bundle) {
                LogUtil.i(App.TAG, "connected google api service succss:");

                if (ActivityCompat.checkSelfPermission(App.getApp(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(App.getApp(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

                if (mCurrentLocation != null) {
                    LogUtil.d(App.TAG, "Google Api current location: " + mCurrentLocation.toString());
                }

                LocationRequest locationRequest = LocationRequest.create().
                        setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(10 * 1000)
                        .setFastestInterval(5 * 1000);

                LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {


                        if (null != location) {
                            LogUtil.e(App.TAG, "Google Api current location updated: " + location.getLatitude()
                                    + "," + location.getLatitude());
                            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());


                            App.getApp().setCurrentLatlng(latlng);

                            SimpleAddrss simpleAddrss = new SimpleAddrss();

                            simpleAddrss.setLat(String.valueOf(latlng.latitude));
                            simpleAddrss.setLng(String.valueOf(latlng.longitude));

//                          simpleAddrss.setAddress("Test");

                            //get address method will cause anr
//                          simpleAddrss.setAddress(getAddress(latlng.latitude, latlng.longitude));// do not call getAddress insde requestlocatoinupdates,

                            App.getApp().setCurrenteSimpleAddrss(simpleAddrss);

                            parseLocationFromlatLng(App.getApp(), latlng, new NetCallBack() {
                                @Override
                                public void onFinish(NetResult result) {
                                    if (null != result) {
                                        onParseLocationSuccess(result);
                                    }
                                }
                            });


                        }
                    }
                });

            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                LogUtil.i(App.TAG, "connection fail");
            }
        }).build();
    }


    public void conenctionApiClient() {
        if (null != apiClient && !apiClient.isConnected()) {
            apiClient.connect();
        }
    }

    public void disconnectApiClient() {
        if (null != apiClient && apiClient.isConnected()) {
            apiClient.disconnect();
        }
    }


    public static App getApp() {
        return app;
    }

    public Activity getActivity() {
        return activity;
    }

    public Service getService() {
        return service;
    }

    public void setActivity(Activity activity) {
        App.activity = activity;
    }

    public void setService(Service service) {
        App.service = service;
    }

    public void savePassword(String username, String password) {
        SharePersistent.savePreferenceEn(App.getApp().getBaseContext(), USERNAME_TAG, username);
        SharePersistent.savePreferenceEn(App.getApp().getBaseContext(), PASSWORD_TAG, password);
    }

    public String[] getPassword() {
        return new String[]{
                SharePersistent.getPreferenceDe(App.getApp().getBaseContext(), USERNAME_TAG),
                SharePersistent.getPreferenceDe(App.getApp().getBaseContext(), PASSWORD_TAG),
        };
    }

    public Student getStudent() {
        return student;
    }

    public void saveStudent(Student student) {
        this.student = student;
    }


    //=======================================================================

    public void startLocation() {

        if (null != mLocationClient && !mLocationClient.isStarted()) {
            mLocationClient.start();
            mLocationClient.requestLocation();
            LogUtil.i(App.TAG, "开始定位...........");
        }

    }


    public void stopLocation() {
        if (null != mLocationClient) {//一直开启定位
//            mLocationClient.stop();
//            LogUtil.i(App.TAG, "停止定位...........");
            LogUtil.i(App.TAG, "客户要求，一直定位，所以即使定位成功也不关闭定位");
        }
    }


    public void onLocationReceive(BDLocation bdLocation) {
        if (null != bdLocation) {
            LatLng latLng = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());

            double lat = latLng.latitude;
            double lng = latLng.longitude;
            String latlng = lat + "," + lng;
            LogUtil.e(App.TAG, "bai du location success:" + latlng + " location:" + bdLocation.getAddress().address);

            this.bdLocation = bdLocation;

            App.getApp().setCurrentLatlng(latLng);

            SimpleAddrss simpleAddrss = new SimpleAddrss();
            simpleAddrss.setLat(String.valueOf(bdLocation.getLatitude()));
            simpleAddrss.setLng(String.valueOf(bdLocation.getLongitude()));
            simpleAddrss.setAddress(bdLocation.getAddrStr());

            App.getApp().setCurrenteSimpleAddrss(simpleAddrss);

//          simpleAddrss.setLat(latlng.lat);


            App.getApp().stopLocation();//need remote this line

            parseLocationFromlatLng(this, latLng, new NetCallBack() {
                @Override
                public void onFinish(NetResult result) {
                    if (null != result) {
                        onParseLocationSuccess(result);
                        stopLocation();
                        disconnectApiClient();
                        LogUtil.e(App.TAG, "location & parse success:" + App.getApp().getCurrenteSimpleAddrss().getAddress());
                    } else {

                    }
                }
            });

        }
    }


    private static Geocoder coder;
    private static BaseTask geoCoderTask;

    public static void parseLocationFromlatLng(Context context, final LatLng latlng, final NetCallBack netCallback) {

        if (!App.isParseLocaitonUseGoogle) {

            String appId = "";
            String token = "";

            ApplicationInfo appInfo = null;
            try {
                appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                appId = appInfo.metaData.getString("com.here.android.maps.appid");
                token = appInfo.metaData.getString("com.here.android.maps.apptoken");
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String url = "";
            url = "https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json?" +
                    "prox=" + latlng.latitude + "," + latlng.longitude + ",200&mode=retrieveAddresses&maxresults=1&gen=8&app_id=" + appId + "&app_code=" + token;

            RequestParams params = new RequestParams();

            OkHttpClientManager.get(url, params,

                    new BaseCallback() {

                        @Override
                        public void onResponse(final Response response) {

                            try {

                                JSONObject jsonObject = new JSONObject(response.body().string());
                                JSONArray addreses = jsonObject.getJSONObject("Response").getJSONArray("View").getJSONObject(0).getJSONArray("Result");

                                LogUtil.i(App.TAG, "adress:" + jsonObject.toString());

                                final ArrayList<SimpleAddrss> simpleAddress = new ArrayList<SimpleAddrss>();

                                if (null != addreses && addreses.length() > 0) {

                                    for (int i = 0, isize = addreses.length(); i < isize; i++) {
                                        JSONObject jo = addreses.getJSONObject(i);

                                        JSONObject locationObj = jo.getJSONObject("Location");


                                        JSONObject DisplayPosition = locationObj.getJSONObject("DisplayPosition");

                                        double lat = DisplayPosition.getDouble("Latitude");
                                        double lon = DisplayPosition.getDouble("Longitude");

                                        JSONObject addrs = locationObj.getJSONObject("Address");

                                        String label = addrs.getString("Label");
//                                    String contry = addrs.getString("Country");
//                                    String postcode = addrs.getString("PostalCode");

                                        SimpleAddrss simple = new SimpleAddrss();
                                        simple.setAddress(label);
                                        simple.setLat(String.valueOf(latlng.latitude));
                                        simple.setLng(String.valueOf(latlng.longitude));

                                        simpleAddress.add(simple);

                                        break;// just need one address

                                    }
                                }


                                NetResult netResult = new NetResult();

                                if (!ListUtiles.isEmpty(simpleAddress)) {
                                    SimpleAddrss sa = simpleAddress.get(0);
                                    LogUtil.i(App.TAG, "addrss:" + sa.getAddress() + " " + sa.getLat() + "," + sa.getLng());
                                    netResult.setTag(sa);
                                    if (null != netCallback) {
                                        netCallback.onFinish(netResult);
                                    }
                                } else {
                                    if (null != netCallback) {
                                        netCallback.onFinish(null);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                LogUtil.i(App.TAG, "parse with here error:" + e.getLocalizedMessage());
                            }

                        }

                        @Override
                        public void onRequestFinish() {


                        }

                        @Override
                        public void onRequestStart() {
                            netCallback.onPreCall();
                        }
                    });
        } else {

            BaseTask.resetTastk(geoCoderTask);

            geoCoderTask = new BaseTask(context, new NetCallBack() {
                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                    NetResult netResult = null;
                    try {
                        if (null == coder) {
                            coder = new Geocoder(App.getApp());
                        }
                        List<Address> address = coder.getFromLocation(latlng.latitude, latlng.longitude, 1);
                        netResult = new NetResult();

                        Address adr = address.get(0);

                        SimpleAddrss sa = SimpleAddrss.fromGoogleAddress(adr);
                        netResult.setTag(sa);

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "parse geo error:" + e.getLocalizedMessage());
                    }

                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    if (null != netCallback) {
                        netCallback.onFinish(result);
                    } else {
                        netCallback.onFinish(null);
                    }
                }
            });
            geoCoderTask.execute(new HashMap<String, String>());
        }

    }

    private void onParseLocationSuccess(NetResult result) {

        SimpleAddrss address = (SimpleAddrss) result.getTag();
        setCurrenteSimpleAddrss(address);

//      LogUtil.e(App.TAG, "Parse address from google server:" + location);
    }


    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            //Receive Location
            StringBuffer sb = new StringBuffer(256);
            sb.append("time : ");
            sb.append(location.getTime());
            sb.append("\nerror code : ");
            sb.append(location.getLocType());
            sb.append("\nlatitude : ");
            sb.append(location.getLatitude());
            sb.append("\nlontitude : ");
            sb.append(location.getLongitude());
            sb.append("\nradius : ");
            sb.append(location.getRadius());

            if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果

                sb.append("\nspeed : ");
                sb.append(location.getSpeed());// 单位：公里每小时
                sb.append("\nsatellite : ");
                sb.append(location.getSatelliteNumber());
                sb.append("\nheight : ");
                sb.append(location.getAltitude());// 单位：米
                sb.append("\ndirection : ");
                sb.append(location.getDirection());// 单位度
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                sb.append("\ndescribe : ");
                sb.append("gps定位成功");

                onLocationReceive(location);

            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                //运营商信息
                sb.append("\noperationers : ");
                sb.append(location.getOperators());
                sb.append("\ndescribe : ");
                sb.append("网络定位成功");
                onLocationReceive(location);
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                sb.append("\ndescribe : ");
                sb.append("离线定位成功，离线定位结果也是有效的");
                onLocationReceive(location);
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                sb.append("\ndescribe : ");
                sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                sb.append("\ndescribe : ");
                sb.append("网络不同导致定位失败，请检查网络是否通畅");
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                sb.append("\ndescribe : ");
                sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
            }

            sb.append("\nlocationdescribe : ");
            sb.append(location.getLocationDescribe());// 位置语义化信息

//            LogUtil.i(App.TAG, sb.toString());

        }
    }


    public BDLocation getBdLocation() {
        return bdLocation;
    }

    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(App.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(App.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public LatLng getCurrentLatlng() {
        return currentLatlng;
    }

    public void setCurrentLatlng(LatLng currentLatlng) {
        this.currentLatlng = currentLatlng;
    }

    @Override
    public void exit() {
        stopLocation();
        disconnectApiClient();
        super.exit();
    }

    @Override
    public void onTerminate() {
        stopLocation();
        disconnectApiClient();
        super.onTerminate();
    }

    public SimpleAddrss getCurrenteSimpleAddrss() {
        return currenteSimpleAddrss;
    }

    public void setCurrenteSimpleAddrss(SimpleAddrss currenteSimpleAddrss) {
        this.currenteSimpleAddrss = currenteSimpleAddrss;
    }
}
