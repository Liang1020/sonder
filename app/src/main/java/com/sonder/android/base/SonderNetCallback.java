package com.sonder.android.base;

import com.sonder.android.App;
import com.sonder.android.dialog.ProcessingDialog;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;

/**
 * Created by leocx on 2016/11/1.
 */

public class SonderNetCallback extends NetCallBack{
    ProcessingDialog processingDialog = new ProcessingDialog(App.getApp().getActivity());
    @Override
    public void onPreCall() {
        processingDialog.show();
        super.onPreCall();
    }

    @Override
    public void onFinish(NetResult result, BaseTask baseTask) {
        processingDialog.dismiss();
        super.onFinish(result, baseTask);
    }
}
