package com.sonder.android.base;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;

import com.bigkoo.pickerview.TimePickerView;
import com.common.BaseActivity;
import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.google.android.gms.maps.model.LatLng;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.EditActivity;
import com.sonder.android.activity.PdfActivity;
import com.sonder.android.activity.PicturePreviewActivity;
import com.sonder.android.dialog.EditDialog;
import com.sonder.android.dialog.MediaDialog;
import com.sonder.android.dialog.PhotoDialog;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Student;
import com.sonder.android.manager.DateManager;
import com.sonder.android.net.NetInterface;
import com.sonder.android.utils.SharedpreferencesUtil;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import me.relex.okhttpmanager.BaseCallback;
import me.relex.okhttpmanager.OkHttpClientManager;
import me.relex.okhttpmanager.RequestParams;

/**
 * Created by leocx on 2016/10/10.
 */

public class SonderBaseActivity extends BaseActivity {


    protected Context context;
    protected Activity activity;


    public final short SHORT_GO_ADDDRESS = 10;
    public final short SHORT_GO_EMERGCY_CONTACT = 11;
    public final short SHORT_GO_VISA = 12;

    public EditDialog editDialog;
    private TimePickerView timePickerView;
    private MediaDialog mediaDialog;
    private PhotoDialog photoDialog;

    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;
    public short TAKE_BIG_PICTURE = 803;
    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short CROP_BIG_PICTURE = 804;
    public short GO_DETAIL_SUCCESS = 805;
    public short BACK_TO_REQUESTS = 806;
    public short SELECT_ADDRESS_CHECK = 807;
    public short SELECT_BACK_WALK = 809;
    public short SELECT_BACK_LEAVE = 810;
    public short GO_CHECK_DETAIL = 811;
    public short GO_OVERLAY_REQUEST = 812;
    public final short REQUEST_CODE_ASK_PERMISSIONS = 813;
    public short GO_PICK_ADDR = 814;
    public short GO_GET_FAVOR = 815;
    public short GO_OPEN_GPS = 816;

    public static final short SHORT_REQUEST_READ_EXTEAL = 1004;

    public boolean needCrop = true;

    public int outputX = 200;
    public int outputY = 200;

    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;
    public SharedpreferencesUtil sharedpreferencesUtil = new SharedpreferencesUtil(this);


    public final static String KEY_INCIDENT = "incident";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.activity = this;
//        this.editDialog = new EditDialog(this);
    }


    public void showTimePicker(TimePickerView.OnTimeSelectListener timerSelect, String title, Date date) {

        closeKeyBorad();

        timePickerView = new TimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);
        timePickerView.setOnTimeSelectListener(timerSelect);
        timePickerView.setTitle(title);
        timePickerView.setTime(date);
        timePickerView.show();
    }


    public void showUpdateDialog(String title, String key, String oldvalue, Student student, EditDialog.OnEditListener onEditListener, int inputType) {

        closeKeyBorad();

        if (null == editDialog) {
            editDialog = new EditDialog(this);
        }
        editDialog.setConttent(this, title, key, oldvalue, student, onEditListener, inputType);
        editDialog.show();
    }

    public void showMediaDialog(boolean needFileChooseDialog, MediaDialog.OnMediaCallbackListener onMediaCallbackListener) {

        closeKeyBorad();

        if (null == mediaDialog) {
            mediaDialog = new MediaDialog(this, needFileChooseDialog);
        }
        mediaDialog.setNeedFile(needFileChooseDialog);
        mediaDialog.setOnMediaCallbackListener(onMediaCallbackListener);
        mediaDialog.show();
    }

    public void showPhotoDialog(PhotoDialog.OnPhotoCallbackListener photoCallbackListener) {
        closeKeyBorad();

        if (null == photoDialog) {
            photoDialog = new PhotoDialog(this);
        }
        photoDialog.setOnPhotoCallbackListener(photoCallbackListener);
        photoDialog.show();
    }


    @Override
    public void onBackPressed() {
        if (null != editDialog && editDialog.isShowing()) {
            editDialog.dismiss();
        }

        if (null != timePickerView && timePickerView.isShowing()) {
            timePickerView.dismiss();
        }

        if (null != photoDialog && photoDialog.isShowing()) {
            photoDialog.dismiss();
        }
        super.onBackPressed();
    }

    protected void startEditActivity(int startTag) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.EDIT_TAG, startTag);
        startActivity(intent);
    }


    public boolean isLocationAccess() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    ) {
                return Tool.isOpenGPS(this);
            } else {
                return false;
            }
        } else {
            return Tool.isOpenGPS(this);
        }
    }


    BaseTask baseTaskUpdate;

    public void updateAll(final NetCallBack netCallBack, final Student student, final HashMap<String, String> updateMap) {

        BaseTask.resetTastk(baseTaskUpdate);

        baseTaskUpdate = new BaseTask(activity, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);

                if (null != netCallBack) {
                    netCallBack.onPreCall(baseTask);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("full_name", student.getFullName());
                    jsonObject.put("preferred_name", student.getPreferredName());
                    jsonObject.put("dob", DateManager.getStringFromDate(student.getDob(), Constant.STRING_DAY_FORMAT2));
                    jsonObject.put("phone_number", student.getPhoneNumber());
                    jsonObject.put("address", student.getAddress());


                    if (null != student.getProfile()) {

                        jsonObject.put("primary_contact_name", student.getProfile().getPrimaryContactName());
                        jsonObject.put("primary_contact_phone", student.getProfile().getPrimaryContactPhone());
                        jsonObject.put("secondary_contact_name", student.getProfile().getSecondaryContactName());
                        jsonObject.put("secondary_contact_phone", student.getProfile().getSecondaryContactPhone());
                        jsonObject.put("education_student_id", student.getProfile().getEducationStudentId());
                        jsonObject.put("education_institution_name", student.getProfile().getEducationInstitutionName());
                        jsonObject.put("education_institution_campus_name", student.getProfile().getEducationInstitutionCampusName());
                        jsonObject.put("education_institution_campus_phone_number", student.getProfile().getEducationInstitutionCampusPhoneNumber());
                        jsonObject.put("education_institution_campus_address", student.getProfile().getEducationInstitutionCampusAddress());
                        jsonObject.put("medical_insurance_provider", student.getProfile().getMedicalInsuranceProvider());
                        jsonObject.put("medical_insurance_member_number", student.getProfile().getMedicalInsuranceMemberNumber());
                        jsonObject.put("medical_existing_conditions", student.getProfile().getMedicalExistingConditions());
                        jsonObject.put("medical_history", student.getProfile().getMedicalHistory());
                        jsonObject.put("medical_allergies", student.getProfile().getMedicalAllergies());
                        jsonObject.put("medical_current_medication", student.getProfile().getMedicalCurrentMedication());


                        jsonObject.put("travel_insurance_provider", student.getProfile().getTravelInsuranceProvider());
                        jsonObject.put("travel_insurance_member_number", student.getProfile().getTravelInsuranceMemberNumber());
                        jsonObject.put("travel_driver_license_number", student.getProfile().getTravelDriverLicenseNumber());
                        jsonObject.put("travel_visa_type", student.getProfile().getTravelVisaType());

                        String dateFormat = Constant.STRING_DAY_FORMAT2;

                        jsonObject.put("travel_visa_start_date", DateManager.getStringFromDate(student.getProfile().getTravelVisaStartDate(), dateFormat));
                        jsonObject.put("travel_visa_end_date", DateManager.getStringFromDate(student.getProfile().getTravelVisaEndDate(), dateFormat));

                    }

                    for (Map.Entry<String, String> entry : updateMap.entrySet()) {
                        jsonObject.put(entry.getKey(), entry.getValue());
                    }


                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        String kv = jsonObject.getString(key);
                        if ("null".equalsIgnoreCase(kv)) {
                            jsonObject.put(key, "");
                        }
                    }

                    netResult = NetInterface.updateProfileAll(jsonObject);

                } catch (Exception e) {
                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                }
                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != netCallBack) {

                    netCallBack.onFinish(result, baseTask);
                }

            }
        });

        baseTaskUpdate.execute(new HashMap<String, String>());
    }


    BaseTask baseTaskGetFavorAddress;

    public void requestFavorAddress(final boolean showDialog, final NetCallBack netCallBack) {
        BaseTask.resetTastk(baseTaskGetFavorAddress);
        baseTaskGetFavorAddress = new BaseTask(this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.getFavorAddress();
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "request favor address:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }
                if (null != result && result.isOk() && null != netCallBack) {
                    netCallBack.onFinish(result);
                } else {
                    LogUtil.i(App.TAG, "load favor list fail..");
                }
            }
        });

        baseTaskGetFavorAddress.execute(new HashMap<String, String>());


    }

    public void parseLocationLatlng(final boolean showDialog, final String location, final NetCallBack netCallBack) {

        String appId = "";
        String token = "";

        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            appId = appInfo.metaData.getString("com.here.android.maps.appid");
            token = appInfo.metaData.getString("com.here.android.maps.apptoken");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String url = "";

        try {
            url = "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext=" + URLEncoder.encode(location, "utf-8") + "&" +
                    "app_id=" + appId + "&app_code=" + token + "&gen=8";
        } catch (Exception e) {
            LogUtil.e(App.TAG, "exception:" + e.getLocalizedMessage());
        }


        RequestParams params = new RequestParams();

        OkHttpClientManager.get(url, params,

                new BaseCallback() {

                    @Override
                    public void onResponse(final Response response) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (showDialog) {
                                    hideProgressDialog();
                                }

                                if (response.code() != 200) {
                                    Tool.ToastShow(SonderBaseActivity.this, R.string.error_net);
                                    return;
                                }


                                try {

                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    JSONArray addreses = jsonObject.getJSONObject("Response").getJSONArray("View").getJSONObject(0).getJSONArray("Result");

                                    if (null != addreses && addreses.length() > 0) {

                                        for (int i = 0, isize = addreses.length(); i < isize; i++) {
                                            JSONObject jo = addreses.getJSONObject(i);
                                            JSONObject locationObj = jo.getJSONObject("Location");
                                            JSONObject DisplayPosition = locationObj.getJSONObject("DisplayPosition");

                                            double lat = DisplayPosition.getDouble("Latitude");
                                            double lon = DisplayPosition.getDouble("Longitude");

                                            JSONObject addrs = locationObj.getJSONObject("Address");

                                            String label = addrs.getString("Label");

                                            SimpleAddrss simple = new SimpleAddrss();
                                            simple.setAddress(label);
                                            simple.setLat(String.valueOf(lat));
                                            simple.setLng(String.valueOf(lon));

//                                            simpleAddress.add(simple);

                                            if (null != netCallBack) {
                                                NetResult netResult = new NetResult();
                                                netResult.setTag(simple);
                                                netCallBack.onFinish(netResult);
                                                break;
                                            }

                                        }
                                    } else {
                                        Tool.ToastShow(SonderBaseActivity.this, R.string.parse_loc_error);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    LogUtil.i(App.TAG, "parse geolocation errir:" + e.getLocalizedMessage());
                                }

                            }

                        });

                    }

                    @Override
                    public void onRequestFinish() {
                        if (showDialog) {
                            hideProgressDialog();
                        }
                    }

                    @Override
                    public void onRequestStart() {
                        if (showDialog) {
                            showProgressDialog(false);
                        }
                    }
                });


    }


    private static Geocoder coder;
    private static BaseTask geoCoderTask;

    public static void parseLocationFrolatLng(Activity activity, final LatLng latlng, final boolean showDialog, final NetCallBack netCallback) {

        if (!App.isParseLocaitonUseGoogle) {

            String appId = "";
            String token = "";

            ApplicationInfo appInfo = null;
            try {
                appInfo = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
                appId = appInfo.metaData.getString("com.here.android.maps.appid");
                token = appInfo.metaData.getString("com.here.android.maps.apptoken");
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String url = "";
            url = "https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json?" +
                    "prox=" + latlng.latitude + "," + latlng.longitude + ",200&mode=retrieveAddresses&maxresults=1&gen=8&app_id=" + appId + "&app_code=" + token;

            RequestParams params = new RequestParams();

            OkHttpClientManager.get(url, params,

                    new BaseCallback() {

                        @Override
                        public void onResponse(final Response response) {

                            try {

                                JSONObject jsonObject = new JSONObject(response.body().string());
                                JSONArray addreses = jsonObject.getJSONObject("Response").getJSONArray("View").getJSONObject(0).getJSONArray("Result");

                                LogUtil.i(App.TAG, "adress:" + jsonObject.toString());

                                final ArrayList<SimpleAddrss> simpleAddress = new ArrayList<SimpleAddrss>();

                                if (null != addreses && addreses.length() > 0) {

                                    for (int i = 0, isize = addreses.length(); i < isize; i++) {
                                        JSONObject jo = addreses.getJSONObject(i);

                                        JSONObject locationObj = jo.getJSONObject("Location");


                                        JSONObject DisplayPosition = locationObj.getJSONObject("DisplayPosition");

                                        double lat = DisplayPosition.getDouble("Latitude");
                                        double lon = DisplayPosition.getDouble("Longitude");

                                        JSONObject addrs = locationObj.getJSONObject("Address");

                                        String label = addrs.getString("Label");
//                                    String contry = addrs.getString("Country");
//                                    String postcode = addrs.getString("PostalCode");

                                        SimpleAddrss simple = new SimpleAddrss();
                                        simple.setAddress(label);
                                        simple.setLat(String.valueOf(lat));
                                        simple.setLng(String.valueOf(lon));

                                        simpleAddress.add(simple);
                                        break;

                                    }
                                }

                                if (!ListUtiles.isEmpty(simpleAddress)) {
                                    SimpleAddrss sa = simpleAddress.get(0);
                                    LogUtil.i(App.TAG, "addrss:" + sa.getAddress() + " " + sa.getLat() + "," + sa.getLng());
                                    if (null != netCallback) {
                                        NetResult netResult = new NetResult();
                                        netResult.setTag(sa);
                                        netCallback.onFinish(netResult);
                                    }
                                } else {
                                    if (null != netCallback) {
                                        netCallback.onFinish(null);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                LogUtil.i(App.TAG, "parse with here error:" + e.getLocalizedMessage());
                            }

                        }

                        @Override
                        public void onRequestFinish() {
                        }

                        @Override
                        public void onRequestStart() {

                        }
                    });

        } else {

            BaseTask.resetTastk(geoCoderTask);

            geoCoderTask = new BaseTask(activity, new NetCallBack() {

                @Override
                public void onPreCall(BaseTask baseTask) {
                    if (showDialog) {
                        baseTask.showDialogForSelf(true);
                    }
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                    NetResult netResult = null;

                    try {
                        if (null == coder) {
                            coder = new Geocoder(App.getApp());
                        }
                        List<Address> address = coder.getFromLocation(latlng.latitude, latlng.longitude, 1);
                        netResult = new NetResult();
                        Address adr = address.get(0);
                        SimpleAddrss sa = SimpleAddrss.fromGoogleAddress(adr);
                        netResult.setTag(sa);

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "parse geo error:" + e.getLocalizedMessage());
                    }


                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    if (showDialog) {
                        baseTask.hideDialogSelf();
                    }
                    if (null != netCallback) {
                        netCallback.onFinish(result);
                    }
                }
            });

            geoCoderTask.execute(new HashMap<String, String>());
        }


    }


    public String getTypeTextbyType(int type) {

        String label = "";

        switch (type) {

            case 0:
                label = getResources().getString(R.string.support_person_personal);
                break;
            case 1:
                label = getResources().getString(R.string.support_person_other);
                break;
            case 2:
                label = getResources().getString(R.string.support_person_Group);
                break;
        }

        return label;

    }


    public String getTypeText(int type) {

        type = type - 1;
        String label = "";

        switch (type) {

            case 0:
                label = getResources().getString(R.string.support_person_personal);
                break;
            case 1:
                label = getResources().getString(R.string.support_person_other);
                break;
            case 2:
                label = getResources().getString(R.string.support_person_Group);
                break;
        }

        return label;

    }


    BaseTask baseTaskGetStatusList;

    public void requestIncident(final String id, final boolean showDialog, final NetCallBack netCallBack) {
        BaseTask.resetTastk(baseTaskGetStatusList);

        baseTaskGetStatusList = new BaseTask(activity, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
                if (null != netCallBack) {
                    netCallBack.onPreCall(baseTask);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getStatusList(id);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "request list detail error:");
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }
                if (null != netCallBack) {
                    netCallBack.onFinish(result, baseTask);
                }
            }
        });

        baseTaskGetStatusList.execute(new HashMap<String, String>());

    }


    BaseTask baseTaskCreateIncient;

    public void requestCreateIncident(final HashMap<String, String> hmap, final FormFile[] files, final boolean showDialog, final NetCallBack netCallBack) {

        BaseTask.resetTastk(baseTaskCreateIncient);
        baseTaskCreateIncient = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.createIncident(hmap, files);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.i(App.TAG, "requestCreateIncident error:" + e.getLocalizedMessage());
                }
                return netResult;
            }


            @Override
            public void onCanCell(BaseTask baseTask) {
                super.onCanCell(baseTask);
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != netCallBack) {

                    netCallBack.onFinish(result, baseTask);
                }

            }

        });

        baseTaskCreateIncient.execute(new HashMap<String, String>());

    }


    BaseTask basetaskUploadHeaderOrPassport;

    public void requestUploadHeaderorPassport(final FormFile[] files, final boolean showDialog, final NetCallBack netCallBack) {
        BaseTask.resetTastk(basetaskUploadHeaderOrPassport);
        basetaskUploadHeaderOrPassport = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(showDialog);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.uploadHeaderOrPassportFile(files);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestUploadHeaderorPassport err:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != netCallBack) {
                    netCallBack.onFinish(result, baseTask);
                }
            }
        });

        basetaskUploadHeaderOrPassport.execute(new HashMap<String, String>());
    }


    BaseTask basetaskIndex;

    public void requestIndex(final boolean showDialog, final NetCallBack netCallBack) {
        BaseTask.resetTastk(basetaskIndex);
        basetaskIndex = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(showDialog);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.index();
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestUploadHeaderorPassport err:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != netCallBack) {
                    netCallBack.onFinish(result, baseTask);
                }
            }
        });

        basetaskIndex.execute(new HashMap<String, String>());
    }


    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }

    }


    public void onTakePhtoClick(boolean needCrop) {
        this.needCrop = needCrop;
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;


//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
//                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
//                    ) {
//                startTakePicture();
//            } else {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
//            }
//        } else {
//            startTakePicture();
//        }


        if (needCrop) {
            PictureChoseActivity.gotoTakePicture(this, outputX, outputY);
        } else {
            PictureChoseActivity.gotoTakePicture(this, -1, -1);
        }


    }


    public void onChosePhtoClick(boolean needCrop) {
        this.needCrop = needCrop;
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                startSelectPicture();
//            } else {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
//            }
//        } else {
//            startSelectPicture();
//        }


        if (needCrop) {
            PictureChoseActivity.gotoSelectPicture(this, 1, "", outputX, outputY);
        } else {
            PictureChoseActivity.gotoSelectPicture(this, 1, "", -1, -1);
        }

    }


    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {
        //======use local api
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uriIn, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        startActivityForResult(intent, requestCode);
    }


    public void gotoPicturePreview(String url) {

        App.getApp().putTemPObject("url", url);
        Tool.startActivity(this, PicturePreviewActivity.class);
    }

    public void gotoPdfPreview(String url) {
        App.getApp().putTemPObject("url", url);
        Tool.startActivity(this, PdfActivity.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


//        String mFileName;
//        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
//            if (resultCode == RESULT_OK && data != null) {
//                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
//                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
//                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//            }
//        }
//        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
//            if (resultCode == RESULT_OK && data != null) {
//                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
//                String newPath = sharedpreferencesUtil.getImageTempNameString2();
//                PhotoLibUtils.copyFile(mFileName, newPath);
//                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
//                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//
//            }
//        }
//        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
//            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//        }
//
//
//        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
//            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
//                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
//                App.firstRun();
//            }
//        }
    }
}
