package com.sonder.android.activity;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.EditDialog;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Student;
import com.sonder.android.net.NetInterface;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Tool;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/1.
 */

public class EducationActivity extends SonderBaseActivity {

    private Student student;


    @BindView(R.id.textView_activityEdu_studentId)
    TextView textViewStudentId;

    @BindView(R.id.textView_activityEdu_college)
    TextView textViewCollege;

    @BindView(R.id.textView_activityEdu_campus)
    TextView textViewCampus;

    @BindView(R.id.textView_activityEdu_phone)
    TextView textViewEduphone;

    @BindView(R.id.textViewEducationAddress)
    TextView textViewAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu);
        ButterKnife.bind(this);
        initView();
    }


    @OnClick(R.id.imageViewBack)
    public void onViewBack() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void initView() {
        initLayout();
    }

    private void initLayout() {

        student = App.getApp().getStudent();

        textViewStudentId.setText(student.getProfile().getEducationStudentId());
        textViewCollege.setText(student.getProfile().getEducationInstitutionName());
        textViewCampus.setText(student.getProfile().getEducationInstitutionCampusName());
        textViewEduphone.setText(student.getProfile().getEducationInstitutionCampusPhoneNumber());
        textViewAddress.setText(student.getProfile().getEducationInstitutionCampusAddress());


    }


    @OnClick(R.id.relativeLayout_activityEdu_studentId)
    public void editStudentId() {

//        startEditActivity(EditActivity.TAG_EDU_STUDENT_ID);

        showUpdateDialog(getInputFromId(R.id.textView_activityEdu_studentIdTitle), "education_student_id", getInput(textViewStudentId), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                if (isSuccess) {
                    initView();
                }

            }
        }, EditorInfo.TYPE_CLASS_TEXT);


    }

    @OnClick(R.id.relativeLayout_activityEdu_college)
    public void editCollege() {
//        startEditActivity(EditActivity.TAG_EDU_COLLEGE);

        showUpdateDialog(getInputFromId(R.id.textView_activityEdu_collegeTitle), "education_institution_name", getInput(textViewCollege), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                if (isSuccess) {
                    initView();
                }

            }
        }, EditorInfo.TYPE_CLASS_TEXT);

    }

    @OnClick(R.id.relativeLayout_activityEdu_campus)
    public void editCampus() {
//        startEditActivity(EditActivity.TAG_EDU_CAMPUS);

        showUpdateDialog(getInputFromId(R.id.textView_activityEdu_collegeTitle), "education_institution_campus_name", getInput(textViewCampus), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                if (isSuccess) {
                    initView();
                }

            }
        }, EditorInfo.TYPE_CLASS_TEXT);


    }

    @OnClick(R.id.relativeLayout_activityEdu_phone)
    public void editPhone() {
//        startEditActivity(EditActivity.TAG_EDU_PHONE);

        showUpdateDialog(getInputFromId(R.id.textView_activityEdu_phoneTitle),
                "education_institution_campus_phone_number",
                getInput(textViewEduphone), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                        if (isSuccess) {
                            initView();
                        }

                    }
                }, EditorInfo.TYPE_CLASS_PHONE);

    }

    @OnClick(R.id.relativeLayoutEducationAddress)
    public void editAddress() {
        Tool.startActivityForResult(this, AddressHereActivity.class, SHORT_GO_ADDDRESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_ADDDRESS && resultCode == RESULT_OK) {

            SimpleAddrss address = (SimpleAddrss) App.getApp().getTempObject("address");
            if (null != address) {

                textViewAddress.setText(address.getAddress());
                JSONObject jsonObject = new JSONObject();

                try {

                    jsonObject.put("education_student_id", getInput(textViewStudentId));
                    jsonObject.put("education_institution_name", getInput(textViewCollege));
                    jsonObject.put("education_institution_campus_name", getInput(textViewCampus));
                    jsonObject.put("education_institution_campus_phone_number", getInput(textViewEduphone));
                    jsonObject.put("education_institution_campus_address", address.getAddress());

                    requestUpadateEducationInfo(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }
    }

    BaseTask baseTaskUpdateEducation;

    private void requestUpadateEducationInfo(final JSONObject jsonObject) {
        BaseTask.resetTastk(baseTaskUpdateEducation);
        baseTaskUpdateEducation = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.upldateEducationInfo(jsonObject);
                } catch (Exception e) {
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
                        App.getApp().saveStudent((Student) result.getData()[0]);
                        initView();
                    } else {
                        Tool.showMessageDialog(result.getMessage(), EducationActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, EducationActivity.this);
                }
            }
        });


        baseTaskUpdateEducation.execute(new HashMap<String, String>());


    }


}
