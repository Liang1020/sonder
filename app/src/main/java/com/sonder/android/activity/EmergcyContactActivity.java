package com.sonder.android.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.Student;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/3.
 */

public class EmergcyContactActivity extends SonderBaseActivity {


    @BindView(R.id.editTextName)
    EditText editTextName;

    @BindView(R.id.editTextphone)
    EditText editTextphone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergcy_contact);
        ButterKnife.bind(this);
        initView();
    }

    @OnClick(R.id.textViewSave)
    public void onClickSave() {


        App.getApp().putTemPObject("name", getInput(editTextName));
        App.getApp().putTemPObject("phone", getInput(editTextphone));

        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.imageViewBack)
    public void onImageViewBack() {

        finish();

    }

    @Override
    public void initView() {

        Student student = App.getApp().getStudent();

        if (null != student && null != student.getProfile()) {

            editTextName.setText(student.getProfile().getPrimaryContactName());
            editTextphone.setText(student.getProfile().getPrimaryContactPhone());
        }

    }
}
