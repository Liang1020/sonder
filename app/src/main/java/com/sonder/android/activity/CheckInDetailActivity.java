package com.sonder.android.activity;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.common.view.CountDownTimerTextView;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.DialogCheckInLeave;
import com.sonder.android.domain.Angel;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckInDetailActivity extends SonderBaseActivity {


    @BindView(R.id.textViewDuration)
    CountDownTimerTextView textViewDuration;

    @BindView(R.id.textViewRemind)
    TextView textViewRemind;

    @BindView(R.id.textViewLocation)
    TextView textViewLocation;

    @BindView(R.id.imageViewArrow1)
    ImageView imageViewArrow1;

    @BindView(R.id.imageViewArrow2)
    ImageView imageViewArrow2;


    @BindView(R.id.buttonCheckIn)
    Button buttonCheckIn;


    DialogCheckInLeave dialogCheckInLeave;

    Angel angel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_detail);
        ButterKnife.bind(this);
        angel = (Angel) App.getApp().getTempObject("angle");
        initView();
    }

    @Override
    public void initView() {
        if (null != angel) {

//            "check_in_time": "2016-12-29 16:07:21",
            // duration

            try {


                String timelable = angel.getCheckInTime().substring(0, 19);
                Date date = Constant.SDFD4.parse(timelable);

                Calendar calendar = Calendar.getInstance();


                int draution = Integer.parseInt(angel.getDuration());

                long duration = draution * 60 * 1000;

                calendar.setTimeInMillis((date.getTime() + duration));

                Date tartget = calendar.getTime();
                Date now = new Date();

                long l = tartget.getTime() - now.getTime();

                if (l > 0) {
                    long day = l / (24 * 60 * 60 * 1000);
                    long hour = (l / (60 * 60 * 1000) - day * 24);
                    long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
                    long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);

                    long[] times = {day, hour, min, s};

                    textViewDuration.setTimes(times);
                    textViewDuration.beginRun();

                } else {
                    textViewDuration.stopRun();
                    textViewDuration.setText("Time over");
                }

            } catch (Exception e) {
                e.printStackTrace();
                LogUtil.i(App.TAG, "eceptino:" + e.getLocalizedMessage());
            }

//          textViewDuration.setText(angel.getDurationText());

            textViewLocation.setText(angel.getAddressText());
            textViewRemind.setText(angel.getNotificationEvey());
        }


    }

    @OnClick(R.id.textViewLocation)
    public void onClickLocation() {
//        Tool.startActivityForResult(this, AddressHereActivity.class, SELECT_ADDRESS_CHECK);
    }

    @OnClick(R.id.buttonCheckIn)
    public void checkIn() {

        if (null == dialogCheckInLeave) {
            dialogCheckInLeave = new DialogCheckInLeave(this);
        }

        dialogCheckInLeave.setDialogCallBackListener(new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {
                if (yesOrNo) {

                    setResult(SELECT_BACK_WALK);
                    finish();

                } else {
                    requestCheckout();
                }


            }
        });
        dialogCheckInLeave.show();
    }


    BaseTask baseTaskCheckOut;

    public void requestCheckout() {
        BaseTask.resetTastk(baseTaskCheckOut);

        baseTaskCheckOut = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("check_out_time", Constant.SDF.format(new Date()));
                    netResult = NetInterface.checkOut(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        App.getApp().saveAngle(null);
                        setResult(SELECT_BACK_LEAVE);
                        finish();
                    } else {
                        Tool.showMessageDialog(result.getMessage(), CheckInDetailActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, CheckInDetailActivity.this);
                }

            }
        });

        baseTaskCheckOut.execute(new HashMap<String, String>());
    }


    private boolean checkValue() {

        if (null == textViewLocation.getTag()) {
            textViewLocation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            return false;
        }
        if (null == textViewDuration.getTag()) {
            textViewDuration.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            return false;
        }

        if (null == textViewRemind.getTag()) {
            textViewRemind.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            return false;
        }

        return true;
    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_ADDRESS_CHECK && resultCode == RESULT_OK) {
            SimpleAddrss address = SimpleAddrss.fromGoogleAddress((Address) App.getApp().getTempObject("address"));
            textViewLocation.setTag(address);
            textViewLocation.setText(address.getAddress());
        }

    }
}
