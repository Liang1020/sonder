package com.sonder.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baoyz.widget.PullRefreshLayout;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressSimpleAdapterWithFavor;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavorAddressActivity extends SonderBaseActivity {


    @BindView(R.id.listView)
    public ListView listView;


    @BindView(R.id.swipeRefreshLayoutNews)
    public PullRefreshLayout swipeRefreshLayout;

    public ArrayList<SimpleAddrss> simpleAddrsses;
    public AddressSimpleAdapterWithFavor addressSimpleAdapter;

    public static boolean needRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favor_address);
        ButterKnife.bind(this);
        needRefresh = false;

        requestFavorList();
        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                requestFavorList();

            }
        });

    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        setResult(RESULT_OK);
        finish();
    }


    BaseTask baesTaskFavorList;

    private void requestFavorList() {

        BaseTask.resetTastk(baesTaskFavorList);
        baesTaskFavorList = new BaseTask(FavorAddressActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                swipeRefreshLayout.setRefreshing(true);
//                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.getFavorAddress();
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "get favor list address error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
//                baseTask.hideDialogSelf();
                swipeRefreshLayout.setRefreshing(false);
                if (null != result) {
                    if (result.isOk()) {


                        simpleAddrsses = (ArrayList<SimpleAddrss>) result.getData()[0];
                        addressSimpleAdapter = new AddressSimpleAdapterWithFavor(FavorAddressActivity.this, simpleAddrsses);
                        addressSimpleAdapter.setArrayListCollected(simpleAddrsses);
                        listView.setAdapter(addressSimpleAdapter);


                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                SimpleAddrss selectedAddress = (SimpleAddrss) addressSimpleAdapter.getItem(position);

                                App.getApp().putTemPObject("address", selectedAddress);
                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                        addressSimpleAdapter.setOnFavorClickListener(new AddressSimpleAdapterWithFavor.OnFavorClickListener() {
                            @Override
                            public void onCLickFavor(final AddressSimpleAdapterWithFavor ada, final SimpleAddrss simpleAddrss, int pos, boolean isFavor) {
                                if (isFavor) {

                                    String favorId = simpleAddrss.getId();
                                    if (!StringUtils.isEmpty(favorId)) {
                                        requestRemoveFavorAddress(simpleAddrss, ada, favorId);
                                    }
                                } else {
                                    if (StringUtils.isEmpty(simpleAddrss.getLat())) {
                                        parseLocationLatlng(true, simpleAddrss.getAddress(), new NetCallBack() {
                                            @Override
                                            public void onFinish(NetResult result) {
                                                if (null != result.getTag()) {

                                                    SimpleAddrss fullAddresss = (SimpleAddrss) result.getTag();
                                                    simpleAddrss.setLat(fullAddresss.getLat());
                                                    simpleAddrss.setLng(fullAddresss.getLng());

                                                    requestFavorAddress(simpleAddrss, addressSimpleAdapter);
                                                }
                                            }
                                        });
                                    } else {
                                        requestFavorAddress(simpleAddrss, addressSimpleAdapter);
                                    }
                                }
                            }
                        });

                    } else {

                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, FavorAddressActivity.this);
                }
            }

        });

        baesTaskFavorList.execute(new HashMap<String, String>());

    }


    BaseTask baseTaskFavorAddresss;

    private void requestRemoveFavorAddress(final SimpleAddrss simpleAddrss, final AddressSimpleAdapterWithFavor ada, final String id) {
        BaseTask.resetTastk(baseTaskFavorAddresss);
        baseTaskFavorAddresss = new BaseTask(FavorAddressActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("address", simpleAddrss.getAddress());
                    jsonObject.put("location_lon", simpleAddrss.getLng());
                    jsonObject.put("location_lat", simpleAddrss.getLat());
                    netResult = NetInterface.removefavorAddress(id);

//                    ArrayList<SimpleAddrss> favorAddresses = (ArrayList<SimpleAddrss>) NetInterface.getFavorAddress().getData()[0];
//                    netResult.setData(new Object[]{simpleAddrss, favorAddresses});

                } catch (Exception e) {
                    LogUtil.i(App.TAG, "favor address error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
                        needRefresh = true;

                        ada.removeColelctData(simpleAddrss);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), FavorAddressActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, FavorAddressActivity.this);
                }

            }
        });

        baseTaskFavorAddresss.execute(new HashMap<String, String>());

    }


    /**
     * favor address
     *
     * @param simpleAddrss
     * @param ada
     */
    private void requestFavorAddress(final SimpleAddrss simpleAddrss, final AddressSimpleAdapterWithFavor ada) {
        BaseTask.resetTastk(baseTaskFavorAddresss);
        baseTaskFavorAddresss = new BaseTask(FavorAddressActivity.this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("address", simpleAddrss.getAddress());
                    jsonObject.put("location_lon", simpleAddrss.getLng());
                    jsonObject.put("location_lat", simpleAddrss.getLat());

                    netResult = NetInterface.favorAddress(jsonObject);

//                  ArrayList<SimpleAddrss> favorAddresses = (ArrayList<SimpleAddrss>) NetInterface.getFavorAddress().getData()[0];
//                  netResult.setData(new Object[]{simpleAddrss, favorAddresses});


                } catch (Exception e) {
                    LogUtil.i(App.TAG, "favor address error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        SimpleAddrss newAddress = (SimpleAddrss) result.getData()[0];

//                        if(StringUtils.isEmpty(simpleAddrss.getId())){
                        simpleAddrss.setId(newAddress.getId());
//                        }

                        ada.addSelectedData(simpleAddrss);

//                      ada.addSelectedData(simpleAddrss);
//                        reloadAdapter(simpleAddrss, ada);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), FavorAddressActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, FavorAddressActivity.this);
                }
            }
        });
        baseTaskFavorAddresss.execute(new HashMap<String, String>());

    }


}
