package com.sonder.android.activity.page;

import android.app.Activity;
import android.view.View;
import android.widget.ListView;

import com.baoyz.widget.PullRefreshLayout;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.common.view.BasePage;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.NewsAdapter;
import com.sonder.android.domain.Feed;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by wangzy on 2016/11/10.
 */

public class PageGustNews extends BasePage {

    PullRefreshLayout swipeRefreshLayout;
    ListView listView;
    private ArrayList<Feed> allFeds;

    public PageGustNews(Activity activity) {
        super(activity);
        this.rootView = View.inflate(activity, R.layout.page_gust_news, null);
        initView();
    }

    @Override
    public void onShow() {
        if (ListUtiles.isEmpty(allFeds)) {
            swipeRefreshLayout.setRefreshing(true);
            requestFeeds();
        }
    }

    @Override
    public void initView() {
        swipeRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayoutNews);
        listView = (ListView) findViewById(R.id.listView);
    }


    BaseTask basetaskFeeds;

    private void requestFeeds() {
        BaseTask.resetTastk(basetaskFeeds);
        basetaskFeeds = new BaseTask(activity, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    JSONObject jso = new JSONObject();
                    netResult = NetInterface.getUnpaidFeed(jso);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestFeeds:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                swipeRefreshLayout.setRefreshing(false);

                if (null != result) {
                    if (result.isOk()) {
                        buildAdapter((ArrayList<Feed>) result.getData()[0]);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), baseTask.activity);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, baseTask.activity);
                }
            }
        });

        basetaskFeeds.execute(new HashMap<String, String>());

    }


    private void buildAdapter(ArrayList<Feed> feeds) {
        this.allFeds=feeds;

        NewsAdapter newsAdapter = new NewsAdapter(activity, feeds);
        listView.setAdapter(newsAdapter);


    }
}
