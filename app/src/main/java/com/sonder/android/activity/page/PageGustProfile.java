package com.sonder.android.activity.page;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.common.util.Tool;
import com.common.view.BasePage;
import com.sonder.android.R;

import java.util.Locale;

import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/10.
 */

public class PageGustProfile extends BasePage {


    View viewLoginEn;
    View viewLoginCn;


    TextView textViewAccessSonderWeb;
    TextView textViewCheckProfile;


    public PageGustProfile(Activity activity) {
        super(activity);
        this.rootView = View.inflate(activity, R.layout.page_gust_profile, null);
        initView();
    }

    @Override
    public void initView() {
        ButterKnife.bind(rootView);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        };

        viewLoginEn = findViewById(R.id.viewLoginEn);
        viewLoginEn.setOnClickListener(onClickListener);

        viewLoginCn = findViewById(R.id.viewLoginCn);
        viewLoginCn.setOnClickListener(onClickListener);


        //    <string name="gust_aftler_access_ourweb">For more information,please visit\n<u><font color="#5A91E9">Sonder Australia</font></u></string>
        //    <string name="gust_check_or_login">To check your profile.\n Please <u><font color="#5A91E9">Login</font></u> first</string>




        textViewAccessSonderWeb=findTextViewById(R.id.textViewAccessSonderWeb);
        textViewCheckProfile=findTextViewById(R.id.textViewCheckProfile);





        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        if (language.endsWith("zh")) {
            viewLoginCn.setVisibility(View.VISIBLE);
            viewLoginEn.setVisibility(View.INVISIBLE);


            String accweb="访问<u><font color=\"#5A91E9\">Sonder Australia</font></u>网站了解更多信息";
            textViewAccessSonderWeb.setText(Html.fromHtml(accweb));

            String loginCehck="<u><font color=\"#5A91E9\">登录</font></u> 后查看资料.";
            textViewCheckProfile.setText(Html.fromHtml(loginCehck));

        } else {
            viewLoginCn.setVisibility(View.INVISIBLE);
            viewLoginEn.setVisibility(View.VISIBLE);

            String accweb="For more information,please visit<br><u><font color=\"#5A91E9\">Sonder Australia</font></u>";
            textViewAccessSonderWeb.setText(Html.fromHtml(accweb));
            String loginCehck="To check your profile.<br> Please <u><font color=\"#5A91E9\">Login</font></u> first";
            textViewCheckProfile.setText(Html.fromHtml(loginCehck));
        }


        findViewById(R.id.textViewAccessSonderWeb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tool.startUrl(activity, "https://sonderaustralia.com/");
            }
        });
    }

}
