package com.sonder.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.JsonHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wangzy on 2016/11/29.
 */

public class LoUserProfileActivity extends SonderBaseActivity {


    MapFragment mapFragment;

    GoogleMap googleMap;

    GoogleApiClient apiClient;

    JSONObject jsonObjectLouser;

    @BindView(R.id.textViewName)
    TextView textViewName;

    @BindView(R.id.textViewPhone)
    TextView textViewPhone;

    @BindView(R.id.imageViewHeader)
    ImageView imageViewHeader;


    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    String phone = "";


    @BindView(R.id.textViewDuration)
    TextView textViewDuration;


    private boolean isFistLocationChanged = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lo_user);
        ButterKnife.bind(this);
        jsonObjectLouser = (JSONObject) App.getApp().getTempObject("loUser");
        initView();


    }

    @Override
    public void initView() {
        initMapFragment();

        textViewName.setText(JsonHelper.getStringFromJson("name", jsonObjectLouser));
        phone = JsonHelper.getStringFromJson("phone", jsonObjectLouser);
        textViewPhone.setText(phone);
        textViewTitle.setText(JsonHelper.getStringFromJson("name", jsonObjectLouser));


        String avatar = JsonHelper.getStringFromJson("avatar", jsonObjectLouser);
        if (!StringUtils.isEmpty(avatar)) {
            Picasso.with(App.getApp()).load(avatar).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_default_header).into(imageViewHeader);
        } else {
            Picasso.with(App.getApp()).load(R.drawable.icon_default_header).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_default_header).into(imageViewHeader);
        }
    }


    @OnClick(R.id.imageViewMessage)
    public void onClickImageViewMessage() {
        Uri smsToUri = Uri.parse("smsto:" + phone);
        Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
        mIntent.putExtra("sms_body", "I need help:");
        startActivity(mIntent);
    }


    @OnClick(R.id.imageViewPhone)
    public void onClickPhone() {

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }

    private void initMapFragment() {

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                LoUserProfileActivity.this.googleMap = googleMap;
                enableMyLocationAddMarker();
            }
        });

    }


    private void enableMyLocationAddMarker() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED&&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    ) {
                setMyLocationEable();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else if (null != googleMap) {
            setMyLocationEable();
        }


        try {

            double lat = jsonObjectLouser.getDouble("last_location_lat");
            double lon = jsonObjectLouser.getDouble("last_location_lon");
            LatLng latLng = new LatLng(lat, lon);

            MarkerOptions location = new MarkerOptions().
                    icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_operator_location))
                    .position(latLng)
                    .title("")
                    .draggable(true);


            googleMap.addMarker(location);

            if (null != App.getApp().getCurrentLatlng()) {

                requestDistance(true, String.valueOf(lat + "," + lon), String.valueOf(App.getApp().getCurrentLatlng().latitude + "," + App.getApp().getCurrentLatlng().longitude));

                LogUtil.i(App.TAG, "commander location:" + lat + "," + lon);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(latLng);
                builder.include(App.getApp().getCurrentLatlng());
                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 20);
                googleMap.animateCamera(cu);
            }

        } catch (Exception e) {

            LogUtil.e(App.TAG, "get commander location error:" + e.getLocalizedMessage());
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        if (null != apiClient && !apiClient.isConnected()) {
            apiClient.connect();
        }
    }

    @Override
    protected void onStop() {

        if (null != apiClient) {
            apiClient.disconnect();
        }
        super.onStop();

    }


    private void setMyLocationEable() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        googleMap.setMyLocationEnabled(true);

        apiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {


            @Override
            public void onConnected(@Nullable Bundle bundle) {

                LogUtil.i(App.TAG, "connected service succss:");

                if (ActivityCompat.checkSelfPermission(LoUserProfileActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(LoUserProfileActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

                if (mCurrentLocation != null) {
                    LogUtil.d(App.TAG, "current location: " + mCurrentLocation.toString());
                }

                LocationRequest locationRequest = LocationRequest.create().
                        setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(10 * 1000)
                        .setFastestInterval(2 * 1000);



                LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        if (null != location) {

                            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
                            App.getApp().setCurrentLatlng(latlng);
                            if (isFistLocationChanged) {
                                isFistLocationChanged = false;
                                animate2(location.getLatitude(), location.getLongitude());
                            }


                        }
                    }
                });

            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                LogUtil.i(App.TAG, "connection fail");
            }
        }).build();


        apiClient.connect();

        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                Location location = googleMap.getMyLocation();
                if (null != location) {
                    LogUtil.i(App.TAG, "my locations:" + location.toString());
                }
                return false;
            }
        });
    }


    private void animate2(double lat, double lng) {
        LatLng latlng = new LatLng(lat, lng);
        float z = googleMap.getMaxZoomLevel() / 2;
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlng, z);
        googleMap.animateCamera(update);
    }


    BaseTask requestDistanceCompute;

    private void requestDistance(final boolean showDialog, final String orgin, final String dst) {
        BaseTask.resetTastk(requestDistanceCompute);
        requestDistanceCompute = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    String lang = Locale.getDefault().getLanguage();

                    URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                            orgin + "&destinations=" +
                            dst + "&mode=bicycling&language=" + lang + "&key=AIzaSyD0vWdf2pZMoGnqy1y8qPLF1eYA2e-Vf3k");

                    LogUtil.i(App.TAG, "disntace matrix:" + url);
                    String json = JsonHelper.getJsonFromInputStream(url.openStream());
                    JSONObject jsonObject = new JSONObject(json);
                    netResult = new NetResult();
                    netResult.setTag(jsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {
                    JSONObject router = (JSONObject) result.getTag();
                    try {
                        JSONArray array = router.getJSONArray("rows");
                        if (null != array && array.length() > 0) {

                            JSONObject jo = array.getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration");
                            String label = jo.getString("text");
                            textViewDuration.setText(label);
                        }

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "get router error:" + e.getLocalizedMessage());
                    }

                }

            }
        });

        requestDistanceCompute.execute(new HashMap<String, String>());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SHORT_REQUEST_READ_EXTEAL && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            enableMyLocationAddMarker();
        }
    }
}
