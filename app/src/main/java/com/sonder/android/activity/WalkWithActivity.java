package com.sonder.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.common.callback.NetCallBackMini;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressSimpleAdapterWithFavor;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.DialogEndWalkWith;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Walk;
import com.sonder.android.net.NetInterface;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;


public class WalkWithActivity extends SonderBaseActivity {


    private PositioningManager posManager;
    private PositioningManager.OnPositionChangedListener positionListener;

    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.editTextInput)
    EditText editTextInput;

    @BindView(R.id.linearlayoutDel)
    View linearlayoutDel;

    @BindView(R.id.buttonStart)
    Button buttonStart;

    @BindView(R.id.textViewLabelsAddress)
    TextView textViewLabelsAddress;

    @BindView(R.id.viewInputContent)
    View viewInputContent;

    private MapFragment mapFragment;
    private Map map;
    private MapMarker myMapMarker, mapTargetMarker, startMarker;

    private AddressSimpleAdapterWithFavor addressAdapter;

    private ArrayList<SimpleAddrss> simpleAddress;
    private boolean paused;
    private boolean isStartWalk = false;
    private Walk currentWalk;


    private MapRoute mapRoute;
    private DialogEndWalkWith dialogEndWalkWith;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_with);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {

        this.simpleAddress = new ArrayList<SimpleAddrss>();
        this.addressAdapter = new AddressSimpleAdapterWithFavor(this, simpleAddress);


        this.addressAdapter.setOnFavorClickListener(new AddressSimpleAdapterWithFavor.OnFavorClickListener() {
            @Override
            public void onCLickFavor(final AddressSimpleAdapterWithFavor ada, final SimpleAddrss simpleAddrss, int pos, boolean isFavor) {

                if (isFavor) {
                    String favorId = simpleAddrss.getId();
                    if (!StringUtils.isEmpty(favorId)) {
                        requestRemoveFavorAddress(simpleAddrss, ada, favorId);
                    }
                } else {

                    if (StringUtils.isEmpty(simpleAddrss.getLat())) {

                        parseLocationLatlng(true, simpleAddrss.getAddress(), new NetCallBack() {
                            @Override
                            public void onFinish(NetResult result) {
                                if (null != result.getTag()) {

                                    SimpleAddrss fullAddresss = (SimpleAddrss) result.getTag();

                                    simpleAddrss.setLat(fullAddresss.getLat());
                                    simpleAddrss.setLng(fullAddresss.getLng());


                                    requestFavorAddress(simpleAddrss, addressAdapter);
                                }
                            }
                        });
                    } else {

                        requestFavorAddress(simpleAddrss, addressAdapter);
                    }
                }
            }
        });

        this.listView.setAdapter(addressAdapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                final AddressSimpleAdapterWithFavor sia = ((AddressSimpleAdapterWithFavor) parent.getAdapter());
                sia.select(position);

                final SimpleAddrss selected = sia.getSelectAddrss();

                if (StringUtils.isEmpty(selected.getLat())) {

                    parseLocationLatlng(true, selected.getAddress(), new NetCallBack() {
                        @Override
                        public void onFinish(NetResult result) {

                            if (null != result) {

                                SimpleAddrss sa = (SimpleAddrss) result.getTag();
                                selected.setLng(sa.getLng());
                                selected.setLat(sa.getLat());
                                fillAddress(selected);

                                listView.setVisibility(View.INVISIBLE);
                            } else {

                                Tool.ToastShow(WalkWithActivity.this, R.string.parse_loc_error);
                            }

                        }
                    });


                } else {
                    fillAddress(selected);
                    listView.setVisibility(View.INVISIBLE);
                }

            }
        });


        refreshFavor();


        positionListener = new PositioningManager.OnPositionChangedListener() {
            @Override
            public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {

                LogUtil.i(App.TAG, "onPositionUpdated:" + geoPosition.toString());

                if (!paused) {
//                    map.setCenter(geoPosition.getCoordinate(), Map.Animation.NONE);

                    GeoCoordinate gt = geoPosition.getCoordinate();
                    LatLng latLng = new LatLng(gt.getLatitude(), gt.getLongitude());

                    App.getApp().setCurrentLatlng(latLng);
                    addMyLocationAndLocaiton(latLng);

                    if (isStartWalk && null != currentWalk) {

                        SimpleAddrss target = ((SimpleAddrss) editTextInput.getTag());
                        refreshWithNewLocation(latLng, target.getLatLng());
                        LogUtil.i(App.TAG, "uplodate location refresh route:" + latLng.toString() + "---" + target.toString());

                    }

                    parseLocationFrolatLng(WalkWithActivity.this, latLng, false, new NetCallBack() {
                        @Override
                        public void onFinish(NetResult result) {
                            if (null != result && null != result.getTag()) {

                                App.getApp().setCurrenteSimpleAddrss((SimpleAddrss) result.getTag());
                                LogUtil.i(App.TAG, "同行解析当前地址成功 :" + App.getApp().getCurrenteSimpleAddrss().getAddress());


                            }
                        }
                    });

                }
                LogUtil.i(App.TAG, "onPositionUpdated:");
            }

            @Override
            public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {
                LogUtil.i(App.TAG, "onPositionFixChanged:");
            }
        };


        currentWalk = (Walk) App.getApp().getTempObject("walk");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            initMapFragment();
        }
    }


    private void refreshFavor() {

        requestFavorAddress(true, new NetCallBack() {
            @Override
            public void onFinish(NetResult result) {
                if (null != result && result.isOk()) {

                    ArrayList<SimpleAddrss> colected = (ArrayList<SimpleAddrss>) result.getData()[0];

                    LogUtil.i(App.TAG, "load favor addresses size:" + ListUtiles.getListSize(colected));

                    addressAdapter.setArrayListCollected(colected);
                    addressAdapter.notifyDataSetChanged();


                }
            }
        });

    }

    private void initWithWalk(SimpleAddrss startAddress, SimpleAddrss endAddress) {
        fillAddress(endAddress);
        showWalkWithUi();

        routePlan(startAddress.getLatLng(), endAddress.getLatLng());
    }

    private void refreshWithNewLocation(LatLng latLng, LatLng target) {
        if (null != map) {
            routePlan(latLng, target);
        }
    }


    private void showWalkWithUi() {
        buttonStart.setBackgroundColor(Color.parseColor("#fe3824"));
        buttonStart.setText(R.string.walk_with_end);
        textViewLabelsAddress.setText(getInput(editTextInput));
        textViewLabelsAddress.setVisibility(View.VISIBLE);
        viewInputContent.setVisibility(View.INVISIBLE);
    }

    private void showEndWalkWithUi() {

        buttonStart.setBackgroundResource(R.drawable.bg_button);
        buttonStart.setText(R.string.walk_with_start);
        textViewLabelsAddress.setVisibility(View.INVISIBLE);
        viewInputContent.setVisibility(View.VISIBLE);

    }


    private void routePlan(LatLng stu, LatLng lo) {
        if (null != stu && null != lo) {

            RouteManager rm = new RouteManager();

            RoutePlan routePlan = new RoutePlan();
            routePlan.addWaypoint(new GeoCoordinate(stu.latitude, stu.longitude));
            routePlan.addWaypoint(new GeoCoordinate(lo.latitude, lo.longitude));

            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);

            routePlan.setRouteOptions(routeOptions);
            rm.calculateRoute(routePlan, new RouteListener());

            LogUtil.i(App.TAG, "routes:" + stu.latitude + "," + stu.longitude + "|" + lo.latitude + "," + lo.longitude);

            Image imgMe = new Image();

            try {

                imgMe.setImageResource(R.drawable.icon_location_now);

            } catch (IOException e) {
                e.printStackTrace();
            }


            if (null != startMarker) {
                map.removeMapObject(startMarker);
            }

            startMarker = new MapMarker(new GeoCoordinate(stu.latitude, stu.longitude), imgMe);
            map.addMapObject(startMarker);
        }


    }

    private class RouteListener implements RouteManager.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
            showProgressDialog(false);

        }

        // Method defined in Listener
        public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> routeResult) {
            hideProgressDialog();
            // If the route was calculated successfully
            if (error == RouteManager.Error.NONE && null != map) {

                mapRoute = new MapRoute(routeResult.get(0).getRoute());
                map.addMapObject(mapRoute);

                GeoBoundingBox gbb = routeResult.get(0).getRoute().getBoundingBox();
                map.zoomTo(gbb, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);
                if (null != mapTargetMarker) {
                    map.removeMapObject(mapTargetMarker);
                }

                try {

                    Image img = new Image();
                    img.setImageResource(R.drawable.icon_leave_location);
                    LatLng targetLatlng = ((SimpleAddrss) editTextInput.getTag()).getLatLng();

                    mapTargetMarker = new MapMarker(new GeoCoordinate(targetLatlng.latitude, targetLatlng.longitude), img);
                    map.addMapObject(mapTargetMarker);


                    if (null != myMapMarker) {
                        map.removeMapObject(myMapMarker);
                    }

//
//                    LatLng currentLatlng = App.getApp().getCurrentLatlng();
//
//                    if (null != currentLatlng) {
//                        Image imgMe = new Image();
//                        imgMe.setImageResource(R.drawable.icon_location_now);
//                        myMapMarker = new MapMarker(new GeoCoordinate(currentLatlng.latitude, currentLatlng.longitude), imgMe);
//                        map.addMapObject(myMapMarker);
//                    }


                } catch (Exception e) {
                    LogUtil.i(App.TAG, "add route fail:" + e.getLocalizedMessage());
                }

            } else {
                // Display a message indicating route calculation failure
                LogUtil.i(App.TAG, "error router:" + error);

                if ("GRAPH_DISCONNECTED".equals(error)) {
                    Tool.showMessageDialog(R.string.walk_no_path, WalkWithActivity.this);
                } else {
                    Tool.showMessageDialog("Here error:" + error.toString(), WalkWithActivity.this);
                }


//                showEndWalkWithUi();
            }


        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        paused = false;
        if (null == posManager) {
            posManager = PositioningManager.getInstance();
        }

        if (posManager != null) {
            posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
            posManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
        }

    }

    @Override
    protected void onPause() {
        if (posManager != null) {
            posManager.stop();
        }
        super.onPause();
        paused = true;
        super.onPause();
    }

    @OnClick(R.id.buttonStart)
    public void onClickStart() {

        if (isStartWalk) {
            if (null == dialogEndWalkWith) {
                dialogEndWalkWith = new DialogEndWalkWith(WalkWithActivity.this);
            }
            dialogEndWalkWith.setDialogCallBackListener(new DialogCallBackListener() {
                @Override
                public void onDone(final boolean yesOrNo) {
                    requestEndWalk(new NetCallBackMini() {
                        @Override
                        public void onFinish(NetResult netResult) {
                            if (yesOrNo) {
                                setResult(RESULT_OK);
                                finish();
                                // check in ui
                            } else {

                                showEndWalkWithUi();
                                clearAddress();
                                // reset walk with
                            }
                        }
                    });
                }
            });
            dialogEndWalkWith.show();
        } else {

            if (null != editTextInput.getTag() && null != App.getApp().getCurrentLatlng()) {

                if (null != App.getApp().getCurrenteSimpleAddrss()) {
                    showWalkWithUi();

                    SimpleAddrss currentAddress = App.getApp().getCurrenteSimpleAddrss();

                    SimpleAddrss targetAddress = (SimpleAddrss) editTextInput.getTag();

                    requestStartWalk(currentAddress, targetAddress);

                } else {
                    Tool.showMessageDialog(R.string.parse_latlng_fail, this);
                }

            } else {
                editTextInput.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                editTextInput.setHint(R.string.walk_tip_address);
            }
        }


    }


    private void actionAfterStart() {

        LatLng currentLatlng = App.getApp().getCurrentLatlng();
        LatLng targetLaglng = ((SimpleAddrss) editTextInput.getTag()).getLatLng();
        routePlan(currentLatlng, targetLaglng);

    }


    BaseTask baseTaskWalk;

    private void requestStartWalk(final SimpleAddrss start, final SimpleAddrss end) {

        BaseTask.resetTastk(baseTaskWalk);
        baseTaskWalk = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("location_address", start.getAddress());
                    jsonObject.put("location_lon", start.getLng());
                    jsonObject.put("location_lat", start.getLat());


                    jsonObject.put("destination_address", end.getAddress());
                    jsonObject.put("destination_lon", end.getLng());
                    jsonObject.put("destination_lat", end.getLat());


                    netResult = NetInterface.startWalk(jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestStartWalk:" + e.getLocalizedMessage());
                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        isStartWalk = true;
                        actionAfterStart();

                    } else {
                        Tool.showMessageDialog(result.getMessage(), WalkWithActivity.this, new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                showEndWalkWithUi();
                            }
                        });
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, WalkWithActivity.this);
                }

            }
        });

        baseTaskWalk.execute(new HashMap<String, String>());
    }


    private void requestEndWalk(final NetCallBackMini netCallBackMini) {

        BaseTask.resetTastk(baseTaskWalk);
        baseTaskWalk = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;

                try {
                    netResult = NetInterface.endWalk();
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestStartWalk:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
                        isStartWalk = false;
                        if (null != mapRoute && null != map) {
                            map.removeMapObject(mapRoute);
                            map.removeMapObject(mapTargetMarker);
                        }
                        if (null != netCallBackMini) {
                            netCallBackMini.onFinish(result);
                        }


                    } else {
                        Tool.showMessageDialog(result.getMessage(), WalkWithActivity.this, new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                showEndWalkWithUi();
                            }
                        });
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, WalkWithActivity.this);
                }

            }
        });

        baseTaskWalk.execute(new HashMap<String, String>());
    }

    @OnClick(R.id.imageViewFavor)
    public void onClickImageFavor() {
        Tool.startActivityForResult(WalkWithActivity.this, FavorAddressActivity.class, GO_GET_FAVOR);
    }

    @OnClick(R.id.viewSelectFromMap)
    public void selectFromlmap() {
        Tool.startActivityForResult(this, AddressSelectHereActivity.class, GO_PICK_ADDR);
    }


    @OnClick(R.id.imageViewBack)
    public void onBackClick() {
        finish();
    }

    @OnTextChanged(R.id.editTextInput)
    public void editTextInput() {
        String input = getInput(editTextInput);
        if (StringUtils.isEmpty(input)) {
            linearlayoutDel.setVisibility(View.INVISIBLE);
        } else {
            linearlayoutDel.setVisibility(View.VISIBLE);
        }

        editTextInput.setTag(null);
        requestAddressList(input);
    }

    @OnClick(R.id.linearlayoutDel)
    public void onClickDel() {
        editTextInput.setText("");
        editTextInput.setTag(null);
    }


    /**
     * auto suggest intput address
     *
     * @param input
     */
    private void requestAddressList(final String input) {

        String appId = "";
        String token = "";

        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            appId = appInfo.metaData.getString("com.here.android.maps.appid");
            token = appInfo.metaData.getString("com.here.android.maps.apptoken");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String url = "";


        try {

            url = "https://places.demo.api.here.com/places/v1/suggest?at=0,0&" +
                    "q=" + URLEncoder.encode(input, "utf-8") +
                    "&app_id=" + appId +
                    "&app_code=" + token;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        OkHttpClient mOkHttpClient = new OkHttpClient();
        final Request request = new Request.Builder().url(url).build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                //String htmlStr =  response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {

                            JSONObject jsonObject = new JSONObject(response.body().string());
                            final JSONArray sgs = jsonObject.getJSONArray("suggestions");
                            simpleAddress.clear();
                            if (null != sgs && sgs.length() > 0) {
                                for (int i = 0, isize = sgs.length(); i < isize; i++) {
                                    String adr = null;
                                    try {
                                        adr = sgs.getString(i);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleAddrss sad = new SimpleAddrss();
                                    sad.setAddress(adr);
                                    simpleAddress.add(sad);
                                }
                                addressAdapter.notifyDataSetChanged();
                            }

//                            boolean show=ListUtiles.isEmpty(simpleAddress) && null==editTextInput.getTag();
//                            listView.setVisibility(show? View.INVISIBLE : View.VISIBLE);

                            if (null == editTextInput.getTag() && !ListUtiles.isEmpty(simpleAddress)) {
                                listView.setVisibility(View.VISIBLE);
                            } else {
                                listView.setVisibility(View.INVISIBLE);
                            }
                        } catch (Exception e) {
                            LogUtil.i(App.TAG, "requestAddressList error:" + e.getLocalizedMessage());
                        }

                    }
                });
            }
        });

    }


    /**
     * favor address
     *
     * @param simpleAddrss
     * @param ada
     */
    private void requestFavorAddress(final SimpleAddrss simpleAddrss, final AddressSimpleAdapterWithFavor ada) {
        BaseTask.resetTastk(baseTaskFavorAddresss);
        baseTaskFavorAddresss = new BaseTask(WalkWithActivity.this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("address", simpleAddrss.getAddress());
                    jsonObject.put("location_lon", simpleAddrss.getLng());
                    jsonObject.put("location_lat", simpleAddrss.getLat());

                    netResult = NetInterface.favorAddress(jsonObject);

//                  ArrayList<SimpleAddrss> favorAddresses = (ArrayList<SimpleAddrss>) NetInterface.getFavorAddress().getData()[0];
//                  netResult.setData(new Object[]{simpleAddrss, favorAddresses});


                } catch (Exception e) {
                    LogUtil.i(App.TAG, "favor address error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        SimpleAddrss newAddress = (SimpleAddrss) result.getData()[0];

//                        if(StringUtils.isEmpty(simpleAddrss.getId())){
                        simpleAddrss.setId(newAddress.getId());
//                        }

                        ada.addSelectedData(simpleAddrss);

//                      ada.addSelectedData(simpleAddrss);
//                        reloadAdapter(simpleAddrss, ada);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), WalkWithActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, WalkWithActivity.this);
                }
            }
        });
        baseTaskFavorAddresss.execute(new HashMap<String, String>());

    }


    BaseTask baseTaskFavorAddresss;

    private void requestRemoveFavorAddress(final SimpleAddrss simpleAddrss, final AddressSimpleAdapterWithFavor ada, final String id) {
        BaseTask.resetTastk(baseTaskFavorAddresss);
        baseTaskFavorAddresss = new BaseTask(WalkWithActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("address", simpleAddrss.getAddress());
                    jsonObject.put("location_lon", simpleAddrss.getLng());
                    jsonObject.put("location_lat", simpleAddrss.getLat());
                    netResult = NetInterface.removefavorAddress(id);

//                    ArrayList<SimpleAddrss> favorAddresses = (ArrayList<SimpleAddrss>) NetInterface.getFavorAddress().getData()[0];
//                    netResult.setData(new Object[]{simpleAddrss, favorAddresses});

                } catch (Exception e) {
                    LogUtil.i(App.TAG, "favor address error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        ada.removeColelctData(simpleAddrss);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), WalkWithActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, WalkWithActivity.this);
                }

            }
        });

        baseTaskFavorAddresss.execute(new HashMap<String, String>());

    }


    /**
     * init map
     */
    private void initMapFragment() {

        // Search for the map fragment to finish setup by calling init().
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                if (error == OnEngineInitListener.Error.NONE) {
                    if (null == posManager) {
                        posManager = PositioningManager.getInstance();
                    }
                    if (null != posManager) {
                        posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
                    }

                    map = mapFragment.getMap();
                    map.getPositionIndicator().setVisible(true);

                    LatLng latLng = App.getApp().getCurrentLatlng();
                    if (null != latLng) {
                        addMyLocationAndLocaiton(latLng);
                    }


                    if (null != currentWalk) {
                        initWithWalk(currentWalk.getStart(), currentWalk.getEnd());
                        isStartWalk = true;
                    }
                } else {
                    LogUtil.e(App.TAG, "Cannot initialize MapFragment (" + error + ")");
                }
            }
        });


    }

    private void addMyLocationAndLocaiton(LatLng latLng) {

        if (null != latLng) {
            try {
                Image img = new Image();
                if (null == myMapMarker) {
                    img.setImageResource(R.drawable.icon_location_now);
                } else {
                    map.removeMapObject(myMapMarker);
                    img.setImageResource(R.drawable.icon_location_now);
                }
                myMapMarker = new MapMarker(new GeoCoordinate(latLng.latitude, latLng.longitude), img);
                map.addMapObject(myMapMarker);

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (null == currentWalk) {
                map.setCenter(new GeoCoordinate(latLng.latitude, latLng.longitude, 0.0), Map.Animation.LINEAR);
                map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
            }

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == GO_PICK_ADDR && resultCode == RESULT_OK) {
            LatLng latLng = (LatLng) App.getApp().getTempObject("latlng");
            parseLocationFrolatLng(WalkWithActivity.this, latLng, true, new NetCallBack() {
                @Override
                public void onFinish(NetResult result) {

                    if (null != result && null != result.getTag()) {

                        SimpleAddrss simpleAddrss = (SimpleAddrss) result.getTag();
                        if (!StringUtils.isEmpty(simpleAddrss.getAddress())) {
                            fillAddress(simpleAddrss);
                        } else {
                            Tool.ToastShow(WalkWithActivity.this, R.string.parse_latlng_fail);
                        }
                        LogUtil.i(App.TAG, "parse success address:" + simpleAddrss.getAddress());
                    } else {
                        Tool.ToastShow(WalkWithActivity.this, R.string.parse_latlng_fail);
                    }
                }
            });
        } else if (requestCode == GO_GET_FAVOR && resultCode == RESULT_OK) {


            SimpleAddrss simpleAddrss = (SimpleAddrss) App.getApp().getTempObject("address");
            if (null != simpleAddrss) {
                fillAddress(simpleAddrss);
            }

            if (FavorAddressActivity.needRefresh) {
                refreshFavor();

//                requestFavorAddress(true, new NetCallBack() {
//                    @Override
//                    public void onFinish(NetResult result) {
//                    }
//                });
            }
        }

    }

    private void clearAddress() {

        editTextInput.setText("");
        editTextInput.setTag(null);
    }

    private void fillAddress(SimpleAddrss simpleAddrss) {
        editTextInput.setText(simpleAddrss.getAddress());
        editTextInput.setTag(simpleAddrss);
    }

    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initMapFragment();
                break;
        }
    }


}
