package com.sonder.android.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.common.util.LogUtil;
import com.common.util.Tool;
import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapMarker;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressSelectHereActivity extends SonderBaseActivity {


    private MapFragment mapFragment;
    private Map map;
    private PositioningManager posManager;
    private PositioningManager.OnPositionChangedListener positionListener;
    private MapMarker myMapMarker;

    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_select_here);
        ButterKnife.bind(this);
        initView();
    }


    @Override
    public void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            initMapFragment();
        }
    }


    @OnClick(R.id.buttonConfirm)
    public void onClickFirm() {
        if (null != map) {
            GeoCoordinate center = map.getCenter();
            LatLng latlng = new LatLng(center.getLatitude(), center.getLongitude());
            App.getApp().putTemPObject("latlng", latlng);
            setResult(RESULT_OK);
            finish();

        } else {
            Tool.ToastShow(this, R.string.walk_map_not_ready);
        }

    }

    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        backFinish();
    }


    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initMapFragment();
                break;
        }
    }

    private void initMapFragment() {

        // Search for the map fragment to finish setup by calling init().
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    if (null == posManager) {
                        posManager = PositioningManager.getInstance();
                    }
                    if (null != posManager) {
                        posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
                    }

                    map = mapFragment.getMap();
                    map.getPositionIndicator().setVisible(true);

                    LatLng latLng = App.getApp().getCurrentLatlng();

                    if (null != latLng) {
                        addMyLocationAndLocaiton(latLng);
                    }

                } else {
                    LogUtil.e(App.TAG, "Cannot initialize MapFragment (" + error + ")");
                }
            }
        });


    }


    private void addMyLocationAndLocaiton(LatLng latLng) {

        if (null != latLng) {
            try {
                if (null == myMapMarker) {
                    Image img = new Image();
                    img.setImageResource(R.drawable.icon_location_now);
                    myMapMarker = new MapMarker(new GeoCoordinate(latLng.latitude, latLng.longitude), img);
                } else {
                    map.removeMapObject(myMapMarker);
                    Image img = new Image();
                    img.setImageResource(R.drawable.icon_location_now);
                    myMapMarker = new MapMarker(new GeoCoordinate(latLng.latitude, latLng.longitude), img);
                }

                map.addMapObject(myMapMarker);

            } catch (IOException e) {
                e.printStackTrace();
            }

            map.setCenter(new GeoCoordinate(latLng.latitude, latLng.longitude, 0.0), Map.Animation.LINEAR);
            map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
        }

    }
}
