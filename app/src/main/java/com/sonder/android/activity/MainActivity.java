package com.sonder.android.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import android.widget.Toast;

import com.common.fragment.BaseFragment;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.ContentAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.fragment.ApplyFragment;
import com.sonder.android.fragment.HomeFragment;
import com.sonder.android.fragment.NewsFragment;
import com.sonder.android.fragment.ProfileFragment;
import com.sonder.android.fragment.SettingFragment;
import com.tab.OnTabItemClickListener;
import com.tab.TabItem;
import com.tab.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends SonderBaseActivity {


    @BindView(R.id.viewPager_activityMain_content)
    ViewPager viewPagerContent;

    @BindView(R.id.textView_activityMain_title)
    TextView textViewMaintitle;

    ContentAdapter adapterFragments;

    BaseFragment currentFragment;

    private int currentTab = 2;

    TabLayout tabLayout = new TabLayout();

    public static boolean needRefreshApplayList = true;
    private List<Fragment> fragments = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApp().addActivity(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }

    private void init() {

        fragments = new ArrayList<>();

        fragments.add(NewsFragment.newInstance());
        fragments.add(ApplyFragment.newInstance());
        fragments.add(HomeFragment.newInstance());
        fragments.add(ProfileFragment.newInstance());
        fragments.add(SettingFragment.newInstance());

        adapterFragments = new ContentAdapter(getFragmentManager(), fragments);
        viewPagerContent.setAdapter(adapterFragments);
        viewPagerContent.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setTab(position);
                tabLayout.checkByIndexWithNoListEner(position);
                currentFragment = (BaseFragment) adapterFragments.getItem(viewPagerContent.getCurrentItem());

            }
        });

        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutDynamic));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutApply));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutHome));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutProfile));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutSetting));

        tabLayout.setOnTabItemClickListener(new OnTabItemClickListener() {
            @Override
            public void onCheckIndex(int index, int total, TabItem tabItem) {

//             currentFragment = (BaseFragment) adapterFragments.getItem(viewPagerContent.getCurrentItem());

                viewPagerContent.setCurrentItem(index);

                currentFragment = (BaseFragment) fragments.get(index);

//
//                FragmentManager fm = getFragmentManager();
//
//                FragmentTransaction tm = fm.beginTransaction();
//
//                tm.replace(R.id.fragmentsRoot, currentFragment);
//
//                tm.commit();

                if (currentFragment instanceof HomeFragment) {
                    ((HomeFragment) currentFragment).onIndexEventTrigger();
                }

                if (currentFragment instanceof ApplyFragment) {
                    ((ApplyFragment) currentFragment).refreshIfNeed();
                }


            }
        });


        tabLayout.checkByIndex(currentTab);
        setTab(currentTab);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (needRefreshApplayList) {
            setApplyNeedRefresh();
            needRefreshApplayList = false;
        }
    }

    public void checkApplys() {
        ((ApplyFragment) fragments.get(1)).needRefresh = true;
        tabLayout.checkByIndex(1);

    }

    public void setApplyNeedRefresh() {

        ((ApplyFragment) fragments.get(1)).needRefresh = true;

    }

    long mPressedTime = 0;

    @Override
    public void onBackPressed() {


        if (null != currentFragment && currentFragment.onBackPressed()) {


        } else {

            long mNowTime = System.currentTimeMillis();
            if ((mNowTime - mPressedTime) > 2000) {
                Toast.makeText(this, getResources().getString(R.string.press_exit_app), Toast.LENGTH_SHORT).show();
                mPressedTime = mNowTime;
            } else {
                App.getApp().exit();
            }
        }


    }


    private void updateTitle(int id) {
        switch (id) {
            case 0:
                textViewMaintitle.setText(getString(R.string.news_title));
                textViewMaintitle.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewMaintitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            case 1:
                textViewMaintitle.setText(getString(R.string.apply_title_list));
                textViewMaintitle.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewMaintitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            case 2:
                textViewMaintitle.setText(getString(R.string.home_title));
                textViewMaintitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                textViewMaintitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
                break;
            case 3:
                textViewMaintitle.setText(getString(R.string.profile_title));
                textViewMaintitle.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewMaintitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            case 4:
                textViewMaintitle.setText(getString(R.string.setting_title));
                textViewMaintitle.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewMaintitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
        }
    }

    private void setTab(int id) {
        currentTab = id;
        updateTitle(currentTab);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null != currentFragment) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (null != currentFragment) {
            currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
