package com.sonder.android.activity.page;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BasePage;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.LoUserProfileHereActivity;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.JsonHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by wangzy on 2016/11/10.
 */

public class PageRequestDetail extends BasePage {

    JSONObject jsonObject;


    private TextView textViewSumry;
    private TextView textViewContentDesc;
    private TextView textViewRequestPerson;
    private TextView textVieweRequestLocation;
    private SonderBaseActivity sonderBaseActivity;
    private TextView textViewCommander;
    private TextView textViewOperator;

    private ImageView imageViewAddPhoto1;
    private ImageView imageViewAddPhoto2;
    private ImageView imageViewAddPhoto3;

    private ArrayList<ImageView> imageViews;
    private RelativeLayout relatveLayoutCommander;


    public PageRequestDetail(Activity activity, JSONObject jsonObject) {
        super(activity);
        this.sonderBaseActivity = (SonderBaseActivity) activity;
        this.jsonObject = jsonObject;
        this.rootView = View.inflate(activity, R.layout.page_request_detail, null);

        initView();

    }


    @Override
    public void initView() {


        this.textViewSumry = findTextViewById(R.id.textViewSumry);
        this.relatveLayoutCommander = findRelativeLayout(R.id.relatveLayoutCommander);
        this.relatveLayoutCommander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    JSONObject loUser = jsonObject.getJSONObject("lo_user");
                    App.getApp().putTemPObject("loUser", loUser);
                    Tool.startActivity(activity, LoUserProfileHereActivity.class);
//                    Tool.startActivity(activity, LoUserProfileActivity.class);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        this.textViewContentDesc = findTextViewById(R.id.textViewContentDesc);
        this.textViewRequestPerson = findTextViewById(R.id.textViewRequestPerson);
        this.textVieweRequestLocation = findTextViewById(R.id.textVieweRequestLocation);
        this.textViewCommander = findTextViewById(R.id.textViewCommander);
        this.textViewOperator = findTextViewById(R.id.textViewOperator);

        imageViewAddPhoto1 = findImageViewById(R.id.imageViewAddPhoto1);
        imageViewAddPhoto2 = findImageViewById(R.id.imageViewAddPhoto2);
        imageViewAddPhoto3 = findImageViewById(R.id.imageViewAddPhoto3);


        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto1);
        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto2);
        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto3);


        this.imageViews = new ArrayList<>();

        this.imageViews.add(imageViewAddPhoto1);
        this.imageViews.add(imageViewAddPhoto2);
        this.imageViews.add(imageViewAddPhoto3);


        fillData(jsonObject);


    }


    public void fillData(JSONObject jsonObject) {
        this.jsonObject = jsonObject;


        if (null != jsonObject) {
            textViewSumry.setText(JsonHelper.getStringFromJson("summary", jsonObject));
            textViewContentDesc.setText(JsonHelper.getStringFromJson("description", jsonObject));
            textViewRequestPerson.setText(sonderBaseActivity.getTypeText(JsonHelper.getIntFromJson("request_person", jsonObject)));
            textVieweRequestLocation.setText(JsonHelper.getStringFromJson("address", jsonObject));
        }


        try {

            if (null != jsonObject.getJSONObject("lo_user")) {
                String name = jsonObject.getJSONObject("lo_user").getString("name");
                textViewCommander.setText(name);
            }

        } catch (Exception e) {

        }

        try {

            if (null != jsonObject.getJSONObject("operator")) {
                String name = jsonObject.getJSONObject("operator").getString("name");
                textViewOperator.setText(name);
            }

        } catch (Exception e) {

        }


        try {

            if (jsonObject.has("attachments") && !StringUtils.isEmpty(jsonObject.getString("attachments")) && !"null".equals(jsonObject.getString("attachments"))) {

                final JSONArray imgs = jsonObject.getJSONArray("attachments");

                final String[] imgsUrls = new String[imgs.length()];

                if (null != imgs && imgs.length() > 0) {

                    for (int i = 0; i < imgs.length(); i++) {
                        JSONObject jo = imgs.getJSONObject(i);
                        String url = JsonHelper.getStringFromJson("attachment_url", jo);
                        imgsUrls[i] = url;
                    }
                }


                int i = 0;

                for (i = 0; i < imgs.length(); i++) {

//                    JSONObject jo = imgs.getJSONObject(i);

                    final String url = imgsUrls[i];
                    Picasso.with(App.getApp()).load(url).
                            resize(App.getApp().PIC_W, App.getApp().PIC_H).
                            centerCrop().
                            placeholder(R.drawable.bg_default_loading).
                            into(imageViews.get(i));

                    imageViews.get(i).setTag(String.valueOf(i));
                    imageViews.get(i).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

//                            int index = Integer.parseInt((String) ((ImageView) v).getTag());
//                            App.getApp().putTemPObject("imgs", imgsUrls);
//                            App.getApp().putTemPObject("index", index);
//                            Tool.startActivity(activity, PicturesActivity.class);
//
                            sonderBaseActivity.gotoPicturePreview(url);

                        }
                    });
                }

                for (; i < imageViews.size(); i++) {
                    imageViews.get(i).setVisibility(View.INVISIBLE);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.i(App.TAG, "parse detail picture errors:" + e.getLocalizedMessage());
        }

    }
}
