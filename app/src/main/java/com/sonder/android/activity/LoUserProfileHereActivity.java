package com.sonder.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.search.DiscoveryRequest;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.net.JsonHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wangzy on 2016/11/29.
 */

public class LoUserProfileHereActivity extends SonderBaseActivity {


    MapFragment mapFragment;
    Map map = null;

    GoogleApiClient apiClient;

    JSONObject jsonObjectLouser;

    @BindView(R.id.textViewName)
    TextView textViewName;

    @BindView(R.id.textViewPhone)
    TextView textViewPhone;

    @BindView(R.id.imageViewHeader)
    ImageView imageViewHeader;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @BindView(R.id.imageViewLoLocation)
    ImageView imageViewLoLocation;

    String phone = "";


    @BindView(R.id.textViewDuration)
    TextView textViewDuration;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private PositioningManager posManager;
    private PositioningManager.OnPositionChangedListener positionListener;

    private boolean isroutePlaned = false;


    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private boolean paused;


    MapMarker myMapMarker;
    DiscoveryRequest searchRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lo_here_user);
        ButterKnife.bind(this);
        jsonObjectLouser = (JSONObject) App.getApp().getTempObject("loUser");
        initView();
    }

    @Override
    public void initView() {

        textViewName.setText(JsonHelper.getStringFromJson("name", jsonObjectLouser));
        phone = JsonHelper.getStringFromJson("phone", jsonObjectLouser);
        textViewPhone.setText(phone);
        textViewTitle.setText(JsonHelper.getStringFromJson("name", jsonObjectLouser));

        String avatar = JsonHelper.getStringFromJson("avatar", jsonObjectLouser);
        if (!StringUtils.isEmpty(avatar)) {
            Picasso.with(App.getApp()).load(avatar).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_default_header).into(imageViewHeader);
        } else {
            Picasso.with(App.getApp()).load(R.drawable.icon_default_header).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_default_header).into(imageViewHeader);
        }

        positionListener = new PositioningManager.OnPositionChangedListener() {
            @Override
            public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {
                if (!paused) {

//                  map.setCenter(geoPosition.getCoordinate(), Map.Animation.NONE);

                    GeoCoordinate gt = geoPosition.getCoordinate();
                    LatLng latLng = new LatLng(gt.getLatitude(), gt.getLongitude());

                    if (null == App.getApp().getCurrenteSimpleAddrss()) {
                        SimpleAddrss simpleAddrss = new SimpleAddrss();
                        App.getApp().setCurrenteSimpleAddrss(simpleAddrss);
                    }

                    App.getApp().setCurrentLatlng(latLng);

                    App.getApp().getCurrenteSimpleAddrss().setLat(String.valueOf(gt.getLatitude()));
                    App.getApp().getCurrenteSimpleAddrss().setLng(String.valueOf(gt.getLongitude()));


                    if (isroutePlaned == false) {
                        addMyLocationAndLocaiton(latLng);

                        try {
                            double lat = jsonObjectLouser.getDouble("last_location_lat");
                            double lon = jsonObjectLouser.getDouble("last_location_lon");
                            LatLng latLngLo = new LatLng(lat, lon);

                            if (null != latLng) {
                                if (null != App.getApp().getCurrentLatlng()) {
                                    requestDistanceWithHere(true, latLngLo, App.getApp().getCurrentLatlng());
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LogUtil.e(App.TAG, "not found lo user location when update locaiton success");
                        }
                        isroutePlaned = true;
                    }
                }

                LogUtil.i(App.TAG, "onPositionUpdated:");
            }

            @Override
            public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {
                LogUtil.i(App.TAG, "onPositionFixChanged:");
            }
        };


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            initMapFragment();
        }
    }


    @OnClick(R.id.imageViewLoLocation)
    public void clickLoLocation() {
        LatLng latLng = (LatLng) imageViewLoLocation.getTag();
        if (null != latLng) {
            map.setCenter(new GeoCoordinate(latLng.latitude, latLng.longitude, 0.0), Map.Animation.LINEAR);
        }
    }

    @OnClick(R.id.imageViewMyLocation)
    public void clickMyLocation() {
        if (null != App.getApp().getCurrentLatlng() && null != map) {
            LatLng latLng = App.getApp().getCurrentLatlng();
            GeoCoordinate geoCoordinate = new GeoCoordinate(latLng.latitude, latLng.longitude);
            map.setCenter(geoCoordinate, Map.Animation.LINEAR);
        } else {
            Tool.ToastShow(this, R.string.location_fail);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        paused = false;

        if (null == posManager) {
            posManager = PositioningManager.getInstance();
        }

        if (posManager != null) {
            posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
            posManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
        }

    }

    @Override
    protected void onPause() {
        if (posManager != null) {
            posManager.stop();
        }
        super.onPause();
        paused = true;

        super.onPause();
    }

    @Override
    protected void onDestroy() {

        if (posManager != null) {
            // Cleanup
            posManager.removeListener(
                    positionListener);
        }
        map = null;
        super.onDestroy();
    }

    private void initMapFragment() {
        // Search for the map fragment to finish setup by calling init().
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    if (null == posManager) {
                        posManager = PositioningManager.getInstance();
                    }
                    if (null != posManager) {
                        posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
                    }

                    map = mapFragment.getMap();
//                  map.getPositionIndicator().setVisible(true);

                    try {

                        double lat = jsonObjectLouser.getDouble("last_location_lat");
                        double lon = jsonObjectLouser.getDouble("last_location_lon");
                        LatLng latLnglo = new LatLng(lat, lon);

                        Image image = new Image();
                        image.setImageResource(R.drawable.icon_operator_location);

                        MapMarker myMapMarker = new MapMarker(new GeoCoordinate(latLnglo.latitude, latLnglo.longitude), image);
                        map.addMapObject(myMapMarker);

//                        imageViewLoLocation.setVisibility(null == latLnglo ? View.GONE : View.VISIBLE);
                        if (null != latLnglo) {
                            imageViewLoLocation.setTag(latLnglo);
                            if (null != App.getApp().getCurrentLatlng()) {
                                requestDistanceWithHere(true, latLnglo, App.getApp().getCurrentLatlng());
                                isroutePlaned = true;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        LogUtil.e(App.TAG, "not found lo user location");
                    }

                    LatLng latLng = App.getApp().getCurrentLatlng();
                    if (null != latLng) {
                        addMyLocationAndLocaiton(latLng);
                    }

                } else {
                    LogUtil.e(App.TAG, "Cannot initialize MapFragment (" + error + ")");
                }
            }
        });


    }


    private void addMyLocationAndLocaiton(LatLng latLng) {

        if (null != latLng) {
            try {
                if (null == myMapMarker) {
                    Image img = new Image();
                    img.setImageResource(R.drawable.icon_location_now);
                    myMapMarker = new MapMarker(new GeoCoordinate(latLng.latitude, latLng.longitude), img);
                } else {
                    map.removeMapObject(myMapMarker);
                    Image img = new Image();
                    img.setImageResource(R.drawable.icon_location_now);
                    myMapMarker = new MapMarker(new GeoCoordinate(latLng.latitude, latLng.longitude), img);
                }

                map.addMapObject(myMapMarker);

            } catch (IOException e) {
                e.printStackTrace();
            }

            map.setCenter(new GeoCoordinate(latLng.latitude, latLng.longitude, 0.0), Map.Animation.LINEAR);
            map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
        }

    }


    @OnClick(R.id.imageViewMessage)
    public void onClickImageViewMessage() {
        Uri smsToUri = Uri.parse("smsto:" + phone);
        Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
        mIntent.putExtra("sms_body", "I need help:");
        startActivity(mIntent);
    }


    @OnClick(R.id.imageViewPhone)
    public void onClickPhone() {

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (null != apiClient && !apiClient.isConnected()) {
            apiClient.connect();
        }
    }

    @Override
    protected void onStop() {

        if (null != apiClient) {
            apiClient.disconnect();
        }
        super.onStop();

    }


    private void requestDistanceWithHere(final boolean showdialog, LatLng stu, LatLng lo) {
        if (null != requestDistanceCompute && requestDistanceCompute.getStatus() == AsyncTask.Status.RUNNING) {
            return;
        } else {
            BaseTask.resetTastk(requestDistanceCompute);
        }
        LogUtil.e(App.TAG, "caculate route and time...");

        String wat0 = lo.latitude + "," + lo.longitude;
        String wat1 = stu.latitude + "," + stu.longitude;

        LogUtil.e(App.TAG, "route plane:" + wat0 + "|" + wat1);

        String appId = "";
        String token = "";

        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            appId = appInfo.metaData.getString("com.here.android.maps.appid");
            token = appInfo.metaData.getString("com.here.android.maps.apptoken");

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String lang = Locale.getDefault().getLanguage();

        final String urlText = "https://route.cit.api.here.com/routing/7.2/calculateroute.json?" +
                "waypoint0=" + wat0 +
                "&waypoint1=" + wat1 +
                "&mode=fastest;car;traffic:enabled&app_id=" + appId + "&app_code=" + token + "&departure=now";


        requestDistanceCompute = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showdialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    URL url = new URL(urlText);
                    LogUtil.i(App.TAG, "disntace matrix:" + url);
                    String json = JsonHelper.getJsonFromInputStream(url.openStream());
                    JSONObject jsonObject = new JSONObject(json);
                    netResult = new NetResult();
                    netResult.setTag(jsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "compute distance route error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showdialog) {
                    baseTask.hideDialogSelf();
                }

                if (isFinishing()) {
                    return;
                }

                if (null != result) {
                    JSONObject router = (JSONObject) result.getTag();
                    LogUtil.i(App.TAG, "router json:" + router);

                    try {

                        //trafficTime
                        String text = router.getJSONObject("response").getJSONArray("route").getJSONObject(0).getJSONObject("summary").getString("text");

                        textViewDuration.setText(Html.fromHtml(text));

                        int time = router.getJSONObject("response").getJSONArray("route").getJSONObject(0).getJSONObject("summary").getInt("trafficTime");

                        String tm = String.format(getResources().getString(R.string.afeter_time_arrival), secToTime(time));
                        textViewDuration.setText(tm);

                        LogUtil.i(App.TAG, "router:" + router);
//                        JSONArray array = router.getJSONArray("rows");
//                        if (null != array && array.length() > 0) {
//                            JSONObject jo = array.getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration");
//                            String label = jo.getString("text");
//                            textViewDuration.setText(label);
//                        }

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "get router error:" + e.getLocalizedMessage());
                        textViewDuration.setText(R.string.can_not_compute_time);
                    }

                } else {
                    textViewDuration.setText(R.string.can_not_compute_time);
//                    Tool.ToastShow(LoUserProfileHereActivity.this, R.string.error_net);
                }

            }
        });

        requestDistanceCompute.execute(new HashMap<String, String>());

        routePlan(stu, lo);
    }


    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
            return "00:00";
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }


    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }


    private class RouteListener implements RouteManager.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
        }

        // Method defined in Listener
        public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> routeResult) {
            // If the route was calculated successfully
            if (error == RouteManager.Error.NONE) {

                MapRoute mapRoute = new MapRoute(routeResult.get(0).getRoute());

                map.addMapObject(mapRoute);
            } else {
                // Display a message indicating route calculation failure
                LogUtil.e(App.TAG, "onCalculateRouteFinished:" + error);
                Tool.ToastShow(LoUserProfileHereActivity.this, R.string.can_not_find_route);
            }
        }
    }


    private void routePlan(LatLng stu, LatLng lo) {
        RouteManager rm = new RouteManager();
        RoutePlan routePlan = new RoutePlan();


        routePlan.addWaypoint(new GeoCoordinate(stu.latitude, stu.longitude));
        routePlan.addWaypoint(new GeoCoordinate(lo.latitude, lo.longitude));


        // START: Nokia, Burnaby
//        routePlan.addWaypoint(new GeoCoordinate(49.1966286, -123.0053635));
//        // END: Airport, YVR
//        routePlan.addWaypoint(new GeoCoordinate(49.1947289, -123.1762924));


        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.FASTEST);

        routePlan.setRouteOptions(routeOptions);
        rm.calculateRoute(routePlan, new RouteListener());
    }

    BaseTask requestDistanceCompute;

    private void requestDistance(final boolean showDialog, final String orgin, final String dst) {
        BaseTask.resetTastk(requestDistanceCompute);
        requestDistanceCompute = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    String lang = Locale.getDefault().getLanguage();

                    URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                            orgin + "&destinations=" +
                            dst + "&mode=bicycling&language=" + lang + "&key=AIzaSyD0vWdf2pZMoGnqy1y8qPLF1eYA2e-Vf3k");

                    LogUtil.i(App.TAG, "disntace matrix:" + url);
                    String json = JsonHelper.getJsonFromInputStream(url.openStream());
                    JSONObject jsonObject = new JSONObject(json);
                    netResult = new NetResult();
                    netResult.setTag(jsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {
                    JSONObject router = (JSONObject) result.getTag();
                    try {
                        JSONArray array = router.getJSONArray("rows");
                        if (null != array && array.length() > 0) {

                            JSONObject jo = array.getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration");
                            String label = jo.getString("text");
                            textViewDuration.setText(label);
                        }

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "get router error:" + e.getLocalizedMessage());
                    }

                }

            }
        });

        requestDistanceCompute.execute(new HashMap<String, String>());

    }


    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initMapFragment();
                break;
        }
    }


}
