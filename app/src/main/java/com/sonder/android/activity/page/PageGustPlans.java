package com.sonder.android.activity.page;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.common.view.BasePage;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

/**
 * Created by wangzy on 2016/11/10.
 */

public class PageGustPlans extends BasePage {


    private WebView webView;

    SonderBaseActivity sonderBaseActivity;

    private String url = "https://sonderaustralia.com/service-packages/";
    private boolean needAutoLoadUrl = true;

    public PageGustPlans(Activity activity) {
        super(activity);
        this.rootView = View.inflate(activity, R.layout.page_gust_plans, null);
        this.sonderBaseActivity = (SonderBaseActivity) activity;
        initView();
    }

    @Override
    public void onShow() {
        if (needAutoLoadUrl) {
            webView.loadUrl(url);
        }

    }

    @Override
    public void initView() {
        webView = (WebView) findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {


        });

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
//                sonderBaseActivity.hideProgressDialog();
                if (url.equals(PageGustPlans.this.url)) {
                    needAutoLoadUrl = false;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                sonderBaseActivity.showProgressDialog();
            }

//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//
//
//                LogUtil.i(App.TAG,"request url:"+request.toString());
//                if (request.toString().equals(url)) {
//                    view.loadUrl(request.toString());
//                    return true;
//                } else {
//
//
//                    return false;
//                }
//
//            }


        });

    }
}
