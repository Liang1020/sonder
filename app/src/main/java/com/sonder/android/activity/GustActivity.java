package com.sonder.android.activity;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.page.PageGustNews;
import com.sonder.android.activity.page.PageGustPlans;
import com.sonder.android.activity.page.PageGustProfile;
import com.sonder.android.base.SonderBaseActivity;
import com.tab.OnTabItemClickListener;
import com.tab.TabItem;
import com.tab.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GustActivity extends SonderBaseActivity {


    TabLayout tabLayout = new TabLayout();
    private int currentTab = 0;

    PageGustProfile pageGustProfile;
    PageGustPlans pageGustPlans;
    PageGustNews pageGustNews;


    @BindView(R.id.relativeLayoutContentContainers)
    RelativeLayout relativeLayoutContentContainers;

    RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gust);

        ButterKnife.bind(this);

        pageGustProfile = new PageGustProfile(this);
        pageGustPlans = new PageGustPlans(this);
        pageGustNews = new PageGustNews(this);


        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutDynamic));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutTutorial));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutProfile));
        tabLayout.addTabItem(findLinearLayout(R.id.linearLayoutPlans));


        tabLayout.setOnTabItemClickListener(new OnTabItemClickListener() {
            @Override
            public void onCheckIndex(int index, int total, TabItem tabItem) {
                relativeLayoutContentContainers.removeAllViews();
                if (index == 0) {
                    relativeLayoutContentContainers.addView(pageGustNews.getRootView(), lpp);
                    pageGustNews.onShow();
                }

                if (index == 1) {


                    Tool.ToastShow(activity, "Coming soon.");
                }


                if (index == 2) {
                    relativeLayoutContentContainers.addView(pageGustProfile.getRootView(), lpp);
                    pageGustProfile.onShow();

                } else if (index == 3) {
                    relativeLayoutContentContainers.addView(pageGustPlans.getRootView(), lpp);
                    pageGustPlans.onShow();
                }


            }
        });

        try {

            boolean isPlane = (Boolean) App.getApp().getTempObject("isPlan");
            if (isPlane) {
                tabLayout.checkByIndex(3);
            } else {
                tabLayout.checkByIndex(2);
            }
        } catch (Exception e) {
            tabLayout.checkByIndex(2);
        }


    }
}
