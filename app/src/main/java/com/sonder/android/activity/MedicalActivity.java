package com.sonder.android.activity;

import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.EditDialog;
import com.sonder.android.domain.Student;
import com.common.net.NetResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/1.
 */

public class MedicalActivity extends SonderBaseActivity {

    private Student student;


    @BindView(R.id.textViewInsuranceProvider)
    TextView textViewInsuranceProvider;

    @BindView(R.id.textView_activityMedical_insuranceNumber)
    TextView textViewInsuranceNumber;

    @BindView(R.id.textViewExistingConditions)
    TextView textViewExistingConditions;

    @BindView(R.id.textViewMedicalHistory)
    TextView textViewMedicalHistory;

    @BindView(R.id.textViewMedicalAllergies)
    TextView textViewMedicalAllergies;

    @BindView(R.id.textViewCurrentMedication)
    TextView textViewCurrentMedication;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical);
        ButterKnife.bind(this);
        initView();
    }

    public void initView() {
        student = App.getApp().getStudent();
        textViewInsuranceProvider.setText(student.getProfile().getMedicalInsuranceProvider());
        textViewInsuranceNumber.setText(student.getProfile().getMedicalInsuranceMemberNumber());
        textViewExistingConditions.setText(student.getProfile().getMedicalExistingConditions());
        textViewMedicalHistory.setText(student.getProfile().getMedicalHistory());
        textViewMedicalAllergies.setText(student.getProfile().getMedicalAllergies());
        textViewCurrentMedication.setText(student.getProfile().getMedicalCurrentMedication());
    }


    @OnClick(R.id.imageViewBack)
    public void onBackClick(){
        finish();
    }

    @OnClick(R.id.relativeLayout_activityMedical_insuranceProvider)
    public void editInsuranceProvider() {
//        startEditActivity(EditActivity.TAG_MEDICAL_INSURANCE_PROVIDER);

        showUpdateDialog(getInputFromId(R.id.textViewInsuranceProviderTitle),
                "medical_insurance_provider", getInput(textViewInsuranceProvider), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                            if(isSuccess){
                                initView();
                            }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);


    }

    @OnClick(R.id.relativeLayout_activityMedical_insuranceNumber)
    public void editInsuranceNumber() {
//        startEditActivity(EditActivity.TAG_MEDICAL_INSURANCE_NUMBER);

        showUpdateDialog(getInputFromId(R.id.textViewInsuranceNumberTitle),
                "medical_insurance_member_number", getInput(textViewInsuranceNumber), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                        if(isSuccess){
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);


    }

    @OnClick(R.id.relativeLayout_activityMedical_existingConditions)
    public void editExistingConditions() {
//        startEditActivity(EditActivity.TAG_MEDICAL_EXISTING_CONDITIONS);

        showUpdateDialog(getInputFromId(R.id.textViewExistingConditionsTitle),
                "medical_existing_conditions", getInput(textViewExistingConditions), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                        if(isSuccess){
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_NUMBER);

    }

    @OnClick(R.id.relativeLayout_activityMedical_history)
    public void editHistory() {
//  startEditActivity(EditActivity.TAG_MEDICAL_HISTORY);

        showUpdateDialog(getInputFromId(R.id.textViewMedicalHistoryTitle),
                "medical_history", getInput(textViewMedicalHistory), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                        if(isSuccess){
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);

    }

    @OnClick(R.id.relativeLayout_activityMedical_allergies)
    public void editAllergies() {
//        startEditActivity(EditActivity.TAG_MEDICAL_ALLERGIES);

        showUpdateDialog(getInputFromId(R.id.textViewAllergiesTitle),
                "medical_allergies", getInput(textViewMedicalAllergies), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                        if(isSuccess){
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);

    }



    @OnClick(R.id.relativeLayout_activityMedical_currentMedication)
    public void editMedication() {
//        startEditActivity(EditActivity.TAG_MEDICAL_CURRENT_MEDICATION);

        showUpdateDialog(getInputFromId(R.id.textViewCurrentMedicationTitle),
                "medical_current_medication", getInput(textViewCurrentMedication), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                        if(isSuccess){
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);

    }

}
