package com.sonder.android.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.MyImgAdapter;
import com.sonder.android.base.SonderBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PicturesActivity extends SonderBaseActivity {


    @BindView(R.id.viewPager)
    ViewPager viewPager;

    MyImgAdapter myImgAdapter;

    String[] imgs;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);
        ButterKnife.bind(this);


//        int index = Integer.parseInt((String)((ImageView) v).getTag());
//        App.getApp().putTemPObject("imgs", imgs);
//        App.getApp().putTemPObject("index", index);

        imgs = (String[]) App.getApp().getTempObject("imgs");
        index = (Integer) App.getApp().getTempObject("index");

        myImgAdapter = new MyImgAdapter(this, imgs);
        viewPager.setAdapter(myImgAdapter);
        viewPager.setCurrentItem(index);

        myImgAdapter.setOnInterfaceClickListener(new MyImgAdapter.OnInterfaceClickListener() {
            @Override
            public void onClickImgUrl(String url) {


//                App.getApp().putTemPObject("url", url);
//                Tool.startActivity(PicturesActivity.this, PicturePreviewActivity.class);


            }
        });


    }




}

