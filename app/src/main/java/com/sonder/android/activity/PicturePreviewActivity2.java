package com.sonder.android.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.common.util.LogUtil;
import com.common.view.ZoomImageView;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PicturePreviewActivity2 extends SonderBaseActivity {


    @BindView(R.id.zoomImageView)
    ImageView zoomImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_preview2);
        ButterKnife.bind(this);

        String url = (String) App.getApp().getTempObject("url");
        LogUtil.i(App.TAG,"urlin preivew:"+url);


        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                zoomImageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        zoomImageView.setTag(target);

        zoomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        Picasso.with(this).load(url).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(target);
//        Picasso.with(this).load(url).resize(400,400).centerCrop().config(Bitmap.Config.RGB_565).into(zoomImageView);


    }
}
