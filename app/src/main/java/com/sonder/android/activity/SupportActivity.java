package com.sonder.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.CustomDialog;
import com.sonder.android.dialog.CustomDialogIOS;
import com.sonder.android.dialog.PersonSelectDialog;
import com.sonder.android.dialog.PhotoDialog;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.utils.PhotoLibUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SupportActivity extends SonderBaseActivity {


    PersonSelectDialog personSelectDialog;

    CustomDialogIOS customDialog;

    @BindView(R.id.textViewRequestPerson)
    TextView textViewRequestPerson;

    @BindView(R.id.textViewHelpLocation)
    TextView textViewHelpLocation;

    @BindView(R.id.editTextDesc)
    EditText editTextDesc;


    @BindView(R.id.imageViewAddPhoto1)
    ImageView imageViewAddPhoto1;

    @BindView(R.id.imageViewAddPhoto2)
    ImageView imageViewAddPhoto2;

    @BindView(R.id.imageViewAddPhoto3)
    ImageView imageViewAddPhoto3;

    ImageView imageViewPicSelect;

    Animation animationShake;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);
        initView();
    }


    @Override
    public void initView() {

        animationShake = AnimationUtils.loadAnimation(this, R.anim.shake);

        personSelectDialog = new PersonSelectDialog(this);
        personSelectDialog.setOnPersonSelectListener(new PersonSelectDialog.OnPersonSelectListener() {
            @Override
            public void OnPersonSelect(int type) {
                textViewRequestPerson.setTag(String.valueOf(type));
                textViewRequestPerson.setText(getTypeTextbyType(type));
            }
        });


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                showPhotoDialog(new PhotoDialog.OnPhotoCallbackListener() {
                    @Override
                    public void chooseType(int chooseTypeClicked) {

                        LogUtil.i(App.TAG, "chooseTypeClicked:" + chooseTypeClicked);

                        imageViewPicSelect = (ImageView) v;

                        if (chooseTypeClicked == 0) {
                            onChosePhtoClick(false);
                        } else {
                            onTakePhtoClick(false);
                        }

                    }
                });
            }
        };
        imageViewAddPhoto1.setOnClickListener(onClickListener);
        imageViewAddPhoto2.setOnClickListener(onClickListener);
        imageViewAddPhoto3.setOnClickListener(onClickListener);

        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto1);
        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto2);
        Picasso.with(App.getApp()).load(R.drawable.icon_add_photo).resize(App.getApp().PIC_W, App.getApp().PIC_H).into(imageViewAddPhoto3);


        if (null != App.getApp().getCurrenteSimpleAddrss()) {

            SimpleAddrss simpleAddrss = App.getApp().getCurrenteSimpleAddrss();

            textViewHelpLocation.setText(simpleAddrss.getAddress());
            textViewHelpLocation.setTag(simpleAddrss);
        }

    }


    @OnClick(R.id.textViewPhoneSSC)
    public void onClickPhoenSSC() {

        String phone = getString(R.string.ssc_phone_call);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SonderBaseActivity.SHORT_REQUEST_READ_EXTEAL && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                onChosePhtoClick(false);
            } else {
                onTakePhtoClick(false);
            }
        }

    }


    private boolean checkValue() {

        if (!Tool.isOpenGPS(this)) {
//            Tool.showMessageDialog(R.string.please_open_gps, this);

            DialogUtils.showConfirmDialog(this, "", getString(R.string.please_open_gps), getString(R.string.confirm), getString(R.string.cancel), new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {

//                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                    startActivityForResult(intent, GO_OPEN_GPS); // 设置完成后返回到原来的界面

                }
            });

            return false;
        }

        if (StringUtils.isEmpty(getInput(editTextDesc))) {
            editTextDesc.startAnimation(animationShake);
            return false;
        }

        if (StringUtils.isEmpty(getInput(textViewRequestPerson))) {
            textViewRequestPerson.startAnimation(animationShake);
            return false;
        }

        if (StringUtils.isEmpty(getInput(textViewHelpLocation))) {
            textViewHelpLocation.startAnimation(animationShake);
            return false;
        }

        return true;
    }


//    private void initGPS() {
//        LocationManager locationManager = (LocationManager) this
//                .getSystemService(Context.LOCATION_SERVICE);
//        // 判断GPS模块是否开启，如果没有则开启
//        if (!locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
//
//            Toast.makeText(TrainDetailsActivity.this, "请打开GPS",
//                    Toast.LENGTH_SHORT).show();
//            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//            dialog.setMessage("请打开GPS");
//            dialog.setPositiveButton("确定",
//                    new android.content.DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface arg0, int arg1) {
//
//                            // 转到手机设置界面，用户设置GPS
//                            Intent intent = new Intent(
//                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                            startActivityForResult(intent, 0); // 设置完成后返回到原来的界面
//
//                        }
//                    });
//            dialog.setNeutralButton("取消", new android.content.DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface arg0, int arg1) {
//                    arg0.dismiss();
//                }
//            } );
//            dialog.show();
//        } else {
//            // 弹出Toast
////          Toast.makeText(TrainDetailsActivity.this, "GPS is ready",
////                  Toast.LENGTH_LONG).show();
////          // 弹出对话框
////          new AlertDialog.Builder(this).setMessage("GPS is ready")
////                  .setPositiveButton("OK", null).show();
//        }
//    }


    @OnClick(R.id.buttonSend)
    public void onClickSend() {
        if (checkValue() == false) {
            return;
        }

        if (null == customDialog) {

            String title = getString(R.string.apply_confirm_title);
            String content = getString(R.string.apply_confirm_content);
            String yes = getString(R.string.apply_confirm_yes);
            String no = getString(R.string.apply_confirm_no);
            customDialog = new CustomDialogIOS(this, title, yes, no);
        }


        customDialog.setDialogCallBackListener(new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {

                LogUtil.i(App.TAG, "yes no:" + yesOrNo);

                if (yesOrNo) {


                } else {


                    if (checkValue()) {


                        HashMap<String, String> hashMap = new HashMap<>();

                        hashMap.put("priority", "3");
                        String sumery = getInputFromId(R.id.editTextDesc);
                        hashMap.put("summary", sumery);
                        String requestPerson = "";

                        try {
                            requestPerson = String.valueOf((Integer.parseInt((String) textViewRequestPerson.getTag()) + 1));
                        } catch (Exception e) {
                        }

                        hashMap.put("description", getInputFromId(R.id.editTextDescDesc));
                        hashMap.put("request_person", requestPerson);

                        if (null != textViewHelpLocation.getTag()) {

                            SimpleAddrss address = (SimpleAddrss) textViewHelpLocation.getTag();
                            hashMap.put("address", address.getAddress());
                            hashMap.put("location_lon", address.getLng());
                            hashMap.put("location_lat", address.getLat());

                        }

                        ArrayList<File> files = new ArrayList<>();


                        String pf1 = (String) imageViewAddPhoto1.getTag();
                        addFiles(pf1, files);

                        String pf2 = (String) imageViewAddPhoto2.getTag();

                        addFiles(pf2, files);

                        String pf3 = (String) imageViewAddPhoto3.getTag();
                        addFiles(pf3, files);

                        FormFile[] formFiles = new FormFile[files.size()];

                        for (int i = 0, isize = files.size(); i < isize; i++) {
                            try {


                                File tf = files.get(i);
                                String param = "attachment" + (i + 1);
                                byte[] data = FileUtils.readFile2Byte(tf.getAbsolutePath());
                                FormFile formFile = new FormFile(param, data, tf.getName(), FormFile.contentType);
                                formFiles[i] = formFile;
                                LogUtil.i(App.TAG, "upload file:" + tf.getName() + "  size:" + data.length);


                            } catch (IOException e) {
                                e.printStackTrace();
                                LogUtil.e(App.TAG, "errors:" + e.getLocalizedMessage());
                            }
                        }


                        requestCreateIncident(hashMap, formFiles, true, new NetCallBack() {
                            @Override
                            public void onFinish(NetResult result, BaseTask baseTask) {

                                if (null != result) {
                                    if (result.isOk()) {

                                        App.getApp().putTemPObject(KEY_INCIDENT, result.getData()[0]);
                                        Tool.startActivityForResult(SupportActivity.this, SupportDetailActivity.class, GO_DETAIL_SUCCESS);

                                    } else {
                                        Tool.showMessageDialog(result.getMessage(), SupportActivity.this);
                                    }
                                } else {
                                    Tool.showMessageDialog(R.string.error_net, SupportActivity.this);
                                }

                            }
                        });
                    }


                }
            }
        });

        customDialog.show();

    }

    @OnClick(R.id.imageViewBack)
    public void onImageViewback() {

        finish();
    }


    @Override
    public void onBackPressed() {

        if (null != customDialog && customDialog.isShowing()) {

            customDialog.dismiss();
            return;
        }

        super.onBackPressed();
    }

    @OnClick(R.id.textViewHelpLocation)
    public void onHelpLocationClick() {
//        Tool.startActivityForResult(this, AddressActivity.class, SHORT_GO_ADDDRESS);


        Tool.startActivityForResult(this, AddressHereActivity.class, SHORT_GO_ADDDRESS);
    }


    @OnClick(R.id.relatveLayoutPersonSelect)
    public void onClickPersonSelect() {
        closeKeyBorad();
        personSelectDialog.show();
    }


    private void onImgeSelect(String path) {
        final File file = new File(path);
        if (file.exists()) {

            LogUtil.e(App.TAG, "selected path:" + path);

            imageViewPicSelect.setTag(file.getAbsolutePath());

            Picasso.with(App.getApp()).load(file).
                    config(Bitmap.Config.RGB_565).
                    resize(App.getApp().PIC_W, App.getApp().PIC_H).
                    centerCrop().
                    into(imageViewPicSelect);
//
//
//            Luban.get(this)
//                    .load(file)                     //传人要压缩的图片
//                    .putGear(Luban.THIRD_GEAR)      //设定压缩档次，默认三挡
//                    .setCompressListener(new OnCompressListener() { //设置回调
//
//                        @Override
//                        public void onStart() {
//                            showProgressDialog(false);
//
//                        }
//
//                        @Override
//                        public void onSuccess(File file) {
//                            hideProgressDialog();
//                            LogUtil.i(App.TAG, "压缩成功:" + file.getAbsolutePath());
//                            imageViewPicSelect.setTag(file.getAbsolutePath());
//
//                            Picasso.with(App.getApp()).load(file).
//                                    config(Bitmap.Config.RGB_565).
//                                    resize(App.getApp().PIC_W, App.getApp().PIC_H).
//                                    centerCrop().
//                                    into(imageViewPicSelect);
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            hideProgressDialog();
//
//                        }
//                    }).launch();    //启动压缩

        } else {
            Tool.ToastShow(this, R.string.common_pic_not_exist);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GO_OPEN_GPS) {


            return;
        }

        if (requestCode == GO_DETAIL_SUCCESS) {
            setResult(BACK_TO_REQUESTS);
            finish();
        } else if (requestCode == SHORT_GO_ADDDRESS && resultCode == RESULT_OK) {
            SimpleAddrss addrss = (SimpleAddrss) App.getApp().getTempObject("address");
            if (null != addrss) {
                textViewHelpLocation.setText(addrss.getAddress());
                textViewHelpLocation.setTag(addrss);
            }
            return;
        }

        if (requestCode == PictureChoseActivity.SHORT_REQUEST_MEDIAS && resultCode == Activity.RESULT_OK) {
            ArrayList<String> fiels = data.getStringArrayListExtra(PictureChoseActivity.keyResult);
            onImgeSelect(fiels.get(0));

        }

        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                try {
                    mFileName = PhotoLibUtils.getDataColumn(this, data.getData(), null, null);
//                String newPath = sharedpreferencesUtil.getImageTempNameString2();
//                PhotoLibUtils.copyFile(mFileName, newPath);
//                String path = sharedpreferencesUtil.getImageTempNameUri().getPath();
                    onImgeSelect(mFileName);
                } catch (Exception e) {
                    Tool.ToastShow(this, R.string.common_pic_not_exist);
                }

            }
        } else if (requestCode == SELECT_PIC_KITKAT && resultCode == Activity.RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                try {
                    mFileName = PhotoLibUtils.getPath(this, data.getData());

//                String newPath = sharedpreferencesUtil.getImageTempNameString2();
//                PhotoLibUtils.copyFile(mFileName, newPath);
//                String path = sharedpreferencesUtil.getImageTempNameUri().getPath();


                    onImgeSelect(mFileName);
                } catch (Exception e) {
                    Tool.ToastShow(this, R.string.common_pic_not_exist);
                }

            }
        } else if (requestCode == TAKE_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            String path = sharedpreferencesUtil.getImageTempNameUri().getPath();
            onImgeSelect(path);
        }

    }


    private void addFiles(String file, ArrayList<File> files) {
        if (!StringUtils.isEmpty(file)) {

            files.add(new File(file));
        }
    }
}
