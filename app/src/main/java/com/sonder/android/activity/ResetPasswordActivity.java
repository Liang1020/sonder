package com.sonder.android.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordActivity extends SonderBaseActivity {


    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @BindView(R.id.editTextConfirmPwd)
    EditText editTextConfirmPwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.textViewDone)
    public void onClickDone() {
        if (checkValue()) {
            requestResetPassword(getInput(editTextPassword));
        }
    }

    private boolean checkValue() {

        String pwd = getInput(editTextPassword);
        if (StringUtils.isEmpty(pwd)) {
            Tool.ToastShow(this, R.string.reset_pwd_empty);
            return false;
        }

        String cpwd = getInput(editTextConfirmPwd);
        if (!pwd.equals(cpwd)) {
            Tool.ToastShow(this, R.string.reset_pwd_notify);
            return false;
        }

        return true;
    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }


    BaseTask baseTaskResetPwd;

    private void requestResetPassword(final String pwd) {

        BaseTask.resetTastk(baseTaskResetPwd);

        baseTaskResetPwd = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("new_password", pwd);
                    netResult = NetInterface.resetPassword(jsonObject);
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "resestpassword error:" + e.getLocalizedMessage());
                }
                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Tool.ToastShow(ResetPasswordActivity.this, result.getMessage());
                    }
                } else {
                    Tool.ToastShow(ResetPasswordActivity.this, R.string.error_net);
                }

            }
        });

        baseTaskResetPwd.execute(new HashMap<String, String>());

    }

}
