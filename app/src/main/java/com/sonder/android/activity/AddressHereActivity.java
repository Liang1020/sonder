package com.sonder.android.activity;

import android.Manifest;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.search.DiscoveryResult;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.Place;
import com.here.android.mpa.search.PlaceLink;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.SearchRequest;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressSimpleAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.SimpleAddrss;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import me.relex.okhttpmanager.BaseCallback;
import me.relex.okhttpmanager.OkHttpClientManager;
import me.relex.okhttpmanager.RequestParams;

import static com.sonder.android.R.id.map;

/**
 * Created by wangzy on 2016/11/7.
 */

public class AddressHereActivity extends SonderBaseActivity {


    @BindView(R.id.editTextInput)
    EditText editTextInput;

    @BindView(R.id.listView)
    ListView listView;

    private List<Address> arrayListAddress;

    private AddressSimpleAdapter addressAdapter;
    private SearchRequest searchRequest;
    private MapFragment mapFragment;

    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private boolean isMapGengineInit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            initMapFragment();
        }
    }

    @OnTextChanged(R.id.editTextInput)
    public void onInchagneChagned() {
        String input = getInput(editTextInput);
        if (StringUtils.computStrlen(input) < 2) {
            return;
        }
        requestAddressList(input);
    }


    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
//                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initMapFragment();
                break;
        }
    }

    private void initMapFragment() {

        // Search for the map fragment to finish setup by calling init().
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(map);
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    isMapGengineInit = true;
                    LogUtil.i(App.TAG, "init map engin done");
                }
            }
        });


    }


    private void searchGivenTest(final String searchText) {
        if (isMapGengineInit) {
            searchRequest = new SearchRequest(searchText);
            GeoCoordinate seattle = new GeoCoordinate(47.592229, -122.315147);
            searchRequest.setSearchCenter(seattle);
            searchRequest.setCollectionSize(1);
            searchRequest.execute(new ResultListener<DiscoveryResultPage>() {

                @Override
                public void onCompleted(DiscoveryResultPage discoveryResultPage, ErrorCode errorCode) {

                    if (errorCode != ErrorCode.NONE) {
                        LogUtil.e(App.TAG, "error search:" + errorCode.toString());
                        Tool.ToastShow(AddressHereActivity.this, R.string.parse_loc_error);
                    } else {

                        LogUtil.i(App.TAG, "search count:" + discoveryResultPage.getItems().size());
                        List<DiscoveryResult> items = discoveryResultPage.getItems();
                        if (ListUtiles.isEmpty(items)) {
                            hideProgressDialog();
                            Tool.ToastShow(AddressHereActivity.this, R.string.parse_loc_error);
                        } else {

                            ((PlaceLink) items.get(0)).getDetailsRequest().execute(new ResultListener<Place>() {
                                @Override
                                public void onCompleted(Place place, ErrorCode errorCode) {
                                    hideProgressDialog();
                                    if (null != place) {

                                        Location loc = place.getLocation();

                                        double lat = loc.getCoordinate().getLatitude();
                                        double lng = loc.getCoordinate().getLongitude();

                                        SimpleAddrss simpleAddrss = new SimpleAddrss();
                                        simpleAddrss.setAddress(searchText);
                                        simpleAddrss.setLat(String.valueOf(lat));
                                        simpleAddrss.setLng(String.valueOf(lng));

                                        putDataBack(simpleAddrss);

                                    } else {
                                        Tool.ToastShow(AddressHereActivity.this, R.string.parse_loc_error);

                                    }
                                }
                            });

                        }

                    }
                }
            });

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgressDialog(false);
                }
            });


        }

    }

    @OnClick(R.id.imageLocation)
    public void onClickCurrentLocation() {

        if (null != App.getApp().getCurrenteSimpleAddrss()) {

            SimpleAddrss sa = App.getApp().getCurrenteSimpleAddrss();
            editTextInput.setText(sa.getAddress());
            editTextInput.setTag(sa);

            return;
        }


        if (null != App.getApp().getCurrentLatlng()) {

            parseLocationFrolatLng(this, App.getApp().getCurrentLatlng(), true, new NetCallBack() {


                @Override
                public void onFinish(NetResult result) {
                    if (null != result && null != result.getTag()) {
                        SimpleAddrss sa = (SimpleAddrss) result.getTag();
                        editTextInput.setText(sa.getAddress());
                        editTextInput.setTag(sa);
                    } else {
                        Tool.ToastShow(AddressHereActivity.this, R.string.location_fail);
                    }
                }
            });

        }
    }


    BaseTask baseTaskAddress;

    private void requestAddressList(final String input) {

        String appId = "";
        String token = "";

        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            appId = appInfo.metaData.getString("com.here.android.maps.appid");
            token = appInfo.metaData.getString("com.here.android.maps.apptoken");

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String url = "";

        try {

            url = "https://places.demo.api.here.com/places/v1/suggest?at=0,0&" +
                    "q=" + URLEncoder.encode(input, "utf-8") +
                    "&app_id=" + appId +
                    "&app_code=" + token;


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        OkHttpClient mOkHttpClient = new OkHttpClient();
        final Request request = new Request.Builder().url(url).build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                //String htmlStr =  response.body().string();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    final ArrayList<SimpleAddrss> simpleAddress = new ArrayList<SimpleAddrss>();

                    JSONArray sgs = jsonObject.getJSONArray("suggestions");

                    if (null != sgs && sgs.length() > 0) {
                        for (int i = 0, isize = sgs.length(); i < isize; i++) {
                            String adr = sgs.getString(i);
                            SimpleAddrss sad = new SimpleAddrss();
                            sad.setAddress(adr);
                            simpleAddress.add(sad);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addressAdapter = new AddressSimpleAdapter(AddressHereActivity.this, simpleAddress);
                                listView.setAdapter(addressAdapter);
                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        AddressSimpleAdapter sia = ((AddressSimpleAdapter) parent.getAdapter());
                                        sia.select(position);
                                        SimpleAddrss sa = sia.getSelectAddrss();
                                        editTextInput.setTag(sa);
                                        editTextInput.setText(sa.getAddress());


                                    }
                                });
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void parseLocationLatlng(final String location) {

        String appId = "";
        String token = "";

        ApplicationInfo appInfo = null;
        try {
            appInfo = this.getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            appId = appInfo.metaData.getString("com.here.android.maps.appid");
            token = appInfo.metaData.getString("com.here.android.maps.apptoken");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String url = "";
        try {

            url = "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext=" + URLEncoder.encode(location, "utf-8") + "&" +
                    "app_id=" + appId + "&app_code=" + token + "&gen=8";

            RequestParams params = new RequestParams();

            OkHttpClientManager.get(url, params,

                    new BaseCallback() {

                        @Override
                        public void onResponse(final Response response) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    hideProgressDialog();

                                    if (response.code() != 200) {

//                                        Tool.ToastShow(AddressHereActivity.this, R.string.error_net);

                                        searchGivenTest(location);
                                        return;
                                    }

                                }
                            });


                            try {

                                JSONObject jsonObject = new JSONObject(response.body().string());
                                JSONArray addreses = jsonObject.getJSONObject("Response").getJSONArray("View").getJSONObject(0).getJSONArray("Result");

                                LogUtil.i(App.TAG, "adress:" + jsonObject.toString());

                                final ArrayList<SimpleAddrss> simpleAddress = new ArrayList<SimpleAddrss>();

                                if (null != addreses && addreses.length() > 0) {

                                    for (int i = 0, isize = addreses.length(); i < isize; i++) {
                                        JSONObject jo = addreses.getJSONObject(i);

                                        JSONObject locationObj = jo.getJSONObject("Location");


                                        JSONObject DisplayPosition = locationObj.getJSONObject("DisplayPosition");

                                        double lat = DisplayPosition.getDouble("Latitude");
                                        double lon = DisplayPosition.getDouble("Longitude");

                                        JSONObject addrs = locationObj.getJSONObject("Address");

                                        String label = addrs.getString("Label");
                                        String contry = addrs.getString("Country");
                                        String postcode = addrs.getString("PostalCode");

                                        SimpleAddrss simple = new SimpleAddrss();
                                        simple.setAddress(label);
                                        simple.setLat(String.valueOf(lat));
                                        simple.setLng(String.valueOf(lon));

                                        simpleAddress.add(simple);

                                    }
                                }


                                if (!ListUtiles.isEmpty(simpleAddress)) {

                                    SimpleAddrss sa = simpleAddress.get(0);
                                    LogUtil.i(App.TAG, "addrss:" + sa.getAddress() + " " + sa.getLat() + "," + sa.getLng());
                                    putDataBack(sa);

                                } else {
//                                    Tool.ToastShow(AddressHereActivity.this, R.string.parse_loc_error);
                                    searchGivenTest(location);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                LogUtil.e(App.TAG, "parse error:" + e.getLocalizedMessage());
                                searchGivenTest(location);
                            }

                        }

                        @Override
                        public void onRequestFinish() {
                            hideProgressDialog();
                        }

                        @Override
                        public void onRequestStart() {

                            showProgressDialog(false);
                        }
                    });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }


    @OnClick(R.id.textViewSave)
    public void onClickSave() {

        final SimpleAddrss sa = (SimpleAddrss) editTextInput.getTag();
        if (null == sa) {
            Tool.ToastShow(this, R.string.support_address);
            return;
        }

        String title = getResources().getString(R.string.coomon_confirm_ask);
        String yes = getResources().getString(R.string.coomon_confirm_ask_yes);
        String no = getResources().getString(R.string.coomon_confirm_ask_no);


        DialogUtils.showConfirmDialog(this, "", title, yes, no, new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {

                if (yesOrNo) {
                    if (null != sa) {
                        LogUtil.i(App.TAG, "adres:" + sa.getAddress());
                        parseLocationLatlng(sa.getAddress());
                    }
//                   App.getApp().putTemPObject("address", addressAdapter.getSelectAddrss());
//                   setResult(RESULT_OK);

                } else {


                }
//                finish();
            }
        });


    }

    private void putDataBack(SimpleAddrss simpleAddrss) {

        App.getApp().putTemPObject("address", simpleAddrss);
        setResult(RESULT_OK);
        finish();
    }


    @OnClick(R.id.imageViewBack)
    public void onClick() {
        finish();
    }


    @OnClick(R.id.textViewSave)
    public void onSaveClick() {


    }

}
