package com.sonder.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.PhotoDialog;
import com.sonder.android.domain.Profile;
import com.sonder.android.domain.Student;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.utils.PhotoLibUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/3.
 */

public class VisaActivity extends SonderBaseActivity {


    @BindView(R.id.imageViewAddPhoto)
    public ImageView imageViewAddPhoto;

    @BindView(R.id.textViewVisaDate)
    TextView textViewVisaDate;

    @BindView(R.id.textViewVisExpiredDate)
    TextView textViewVisExpiredDate;


    SimpleDateFormat simpleDateFormat = Constant.SDFD2;

    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visa);
        ButterKnife.bind(this);

        initView();
    }

    @Override
    public void initView() {

        student = App.getApp().getStudent();
        if (null != student && null != student.getProfile()) {


            Profile profile = student.getProfile();


            try {
                textViewVisaDate.setText(simpleDateFormat.format(profile.getTravelVisaStartDate()));
            } catch (Exception e) {

                LogUtil.e(App.TAG,"start date error");
            }


            try {
                textViewVisExpiredDate.setText(simpleDateFormat.format(profile.getTravelVisaEndDate()));
            } catch (Exception e) {
                LogUtil.e(App.TAG,"end date error");
            }
            try {

                if (!StringUtils.isEmpty(profile.getTravelPassportPhotoUrl())) {
                    Picasso.with(App.getApp()).load(profile.getTravelPassportPhotoUrl()).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_default_header).into(imageViewAddPhoto);
                }

            } catch (Exception e) {
                LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
            }


        }
    }


    @OnClick(R.id.imageViewBack)
    public void onBackClick() {
        finish();
    }


    @OnClick(R.id.textViewSave)
    public void onSaveClick() {

        if (checkValue()) {
            String msg = getResources().getString(R.string.coomon_confirm_ask);
            String yes = getResources().getString(R.string.coomon_confirm_ask_yes);
            String no = getResources().getString(R.string.coomon_confirm_ask_no);

            DialogUtils.showConfirmDialog(this, "", msg, yes, no, new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {

                        String date1 = getInput(textViewVisaDate);
                        String date2 = getInput(textViewVisExpiredDate);

                        HashMap<String, String> hmap = new HashMap<String, String>();

                        hmap.put("travel_visa_start_date", date1);
                        hmap.put("travel_visa_end_date", date2);

                        updateAll(new NetCallBack() {
                            @Override
                            public void onFinish(NetResult result, BaseTask baseTask) {

                                if (null != result) {
                                    if (result.isOk()) {

                                        App.getApp().saveStudent((Student) result.getData()[0]);
//                                        initView();
                                        Tool.ToastShow(VisaActivity.this, R.string.common_save_success);
//                                        finish();

                                    } else {
                                        Tool.showMessageDialog(result.getMessage(), VisaActivity.this);
                                    }
                                } else {
                                    Tool.showMessageDialog(R.string.error_net, VisaActivity.this);
                                }
                            }
                        }, App.getApp().getStudent(), hmap);


                    }
                }
            });

        }


    }


    private boolean checkValue() {

        String labelDate = getInput(textViewVisaDate);

        if (StringUtils.isEmpty(labelDate)) {

            textViewVisaDate.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

            return false;
        }

        String labelDateExpired = getInput(textViewVisExpiredDate);
        if (StringUtils.isEmpty(labelDateExpired)) {

            textViewVisExpiredDate.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

            return false;
        }

        return true;


    }


    @OnClick(R.id.imageViewAddPhoto)
    public void addPhototo() {


        showPhotoDialog(new PhotoDialog.OnPhotoCallbackListener() {
            @Override
            public void chooseType(int chooseTypeClicked) {

                int type = chooseTypeClicked;

                switch (type) {
                    case 0: {
                        onChosePhtoClick(false);
                    }
                    break;

                    case 1: {
                        onTakePhtoClick(false);
                    }
                    break;

                }


            }
        });


//        showMediaDialog(true, new MediaDialog.OnMediaCallbackListener() {
//            @Override
//            public void chooseType(int chooseTypeClicked) {
//
//
//
//            }
//        });


    }


    @OnClick(R.id.textViewVisaDate)
    public void onVisDateClick() {

        Date date = new Date();

        try {
            String datelabel = getInput(textViewVisaDate);
            date = simpleDateFormat.parse(datelabel);
        } catch (Exception e) {

        }

        showTimePicker(new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date) {
                textViewVisaDate.setText(simpleDateFormat.format(date));
            }
        }, getString(R.string.personal_visa_date), date);

    }

    @OnClick(R.id.textViewVisExpiredDate)
    public void onClickExpiredDate() {

        Date date = new Date();

        try {
            String datelabel = getInput(textViewVisExpiredDate);
            date = simpleDateFormat.parse(datelabel);
        } catch (Exception e) {
        }

        showTimePicker(new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date) {
                textViewVisExpiredDate.setText(simpleDateFormat.format(date));
            }
        }, getString(R.string.personal_visa_expired), date);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SonderBaseActivity.SHORT_REQUEST_READ_EXTEAL && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                onChosePhtoClick(false);
            } else {
                onTakePhtoClick(false);
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode== PictureChoseActivity.SHORT_REQUEST_MEDIAS && resultCode==Activity.RESULT_OK){
            ArrayList<String> fiels=data.getStringArrayListExtra(PictureChoseActivity.keyResult);
            onVisaPictureSelect(fiels.get(0));

        }


        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                mFileName = PhotoLibUtils.getDataColumn(this, data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                String path = sharedpreferencesUtil.getImageTempNameUri().getPath();
                onVisaPictureSelect(path);

            }
        } else if (requestCode == SELECT_PIC_KITKAT && resultCode == Activity.RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(this, data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                String path = sharedpreferencesUtil.getImageTempNameUri().getPath();
                onVisaPictureSelect(path);
            }
        } else if (requestCode == TAKE_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            String path = sharedpreferencesUtil.getImageTempNameUri().getPath();
            onVisaPictureSelect(path);
        }

    }

    private void onVisaPictureSelect(String path) {

        LogUtil.i(App.TAG, "path:" + path);

        if (!StringUtils.isEmpty(path)) {
            File file = new File(path);
            if (file.exists()) {

                try {
                    FormFile formFile = new FormFile("passport_photo", FileUtils.readFile2Byte(path), "passport_photo", FormFile.contentType);
                    requestUploadHeaderorPassport(new FormFile[]{formFile}, true, new NetCallBack() {
                        @Override
                        public void onFinish(NetResult result, BaseTask baseTask) {

                            if (null != result) {

                                if (result.isOk()) {

                                    try {
                                        JSONObject stu = (JSONObject) result.getData()[0];
                                        student = JsonHelper.parseStudentInfo(stu.toString());
                                        App.getApp().saveStudent(student);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        LogUtil.e(App.TAG, "upload passport error:" + e.getLocalizedMessage());
                                    }

                                    initView();

                                } else {
                                    Tool.showMessageDialog(result.getMessage(), VisaActivity.this);
                                }
                            } else {
                                Tool.showMessageDialog(R.string.error_net, VisaActivity.this);
                            }

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();

                    LogUtil.e(App.TAG, "onVisaPictureSelect:" + e.getLocalizedMessage());
                }


            }
        }

    }

}
