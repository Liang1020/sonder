package com.sonder.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;


public class SplashActivity extends Activity {


    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_SEE_GUILD = 1004;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApp().addActivity(this);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    /*|| ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED*/
                    ) {
//                App.getApp().startLocation();
                App.getApp().conenctionApiClient();
                delayGo();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, SHORT_ACCESS_LOCATION);
            }
        } else {
//            App.getApp().startLocation();
            App.getApp().conenctionApiClient();
            delayGo();
        }

    }

    private void delayGo() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                boolean isSeeGuild = SharePersistent.getBoolean(SplashActivity.this, App.KEY_GUILD);
                if (isSeeGuild) {
                    gotoLogin();
                } else {
//                    Tool.startActivityForResult(SplashActivity.this, GuidActivity.class, SHORT_SEE_GUILD);
                    Tool.startActivity(SplashActivity.this, LoginActivity.class);
                }
            }
        }, 1 * 1000);

    }

    private void gotoLogin() {
        Tool.startActivity(SplashActivity.this, LoginActivity.class);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SHORT_SEE_GUILD && resultCode == RESULT_OK) {
            gotoLogin();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SHORT_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                App.getApp().startLocation();
                App.getApp().conenctionApiClient();
            }
            delayGo();

            return;
        }
    }


}
