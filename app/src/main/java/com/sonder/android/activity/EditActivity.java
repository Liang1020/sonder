package com.sonder.android.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.ProcessingDialog;
import com.sonder.android.dialog.SaveProfileDialog;
import com.sonder.android.domain.Student;
import com.sonder.android.manager.DateManager;
import com.sonder.android.manager.KeyboardManager;
import com.sonder.android.manager.StringManager;
import com.sonder.android.net.NetInterface;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.LogUtil;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/3.
 */

public class EditActivity extends SonderBaseActivity {
    public static final String EDIT_TAG = "edit";
    public static final int TAG_DEFAULT = 0x0000;
    public static final int TAG_EDU_STUDENT_ID = 0x0001;
    public static final int TAG_EDU_COLLEGE = 0x0002;
    public static final int TAG_EDU_CAMPUS = 0x0003;
    public static final int TAG_EDU_PHONE = 0x0004;
    public static final int TAG_EDU_ADDRESS = 0x0005;
    public static final int TAG_PERSONAL_CHINESENAME = 0x0006;
    public static final int TAG_PERSONAL_ENGLISHNAME = 0x0007;
    public static final int TAG_PERSONAL_DOB = 0x0008;
    public static final int TAG_PERSONAL_PHONE = 0x0009;
    public static final int TAG_PERSONAL_ADDRESS = 0x0010;
    public static final int TAG_PERSONAL_EMERGENCY = 0x0011;
    public static final int TAG_MEDICAL_INSURANCE_PROVIDER = 0x0012;
    public static final int TAG_MEDICAL_INSURANCE_NUMBER = 0x0013;
    public static final int TAG_MEDICAL_EXISTING_CONDITIONS = 0x0014;
    public static final int TAG_MEDICAL_HISTORY = 0x0015;
    public static final int TAG_MEDICAL_ALLERGIES = 0x0016;
    public static final int TAG_MEDICAL_CURRENT_MEDICATION = 0x0017;
    public static final int TAG_TRAVEL_INSURANCE_PROVIDER = 0x0018;
    public static final int TAG_TRAVEL_MEMBER_NUMBER = 0x0019;
    public static final int TAG_TRAVEL_DRIVER_LICENSE = 0x0020;
    public static final int TAG_TRAVEL_VISA = 0x0021;


    private Student student;
    private Student tempStu;
    String title = "";
    String value = "";

    @BindView(R.id.textView_activityEdit_title)
    TextView textView_activityEdit_title;

    @BindView(R.id.editText_activityEdit_input)
    EditText editText_activityEdit_input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        init();
    }


    @OnClick(R.id.imageView_activityEdit_back)
    public void save() {
        KeyboardManager.hideInputKeyboard();
        SaveProfileDialog saveProfileDialog = new SaveProfileDialog(this) {
            @Override
            public void onDismiss(int tag) {
                switch (tag) {
                    case SaveProfileDialog.TAG_SAVE:
                        int editTag = getIntent().getIntExtra(EDIT_TAG, TAG_DEFAULT);
                        tempStu = new Student(App.getApp().getStudent());
                        value = getInput(editText_activityEdit_input);
                        switch (editTag) {
                            case TAG_EDU_STUDENT_ID:
                                tempStu.getProfile().setEducationStudentId(value);
                                break;
                            case TAG_EDU_COLLEGE:
                                tempStu.getProfile().setEducationInstitutionCampusName(value);
                                break;
                            case TAG_EDU_CAMPUS:
                                tempStu.getProfile().setEducationInstitutionCampusName(value);
                                break;
                            case TAG_EDU_PHONE:
                                tempStu.getProfile().setEducationInstitutionCampusPhoneNumber(value);
                                break;
                            case TAG_PERSONAL_CHINESENAME:
                                tempStu.setFullName(value);
                                break;
                            case TAG_PERSONAL_ENGLISHNAME:
                                tempStu.setPreferredName(value);
                                break;
                            case TAG_PERSONAL_DOB:

                                tempStu.setDob(DateManager.getDateFromString(value,  com.common.util.Constant.STRING_DAY_FORMAT2));
                                break;
                            case TAG_PERSONAL_PHONE:
                                tempStu.setPhoneNumber(value);
                                break;
                            case TAG_PERSONAL_ADDRESS:
                                tempStu.setAddress(value);
                                break;
                            case TAG_PERSONAL_EMERGENCY:
                                break;
                            case TAG_MEDICAL_INSURANCE_PROVIDER:
                                tempStu.getProfile().setMedicalInsuranceProvider(value);
                                break;
                            case TAG_MEDICAL_INSURANCE_NUMBER:
                                tempStu.getProfile().setMedicalInsuranceMemberNumber(value);
                                break;
                            case TAG_MEDICAL_EXISTING_CONDITIONS:
                                tempStu.getProfile().setMedicalExistingConditions(value);
                                break;
                            case TAG_MEDICAL_HISTORY:
                                tempStu.getProfile().setMedicalHistory(value);
                                break;
                            case TAG_MEDICAL_ALLERGIES:
                                tempStu.getProfile().setMedicalAllergies(value);
                                break;
                            case TAG_MEDICAL_CURRENT_MEDICATION:
                                tempStu.getProfile().setMedicalCurrentMedication(value);
                                break;
                            case TAG_TRAVEL_INSURANCE_PROVIDER:
                                tempStu.getProfile().setTravelInsuranceProvider(value);
                                break;
                            case TAG_TRAVEL_MEMBER_NUMBER:
                                tempStu.getProfile().setTravelInsuranceMemberNumber(value);
                                break;
                            case TAG_TRAVEL_DRIVER_LICENSE:
                                tempStu.getProfile().setTravelDriverLicenseNumber(value);
                                break;
                            case TAG_TRAVEL_VISA:
                                break;
                        }
                        updateProfile();

                        break;
                    case SaveProfileDialog.TAG_QUIT:
                        LogUtil.v(App.TAG, "quit");
                        finish();
                        break;
                }
            }
        };
        saveProfileDialog.show();
    }


    private void init() {
        student = App.getApp().getStudent();
        initLayout();
    }

    private void initLayout() {
        int tag = getIntent().getIntExtra(EDIT_TAG, TAG_DEFAULT);
        tempStu = new Student(App.getApp().getStudent());

        switch (tag) {
            case TAG_EDU_STUDENT_ID:
                title = StringManager.getString(R.string.studentId);
                value = student.getProfile().getEducationStudentId();
                break;
            case TAG_EDU_COLLEGE:
                title = StringManager.getString(R.string.college);
                value = student.getProfile().getEducationInstitutionName();
                break;
            case TAG_EDU_CAMPUS:
                title = StringManager.getString(R.string.campus);
                value = student.getProfile().getEducationInstitutionCampusName();
                break;
            case TAG_EDU_PHONE:
                title = StringManager.getString(R.string.phone_num_edu);
                value = student.getProfile().getEducationInstitutionCampusPhoneNumber();
                break;
            case TAG_PERSONAL_CHINESENAME:
                title = StringManager.getString(R.string.chinese_name);
                value = student.getFullName();
                tempStu.setFullName(value);
                break;
            case TAG_PERSONAL_ENGLISHNAME:
                title = StringManager.getString(R.string.english_name);
                value = student.getPreferredName();
                tempStu.setPreferredName(value);
                break;
            case TAG_PERSONAL_DOB:
                title = StringManager.getString(R.string.dob);
                value = DateManager.getStringFromDate(student.getDob(), "dd-MM-yyyy");
                tempStu.setDob(student.getDob());
                break;
            case TAG_PERSONAL_PHONE:
                title = StringManager.getString(R.string.phone_num);
                value = student.getPhoneNumber();
                tempStu.setPhoneNumber(value);
                break;
            case TAG_PERSONAL_ADDRESS:
                title = StringManager.getString(R.string.address);
                value = student.getAddress();
                tempStu.setAddress(value);
                break;
            case TAG_PERSONAL_EMERGENCY:
                title = StringManager.getString(R.string.emergency_contact);
                break;
            case TAG_MEDICAL_INSURANCE_PROVIDER:
                title = StringManager.getString(R.string.insurance_provider);
                value = student.getProfile().getMedicalInsuranceProvider();
                break;
            case TAG_MEDICAL_INSURANCE_NUMBER:
                title = StringManager.getString(R.string.insurance_number);
                value = student.getProfile().getMedicalInsuranceMemberNumber();
                break;
            case TAG_MEDICAL_EXISTING_CONDITIONS:
                title = StringManager.getString(R.string.existing_conditions);
                value = student.getProfile().getMedicalExistingConditions();
                break;
            case TAG_MEDICAL_HISTORY:
                title = StringManager.getString(R.string.medical_history);
                value = student.getProfile().getMedicalHistory();
                break;
            case TAG_MEDICAL_ALLERGIES:
                title = StringManager.getString(R.string.allergies);
                value = student.getProfile().getMedicalAllergies();
                break;
            case TAG_MEDICAL_CURRENT_MEDICATION:
                title = StringManager.getString(R.string.current_medication);
                value = student.getProfile().getMedicalCurrentMedication();
                break;
            case TAG_TRAVEL_INSURANCE_PROVIDER:
                title = StringManager.getString(R.string.travel_insurance_provider);
                value = student.getProfile().getTravelInsuranceProvider();
                break;
            case TAG_TRAVEL_MEMBER_NUMBER:
                title = StringManager.getString(R.string.travel_insurance_member_number);
                value = student.getProfile().getTravelInsuranceMemberNumber();
                break;
            case TAG_TRAVEL_DRIVER_LICENSE:
                title = StringManager.getString(R.string.travel_driver_license_number);
                value = student.getProfile().getTravelDriverLicenseNumber();
                break;
            case TAG_TRAVEL_VISA:
                title = StringManager.getString(R.string.travel_visa);
                value = student.getProfile().getTravelVisaType();
                break;
        }
        textView_activityEdit_title.setText(title);
        editText_activityEdit_input.setText(value);
        editText_activityEdit_input.setSelection(editText_activityEdit_input.getText().toString().length());
        editText_activityEdit_input.setFocusable(true);
        KeyboardManager.showInputKeyboard();
    }


    public void updateProfile() {
        BaseTask baseTaskLogin = new BaseTask(new NetCallBack() {
            public ProcessingDialog processingDialog;

            @Override
            public void onPreCall(BaseTask baseTask) {
                processingDialog = new ProcessingDialog(activity);
                processingDialog.show();
                super.onPreCall(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("full_name", tempStu.getFullName());
                    jsonObject.put("preferred_name", tempStu.getPreferredName());
                    jsonObject.put("dob", DateManager.getStringFromDate(tempStu.getDob(), Constant.STRING_DAY_FORMAT2));
                    jsonObject.put("phone_number", tempStu.getPhoneNumber());
                    jsonObject.put("address", tempStu.getAddress());
                    jsonObject.put("primary_contact_name", tempStu.getProfile().getPrimaryContactName());
                    jsonObject.put("primary_contact_phone", tempStu.getProfile().getPrimaryContactPhone());
                    jsonObject.put("secondary_contact_name", tempStu.getProfile().getSecondaryContactName());
                    jsonObject.put("Secondary_contact_phone", tempStu.getProfile().getSecondaryContactPhone());
//                    jsonObject.put("education_student_id", tempStu.getProfile().getEducationStudentId());
//                    jsonObject.put("education_institution_name", tempStu.getProfile().getEducationInstitutionName());
//                    jsonObject.put("education_institution_campus_name", tempStu.getProfile().getEducationInstitutionCampusName());
//                    jsonObject.put("education_institution_campus_phone_number", tempStu.getProfile().getEducationInstitutionCampusPhoneNumber());
//                    jsonObject.put("education_institution_campus_address", tempStu.getProfile().getEducationInstitutionCampusAddress());
//                    jsonObject.put("medical_insurance_provider", tempStu.getProfile().getMedicalInsuranceProvider());
//                    jsonObject.put("medical_insurance_member_number", tempStu.getProfile().getMedicalInsuranceMemberNumber());
//                    jsonObject.put("medical_existing_conditions", tempStu.getProfile().getMedicalExistingConditions());
//                    jsonObject.put("medical_history", tempStu.getProfile().getMedicalHistory());
//                    jsonObject.put("medical_allergies", tempStu.getProfile().getMedicalAllergies());
//                    jsonObject.put("medical_current_medication", tempStu.getProfile().getMedicalCurrentMedication());
//                    jsonObject.put("travel_insurance_provider", tempStu.getProfile().getTravelInsuranceProvider());
//                    jsonObject.put("travel_insurance_member_number", tempStu.getProfile().getTravelInsuranceMemberNumber());
//                    jsonObject.put("travel_driver_license_number", tempStu.getProfile().getTravelDriverLicenseNumber());
//                    jsonObject.put("travel_visa_type", tempStu.getProfile().getTravelVisaType());
//                    jsonObject.put("travel_visa_start_date", DateManager.getStringFromDate(tempStu.getProfile().getTravelVisaStartDate(), dateFormat));
//                    jsonObject.put("travel_visa_end_date", DateManager.getStringFromDate(tempStu.getProfile().getTravelVisaEndDate(), dateFormat));
                    netResult = NetInterface.updateProfile(jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "net error:" + e.toString());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                processingDialog.dismiss();
                if (result.isOk()) {
                    LogUtil.i(App.TAG, "login success!!!!!!:" + result.getMessage());
                    App.getApp().saveStudent(tempStu);
                    finish();
                } else {
//                    Toast.makeText(activity, StringManager.getString(R.string.login_error), Toast.LENGTH_SHORT).show();
                }

                super.onFinish(result, baseTask);
            }
        }, activity);
        HashMap<String, String> hmap = new HashMap<>();
        baseTaskLogin.execute(hmap);
    }

    @Override
    public void onBackPressed() {
        save();
    }
}
