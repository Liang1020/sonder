package com.sonder.android.activity;

import android.animation.ValueAnimator;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.DownloadTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnLoadCompleteListener;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

import java.io.File;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PdfActivity extends SonderBaseActivity {


    PDFView pdfView;


    public static String filePath;

    private boolean isHideTopBar = true;
    private int headerHeight = 0;


    private File fileCurrent;


//    private static final boolean AUTO_HIDE = true;
//    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
//    private static final boolean TOGGLE_ON_CLICK = true;
//    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
//    private SystemUiHider mSystemUiHider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        ButterKnife.bind(this);
        initView();
    }


    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }


    public void initView() {
        pdfView = (PDFView) findViewById(R.id.pdfview);
        pdfView.setOrientation(LinearLayout.VERTICAL);
        pdfView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHideTopbar();
            }
        });


        String url = (String) App.getApp().getTempObject("url");
        LogUtil.i(App.TAG, "url:" + filePath);

        filePath = getCacheDir() + File.separator + System.currentTimeMillis() + ".pdf";

        LogUtil.i(App.TAG, "file path:" + filePath);

        DownloadTask downloadTask = new DownloadTask(this, url, filePath, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    loadContent();
                } else {
                    LogUtil.i(App.TAG, "net error pdf downlaod");
                }

            }
        });

        downloadTask.execute(new HashMap<String, String>());

    }


    private void loadContent() {


        OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
            @Override
            public void onPageChanged(int page, int pageCount) {
            }
        };

        if (!StringUtils.isEmpty(filePath)) {

//            String fname = filePath.substring(filePath.lastIndexOf("/") + 1);
            fileCurrent = new File(filePath);

            pdfView.fromFile(fileCurrent)
                    .enableSwipe(true)
                    .showMinimap(false)
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(int nbPages) {
                            pdfView.postInvalidate();
                        }
                    })
                    .onPageChange(onPageChangeListener)
                    .load();

        }


    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        loadContent();

    }


    private void showHideTopbar() {


        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, headerHeight);
        if (isHideTopBar) {
            ValueAnimator animator = ValueAnimator.ofInt(0, -headerHeight);
            animator.setDuration(500);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    lp.topMargin = (int) animation.getAnimatedValue();
                }
            });
            animator.start();
            isHideTopBar = false;
            setFullScreen();
        } else {
            ValueAnimator animator = ValueAnimator.ofInt(-headerHeight, 0);
            animator.setDuration(500);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    lp.topMargin = (int) animation.getAnimatedValue();
                }
            });
            animator.start();
            isHideTopBar = true;
            cancelFullScreen();
        }

    }

    /**
     * 设置为全屏显示
     */
    private void setFullScreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * 退出全屏显示
     */
    private void cancelFullScreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
