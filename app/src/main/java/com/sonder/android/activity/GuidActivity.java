package com.sonder.android.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;

import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.GuildPagerAdapter;
import com.sonder.android.base.SonderBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuidActivity extends SonderBaseActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_guid);
        ButterKnife.bind(this);


        boolean fromSetrting = false;

        try {
            fromSetrting = (Boolean) App.getApp().getTempObject("isFromSetting");
        } catch (Exception e) {
        }


        GuildPagerAdapter guildPagerAdapter = new GuildPagerAdapter(this, fromSetrting);

        guildPagerAdapter.setOnSkipOrStartClicKListener(new GuildPagerAdapter.OnSkipOrStartClicKListener() {
            @Override
            public void onClickSkip() {
                SharePersistent.saveBoolean(GuidActivity.this, "see_guild", true);
                Tool.startActivity(GuidActivity.this, LoginActivity.class);
                finish();
            }
        });


        viewPager.setAdapter(guildPagerAdapter);

//        indicatorView.setIndicatorSize(5, R.drawable.icon_dot_uncheck, R.drawable.icon_dot_checnk);
//        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
//            @Override
//            public void onPageSelected(int position) {
//                indicatorView.checkIndex(position);
//            }
//        });

    }
}
