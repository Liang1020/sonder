package com.sonder.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.onesignal.OneSignal;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.Student;
import com.sonder.android.net.NetInterface;
import com.sonder.android.service.UpdateLocationService;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends SonderBaseActivity {
    private Activity activity;

    @BindView(R.id.button_activityLogin_login)
    public Button button_activity_login_login;

    @BindView(R.id.editText_activityLogin_email)
    public EditText editTextEemail;

    @BindView(R.id.editText_activityLogin_password)
    public EditText editTextPassword;

    private short SHORT_GO_MAIN = 100;
    private short SHORT_GO_GUST = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        App.getApp().addActivity(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initView();
    }


    @OnClick(R.id.textViewTc)
    public void onClicktc() {
        Tool.startUrl(this, getString(R.string.tc_link));
    }

    @OnClick(R.id.button_activityLogin_login)
    public void login() {
        login(getInput(editTextEemail), getInput(editTextPassword));
    }


    long mPressedTime = 0;

    @Override
    public void onBackPressed() {

        long mNowTime = System.currentTimeMillis();
        if ((mNowTime - mPressedTime) > 2000) {
            Toast.makeText(this, getResources().getString(R.string.press_exit_app), Toast.LENGTH_SHORT).show();
            mPressedTime = mNowTime;
        } else {
            App.getApp().exit();
        }


    }


    BaseTask baseTaskLogin;

    public void login(final String username, final String password) {


        BaseTask.resetTastk(baseTaskLogin);

        baseTaskLogin = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", username.trim());
                    jsonObject.put("password", password);
                    netResult = NetInterface.login(jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "net error:" + e.toString());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();


                if (null != result) {
                    if (result.isOk()) {

                        App.getApp().savePassword(username, password);
                        App.getApp().saveApiToken((String) result.getData()[0]);


                        Student student = (Student) result.getData()[1];
                        String stuDentId = String.valueOf(student.getStudentId());


                        OneSignal.sendTag("userId", stuDentId);

                        LogUtil.e(App.TAG, "==========userId:" + stuDentId);

                        App.getApp().saveStudent(student);

                        LogUtil.v(App.TAG, "token: " + App.getApp().getApiToken());

                        if (0 == App.getApp().getStudent().getPlanLevel()) {

                            Tool.showMessageDialog(R.string.gust_time_expired, LoginActivity.this, new DialogCallBackListener() {
                                @Override
                                public void onDone(boolean yesOrNo) {

                                    App.getApp().putTemPObject("isPlan", true);
                                    Tool.startActivityForResult(activity, GustActivity.class, SHORT_GO_GUST);
                                }
                            });

                        } else {

                            Tool.startActivityForResult(activity, MainActivity.class, SHORT_GO_MAIN);

                            UpdateLocationService.startUpdateLocation(LoginActivity.this);

                        }

//                      activity.finish();
                    } else {

                        if ("401".equals(result.getCode())) {
                            Tool.showMessageDialog(R.string.login_error_401, LoginActivity.this);
                        } else {
                            Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
                        }
                    }


                } else {

                    Tool.showMessageDialog(R.string.error_net, LoginActivity.this);
                }


            }
        }, activity);
        HashMap<String, String> hmap = new HashMap<>();
        baseTaskLogin.execute(hmap);
    }


    @OnClick(R.id.textView_activityLogin_forgetPassword)
    public void forgetPassword() {


        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        intent.putExtra("email", getInput(editTextEemail));
        startActivity(intent);


    }

    @OnClick(R.id.textView_activityLogin_guest)
    public void visitAsGuest() {
//        startActivity(new Intent(this, MainActivity.class));
        Tool.startActivityForResult(this, GustActivity.class, SHORT_GO_GUST);

    }


    public void initView() {
        String loginInfo[] = App.getApp().getPassword();
        String username = loginInfo[0];
        String password = loginInfo[1];
        if (username != null && password != null) {
            editTextEemail.setText(username);
            editTextPassword.setText(password);
            login(username, password);
        }


        if (SharePersistent.getBoolean(this, App.KEY_FIST_USE, false) == false) {

            SharePersistent.saveBoolean(this, App.KEY_TOUCH_ID, true);

            SharePersistent.saveBoolean(this, App.KEY_FIST_USE, true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_MAIN && resultCode == RESULT_CANCELED) {
            editTextPassword.setText("");
        }

    }
}

