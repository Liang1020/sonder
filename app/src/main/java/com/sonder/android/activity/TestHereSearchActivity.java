package com.sonder.android.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.here.android.mpa.search.DiscoveryResult;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.SearchRequest;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestHereSearchActivity extends SonderBaseActivity {


    @BindView(R.id.button2)
    Button button2;

    @BindView(R.id.editText)
    EditText editText;

    @BindView(R.id.textView)
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_here_search);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.button2)
    public void onClickr2() {

        SearchRequest searchRequest = new SearchRequest(getInput(editText));
        searchRequest.execute(new ResultListener<DiscoveryResultPage>() {


            @Override
            public void onCompleted(DiscoveryResultPage discoveryResultPage, ErrorCode errorCode) {
                if (errorCode != ErrorCode.NONE) {
                    // Handle error
                } else {
                    // Process result data
                    List<DiscoveryResult> items = discoveryResultPage.getItems();
                    textView.setText("");
                    for (DiscoveryResult dr : items) {

                        textView.append(dr.getTitle() + ":" + dr.getVicinity() + ":" + dr.getResultType() + ":" + dr.getIconUrl() + "\n");
                    }

                }

            }
        });


    }


}
