package com.sonder.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BasePage;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.page.PageRequestDetail;
import com.sonder.android.activity.page.PageRequestList;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.CustomDialog;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.net.NetInterface;
import com.sonder.android.utils.IncidentIdFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by wangzy on 2016/11/9.
 */

public class SupportDetailActivity extends SonderBaseActivity {


    @BindView(R.id.segmentedGroup)
    info.hoang8f.android.segmented.SegmentedGroup segmentedGroup;

    @BindView(R.id.relativelayoutContainer)
    RelativeLayout relativelayoutContainer;


    View viewRequestDetail;
    PageRequestDetail pageRequestDetail;


    View viewRequestList;
    PageRequestList pageRequestList;

    CustomDialog customDialog;


    JSONObject jsonObjectIncident;

    BasePage currentpage;

    String incidentId = "";


    private short SHORT_GO_RATING = 100;


    @BindView(R.id.textViewCanceRequest)
    TextView textViewCanceRequest;

    @BindView(R.id.textViewCommit)
    TextView textViewCommit;


    @BindView(R.id.textView_activityEdit_title)
    TextView textView_activityEdit_title;

    private boolean needBackResultOk = false;
    private boolean fromApplay = false;
    private static int currentIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_detail);
        ButterKnife.bind(this);
        jsonObjectIncident = (JSONObject) App.getApp().getTempObject(KEY_INCIDENT);

        try {
            fromApplay = (Boolean) App.getApp().getTempObject("fromApply");
        } catch (Exception e) {
        }

        currentIndex = -1;

        initView();

    }

    @Override
    public void initView() {


        if (null != jsonObjectIncident) {

            String status = JsonHelper.getStringFromJson("status", jsonObjectIncident);
            if ("2".equals(status)) {
                textViewCanceRequest.setVisibility(View.INVISIBLE);

                if (StringUtils.isEmpty(JsonHelper.getStringFromJson("rating_time", jsonObjectIncident)) || "null".equalsIgnoreCase(JsonHelper.getStringFromJson("rating_time", jsonObjectIncident))) {
                    textViewCommit.setVisibility(View.VISIBLE);
                }
            }

            String priority = JsonHelper.getStringFromJson("priority", jsonObjectIncident);
            if ("2".equals(priority)) {
                textViewCanceRequest.setVisibility(View.INVISIBLE);
            }

            textView_activityEdit_title.setText(IncidentIdFormat.formatINcidents(JsonHelper.getStringFromJson("incident_id", jsonObjectIncident)));
        }


        pageRequestDetail = new PageRequestDetail(this, jsonObjectIncident);

        try {
            if (null != jsonObjectIncident && null != jsonObjectIncident.getJSONArray("posts")) {
                incidentId = jsonObjectIncident.getString("incident_id");
            }
        } catch (Exception e) {
        }

        pageRequestList = new PageRequestList(this, jsonObjectIncident);

        viewRequestList = pageRequestList.getRootView();
        viewRequestDetail = pageRequestDetail.getRootView();


        RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        relativelayoutContainer.removeAllViews();

        relativelayoutContainer.addView(viewRequestDetail, lpp);
        relativelayoutContainer.addView(viewRequestList, lpp);


        if (-1 == currentIndex) {
            segmentedGroup.check(R.id.radioButtonDetail);
            showDetail();
        } else {
            segmentedGroup.check(currentIndex);
            if (currentIndex == R.id.radioButtonDetail) {
                showDetail();
            } else {
                showList();
            }


        }


        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                currentIndex = checkedId;

                 refreshSupportDetails();

                if (checkedId == R.id.radioButtonDetail) {
                    showDetail();
                } else {
                    showList();
                }

            }
        });

    }


    /**
     * refresh list
     */
    public void refreshSupportDetails() {


        requestIncident(incidentId, false, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && result.isOk()) {
                    jsonObjectIncident = (JSONObject) result.getData()[0];

                    if (null != pageRequestDetail) {
                        pageRequestDetail.fillData(jsonObjectIncident);
                    }
                    if(null!=pageRequestList){
                        pageRequestList.fillData(jsonObjectIncident);
                    }

//                    initView();
                }
            }
        });


    }


    @OnClick(R.id.textViewCommit)
    public void onCLickCommit() {
        App.getApp().putTemPObject("incidentId", incidentId);
        Tool.startActivityForResult(SupportDetailActivity.this, RateActivity.class, SHORT_GO_RATING);
    }


    @OnClick(R.id.textViewCanceRequest)
    public void onCancelRequest() {

        if (null == customDialog) {
            customDialog = new CustomDialog(this, getResources().getString(R.string.dialog_request_title),

                    getResources().getString(R.string.dialog_request_content),
                    getString(R.string.dialog_close_incident_yes),
                    getResources().getString(R.string.dialog_close_incident_no)
            );
        }

        customDialog.setDialogCallBackListener(new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {
                LogUtil.i(App.TAG, "cancel or abort:" + yesOrNo);
                if (yesOrNo) {
                    requestCloseIncident(incidentId);
                } else {
//                    finish();
                }

            }
        });
        customDialog.show();

    }

    @OnClick(R.id.imageViewback)
    public void onClickback() {

        if (!fromApplay) {
            setResult(RESULT_OK);
        }


        finish();
    }

    private void showDetail() {


        viewRequestDetail.setVisibility(View.VISIBLE);
        pageRequestDetail.onShow();

        viewRequestList.setVisibility(View.GONE);
        pageRequestList.onHide();

        currentpage = pageRequestDetail;
    }

    private void showList() {


        viewRequestDetail.setVisibility(View.GONE);
        pageRequestDetail.onHide();

        viewRequestList.setVisibility(View.VISIBLE);
        pageRequestList.onShow();
        currentpage = pageRequestList;
    }


    BaseTask baseTaskCloseIncient;

    private void requestCloseIncident(final String incidnetId) {
        BaseTask.resetTastk(baseTaskCloseIncient);
        baseTaskCloseIncient = new BaseTask(activity, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.closeIncident(incidnetId);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {

                        customDialog.removeOlderDialogView();
                        String title = activity.getResources().getString(R.string.apply_dialog_done_title);
                        String content = activity.getResources().getString(R.string.apply_dialog_done_desc);
                        String yes = activity.getResources().getString(R.string.apply_dialog_done_yes);
                        String no = activity.getResources().getString(R.string.apply_dialog_done_no);

                        textViewCommit.setVisibility(View.VISIBLE);
                        textViewCanceRequest.setVisibility(View.INVISIBLE);

                        customDialog = new CustomDialog((SonderBaseActivity) activity, title, content, yes, no);
                        customDialog.setDialogCallBackListener(new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {

                                LogUtil.i(App.TAG, "yes or no:" + yesOrNo);
                                if (yesOrNo) {
                                    App.getApp().putTemPObject("incidentId", incidnetId);
                                    Tool.startActivityForResult(SupportDetailActivity.this, RateActivity.class, SHORT_GO_RATING);
                                } else {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }
                        });

                        customDialog.show();


                    } else {
                        Tool.showMessageDialog(result.getMessage(), SupportDetailActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, SupportDetailActivity.this);
                }

            }
        });


        baseTaskCloseIncient.execute(new HashMap<String, String>());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (null != currentpage) {
            currentpage.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_RATING && resultCode == RESULT_OK) {
            textViewCommit.setVisibility(View.GONE);
            needBackResultOk = true;
            setResult(RESULT_OK);
            finish();

        }


        if (requestCode == SHORT_GO_RATING && (currentpage instanceof PageRequestList)) {
            pageRequestList.needRefresh = true;
            currentpage.onShow();
            return;
        }


        if (null != currentpage) {
            currentpage.onActivityResult(requestCode, resultCode, data);
        }


    }
}
