package com.sonder.android.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.Angel;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.net.NetInterface;
import com.sonder.android.service.AlarmIntentServce;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckInActivity extends SonderBaseActivity {


    OptionsPickerView optionsPickerViewDuration, optionsPickerViewRemind;


    @BindView(R.id.textViewDuration)
    TextView textViewDuration;

    @BindView(R.id.textViewRemind)
    TextView textViewRemind;

    @BindView(R.id.textViewLocation)
    TextView textViewLocation;

    @BindView(R.id.imageViewArrow1)
    ImageView imageViewArrow1;

    @BindView(R.id.imageViewArrow2)
    ImageView imageViewArrow2;


    @BindView(R.id.buttonCheckIn)
    Button buttonCheckIn;

    private String duration = "";
    private String remindFrq = "";

    private boolean isSuccess = false;

    Animation animationShake;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        ButterKnife.bind(this);
        animationShake = AnimationUtils.loadAnimation(this, R.anim.shake);

    }

    @OnClick(R.id.relatveLayoutCheckDuration)
    public void checkDuration() {
        showDurationDialog();

    }

    @OnClick(R.id.textViewLocation)
    public void onClickLocation() {
        Tool.startActivityForResult(this, AddressHereActivity.class, SELECT_ADDRESS_CHECK);
    }

    @OnClick(R.id.buttonCheckIn)
    public void checkIn() {

        if (checkValue() == false) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (Settings.canDrawOverlays(this)) {

                requestCheckIn();
            } else {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivityForResult(intent, GO_OVERLAY_REQUEST);
            }

        } else {
            requestCheckIn();
        }

    }

    private void requestCheckIn() {


        JSONObject jso = new JSONObject();
        SimpleAddrss sa = (SimpleAddrss) textViewLocation.getTag();

        try {

            jso.put("address", sa.getAddress());
            jso.put("location_lon", sa.getLat());
            jso.put("location_lat", sa.getLng());
            jso.put("duration", duration);
            jso.put("notification_every", remindFrq);
            jso.put("check_in_time", Constant.SDF.format(new Date()));

            requestCheckIn(jso);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private boolean checkValue() {

        if (null == textViewLocation.getTag()) {
            textViewLocation.startAnimation(animationShake);
            return false;
        }
        if (null == textViewDuration.getTag()) {
            findViewById(R.id.relatveLayoutCheckDuration).startAnimation(animationShake);
            return false;
        }

        if (null == textViewRemind.getTag()) {
            findViewById(R.id.relatveLayoutRemind).startAnimation(animationShake);
            return false;
        }


        int duration = Integer.parseInt((String) textViewDuration.getTag());
        int remind = Integer.parseInt((String) textViewRemind.getTag());
        if (remind > duration) {
            Tool.ToastShow(CheckInActivity.this, R.string.duration_biger_than_remind);
            return false;
        }

        return true;
    }


    BaseTask baseTaskCheckIn;

    private void requestCheckIn(final JSONObject jso) {
        BaseTask.resetTastk(baseTaskCheckIn);
        baseTaskCheckIn = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.checkIn(jso);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "exception:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {
                        isSuccess = true;
                        try {
//
//                            String location = getInput(textViewLocation);
//                            String duration = getInput(textViewDuration);
//                            String remind = getInput(textViewRemind);
//
//                            JSONObject detail = new JSONObject();
//                            detail.put("location", location);
//                            detail.put("duration", duration);
//                            detail.put("remind", remind);

                            Angel angel = (Angel) result.getData()[0];

                            App.getApp().saveAngle(angel);
                            App.getApp().putTemPObject("angle", angel);

                            String remindTime = (String) textViewRemind.getTag();
                            if (!"0".equals(remindTime)) {
                                AlarmIntentServce.startService(CheckInActivity.this);
                            }
                            Tool.startActivityForResult(CheckInActivity.this, CheckInDetailActivity.class, GO_CHECK_DETAIL);

                        } catch (Exception e) {

                        }

                    } else {
                        Tool.showMessageDialog(result.getMessage(), CheckInActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, CheckInActivity.this);
                }

            }
        });

        baseTaskCheckIn.execute(new HashMap<String, String>());

    }


    public void setDisEnable() {

        imageViewArrow1.setVisibility(View.INVISIBLE);
        imageViewArrow2.setVisibility(View.INVISIBLE);
        buttonCheckIn.setBackgroundColor(Color.parseColor("#FE3824"));
        buttonCheckIn.setText(R.string.check_in_leave);

    }


    private void showDurationDialog() {

        final String[] arys = getResources().getStringArray(R.array.check_durations);
        final String valudruation[] = getResources().getStringArray(R.array.check_durations_val);

        if (null == optionsPickerViewDuration) {
            optionsPickerViewDuration = new OptionsPickerView<String>(this);
            optionsPickerViewDuration.setTitle(getResources().getString(R.string.check_in_duration_title));

            ArrayList<String> options = new ArrayList();
            for (String str : arys) {
                options.add(str);
            }
            optionsPickerViewDuration.setPicker(options);
            optionsPickerViewDuration.setCyclic(false);
        }


        if (null != textViewDuration.getTag()) {
            try {
                optionsPickerViewDuration.setSelectOptions(Integer.parseInt((String) textViewDuration.getTag()));
            } catch (Exception e) {
            }
        }

        optionsPickerViewDuration.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                textViewDuration.setTag(String.valueOf(options1));
                textViewDuration.setText(arys[options1]);

                duration = valudruation[options1];

            }
        });

        optionsPickerViewDuration.show();
    }


    @OnClick(R.id.relatveLayoutRemind)
    public void onClickRemind() {
        showRemindDialog();

    }

    private void showRemindDialog() {

        final String[] arys = getResources().getStringArray(R.array.check_durations_remind);
        final String valueremind[] = getResources().getStringArray(R.array.check_durations_remind_val);

        if (null == optionsPickerViewRemind) {
            optionsPickerViewRemind = new OptionsPickerView<String>(this);
            optionsPickerViewRemind.setTitle(getResources().getString(R.string.check_in_freqence_title));

            ArrayList<String> options = new ArrayList();
            for (String str : arys) {
                options.add(str);
            }
            optionsPickerViewRemind.setPicker(options);
            optionsPickerViewRemind.setCyclic(false);
        }


        if (null != textViewRemind.getTag()) {
            try {
                optionsPickerViewRemind.setSelectOptions(Integer.parseInt((String) textViewDuration.getTag()));
            } catch (Exception e) {
            }
        }

        optionsPickerViewRemind.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {

                textViewRemind.setTag(String.valueOf(options1));
                textViewRemind.setText(arys[options1]);
                remindFrq = valueremind[options1];


            }
        });

        optionsPickerViewRemind.show();
    }

    @OnClick(R.id.imageViewBack)
    public void onClickBack() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_ADDRESS_CHECK && resultCode == RESULT_OK) {
            SimpleAddrss address = ((SimpleAddrss) App.getApp().getTempObject("address"));
            textViewLocation.setTag(address);
            textViewLocation.setText(address.getAddress());
            return;
        }

        if (requestCode == GO_CHECK_DETAIL) {
            setResult(resultCode);
            finish();
        }

        if (requestCode == GO_OVERLAY_REQUEST) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(App.getApp())) {
                    requestCheckIn();
                }
            }

        }

    }
}
