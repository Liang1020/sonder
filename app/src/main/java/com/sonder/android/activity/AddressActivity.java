package com.sonder.android.activity;

import android.location.Address;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressAdapter;
import com.sonder.android.base.SonderBaseActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by wangzy on 2016/11/7.
 */

public class AddressActivity extends SonderBaseActivity {


    @BindView(R.id.editTextInput)
    EditText editTextInput;

    @BindView(R.id.listView)
    ListView listView;

    private List<Address> arrayListAddress;

    AddressAdapter addressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
    }

    @OnTextChanged(R.id.editTextInput)
    public void onInchagneChagned() {
        String input = getInput(editTextInput);
        if (StringUtils.computStrlen(input) < 2) {
            return;
        }
        requestAddressList(input);
    }


    BaseTask baseTaskAddress;

    private void requestAddressList(final String input) {
        BaseTask.resetTastk(baseTaskAddress);

        baseTaskAddress = new BaseTask(this, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    arrayListAddress = Tool.getLocationFromAddress(baseTask.activity, input, 10, Locale.getDefault());
                    netResult = new NetResult();
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result) {

                    addressAdapter = new AddressAdapter(AddressActivity.this, arrayListAddress);
                    listView.setAdapter(addressAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ((AddressAdapter) parent.getAdapter()).select(position);
                        }
                    });


                }
            }
        });

        baseTaskAddress.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.textViewSave)
    public void onClickSave() {


        if (null == addressAdapter || null == addressAdapter.getSelectAddrss()) {

            Tool.ToastShow(this, R.string.support_address);

            return;
        }


        String title = getResources().getString(R.string.coomon_confirm_ask);
        String yes = getResources().getString(R.string.coomon_confirm_ask_yes);
        String no = getResources().getString(R.string.coomon_confirm_ask_no);


        DialogUtils.showConfirmDialog(this, "", title, yes, no, new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {

                if (yesOrNo) {

                    App.getApp().putTemPObject("address", addressAdapter.getSelectAddrss());
                    setResult(RESULT_OK);

                } else {


                }
                finish();
            }
        });


    }


    @OnClick(R.id.imageViewBack)
    public void onClick() {
        finish();
    }


    @OnClick(R.id.textViewSave)
    public void onSaveClick() {


    }

}
