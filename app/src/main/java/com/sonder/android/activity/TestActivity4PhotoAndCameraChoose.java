package com.sonder.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.sonder.android.App;
import com.sonder.android.R;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestActivity4PhotoAndCameraChoose extends Activity {


    @BindView(R.id.imageViewPicture)
    ImageView imageViewPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.textViewAddPhoto)
    public void onClickAddPhoto() {


        PictureChoseActivity.gotoSelectPicture(this, 1, "这里是错误提示，当你在照片选择界面上选择的照片数量超过指定个数数会tost这段", -1, -1);

    }

    @OnClick(R.id.textViewTake)
    public void onTakePhoto() {
        PictureChoseActivity.gotoTakePicture(this, 400, 400);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PictureChoseActivity.SHORT_REQUEST_MEDIAS && resultCode == RESULT_OK) {

            ArrayList<String> datas = data.getStringArrayListExtra(PictureChoseActivity.keyResult);
            String url = datas.get(0);
            imageViewPicture.setImageURI(Uri.parse(url));

            LogUtil.i(App.TAG, "url:" + url);
            LogUtil.i(App.TAG, "rorate img:" + PictureChoseActivity.getBitmapDegree(url));

            byte[] bytes = new byte[0];
            try {
                bytes = FileUtils.readFile2Byte(url);
                LogUtil.i(App.TAG, "byte size:" + bytes.length);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
