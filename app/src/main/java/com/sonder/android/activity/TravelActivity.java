package com.sonder.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.EditDialog;
import com.sonder.android.domain.Student;
import com.sonder.android.utils.PhotoLibUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/1.
 */

public class TravelActivity extends SonderBaseActivity {

    private Student student;

    @BindView(R.id.textViewInsuranceProvider)
    TextView textViewInsuranceProvider;

    @BindView(R.id.textViewInsuranceMemberNumber)
    TextView textViewInsuranceMemberNumber;

    @BindView(R.id.textViewDriverLicenseNumber)
    TextView textViewDriverLicenseNumber;

    @BindView(R.id.textViewTravelVisa)
    TextView textView_activityTravel_visa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {
        student = App.getApp().getStudent();

        if (null != student && null != student.getProfile()) {

            textViewInsuranceProvider.setText(student.getProfile().getTravelInsuranceProvider());
            textViewInsuranceMemberNumber.setText(student.getProfile().getTravelInsuranceMemberNumber());
            textViewDriverLicenseNumber.setText(student.getProfile().getTravelDriverLicenseNumber());
            textView_activityTravel_visa.setText(student.getProfile().getTravelVisaType());
        }

    }

    @OnClick(R.id.imageViewBack)
    public void onimageViewBack() {
        finish();
    }

    @OnClick(R.id.relativeLayout_activityTravel_insuranceProvider)
    public void editInsuranceProvider() {
//        startEditActivity(EditActivity.TAG_TRAVEL_INSURANCE_PROVIDER);

        showUpdateDialog(getInputFromId(R.id.textViewInsuranceProviderTitle), "travel_insurance_provider", getInput(textViewInsuranceProvider), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                if (isSuccess) {
                    initView();
                }
            }
        }, EditorInfo.TYPE_CLASS_TEXT);

    }

    @OnClick(R.id.relativeLayout_activityTravel_insuranceMemberNumber)
    public void editMemberNumber() {
//        startEditActivity(EditActivity.TAG_TRAVEL_MEMBER_NUMBER);

        showUpdateDialog(getInputFromId(R.id.textViewInsuranceMemberNumberTitle), "travel_insurance_member_number", getInput(textViewInsuranceMemberNumber), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                if (isSuccess) {
                    initView();
                }
            }
        }, EditorInfo.TYPE_CLASS_TEXT);

    }

    @OnClick(R.id.relativeLayout_activityTravel_driverLicenseNumber)
    public void editDirverLicense() {
//        startEditActivity(EditActivity.TAG_TRAVEL_DRIVER_LICENSE);

        showUpdateDialog(getInputFromId(R.id.textViewDriverLicenseNumberTitle), "travel_driver_license_number", getInput(textViewDriverLicenseNumber), App.getApp().getStudent(), new EditDialog.OnEditListener() {
            @Override
            public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {
                if (isSuccess) {
                    initView();
                }
            }
        }, EditorInfo.TYPE_CLASS_TEXT);
    }

    @OnClick(R.id.relativeLayout_activityTravel_visa)
    public void editVisa() {
//        startEditActivity(EditActivity.TAG_TRAVEL_VISA);
        Tool.startActivityForResult(this, VisaActivity.class, SHORT_GO_VISA);
    }


}


