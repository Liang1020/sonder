package com.sonder.android.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends SonderBaseActivity {


    @BindView(R.id.editText_activityForgetPassword_email)
    EditText editTextEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        String email = getIntent().getStringExtra("email");
        editTextEmail.setText(email);

    }

    @OnClick(R.id.textViewContactLink)
    public void clickContack(){
        Tool.startUrl(this,"https://sonderaustralia.com/contact-us/?lang=en");
    }


    @OnClick(R.id.button_activityForgetPassword_submit)
    public void onCLickSubmit() {

        String email = getInput(editTextEmail);
        if (!StringUtils.isEmpty(email)) {
            requestForgotPassowrd(email);
        } else {
            Tool.ToastShow(this, R.string.forget_pwd_email_empty);
        }
    }


    @OnClick(R.id.imageViewBack)
    public void submit() {
        finish();
    }

    BaseTask forgotPassword;

    private void requestForgotPassowrd(final String email) {
        BaseTask.resetTastk(forgotPassword);

        forgotPassword = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("email", email);

                    netResult = NetInterface.forgotPwd(jsonObject);

                } catch (Exception e) {

                    LogUtil.e(App.TAG, "rerquest forget password error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {

                    if (result.isOk()) {
                        Tool.showMessageDialog(R.string.forget_pwd_email_success, ForgetPasswordActivity.this,new DialogCallBackListener(){

                            @Override
                            public void onDone(boolean yesOrNo) {
                                finish();
                            }
                        });
                    } else {
                        Tool.showMessageDialog(result.getMessage(), ForgetPasswordActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, ForgetPasswordActivity.this);
                }
            }
        });

        forgotPassword.execute(new HashMap<String, String>());
    }

}
