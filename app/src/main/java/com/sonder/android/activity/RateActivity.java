package com.sonder.android.activity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RatingBar;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.NetInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RateActivity extends SonderBaseActivity {


    @BindView(R.id.ratignBarSS)
    RatingBar ratignBarSS;


    @BindView(R.id.ratignBarSQ)
    RatingBar ratignBarSQ;


    @BindView(R.id.ratignBarSA)
    RatingBar ratignBarSA;


    @BindView(R.id.editTextEditCommit)
    EditText editTextEditCommit;

    String incidentId;


    Animation animationShake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        ButterKnife.bind(this);
        incidentId = (String) App.getApp().getTempObject("incidentId");
        animationShake = AnimationUtils.loadAnimation(this, R.anim.shake);


    }


    @OnClick(R.id.buttonSend)
    public void onSendClick() {

        if (checkValue()) {

            float rateSS = ratignBarSS.getRating();
            float ratSQ = ratignBarSQ.getRating();
            float rateSA = ratignBarSA.getRating();

            JSONObject jo = new JSONObject();

            try {
                jo.put("incident_id", incidentId);
                jo.put("rating_result_score", rateSS);
                jo.put("rating_quality_score", ratSQ);
                jo.put("rating_attitude_score", rateSA);
                jo.put("rating_comment", getInput(editTextEditCommit));

                requestRate(jo);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    private boolean checkValue() {


        float rateSS = ratignBarSS.getRating();
        if (rateSS <= 0) {
            ratignBarSS.startAnimation(animationShake);
            return false;
        }

        float ratSQ = ratignBarSQ.getRating();
        if (ratSQ <= 0) {
            ratignBarSQ.startAnimation(animationShake);
            return false;
        }


        float rateSA = ratignBarSA.getRating();
        if (rateSA <= 0) {
            ratignBarSA.startAnimation(animationShake);
            return false;
        }


        return true;
    }


    BaseTask baseTaskRate;

    private void requestRate(final JSONObject jsonObject) {
        BaseTask.resetTastk(baseTaskRate);

        baseTaskRate = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.rateIncident(jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {

                        setResult(RESULT_OK);
                        finish();

                        LogUtil.e(App.TAG, "rate success:");
                    } else {
                        Tool.showMessageDialog(result.getMessage(), RateActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, RateActivity.this);
                }
            }


        });
        baseTaskRate.execute(new HashMap<String, String>());


    }


    @OnClick(R.id.imageViewBack)
    public void onBackClick() {
        finish();
    }
}
