package com.sonder.android.activity.page;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.common.adapter.TextWatcherAdapter;
import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BasePage;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.RequestStatusAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.RequestStatus;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.net.NetInterface;
import com.sonder.android.utils.PhotoLibUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by wangzy on 2016/11/10.
 */

public class PageRequestList extends BasePage {


    private ListView listView;
    private LinearLayout linearLayoutMeidaChoose;
    private ImageView imageViewCloseOnMedia;
    private EditText editTextSend;
    private TextView textViewSend;

    private int operateHeight;

    private boolean isFirst = true;
    private boolean isAnimate = false;
    private boolean isHide = true;
    private ImageView imageViewAdd;

    private JSONArray jsonArrayPosts;

    private String incidentId;
    RequestStatusAdapter requestStatusAdapter;

    private SonderBaseActivity sonderBaseActivity;

    private final int POST_TYPE_TEXT = 1;
    private final int POST_TYPE_IMG = 2;
    private final int POST_TYPE_PDF = 3;

    private String status = "";

    private View relativelayoutBottom;
    private View viewDivider;


    private JSONObject jsonObject;

    public PageRequestList(Activity activity, JSONArray jsonArrayPosts, String incidentId, String status, JSONObject jsonObject) {
        super(activity);
        this.rootView = View.inflate(activity, R.layout.page_request_list, null);
        this.jsonArrayPosts = jsonArrayPosts;
        this.incidentId = incidentId;
        this.sonderBaseActivity = (SonderBaseActivity) activity;
        this.status = status;
        this.jsonObject = jsonObject;

        initView();
    }

    public PageRequestList(Activity activity, JSONObject jsonObjectIncident) {
        super(activity);
        this.rootView = View.inflate(activity, R.layout.page_request_list, null);

        JSONArray jsonArray = new JSONArray();

        try {
            if (null != jsonObjectIncident && null != jsonObjectIncident.getJSONArray("posts")) {
                jsonArray = jsonObjectIncident.getJSONArray("posts");
                this.incidentId = jsonObjectIncident.getString("incident_id");
            }
        } catch (Exception e) {
        }


        this.jsonArrayPosts = jsonArray;
        this.sonderBaseActivity = (SonderBaseActivity) activity;
        this.status = JsonHelper.getStringFromJson("status", jsonObjectIncident);
        this.jsonObject = jsonObjectIncident;

        initView();
    }

    @Override
    public void initView() {


        this.relativelayoutBottom = findViewById(R.id.relativelayoutBottom);
        this.viewDivider = findViewById(R.id.viewDivider);

        if ("2".equals(status)) {
            this.relativelayoutBottom.setVisibility(View.GONE);
            this.viewDivider.setVisibility(View.GONE);
        }

        this.listView = (ListView) findListViewById(R.id.listView);
        this.linearLayoutMeidaChoose = findLinearLayout(R.id.linearLayoutMeidaChoose);
        this.imageViewCloseOnMedia = findImageViewById(R.id.imageViewClose);

        this.editTextSend = findEditTextById(R.id.editTextSend);

        this.textViewSend = findTextViewById(R.id.textViewSend);
        this.textViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTextPostCreate(getInput(editTextSend));
            }
        });

        this.editTextSend.addTextChangedListener(new TextWatcherAdapter() {

            @Override
            public void afterTextChanged(Editable s) {

                String input = sonderBaseActivity.getInput(editTextSend);
                if (StringUtils.isEmpty(input)) {

                    imageViewAdd.setVisibility(View.VISIBLE);
                    textViewSend.setVisibility(View.INVISIBLE);
                } else {
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    textViewSend.setVisibility(View.VISIBLE);
                }

            }
        });


        this.imageViewAdd = findImageViewById(R.id.imageViewAdd);
        this.imageViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showBottomLayout();
            }
        });


        this.linearLayoutMeidaChoose.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                operateHeight = linearLayoutMeidaChoose.getHeight();
                if (isFirst) {
                    isFirst = false;
//                    hideBottomInmidatly();
                    linearLayoutMeidaChoose.setVisibility(View.GONE);
                }
            }
        });

        this.imageViewCloseOnMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBotomLayout();
            }
        });


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    case R.id.linearlayoutPhotoChoose:
                        sonderBaseActivity.onChosePhtoClick(false);
                        break;
                    case R.id.linearlayoutCameraTake:
                        sonderBaseActivity.onTakePhtoClick(false);
                        break;
                    case R.id.linearlayoutFileChoose: {
                        checkPermissionsAndOpenFilePicker();
                    }
                    break;
                }

            }
        };


        findViewById(R.id.linearlayoutPhotoChoose).setOnClickListener(onClickListener);
        findViewById(R.id.linearlayoutCameraTake).setOnClickListener(onClickListener);
        findViewById(R.id.linearlayoutFileChoose).setOnClickListener(onClickListener);

        fillData(jsonObject);

    }


    public void fillData(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        String lo = "";

        try {
            lo = jsonObject.getJSONObject("lo_user").getString("name");
        } catch (Exception e) {
        }

        String ssc = "";
        try {
            ssc = jsonObject.getJSONObject("operator").getString("name");
        } catch (Exception e) {

        }

        try {
            if (null != jsonObject && null != jsonObject.getJSONArray("posts")) {
                jsonArrayPosts = jsonObject.getJSONArray("posts");
                this.incidentId = jsonObject.getString("incident_id");
            }
        } catch (Exception e) {
        }


        if (null == this.requestStatusAdapter) {
            this.requestStatusAdapter = new RequestStatusAdapter(activity, jsonArrayPosts, lo, ssc);
            this.listView.setAdapter(requestStatusAdapter);
        } else {
            this.requestStatusAdapter.reloadData(jsonArrayPosts);
        }


        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {

                    JSONObject jsonObject = jsonArrayPosts.getJSONObject(position);

                    LogUtil.i(App.TAG, "jsonobject:" + jsonObject.toString());

                    int type = getItemViewType(jsonObject);
                    switch (type) {
                        case RequestStatus.TYPE_PDF: {
                            String url = JsonHelper.getStringFromJson("attachment_url", jsonObject);
                            sonderBaseActivity.gotoPdfPreview(url);

                        }
                        break;

                        case RequestStatus.TYPE_IMG: {
                            String url = JsonHelper.getStringFromJson("attachment_url", jsonObject);
                            LogUtil.i(App.TAG, "preview url:" + url);
                            sonderBaseActivity.gotoPicturePreview(url);

                        }
                        break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        scrollBottom();
    }


    public void scrollBottom() {
        try {

//          istView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            this.listView.postDelayed(new Runnable() {
                @Override
                public void run() {

                    listView.setSelection(requestStatusAdapter.getCount() - 1);
                }
            }, 300);


        } catch (Exception e) {
            LogUtil.i(App.TAG, "error:" + e.getLocalizedMessage());
        }

    }


    public int getItemViewType(JSONObject jsonObject) {
//        "1 = Free Text , 2 = Image , 3 = PDF , 4 = LO Assigned"
        int ptype = 0;
        try {
            ptype = jsonObject.getInt("post_type");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ptype;
    }


    private void showBottomLayout() {

        if (isHide) {
            showBottomLayout(-operateHeight, 0);
            isHide = false;
        }

        imageViewAdd.setEnabled(false);
    }

    private void hideBotomLayout() {
        if (isHide == false) {
            showBottomLayout(0, -operateHeight);
            isHide = true;
            imageViewAdd.setEnabled(true);
        }

    }


    @Override
    public void onShow() {
        ((SonderBaseActivity) activity).requestIncident(incidentId, false, new NetCallBack() {

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result) {
                    if (result.isOk()) {
                        JSONObject jdata = (JSONObject) result.getData()[0];

                        try {
                            if (null != jdata && null != jdata.getJSONArray("posts") && jdata.getJSONArray("posts").length() > 0) {
                                jsonArrayPosts = jdata.getJSONArray("posts");

                                String lo = "";

                                try {
                                    lo = jsonObject.getJSONObject("lo_user").getString("name");
                                } catch (Exception e) {
                                }

                                String ssc = "";

                                try {
                                    ssc = jsonObject.getJSONObject("operator").getString("name");
                                } catch (Exception e) {

                                }
                                if (null != requestStatusAdapter) {
                                    requestStatusAdapter.reloadData(jsonArrayPosts);
                                } else {
                                    requestStatusAdapter = new RequestStatusAdapter(activity, jsonArrayPosts, lo, ssc);
                                    listView.setAdapter(requestStatusAdapter);
                                }
                                scrollBottom();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LogUtil.i(App.TAG, "errors:" + e.getLocalizedMessage());
                        }

                    } else {
                        Tool.showMessageDialog(result.getMessage(), activity);
                    }

                } else {

                }

            }
        });
    }


    public void openFilePicker() {
        new MaterialFilePicker()
                .withActivity(activity)
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .withFilter(Pattern.compile(".*\\.pdf$"))
                .start();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        hideBotomLayout();

        if (requestCode == PictureChoseActivity.SHORT_REQUEST_MEDIAS && resultCode == Activity.RESULT_OK) {
            ArrayList<String> fiels = data.getStringArrayListExtra(PictureChoseActivity.keyResult);
            onMediaSelect(fiels.get(0), POST_TYPE_IMG);
        }


        String mFileName;
        if (requestCode == sonderBaseActivity.SELECT_PIC && resultCode == Activity.RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                mFileName = PhotoLibUtils.getDataColumn(sonderBaseActivity, data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sonderBaseActivity.sharedpreferencesUtil.getImageTempNameString2());
                String path = sonderBaseActivity.sharedpreferencesUtil.getImageTempNameUri().getPath();
                onMediaSelect(path, POST_TYPE_IMG);
            }
        } else if (requestCode == sonderBaseActivity.SELECT_PIC_KITKAT && resultCode == Activity.RESULT_OK) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(sonderBaseActivity, data.getData());
                String newPath = sonderBaseActivity.sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                String path = sonderBaseActivity.sharedpreferencesUtil.getImageTempNameUri().getPath();
                onMediaSelect(path, POST_TYPE_IMG);
            }
        } else if (requestCode == sonderBaseActivity.TAKE_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            String path = sonderBaseActivity.sharedpreferencesUtil.getImageTempNameUri().getPath();
            onMediaSelect(path, POST_TYPE_IMG);

        } else if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            onMediaSelect(path, POST_TYPE_PDF);
        }

    }

    private void onMediaSelect(String path, int type) {

        LogUtil.i(App.TAG, "select media path:" + path);

        if (!StringUtils.isEmpty(path)) {
            File file = new File(path);
            if (file.exists()) {
                try {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("post_type", String.valueOf(type));
                    hashMap.put("incident_id", incidentId);

                    String fname = file.getName();

                    FormFile formFile = new FormFile("attachment", FileUtils.readFile2Byte(path), fname, FormFile.contentType);
                    requestPostCreate(hashMap, new FormFile[]{formFile});

                } catch (IOException e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "onmediaselect orror:" + e.getLocalizedMessage());
                }

            }
        }
    }


    private void sendTextPostCreate(String text) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("post_type", String.valueOf(1));
        hashMap.put("incident_id", incidentId);
        hashMap.put("content", text);
        requestPostCreate(hashMap, new FormFile[]{});


    }


    public void onPostSuccess(NetResult netResult) {

        hideBotomLayout();


        JSONObject jsonObject = (JSONObject) netResult.getData()[0];

        requestStatusAdapter.addPost(jsonObject);

        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(requestStatusAdapter.getCount() - 1);
            }
        });
    }


    BaseTask baseTaskPostCreate;

    private void requestPostCreate(final HashMap<String, String> map, final FormFile[] files) {
        BaseTask.resetTastk(baseTaskPostCreate);

        baseTaskPostCreate = new BaseTask(activity, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.postCreate(map, files);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestPostCreate:" + e.getLocalizedMessage());
                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        onPostSuccess(result);
                        editTextSend.setText("");

                    } else {
                        Tool.showMessageDialog(result.getMessage(), activity);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, activity);
                }
            }
        });

        baseTaskPostCreate.execute(new HashMap<String, String>());

    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openFilePicker();
            } else {
                showError();
            }
        }


        if (requestCode == SonderBaseActivity.SHORT_REQUEST_READ_EXTEAL && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            if (sonderBaseActivity.type_request_picture == sonderBaseActivity.TYPE_REQEST_PICTURE_SELECT) {
                sonderBaseActivity.onChosePhtoClick(false);
            } else {
                sonderBaseActivity.onTakePhtoClick(false);
            }
        }


    }

    private void showBottomLayout(int from, int to) {
        if (isAnimate) {
            return;
        }

        ValueAnimator animator = ValueAnimator.ofInt(from, to);
        animator.setDuration(300);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int v = (int) animation.getAnimatedValue();
                LinearLayout.LayoutParams lpp = (LinearLayout.LayoutParams) linearLayoutMeidaChoose.getLayoutParams();
                lpp.bottomMargin = v;
                linearLayoutMeidaChoose.setLayoutParams(lpp);
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                linearLayoutMeidaChoose.setVisibility(View.VISIBLE);
                isAnimate = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimate = false;
            }
        });

        animator.start();

    }

    private void hideBottomInmidatly() {
        LinearLayout.LayoutParams lpp = (LinearLayout.LayoutParams) linearLayoutMeidaChoose.getLayoutParams();
        lpp.bottomMargin = -operateHeight;
        linearLayoutMeidaChoose.setLayoutParams(lpp);
    }


}
