package com.sonder.android.activity;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.AddressAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.EditDialog;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Student;
import com.sonder.android.manager.DateManager;
import com.bigkoo.pickerview.TimePickerView;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.Tool;

import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/11/1.
 */

public class PersonalActivity extends SonderBaseActivity {

    private Student student;

    @BindView(R.id.textView_activityPersonal_chineseName)
    TextView textView_activityPersonal_chineseName;

    @BindView(R.id.textView_activityPersonal_englishName)
    TextView textView_activityPersonal_englishName;

    @BindView(R.id.textView_activityPersonal_dob)
    TextView textView_activityPersonal_dob;

    @BindView(R.id.textView_activityPersonal_phone)
    TextView textView_activityPersonal_phone;

    @BindView(R.id.textView_activityPersonal_email)
    TextView textView_activityPersonal_email;


    @BindView(R.id.textViewAddress)
    TextView textViewAddress;

    @BindView(R.id.relativeLayoutEmergencyContact)
    LinearLayout relativeLayoutEmergencyContact;

    @BindView(R.id.textViewEmergencyContact)
    TextView textViewEmergencyContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        ButterKnife.bind(this);
        initView();
    }


    @OnClick(R.id.relativeLayoutEmergencyContact)
    public void onClickEmerencyContact() {
        Tool.startActivityForResult(this, EmergcyContactActivity.class, SHORT_GO_EMERGCY_CONTACT);
    }


    @OnClick(R.id.relativelayoutAddress)
    public void onAddressCLick() {
        Tool.startActivityForResult(this, AddressHereActivity.class, SHORT_GO_ADDDRESS);

    }


    @OnClick(R.id.imageViewBack)
    public void onBackCLick() {
        finish();
    }


    @OnClick(R.id.relativeLayout_activityPersonal_chineseName)
    public void editChineseName() {


        showUpdateDialog(
                getInputFromId(R.id.textView_activityPersonal_chineseNameTitle),
                "full_name",
                getInputFromId(R.id.textView_activityPersonal_chineseName), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                        if (isSuccess) {
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);

//        startEditActivity(EditActivity.TAG_PERSONAL_CHINESENAME);

    }

    @OnClick(R.id.relativeLayout_activityPersonal_englishName)
    public void editEnglishName() {
//        startEditActivity(EditActivity.TAG_PERSONAL_ENGLISHNAME);

        showUpdateDialog(
                getInputFromId(R.id.textView_activityPersonal_collegeTitle),
                "preferred_name",
                getInputFromId(R.id.textView_activityPersonal_englishName), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                        if (isSuccess) {
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_TEXT);

    }

    @OnClick(R.id.relativeLayout_activityPersonal_dob)
    public void editDob() {
        Date date = new Date();
        try {
            String inputTime = getInput(textView_activityPersonal_dob);
            date = Constant.SDFD2.parse(inputTime);
        } catch (Exception e) {
        }

        showTimePicker(new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(final Date date) {

                HashMap<String, String> map = new HashMap<String, String>();
                map.put("dob", Constant.SDFD2.format(date));

                updateAll(new NetCallBack() {
                    @Override
                    public void onFinish(NetResult result, BaseTask baseTask) {
                        if (null != result) {
                            if (result.isOk()) {
                                textView_activityPersonal_dob.setText(Constant.SDFD2.format(date));
                                App.getApp().saveStudent((Student) result.getData()[0]);
                            } else {
                                Tool.showMessageDialog(result.getMessage(), PersonalActivity.this);
                            }

                        } else {
                            Tool.showMessageDialog(R.string.error_net, PersonalActivity.this);
                        }
                    }
                }, App.getApp().getStudent(), map);
//
//  showUpdateDialog();


            }
        }, getInputFromId(R.id.textView_activityPersonal_campusTitle), date);

//        startEditActivity(EditActivity.TAG_PERSONAL_DOB);

    }

    @OnClick(R.id.relativeLayout_activityPersonal_phone)
    public void editPhone() {
//        startEditActivity(EditActivity.TAG_PERSONAL_PHONE);

        showUpdateDialog(
                getInputFromId(R.id.textView_activityPersonal_phoneTitle),
                "phone_number",
                getInput(textView_activityPersonal_phone), App.getApp().getStudent(), new EditDialog.OnEditListener() {
                    @Override
                    public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult) {

                        if (isSuccess) {
                            initView();
                        }
                    }
                }, EditorInfo.TYPE_CLASS_PHONE);


    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void initView() {
        initLayout();
    }

    private void initLayout() {
        student = App.getApp().getStudent();

        textView_activityPersonal_chineseName.setText(student.getFullName());
        textView_activityPersonal_englishName.setText(student.getPreferredName());
        textView_activityPersonal_dob.setText(DateManager.getStringFromDate(student.getDob(), Constant.STRING_DAY_FORMAT2));
        textView_activityPersonal_phone.setText(student.getPhoneNumber());
        textView_activityPersonal_email.setText(student.getEmail());
        textViewAddress.setText(student.getAddress());


        if (null != student.getProfile()) {

            String ename = student.getProfile().getPrimaryContactName();
            String ephone = student.getProfile().getPrimaryContactPhone();
            textViewEmergencyContact.setText(ename + ":" + ephone);

        }

    }


    BaseTask baseTaskUpdatePersonInfo;

    private void requestUpdatepersonInfo() {
        BaseTask.resetTastk(baseTaskUpdatePersonInfo);

        baseTaskUpdatePersonInfo = new BaseTask(this, new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;


                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                super.onFinish(result, baseTask);
            }
        });

        baseTaskUpdatePersonInfo.execute(new HashMap<String, String>());

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_ADDDRESS && resultCode == RESULT_OK) {
            SimpleAddrss address = (SimpleAddrss) App.getApp().getTempObject("address");
            if (null != address) {
                textViewAddress.setText(address.getAddress());
            }
        } else if (requestCode == SHORT_GO_EMERGCY_CONTACT && RESULT_OK == resultCode) {

            final String name = (String) App.getApp().getTempObject("name");
            final String phone = (String) App.getApp().getTempObject("phone");


            HashMap<String, String> hmap = new HashMap<>();
            hmap.put("primary_contact_name", name);
            hmap.put("primary_contact_phone", phone);

            updateAll(new NetCallBack() {
                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    if (null != result) {
                        if (result.isOk()) {

                            textViewEmergencyContact.setText(name + "-" + phone);
                        } else {
                            Tool.showMessageDialog(result.getMessage(), PersonalActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog(R.string.error_net, PersonalActivity.this);
                    }
//                    textViewEmergencyContact.setText(name + ":" + phone);
                }
            }, App.getApp().getStudent(), hmap);

        }

    }


}
