package com.sonder.android.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.fragment.BaseFragment;
import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.CheckInActivity;
import com.sonder.android.activity.CheckInDetailActivity;
import com.sonder.android.activity.MainActivity;
import com.sonder.android.activity.SupportActivity;
import com.sonder.android.activity.WalkWithActivity;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.dialog.AngelDialog;
import com.sonder.android.dialog.CustomDialog;
import com.sonder.android.dialog.EmergentDialog;
import com.sonder.android.domain.Angel;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Walk;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.net.NetInterface;
import com.sonder.android.service.UpdateLocationService;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;


/**
 * Created by leocx on 2016/10/12.
 */

public class HomeFragment extends BaseFragment {

    private AngelDialog angleDialog;
    private EmergentDialog emergentDialog;
    private CustomDialog customDialog;


    private short SHORT_REQUEST_LOCATION_SETTING = 20;
    private short SHORT_DO_CALL = 39;
    private short SHORT_DO_APPLY = 40;
    private short SHORT_CHECK_IN = 41;
    private short GO_ANGLE_DETAIL = 42;
    private short GO_WALK = 43;


    @BindView(R.id.imageView_fragmentHome_alarm)
    public ImageView imageViewAlarm;

    @BindView(R.id.textView_fragmentHome_alarm)
    public TextView textViewAlarm;

    @BindView(R.id.viewClickAngle)
    public View viewClickAngle;

    @BindView(R.id.viewLeave)
    public View viewLeave;

    @BindView(R.id.imageViewWalkFash)
    ImageView imageViewWalkFash;

    private String phone = "";
    private SonderBaseActivity sonderBaseActivity;
    private boolean hasDistress = false;


    private BaseTask basetaskWaitCheckIn;
    private BaseTask baseTaskWalking;

    private Walk walkCurrent;
    private Angel angleCurrent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);
        this.sonderBaseActivity = (SonderBaseActivity) getActivity();
        this.phone = getString(R.string.ssc_phone);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        angleDialog = new AngelDialog(getActivity());
        emergentDialog = new EmergentDialog(getActivity());
//        textViewAlarm.setVisibility(View.GONE);
    }

    @Override
    public boolean onBackPressed() {
        if (null != customDialog && customDialog.isShowing()) {
            customDialog.dismiss();
            return true;
        }
        if (null != angleDialog && angleDialog.isShowing()) {
            angleDialog.dismiss();
            return true;
        }
        if (null != emergentDialog && emergentDialog.isShowing()) {
            emergentDialog.dismiss();
            return true;
        }

        return false;
    }

    @OnClick(R.id.viewClickApplyHelp)
    public void request() {

        if (Tool.isOpenGPS(sonderBaseActivity) == false || sonderBaseActivity.isLocationAccess() == false) {


            DialogUtils.showConfirmDialog(getActivity(), "", getString(R.string.location_notify),
                    getString(R.string.confirm),
                    getString(R.string.cancel), new DialogCallBackListener() {

                        @Override
                        public void onDone(boolean yesOrNo) {

                            if (yesOrNo) {
                                Tool.openGpsSettignWithResultCode(getActivity(), SHORT_REQUEST_LOCATION_SETTING);
                            }

                        }
                    });

        } else {
            Tool.startActivityForResult(getActivity(), SupportActivity.class, SHORT_DO_APPLY);
        }
    }


    @OnClick(R.id.viewLeave)
    public void onCLickLeave() {

    }

    @OnClick(R.id.viewClickAngle)
    public void angle() {
        angleDialog.show();
        angleDialog.setOnGuidDdialogClickListener(new AngelDialog.OnGuidDdialogClickListener() {
            @Override
            public void onClickCheck() {
                gotoCheckIn();
            }

            @Override
            public void onClickWorkWith() {
                gotoWalkWith(walkCurrent);
            }
        });
    }


    @OnClick(R.id.viewClickEhep)
    public void emergentRequest() {
        emergentDialog.setOnEmergenceDialogClickListener(new EmergentDialog.OnEmergenceDialogClickListener() {
            @Override
            public void onClickTypeSupport(int type) {

                emergentDialog.dismiss();

                switch (type) {

                    case EmergentDialog.TYPE_REQUEST_EMERGENCY: {

                        String title = getString(R.string.apply_dialog_emergcy_title);
                        String content = getString(R.string.apply_dialog_emergcy_content);
                        String yes = getString(R.string.apply_dialog_emergcy_yes);
                        String no = getString(R.string.apply_dialog_emergcy_no);

                        emergentDialog.removeOlderDialogView();

                        if (null == customDialog) {
                            customDialog = new CustomDialog((SonderBaseActivity) getActivity(), title, content, no, yes);
                        }
                        customDialog.setDialogCallBackListener(new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                customDialog.dismiss();

                                if (yesOrNo) {

                                    Tool.startActivity(getActivity(), SupportActivity.class);
                                } else {
//                                    doCall();

                                    HashMap<String, String> params = new HashMap<String, String>();


                                    params.put("priority", "1");
                                    params.put("summary", "Emergency request");
                                    params.put("description", "Emergency request");
                                    params.put("request_person", "1");

                                    sendAlarm(params);


                                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }
                        });
                        customDialog.show();


                    }
                    break;

                    case EmergentDialog.TYPE_REQUEST_PHONESSC: {
                        doCall();
                    }
                    break;

                    case EmergentDialog.TYPE_REQUEST_NEEDHELP: {
                        Tool.startActivityForResult(getActivity(), SupportActivity.class, SHORT_DO_APPLY);
                    }
                    break;

                }


            }
        });
        emergentDialog.show();
    }

    public void doCall() {

        phone = getString(R.string.ssc_phone_call);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intentPhone = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                startActivity(intentPhone);
                return;
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, SHORT_DO_CALL);
                return;
            }
        } else {
            Intent intentPhone = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            startActivity(intentPhone);
        }

    }

    private void gotoCheckIn() {
        Tool.startActivityForResult(getActivity(), CheckInActivity.class, SHORT_CHECK_IN);
    }


    private void gotoWalkWith(Walk walk) {
        App.getApp().putTemPObject("walk", walk);
        Tool.startActivityForResult(getActivity(), WalkWithActivity.class, GO_WALK);
    }


    private void requestCheckWalking() {
        BaseTask.resetTastk(baseTaskWalking);

        baseTaskWalking = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
//                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.checkWalk();
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "error walking check:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
//                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        walkCurrent = (Walk) result.getData()[0];
                        if (null != walkCurrent) {
                            imageViewWalkFash.setImageResource(R.drawable.anim_walk_flash);
                            AnimationDrawable ani = (AnimationDrawable) imageViewWalkFash.getDrawable();
                            ani.start();
                        } else {
                            imageViewWalkFash.setImageResource(R.drawable.selector_request_guider);

                        }
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, getActivity());
                }

            }
        });
        baseTaskWalking.execute(new HashMap<String, String>());
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SHORT_DO_CALL && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            doCall();
        } else if (requestCode == SHORT_ACCESS_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            App.getApp().startLocation();
            App.getApp().conenctionApiClient();
        }
    }

    @OnClick(R.id.viewLeave)
    public void onClickLeanve() {
        Angel angle = (Angel) viewLeave.getTag();
        if (null != angle) {
            App.getApp().putTemPObject("angle", angle);
            Tool.startActivityForResult(getActivity(), CheckInDetailActivity.class, GO_ANGLE_DETAIL);
        }
    }


    @OnLongClick(R.id.imageView_fragmentHome_alarm)
    public boolean alarm() {

        if (Tool.isOpenGPS(sonderBaseActivity) == false || sonderBaseActivity.isLocationAccess() == false) {

            DialogUtils.showConfirmDialog(getActivity(), "", getString(R.string.location_notify),
                    getString(R.string.confirm),
                    getString(R.string.cancel), new DialogCallBackListener() {

                        @Override
                        public void onDone(boolean yesOrNo) {

                            if (yesOrNo) {
                                Tool.openGpsSettignWithResultCode(getActivity(), SHORT_REQUEST_LOCATION_SETTING);
                            }

                        }
                    });


            return true;
        }

        if (null == App.getApp().getCurrenteSimpleAddrss() || StringUtils.isEmpty(App.getApp().getCurrenteSimpleAddrss().getAddress())) {
            Tool.showMessageDialog(R.string.location_not_location, getActivity(), new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                }
            });
            return true;
        }


        if (isAnimateCountDown) {
            try {
                AnimationDrawable anmd = (AnimationDrawable) imageViewAlarm.getDrawable();
                if (null != anmd) {
                    anmd.stop();
                    setAlarmNormal();
                }
            } catch (Exception e) {
                setAlarmNormal();
            }
            textViewAlarm.setText(R.string.long_click_for_sending_incident);
            isAnimateCountDown = false;
            return true;
        }

        if (hasDistress == false) {
            setAlarmCountdown();
            textViewAlarm.setText(R.string.long_click_for_cancel_incident);
        }


        return true;
    }

//    @OnClick(R.id.imageView_fragmentHome_alarm)
//    public void onClickAlarm() {
//
//        if (isAnimateCountDown) {
//
//            try {
//                AnimationDrawable anmd = (AnimationDrawable) imageViewAlarm.getDrawable();
//                if (null != anmd) {
//                    anmd.stop();
//                    setAlarmNormal();
//                }
//            }catch (Exception e){
//                setAlarmNormal();
//            }
//
//
//        }
//
//    }


    @Override
    public void onResume() {
        super.onResume();
        if (hasDistress) {
            setAlarmProcess();
        }

        onIndexEventTrigger();
    }


    private void setAlarmNormal() {

        vibrate();

        textViewAlarm.setText(R.string.long_click_for_sending_incident);
        imageViewAlarm.clearAnimation();
        imageViewAlarm.setImageResource(R.drawable.icon_alarm);
        handler.removeCallbacksAndMessages(null);


    }

    private void setAlarmProcess() {

        textViewAlarm.setText(R.string.sonder_active);

        imageViewAlarm.clearAnimation();
        imageViewAlarm.setImageResource(R.drawable.anim_process_ing);
        AnimationDrawable anmd = (AnimationDrawable) imageViewAlarm.getDrawable();
        anmd.setOneShot(false);
        anmd.start();

    }


    private boolean isAnimateCountDown = false;

    private void setAlarmCountdown() {


        imageViewAlarm.setImageResource(R.drawable.anim_cunt_down);
        AnimationDrawable anmd = (AnimationDrawable) imageViewAlarm.getDrawable();
        anmd.start();
        isAnimateCountDown = true;
        int duration = 0;
        for (int i = 0; i < anmd.getNumberOfFrames(); i++) {
            duration += anmd.getDuration(i);
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                isAnimateCountDown = false;

                setAlarmProcess();
                //send distress indident
                sendAlarm(null);

            }
        }, duration);

    }


    private void vibrate() {

        if (null != getActivity()) {

            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            long[] pattern = {0, 90};   // 停止 开启 停止 开启
            vibrator.vibrate(pattern, -1);
        }

    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
//        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }


    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

        }
    };


    //////////////////////////////////////////////////////////

    private void sendAlarm(final HashMap<String, String> keys) {

        if (Tool.isOpenGPS(sonderBaseActivity) == false || sonderBaseActivity.isLocationAccess() == false) {


            DialogUtils.showConfirmDialog(getActivity(), "", getString(R.string.location_notify),
                    getString(R.string.confirm),
                    getString(R.string.cancel), new DialogCallBackListener() {

                        @Override
                        public void onDone(boolean yesOrNo) {
                            if (yesOrNo) {
                                Tool.openGpsSettignWithResultCode(getActivity(), SHORT_REQUEST_LOCATION_SETTING);
                            }

                        }
                    });


            return;
        }

        if (null == App.getApp().getCurrenteSimpleAddrss() || StringUtils.isEmpty(App.getApp().getCurrenteSimpleAddrss().getAddress())) {
            Tool.showMessageDialog(R.string.location_not_location, getActivity(), new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                }
            });
            return;
        }

        setAlarmNormal();

        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("priority", "2");
        hashMap.put("summary", "Distress request");
        hashMap.put("description", " Distress request");
        hashMap.put("request_person", "1");

        if (null != keys) {
            hashMap.putAll(keys);
        }

        SimpleAddrss currentAddress = App.getApp().getCurrenteSimpleAddrss();

        hashMap.put("location_lon", currentAddress.getLng());
        hashMap.put("location_lat", currentAddress.getLat());
        hashMap.put("address", currentAddress.getAddress());


        ((SonderBaseActivity) getActivity()).requestCreateIncident(hashMap, new FormFile[]{}, true, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result) {
                    if (result.isOk()) {
                        ((MainActivity) sonderBaseActivity).setApplyNeedRefresh();
                        //send distress from red button
                        if (null == keys) {
                            setAlarmProcess();
                        }
//                        App.getApp().putTemPObject(SonderBaseActivity.KEY_INCIDENT, result.getData()[0]);
//                        Tool.startActivity(getActivity(), SupportDetailActivity.class);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), getActivity());
                        setAlarmNormal();
                    }
                } else {
                    setAlarmNormal();
                    Tool.showMessageDialog(R.string.error_net, getActivity());
                }


            }
        });

    }


    public void onIndexEventTrigger() {
        LogUtil.i(App.TAG, "request incident request:======");
        if (null == sonderBaseActivity) {
            sonderBaseActivity = (SonderBaseActivity) getActivity();
        }

        if (null != sonderBaseActivity) {

            if (!isAnimateCountDown) {

                sonderBaseActivity.requestIndex(false, new NetCallBack() {
                    @Override
                    public void onFinish(NetResult result, BaseTask baseTask) {
                        if (null == getActivity()) {
                            return;
                        }

                        if (null != result) {
                            if (result.isOk()) {

                                //===========request===============
//                            "has_request": "1=has ,0=hasn't",
//                            "has_distress": "1=has ,0=hasn't",
//                            "has_emergency": "1=has ,0=hasn't",
//                            "angel_flag": "0 nothing ,1=has angel ,2=has walk with me "

                                JSONObject jsonObject = (JSONObject) result.getData()[0];
                                String has_distress = JsonHelper.getStringFromJson("has_distress", jsonObject);
                                LogUtil.i(App.TAG, " index api call success:" + has_distress);

                                //=========distress========
                                if ("1".equalsIgnoreCase(has_distress)) {
                                    setAlarmProcess();
                                    hasDistress = true;
//                                    textViewAlarm.setVisibility(View.GONE);
                                    textViewAlarm.setText(R.string.sonder_active);

                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_DISTRESS_REQUEST, true);
                                } else {
                                    hasDistress = false;
                                    setAlarmNormal();
                                    textViewAlarm.setVisibility(View.VISIBLE);
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_DISTRESS_REQUEST, false);
                                }


                                String request = JsonHelper.getStringFromJson("has_request", jsonObject);
                                if ("1".equals(request)) {
                                    //10 minutes 10分钟更新一次
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_STAND_REQUEST, true);
                                } else {
                                    //// stop udpate
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_STAND_REQUEST, false);
                                }

                                String angel_flag = JsonHelper.getStringFromJson("angel_flag", jsonObject);
                                if (!"0".equals(angel_flag)) {//10 seconds update rate
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_GUARDIAN_REQUEST, true);
                                } else {
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_GUARDIAN_REQUEST, false);
                                }

                                String has_emergency = JsonHelper.getStringFromJson("has_emergency", jsonObject);
                                if (!"0".equals(has_emergency)) {//10 seconds update rate
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_EMERGENCY_REQUEST, true);
                                } else {
                                    UpdateLocationService.requestServiceWithActionAndParam(getActivity(), UpdateLocationService.ACTION_UPDTE_EMERGENCY_REQUEST, false);
                                }


                            }
                        }

                    }
                });

            }


            refreshGuardAndWalk();

        }

    }


    private void refreshGuardAndWalk() {
        if (null == walkCurrent && null == angleCurrent) {
            requestWaitCheckIn();
            requestCheckWalking();
        }

    }


    public void requestWaitCheckIn() {
        BaseTask.resetTastk(basetaskWaitCheckIn);

        basetaskWaitCheckIn = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.waittingCheckOut();
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "exception request wait check in:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result && result.isOk()) {

                    Object[] ret = result.getData();
                    if (null != ret) {

                        angleCurrent = (Angel) ret[0];
                        viewLeave.setVisibility(View.VISIBLE);
                        viewClickAngle.setVisibility(View.INVISIBLE);
                        viewLeave.setTag(angleCurrent);

                    } else {
                        viewLeave.setVisibility(View.INVISIBLE);
                        viewClickAngle.setVisibility(View.VISIBLE);
                    }
                } else {
                    viewLeave.setVisibility(View.INVISIBLE);
                    viewClickAngle.setVisibility(View.VISIBLE);
                }

            }
        });
        basetaskWaitCheckIn.execute(new HashMap<String, String>());

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_REQUEST_LOCATION_SETTING) {
            if (Tool.isOpenGPS(getActivity())) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        App.getApp().startLocation();
                        App.getApp().conenctionApiClient();

                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, SHORT_ACCESS_LOCATION);
                    }
                } else {
                    App.getApp().startLocation();
                    App.getApp().conenctionApiClient();
                }

            }

        } else if (requestCode == SHORT_DO_APPLY && null != sonderBaseActivity && resultCode == sonderBaseActivity.BACK_TO_REQUESTS) {
            ((MainActivity) sonderBaseActivity).checkApplys();
        } else if (requestCode == SHORT_CHECK_IN || requestCode == GO_ANGLE_DETAIL) {

            requestWaitCheckIn();
        } else if (requestCode == GO_WALK && resultCode == Activity.RESULT_OK) {

            walkCurrent = null;
            gotoCheckIn();
        }
    }
}
