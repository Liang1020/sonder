package com.sonder.android.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.fragment.BaseFragment;
import com.common.net.FormFile;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.learnncode.mediachooser.activity.PictureChoseActivity;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.EducationActivity;
import com.sonder.android.activity.MedicalActivity;
import com.sonder.android.activity.PersonalActivity;
import com.sonder.android.activity.TravelActivity;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.base.SonderNetCallback;
import com.sonder.android.dialog.PhotoDialog;
import com.sonder.android.domain.Student;
import com.sonder.android.manager.TextManager;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.net.NetInterface;
import com.sonder.android.view.CircleImageView;
import com.sonder.android.view.ProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by leocx on 2016/10/12.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.progressBarFinishRate)
    ProgressBar progressBarFinishRate;

    @BindView(R.id.textView_fragmentProfile_name)
    TextView textView_fragmentProfile_name;

    @BindView(R.id.textView_fragmentProfile_username)
    TextView textView_fragmentProfile_username;

    private Student student;

    @BindView(R.id.lienarLayoutHeaderCircle)
    LinearLayout lienarLayoutHeaderCircle;

    @BindView(R.id.imageViewHeader)
    CircleImageView imageViewHeader;

    SonderBaseActivity sonderBaseActivity;

    private short SHORT_REQUEST_PERMISSION = 13;

    @BindView(R.id.relativeLayoutCovers)
    RelativeLayout relativeLayoutCovers;

    @BindView(R.id.textViewFingerPrintStatus)
    TextView textViewFingerPrintStatus;


    FingerprintManagerCompat manager = null;
    CancellationSignal mCancellationSignal = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sonderBaseActivity = (SonderBaseActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_profile, null);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }


    private void initView() {
        student = App.getApp().getStudent();

        if (null != student) {

            progressBarFinishRate.setTextBeforeRate(getResources().getString(R.string.personal_info_complte));
            progressBarFinishRate.setTextAfterRate("%");
            progressBarFinishRate.setFilledColor(R.color.colorProgressBarFilled);
            progressBarFinishRate.setUnfilledColor(R.color.colorProgressBarUnfilled);
            progressBarFinishRate.setTextColor(R.color.colorWhite);
            progressBarFinishRate.setTextSize((int) TextManager.getSizeByDp(R.dimen.Text_Subheading));
            progressBarFinishRate.setRateFormat(" %.0f");


            lienarLayoutHeaderCircle.post(new Runnable() {
                @Override
                public void run() {
                    int ch = lienarLayoutHeaderCircle.getHeight();
                    LogUtil.e(App.TAG, "=======ch:" + ch);

                    if (!StringUtils.isEmpty(student.getAvatar())) {

                        Picasso.with(App.getApp()).load(student.getAvatar())
                                .config(Bitmap.Config.RGB_565)
                        .resize(ch, ch)
//                        .centerCrop()
                                .placeholder(R.drawable.icon_default_header)
                                .into(imageViewHeader);
                    }


                }
            });

            updateStudent();
        }

    }


    @OnTouch(R.id.relativeLayoutCovers)
    public boolean ontouchCover() {

        isLastBackPresed = true;
        hideCover();

        return true;
    }


    private boolean isLastBackPresed = false;

    @Override
    public boolean onBackPressed() {

        if (relativeLayoutCovers.getVisibility() == View.VISIBLE) {

            isLastBackPresed = true;

            hideCover();
            return true;
        }


        return false;

    }

    @OnClick(R.id.imageViewHeader)
    public void onClickHeaderImg() {
        LogUtil.e(App.TAG, "=============================");

        onSecurityTriger(false, new NetCallBack() {

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (result.isOk()) {

                    ((SonderBaseActivity) getActivity()).showPhotoDialog(new PhotoDialog.OnPhotoCallbackListener() {
                        @Override
                        public void chooseType(int chooseTypeClicked) {

                            LogUtil.i(App.TAG, "==onClickHeaderImg==");

                            switch (chooseTypeClicked) {

                                case 0: {
                                    ((SonderBaseActivity) getActivity()).onChosePhtoClick(true);
                                }
                                break;

                                case 1: {
                                    ((SonderBaseActivity) getActivity()).onTakePhtoClick(true);
                                }
                                break;
                            }


                        }
                    });

                }

            }
        });


    }


    private void showCoverWithMsg(int msg) {
        textViewFingerPrintStatus.setText(msg);
        relativeLayoutCovers.setVisibility(View.VISIBLE);
    }

    private void showCoverWithMsg(String msg) {
        relativeLayoutCovers.setVisibility(View.VISIBLE);
        if (relativeLayoutCovers.getVisibility() == View.VISIBLE) {
            textViewFingerPrintStatus.setText(msg);
        }

    }


    private void hideCover() {
        relativeLayoutCovers.setVisibility(View.GONE);
        if (null != mCancellationSignal) {
//            mCancellationSignal.cancel();
        }
    }


    public void onSecurityTriger(final boolean isUsePwd, final NetCallBack netCallBack) {

        if (isUsePwd) {
            unlockWithPassword(netCallBack);
            return;
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isSportFingerPrint()) {
            FingerprintManager manager = (FingerprintManager) getActivity().getSystemService(Context.FINGERPRINT_SERVICE);

            if (null != manager && SharePersistent.getBoolean(getActivity(), App.KEY_TOUCH_ID) && manager.isHardwareDetected() && manager.hasEnrolledFingerprints()) {


                unLockWithFingerPrint(netCallBack);

            } else {
                unlockWithPassword(netCallBack);
            }

//            if (getActivity().checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//
//                if (null != manager && manager.isHardwareDetected() && manager.hasEnrolledFingerprints()) {
//
//                    unLockWithFingerPrint(netCallBack);
//
//                } else {
//                    unlockWithPassword(netCallBack);
//                }
//            } else {
//                String[] permisson = {Manifest.permission.USE_FINGERPRINT};
//                getActivity().requestPermissions(permisson, SHORT_REQUEST_PERMISSION);
//            }


        } else {
            unlockWithPassword(netCallBack);
        }


//
//
//        String title = "";
//        String msg = "";
//        String yes = getResources().getString(R.string.common_postive);
//        String no = getResources().getString(R.string.common_postive);
//
//
//        DialogUtils.showInputPWDDialog(getActivity(), title, msg, yes, no, new DialogCallBackListener() {
//
//            @Override
//            public void onCallBack(boolean yesNo, String text) {
//                if (yesNo) {
//                    String[] datas = App.getApp().getPassword();
//                    login(datas[0], text, netCallBack);
//                }
//            }
//        });

    }


    public class FingerPrintCallBack extends FingerprintManagerCompat.AuthenticationCallback {


        private NetCallBack netCallBack;
        private NetResult netResult;

        public static final int ERROR_MATCHED = 0;
        public static final int ERROR_MATCH_OVER_TIMES = 1;
        public static final int ERROR_MATCH_FAIL = -1;

        public FingerPrintCallBack(NetCallBack netCallBack) {
            this.netCallBack = netCallBack;
            this.netResult = new NetResult();
        }

        //============================================================================================================
        // 当出现错误的时候回调此函数，比如多次尝试都失败了的时候，errString是错误信息
        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            LogUtil.i(App.TAG, "onAuthenticationError: " + errString);


            if (isLastBackPresed) {
                isLastBackPresed = false;
                return;
            }
            showCoverWithMsg(R.string.unlock_by_finger_fail_lator);


            if (null != netCallBack) {
                netResult.setTag(ERROR_MATCH_FAIL);
                netResult.setMessage(errString.toString());
                netCallBack.onFinish(netResult, null);
            }

        }

        // 当指纹验证失败的时候会回调此函数，失败之后允许多次尝试，失败次数过多会停止响应一段时间然后再停止sensor的工作
        @Override
        public void onAuthenticationFailed() {
            showCoverWithMsg(R.string.unlock_by_finger_fail);
            LogUtil.i(App.TAG, "onAuthenticationFailed: " + "验证失败");
            if (null != netCallBack) {
                netResult.setTag(ERROR_MATCH_FAIL);
                netCallBack.onFinish(netResult, null);
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            LogUtil.i(App.TAG, "onAuthenticationHelp: " + helpString);


            if (null != netCallBack) {
                netResult.setTag(ERROR_MATCH_FAIL);
                netCallBack.onFinish(netResult, null);
            }
        }

        //==============================================================================================================================
        // 当验证的指纹成功时会回调此函数，然后不再监听指纹sensor
        @Override
        public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
            LogUtil.i(App.TAG, "onAuthenticationSucceeded: " + "验证成功");
            hideCover();
            if (null != netCallBack) {
                netResult.setCode(NetResult.CODE_OK);
                netResult.setTag(ERROR_MATCHED);
                netCallBack.onFinish(netResult, null);
            }

        }


    }

    private void unLockWithFingerPrint(final NetCallBack netCallBack) {

        showCoverWithMsg(R.string.unlock_by_finger);

        if (manager == null) {
            manager = FingerprintManagerCompat.from(getActivity());
            mCancellationSignal = new CancellationSignal();
        }


        manager.authenticate(null, 0, mCancellationSignal, new FingerPrintCallBack(netCallBack), null);

        LogUtil.e(App.TAG, "finger print start....");

    }


    private void unlockWithPassword(final NetCallBack netCallBack) {
        String title = getString(R.string.unlock_title);
        String msg = getString(R.string.unlock_msg);
        String yes = getResources().getString(R.string.common_postive);
        String no = getResources().getString(R.string.common_negtive);

        final View view = View.inflate(getActivity(), R.layout.dialog_secrity, null);
        final View fingerUnlock = view.findViewById(R.id.linearlayoutFingerUnlock);


        new AlertDialog.Builder(getActivity()).setView(view).setMessage(msg).
                setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String inputPwd = sonderBaseActivity.getInput((EditText) view.findViewById(R.id.editTextInput));
                        String[] datas = App.getApp().getPassword();
                        login(datas[0], inputPwd, netCallBack);


                    }
                })
                .setNegativeButton(no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();


    }


    public boolean isSportFingerPrint() {
        try {
            Class.forName("android.hardware.fingerprint.FingerprintManager"); // 通过反射判断是否存在该类
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }


    BaseTask baseTaskLogin;

    public void login(final String username, final String password, final NetCallBack netCallBack) {


        BaseTask.resetTastk(baseTaskLogin);

        baseTaskLogin = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", username.trim());
                    jsonObject.put("password", password);
                    netResult = NetInterface.login(jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "net error:" + e.toString());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();


                if (null != result) {
                    if (result.isOk()) {

                        App.getApp().savePassword(username, password);
                        App.getApp().saveApiToken((String) result.getData()[0]);


//                        Student student = (Student) result.getData()[1];
//                        String stuDentId = String.valueOf(student.getStudentId());
//
//                        OneSignal.sendTag("userId", stuDentId);
//
//                        LogUtil.e(App.TAG, "==========userId:" + stuDentId);
//
//                        App.getApp().saveStudent(student);
//
//                        LogUtil.v(App.TAG, "token: " + App.getApp().getApiToken());


                        if (null != netCallBack) {
                            netCallBack.onFinish(result, baseTask);
                        }

//                      activity.finish();
                    } else {

                        if ("401".equals(result.getCode())) {
                            Tool.showMessageDialog(R.string.login_error_401, sonderBaseActivity);
                        } else {
                            Tool.showMessageDialog(result.getMessage(), sonderBaseActivity);
                        }
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, sonderBaseActivity);
                }


            }
        }, sonderBaseActivity);
        HashMap<String, String> hmap = new HashMap<>();
        baseTaskLogin.execute(hmap);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.i(App.TAG, "onActivityResult in profile activity");

        if (requestCode == PictureChoseActivity.SHORT_REQUEST_MEDIAS && resultCode == Activity.RESULT_OK) {
            ArrayList<String> fiels = data.getStringArrayListExtra(PictureChoseActivity.keyResult);
            onCropImgSuccess(fiels.get(0));
        }


    }

    private void onCropImgSuccess(String path) {

        if (!StringUtils.isEmpty(path)) {
            LogUtil.i(App.TAG, "croped path:" + path);
            File file = new File(path);
            if (file.exists()) {

                try {

                    byte[] data = FileUtils.readFile2Byte(path);
                    FormFile formFile = new FormFile("avatar", data, "avatar", FormFile.contentType);
                    sonderBaseActivity.requestUploadHeaderorPassport(new FormFile[]{formFile}, true, new NetCallBack() {
                        @Override
                        public void onFinish(NetResult result, BaseTask baseTask) {
                            if (null != result) {
                                if (result.isOk()) {
                                    LogUtil.i(App.TAG, "上传成功");
                                    try {
                                        student.getAvatar();

                                        LogUtil.i(App.TAG, "old student avatar:" + student.getAvatar());

                                        JSONObject jo = (JSONObject) result.getData()[0];
                                        student = JsonHelper.parseStudentInfo(jo.toString());
                                        App.getApp().saveStudent(student);

                                        LogUtil.i(App.TAG, "new student avatar:" + student.getAvatar());

                                        initView();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        LogUtil.e(App.TAG, "parse upload student update error:" + e.getLocalizedMessage());
                                    }

                                } else {
                                    Tool.showMessageDialog(result.getMessage(), getActivity());
                                }
                            } else {
                                Tool.showMessageDialog(R.string.error_net, getActivity());
                            }
                        }
                    });
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "上传文件失败");
                }

            }


        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        LogUtil.i(App.TAG, "onRequestPermissionsResult profile fragment");

        if (requestCode == SonderBaseActivity.SHORT_REQUEST_READ_EXTEAL && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            SonderBaseActivity sbActivity = ((SonderBaseActivity) getActivity());
            if (sbActivity.type_request_picture == sbActivity.TYPE_REQEST_PICTURE_SELECT) {
                sbActivity.onChosePhtoClick(true);
            } else {
                sbActivity.onTakePhtoClick(true);
            }
        } else if (requestCode == SHORT_REQUEST_PERMISSION) {

            LogUtil.i(App.TAG, "rquest finger permission:" + (grantResults[0] == PackageManager.PERMISSION_GRANTED));

        }

    }

    @OnClick(R.id.imageViewProfilePersonal)
    public void startPersonalActivity() {


        onSecurityTriger(false, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (result.isOk()) {
                    startActivity(new Intent(getActivity(), PersonalActivity.class));
                }
            }
        });


    }

    @OnClick(R.id.viewStuExpercenInfo)
    public void startEduActivity() {

        onSecurityTriger(false, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (result.isOk()) {
                    startActivity(new Intent(getActivity(), EducationActivity.class));

                }


            }
        });


    }

    @OnClick(R.id.viewMediaInfo)
    public void startMedicalActivity() {


        onSecurityTriger(false, new NetCallBack() {

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (result.isOk()) {
                    startActivity(new Intent(getActivity(), MedicalActivity.class));
                }


            }
        });


    }

    @OnClick(R.id.viewTravelInfo)
    public void startTravelActivity() {

        onSecurityTriger(false, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (result.isOk()) {
                    startActivity(new Intent(getActivity(), TravelActivity.class));
                }

            }
        });


    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    private void updateStudent() {
        LogUtil.v(App.TAG, "update student");

        if (null == student) {
            LogUtil.e(App.TAG, "studnet is error:.....");
            return;
        }
        textView_fragmentProfile_name.setText(student.getFullName());
        textView_fragmentProfile_username.setText(student.getEmail());

        if (progressBarFinishRate != null) {
            progressBarFinishRate.setRate(0);
            progressBarFinishRate.setRateByAnim(student.getProfileCompletion() / 100f);
        }
    }


    BaseTask baseTask;

    @Override
    public void onResume() {
        super.onResume();
        if (needRefresh) {
            refreshProfile();
        }
    }


    private void refreshProfile() {

        BaseTask.resetTastk(baseTask);

        baseTask = new BaseTask(getActivity(), new SonderNetCallback() {


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                try {
                    return NetInterface.getStudentInfo(new JSONObject());
                } catch (Exception e) {
                    LogUtil.e(App.TAG, "net error: " + e.toString());
                    return null;
                }
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                super.onFinish(result, baseTask);

                if (null != result) {
                    if (result.isOk()) {
                        student = (Student) result.getData()[0];
                        App.getApp().saveStudent(student);
                        updateStudent();
                        needRefresh = false;
                    } else {
                        Tool.showMessageDialog(result.getMessage(), getActivity());
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, getActivity());
                }


            }
        });
        baseTask.execute(new HashMap<String, String>());

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            updateStudent();
        } else {
            if (progressBarFinishRate != null) {
                progressBarFinishRate.setRate(0.0f);
            }
        }
    }
}
