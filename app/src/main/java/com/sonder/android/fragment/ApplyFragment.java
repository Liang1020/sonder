package com.sonder.android.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.baoyz.widget.PullRefreshLayout;
import com.common.fragment.BaseFragment;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.SupportDetailActivity;
import com.sonder.android.adapter.RequestExpandAdapter;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.net.NetInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leocx on 2016/10/12.
 */

public class ApplyFragment extends BaseFragment {


    @BindView(R.id.listView)
    ExpandableListView listView;

    @BindView(R.id.swipeRefreshLayout)
    public PullRefreshLayout swipeRefreshLayout;

    RequestExpandAdapter requestExpandAdapter;
    ArrayList<JSONObject> arrayListGroups;
    ArrayList<ArrayList<JSONObject>> arrayListDatas;

    private short SHORT_GO_DETAIL = 33;

    public static ApplyFragment newInstance() {
        ApplyFragment fragment = new ApplyFragment();
        Bundle args = new Bundle();
//        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_apply, null);
        ButterKnife.bind(this, view);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestIncientList();
            }
        });

        arrayListGroups = new ArrayList<>();
        arrayListDatas = new ArrayList<>();

//        ArrayList<JSONObject> ingList = new ArrayList<>();
//        ArrayList<JSONObject> closeList = new ArrayList<>();
//
//        arrayListDatas.add(ingList);
//        arrayListDatas.add(closeList);
//
//        requestExpandAdapter = new RequestExpandAdapter(getActivity(), arrayListGroups, arrayListDatas);
//        listView.setAdapter(requestExpandAdapter);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();


        refreshIfNeed();

    }

    public void refreshIfNeed() {
        if (null == requestExpandAdapter || ListUtiles.getListSize(arrayListDatas) == 0 || arrayListDatas.get(0).size() == 0 || needRefresh == true) {
            refreshWithUi();
        }

    }

    public void refreshWithUi() {

        if (null != swipeRefreshLayout) {
            swipeRefreshLayout.setRefreshing(true);
            requestIncientList();
        }

    }

    public void buildAdapter(JSONObject jsonObject) {

        try {

            ArrayList<JSONObject> ingList = new ArrayList<>();
            ArrayList<JSONObject> closeList = new ArrayList<>();

            arrayListDatas.clear();

            arrayListDatas.add(ingList);
            arrayListDatas.add(closeList);


            JSONArray opens = jsonObject.getJSONArray("opens");

            for (int i = 0, isize = opens.length(); i < isize; i++) {
                ingList.add(opens.getJSONObject(i));
            }

            JSONArray closes = jsonObject.getJSONArray("closes");
            for (int i = 0, isize = closes.length(); i < isize; i++) {
                closeList.add(closes.getJSONObject(i));
            }


            Comparator<JSONObject> comprator = new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    try {
                        return o1.getInt("incident_id") <= o2.getInt("incident_id") ? 1 : -1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            };


            Collections.sort(ingList, comprator);
            Collections.sort(closeList, comprator);

//            Collections.reverse(ingList);
//            Collections.reverse(closeList);


            arrayListGroups.clear();

            JSONObject groupIngTag = new JSONObject();
            groupIngTag.put("type", "opens");
            arrayListGroups.add(groupIngTag);


            JSONObject groupClosetag = new JSONObject();
            groupClosetag.put("type", "closed");
            arrayListGroups.add(groupClosetag);


            requestExpandAdapter = new RequestExpandAdapter(getActivity(), arrayListGroups, arrayListDatas);
            listView.setAdapter(requestExpandAdapter);
            listView.expandGroup(0, true);

            listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                    try {

                        JSONObject jo = arrayListDatas.get(groupPosition).get(childPosition);

                        if (null != jo && jo.has("summary")) {

                            String incident_id = jo.getString("incident_id");

                            ((SonderBaseActivity) getActivity()).requestIncident(incident_id, true, new NetCallBack() {
                                @Override
                                public void onFinish(NetResult result, BaseTask baseTask) {


                                    if (null != result) {
                                        if (result.isOk()) {

                                            App.getApp().putTemPObject("fromApply", true);
                                            App.getApp().putTemPObject(SonderBaseActivity.KEY_INCIDENT, (JSONObject) result.getData()[0]);
                                            Tool.startActivityForResult(getActivity(), SupportDetailActivity.class, SHORT_GO_DETAIL);

                                        } else {
                                            Tool.showMessageDialog(result.getMessage(), getActivity());
                                        }

                                    } else {
                                        Tool.showMessageDialog(R.string.error_net, getActivity());
                                    }

                                }
                            });
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    return false;
                }
            });


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {

                        JSONObject jo = (JSONObject) (parent.getAdapter().getItem(position));

                        if (null != jo && jo.has("summary")) {

                            String incident_id = jo.getString("incident_id");

                            ((SonderBaseActivity) getActivity()).requestIncident(incident_id, true, new NetCallBack() {
                                @Override
                                public void onFinish(NetResult result, BaseTask baseTask) {


                                    if (null != result) {
                                        if (result.isOk()) {
                                            App.getApp().putTemPObject(SonderBaseActivity.KEY_INCIDENT, (JSONObject) result.getData()[0]);
                                            Tool.startActivityForResult(getActivity(), SupportDetailActivity.class, SHORT_GO_DETAIL);
                                        } else {
                                            Tool.showMessageDialog(result.getMessage(), getActivity());
                                        }

                                    } else {
                                        Tool.showMessageDialog(R.string.error_net, getActivity());
                                    }

                                }
                            });
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.TAG, "request erros list");
        }


    }

    BaseTask baseTaskGetIncient;

    public void requestIncientList() {
        BaseTask.resetTastk(baseTaskGetIncient);

        baseTaskGetIncient = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getIncidentList();
                } catch (Exception e) {
                }
                return netResult;
            }

            @Override
            public void onCanCell(BaseTask baseTask) {

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                needRefresh = false;
                swipeRefreshLayout.setRefreshing(false);

                if (null != result) {
                    if (result.isOk()) {
                        buildAdapter((JSONObject) result.getData()[0]);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), getActivity());
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, getActivity());
                }
            }
        });

        baseTaskGetIncient.execute(new HashMap<String, String>());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_DETAIL && resultCode == Activity.RESULT_OK) {

            swipeRefreshLayout.setRefreshing(true);
            requestIncientList();
        }


    }
}
