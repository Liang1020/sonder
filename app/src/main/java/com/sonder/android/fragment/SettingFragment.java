package com.sonder.android.fragment;


import android.app.Activity;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.common.fragment.BaseFragment;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.onesignal.OneSignal;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.GuidActivity;
import com.sonder.android.activity.ResetPasswordActivity;
import com.sonder.android.service.UpdateLocationService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by leocx on 2016/10/12.
 */

public class SettingFragment extends BaseFragment {


    @BindView(R.id.viewSplitTouchId)
    View viewSplitTouchId;

    @BindView(R.id.lienarlayoutTouchId)
    View lienarlayoutTouchId;


    @BindView(R.id.switchTouchId)
    Switch switchTouchId;

    @BindView(R.id.textViewVersionLabel)
    TextView textViewVersionLabel;

    public final String buildNumber = "201702091200";

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
//        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @OnCheckedChanged(R.id.switchTouchId)
    public void onChangedChagne() {
        LogUtil.i(App.TAG, "chagne switch:" + switchTouchId.isChecked());
        SharePersistent.saveBoolean(getActivity(), App.KEY_TOUCH_ID, switchTouchId.isChecked());


    }


    @OnClick(R.id.lienarLayoutTuturial)
    public void onTtClick() {

        App.getApp().putTemPObject("isFromSetting", true);
        Tool.startActivity(getActivity(), GuidActivity.class);
//        App.getApp().putTemPObject("isFromSetting",true);
//        Tool.startActivity(getActivity(), GuidActivity.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, null);
        ButterKnife.bind(this, view);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isSportFingerPrint()) {


            FingerprintManager manager = (FingerprintManager) getActivity().getSystemService(Context.FINGERPRINT_SERVICE);
            if (null != manager && manager.isHardwareDetected() && manager.hasEnrolledFingerprints()) {
                showTouchIdSettig();


            } else {
                hideTouchIdSetting();
            }


        } else {
            hideTouchIdSetting();
        }

        textViewVersionLabel.setText("Version " + Tool.getVersionName(getActivity()) + " " + buildNumber);

        return view;
    }

    private void hideTouchIdSetting() {

        viewSplitTouchId.setVisibility(View.GONE);
        lienarlayoutTouchId.setVisibility(View.GONE);
    }

    private void showTouchIdSettig() {
        viewSplitTouchId.setVisibility(View.VISIBLE);
        lienarlayoutTouchId.setVisibility(View.VISIBLE);
        switchTouchId.setChecked(SharePersistent.getBoolean(getActivity(), App.KEY_TOUCH_ID));

    }


    public boolean isSportFingerPrint() {
        try {
            Class.forName("android.hardware.fingerprint.FingerprintManager"); // 通过反射判断是否存在该类
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    @OnClick(R.id.linearlayoutLogout)
    public void onClickLogout() {
        getActivity().setResult(Activity.RESULT_CANCELED);
        App.getApp().saveStudent(null);
        OneSignal.sendTag("userId", "-1");

        UpdateLocationService.stopUpldateLocation(getActivity());

        getActivity().finish();
    }

    @OnClick(R.id.lienarlayoutTc)
    public void onCLickTc() {
        Tool.startUrl(getActivity(), getString(R.string.tc_link));
    }


    @OnClick(R.id.linearlayoutResetPwd)
    public void onClickResetpwd() {
        Tool.startActivity(getActivity(), ResetPasswordActivity.class);
    }
}
