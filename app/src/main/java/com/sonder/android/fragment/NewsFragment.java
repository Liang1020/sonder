package com.sonder.android.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.baoyz.widget.PullRefreshLayout;
import com.common.fragment.BaseFragment;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.adapter.NewsAdapter;
import com.sonder.android.domain.Feed;
import com.sonder.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leocx on 2016/10/12.
 */

public class NewsFragment extends BaseFragment {


    private ArrayList<Feed> allFeds;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @BindView(R.id.swipeRefreshLayoutNews)
    PullRefreshLayout swipeRefreshLayout;

    @BindView(R.id.listView)
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news, null);
        ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                requestFeeds();

            }
        });


        return view;

    }


    @Override
    public void onResume() {
        super.onResume();

        if (ListUtiles.isEmpty(allFeds)) {
            swipeRefreshLayout.setRefreshing(true);
            requestFeeds();
        }
    }


    BaseTask basetaskFeeds;

    private void requestFeeds() {
        BaseTask.resetTastk(basetaskFeeds);

        basetaskFeeds = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;

                try {

                    JSONObject jso = new JSONObject();
                    netResult = NetInterface.getFeed(jso);

                } catch (Exception e) {
                    LogUtil.e(App.TAG, "requestFeeds:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                swipeRefreshLayout.setRefreshing(false);

                if (null != result) {
                    if (result.isOk()) {
                        buildAdapter((ArrayList<Feed>) result.getData()[0]);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), baseTask.activity);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, baseTask.activity);
                }
            }
        });

        basetaskFeeds.execute(new HashMap<String, String>());

    }


    private void buildAdapter(ArrayList<Feed> feeds) {

        if(null!=getActivity()){
            NewsAdapter newsAdapter = new NewsAdapter(getActivity(), feeds);
            listView.setAdapter(newsAdapter);
        }


    }


}
