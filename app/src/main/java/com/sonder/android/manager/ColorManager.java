package com.sonder.android.manager;

import android.os.Build;

import com.sonder.android.App;


/**
 * Created by leocx on 2016/9/8.
 */
public class ColorManager {
    public static int getColor(int resourceId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return App.getApp().getResources().getColor(resourceId, null);
        } else {
            return App.getApp().getResources().getColor(resourceId);
        }
    }

}
