package com.sonder.android.manager;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sonder.android.App;

/**
 * Created by leocx on 2016/11/3.
 */

public class KeyboardManager {

    public static void showInputKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) App.getApp().getBaseContext().
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void hideInputKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) App.getApp().getBaseContext().
                getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = App.getApp().getActivity().getCurrentFocus();
        if (view == null) {
            view = new View(App.getApp().getActivity());
        }
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
