package com.sonder.android.manager;


import com.sonder.android.App;

/**
 * Created by leocx on 2016/9/7.
 */
public class StringManager {
    public static String getString(int resourceId) {
        App app = (App) App.getApp();
        return app.getBaseContext().getResources().getString(resourceId);
    }


}
