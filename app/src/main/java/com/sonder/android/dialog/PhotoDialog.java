package com.sonder.android.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.bigkoo.pickerview.view.MyBasePicker;

/**
 * Created by leocx on 2016/10/26.
 */

public class PhotoDialog extends MyBasePicker {


    private SonderBaseActivity sonderBaseActivity;
    public OnPhotoCallbackListener onPhotoCallbackListener;

    public PhotoDialog(SonderBaseActivity sonderBaseActivity) {
        super((Activity) sonderBaseActivity);
        this.sonderBaseActivity = sonderBaseActivity;
        LayoutInflater.from(context).inflate(R.layout.dialog_choose_photo, contentContainer);


        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        findViewById(R.id.linearlayoutPhotoChoose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoCallbackListener) {
                    onPhotoCallbackListener.chooseType(0);
                }

            }
        });

        findViewById(R.id.linearlayoutCameraTake).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoCallbackListener) {
                    onPhotoCallbackListener.chooseType(1);
                }
            }
        });


    }


    public void setNeedFile(boolean needFileChoose){

        int fv=needFileChoose?View.VISIBLE:View.GONE;
        findViewById(R.id.linearlayoutFileChoose).setVisibility(fv);
    }

    public OnPhotoCallbackListener getOnPhotoCallbackListener() {
        return onPhotoCallbackListener;
    }

    public void setOnPhotoCallbackListener(OnPhotoCallbackListener onPhotoCallbackListener) {
       this.onPhotoCallbackListener = onPhotoCallbackListener;
    }

    public static interface OnPhotoCallbackListener {

        public void chooseType(int chooseTypeClicked);

    }


}
