package com.sonder.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.sonder.android.R;
import com.bigkoo.pickerview.view.MyBasePicker;

/**
 * Created by leocx on 2016/10/26.
 */

public class EmergentDialog extends MyBasePicker {


    public final static int TYPE_REQUEST_EMERGENCY = 0;
    public final static int TYPE_REQUEST_PHONESSC = 1;
    public final static int TYPE_REQUEST_NEEDHELP = 2;

    public OnEmergenceDialogClickListener onEmergenceDialogClickListener;


    public EmergentDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_emergency, contentContainer);
        initView();
    }

    public void initView() {


        View.OnClickListener onClickListener = new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                dismiss();

                int vid = v.getId();
                switch (vid) {

                    case R.id.imageViewCloseOnEmergcy33:
                        break;
                    case R.id.viewRequestEmergency:
                        if (null != onEmergenceDialogClickListener) {
                            onEmergenceDialogClickListener.onClickTypeSupport(TYPE_REQUEST_EMERGENCY);
                        }

                        break;

                    case R.id.viewRequestPhonessc:
                        if (null != onEmergenceDialogClickListener) {
                            onEmergenceDialogClickListener.onClickTypeSupport(TYPE_REQUEST_PHONESSC);
                        }

                        break;
                    case R.id.viewRequestNeedHelp:

                        if (null != onEmergenceDialogClickListener) {
                            onEmergenceDialogClickListener.onClickTypeSupport(TYPE_REQUEST_NEEDHELP);
                        }

                        break;


                }

            }
        };


        findViewById(R.id.imageViewCloseOnEmergcy33).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.viewRequestEmergency).setOnClickListener(onClickListener);
        findViewById(R.id.viewRequestPhonessc).setOnClickListener(onClickListener);
        findViewById(R.id.viewRequestNeedHelp).setOnClickListener(onClickListener);
        findViewById(R.id.viewRoot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }



    public OnEmergenceDialogClickListener getOnEmergenceDialogClickListener() {
        return onEmergenceDialogClickListener;
    }

    public void setOnEmergenceDialogClickListener(OnEmergenceDialogClickListener onEmergenceDialogClickListener) {
        this.onEmergenceDialogClickListener = onEmergenceDialogClickListener;
    }

    public static interface OnEmergenceDialogClickListener {

        public void onClickTypeSupport(int type);

    }

}
