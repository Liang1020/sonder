package com.sonder.android.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.DialogCallBackListener;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

/**
 * Created by leocx on 2016/10/26.
 */

public class DialogCheckInLeave extends MyBasePicker {


    private SonderBaseActivity sonderBaseActivity;

    private String title;
    private String content;
    private String yes;
    private String no;


    private DialogCallBackListener dialogCallBackListener;


    public DialogCheckInLeave(SonderBaseActivity sonderBaseActivity) {
        super((Activity) sonderBaseActivity);
        this.sonderBaseActivity = sonderBaseActivity;
        LayoutInflater.from(context).inflate(R.layout.dialog_leave, contentContainer);

        findViewById(R.id.textViewStartWalk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != dialogCallBackListener) {
                    dialogCallBackListener.onDone(true);
                }

            }
        });

        findViewById(R.id.textViewLeave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != dialogCallBackListener) {
                    dialogCallBackListener.onDone(false);
                }

            }
        });

        findViewById(R.id.textViewCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }

    public DialogCallBackListener getDialogCallBackListener() {
        return dialogCallBackListener;
    }

    public void setDialogCallBackListener(DialogCallBackListener dialogCallBackListener) {
        this.dialogCallBackListener = dialogCallBackListener;
    }
}
