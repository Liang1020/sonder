package com.sonder.android.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

/**
 * Created by leocx on 2016/10/26.
 */

public class MediaDialog extends MyBasePicker {


    private SonderBaseActivity sonderBaseActivity;
    public OnMediaCallbackListener onMediaCallbackListener;

    public MediaDialog(SonderBaseActivity sonderBaseActivity, boolean needFileChoose) {
        super((Activity) sonderBaseActivity);
        this.sonderBaseActivity = sonderBaseActivity;
        LayoutInflater.from(context).inflate(R.layout.dialog_choose_media, contentContainer);

        findViewById(R.id.imageViewCloseOnMedia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        findViewById(R.id.linearlayoutPhotoChoose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onMediaCallbackListener) {
                    onMediaCallbackListener.chooseType(0);
                }
            }
        });

        findViewById(R.id.linearlayoutCameraTake).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onMediaCallbackListener) {
                    onMediaCallbackListener.chooseType(1);
                }
            }
        });


        int fv = needFileChoose ? View.VISIBLE : View.GONE;


        findViewById(R.id.linearlayoutFileChoose).setVisibility(fv);
        findViewById(R.id.linearlayoutFileChoose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onMediaCallbackListener) {
                    onMediaCallbackListener.chooseType(2);
                }
            }
        });

    }


    public void setNeedFile(boolean needFileChoose) {

        int fv = needFileChoose ? View.VISIBLE : View.GONE;
        findViewById(R.id.linearlayoutFileChoose).setVisibility(fv);
    }

    public MediaDialog.OnMediaCallbackListener getOnMediaCallbackListener() {
        return onMediaCallbackListener;
    }

    public void setOnMediaCallbackListener(MediaDialog.OnMediaCallbackListener onMediaCallbackListener) {
        onMediaCallbackListener = onMediaCallbackListener;
    }

    public static interface OnMediaCallbackListener {

        public void chooseType(int chooseTypeClicked);

    }


}
