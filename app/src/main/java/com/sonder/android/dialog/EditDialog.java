package com.sonder.android.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;
import com.sonder.android.domain.Student;
import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.Tool;

import java.util.HashMap;

/**
 * Created by leocx on 2016/10/26.
 */

public class EditDialog extends MyBasePicker {


    private OnEditListener onEditListener;
    private SonderBaseActivity sonderBaseActivity;
    private HashMap<String, String> hashMap;
    private String key;
    private Student student;
    private EditText editTextInput;


    public EditDialog(SonderBaseActivity sonderBaseActivity) {
        super((Activity) sonderBaseActivity);
        this.sonderBaseActivity = sonderBaseActivity;
        this.hashMap = new HashMap<String, String>();
        LayoutInflater.from(context).inflate(R.layout.dialog_edit, contentContainer);
    }

    public EditDialog(final SonderBaseActivity sonderBaseActivity, String title, final String key, String oldValue, final Student student, OnEditListener onEditListener) {
        super((Activity) sonderBaseActivity);
        init(sonderBaseActivity, title, key, oldValue, student, onEditListener, EditorInfo.TYPE_CLASS_TEXT);
    }


    private void init(final SonderBaseActivity sonderBaseActivity, String title, final String key, String oldValue, final Student student, final OnEditListener onEditListener, int inputType) {

        this.student = student;
        this.onEditListener = onEditListener;

        ((TextView) findViewById(R.id.textViewEditTitle)).setText(title);
        editTextInput = ((EditText) findViewById(R.id.editTextEditInput));
        editTextInput.setText(oldValue);
        editTextInput.setInputType(inputType);

        findViewById(R.id.imageViewBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.textViewSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = sonderBaseActivity.getResources().getString(R.string.coomon_confirm_ask);
                String yes = sonderBaseActivity.getResources().getString(R.string.coomon_confirm_ask_yes);
                String no = sonderBaseActivity.getResources().getString(R.string.coomon_confirm_ask_no);

                DialogUtils.showConfirmDialog(sonderBaseActivity, "", msg, yes, no, new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {
                        if (yesOrNo) {
                            hashMap.put(key, sonderBaseActivity.getInput(editTextInput));
                            sonderBaseActivity.updateAll(new NetCallBack() {
                                @Override
                                public void onFinish(NetResult result, BaseTask baseTask) {
                                    if (null != result) {
                                        if (result.isOk()) {

                                            Student newStudent = (Student) result.getData()[0];
                                            App.getApp().saveStudent(newStudent);

                                            if (null != onEditListener) {
                                                onEditListener.onRequestEnd(true, newStudent, result);
                                            }

                                            dismiss();
                                        } else {
                                            if (null != onEditListener) {
                                                onEditListener.onRequestEnd(false, null, result);
                                            }
                                            Tool.showMessageDialog(result.getMessage(), sonderBaseActivity);
                                        }

                                    } else {
                                        Tool.showMessageDialog(R.string.error_net, sonderBaseActivity);
                                        if (null != onEditListener) {
                                            onEditListener.onRequestEnd(false, null, null);
                                        }
                                    }


                                }
                            }, student, hashMap);

                        } else {
                            dismiss();
                        }
                    }
                });


            }
        });


        this.key = key;
        this.student = student;
    }


    public void setConttent(final SonderBaseActivity sonderBaseActivity, String title, final String key, String oldValue, final Student student, OnEditListener onEditListener, int inputType) {
        init(sonderBaseActivity, title, key, oldValue, student, onEditListener, inputType);
    }

    public static interface OnEditListener {

        public void onRequestEnd(boolean isSuccess, Student student, NetResult netResult);

    }

    public OnEditListener getOnEditListener() {
        return onEditListener;
    }

    public void setOnEditListener(OnEditListener onEditListener) {
        this.onEditListener = onEditListener;
    }
}
