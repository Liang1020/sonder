package com.sonder.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.sonder.android.R;

/**
 * Created by leocx on 2016/10/26.
 */

public class PersonSelectDialog extends MyBasePicker implements View.OnClickListener {


    public OnPersonSelectListener onPersonSelectListener;

    public PersonSelectDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_personal_select, contentContainer);
        initView();
    }

    public void initView() {


        View.OnClickListener onClickListener = new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                dismiss();

                int vid = v.getId();
                switch (vid) {

                    case R.id.textViewPerson:
                        if (null != onPersonSelectListener) {

                            onPersonSelectListener.OnPersonSelect(0);
                        }

                        break;

                    case R.id.textViewOther:
                        if (null != onPersonSelectListener) {

                            onPersonSelectListener.OnPersonSelect(1);
                        }

                        break;
                    case R.id.textViewGroup:
                        if (null != onPersonSelectListener) {

                            onPersonSelectListener.OnPersonSelect(2);
                        }
                        break;

                    default:
                        if (null != onPersonSelectListener) {

                            onPersonSelectListener.OnPersonSelect(3);
                        }

                }

            }
        };


        findViewById(R.id.textViewPerson).setOnClickListener(onClickListener);
        findViewById(R.id.textViewOther).setOnClickListener(onClickListener);
        findViewById(R.id.textViewGroup).setOnClickListener(onClickListener);
        findViewById(R.id.textViewCancel).setOnClickListener(onClickListener);


    }

    @Override
    public void onClick(View v) {

    }


    public static interface OnPersonSelectListener {

        public void OnPersonSelect(int type);
    }

    public OnPersonSelectListener getOnPersonSelectListener() {
        return onPersonSelectListener;
    }

    public void setOnPersonSelectListener(OnPersonSelectListener onPersonSelectListener) {
        this.onPersonSelectListener = onPersonSelectListener;
    }
}
