package com.sonder.android.dialog;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.sonder.android.App;
import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.LogUtil;
import com.sonder.android.R;

/**
 * Created by leocx on 2016/10/26.
 */

public class RequestDialog extends MyBasePicker implements View.OnClickListener {

    private ImageView imageView_requestDialog_close;

    public RequestDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_request, contentContainer);
        setBackgroundColor(Color.argb(100, 255, 255, 255));
        initLayout();
    }

    public void initLayout() {
        imageView_requestDialog_close = (ImageView) findViewById(R.id.imageView_requestDialog_close);
        imageView_requestDialog_close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_requestDialog_close:
                dismiss();
                LogUtil.v(App.TAG, "click");
                break;
        }

    }
}
