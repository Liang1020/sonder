package com.sonder.android.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.DialogCallBackListener;
import com.sonder.android.R;
import com.sonder.android.base.SonderBaseActivity;

/**
 * Created by leocx on 2016/10/26.
 */

public class CustomDialog extends MyBasePicker {


    private SonderBaseActivity sonderBaseActivity;

    private String title;
    private String content;
    private String yes;
    private String no;


    private DialogCallBackListener dialogCallBackListener;


    public CustomDialog(SonderBaseActivity sonderBaseActivity, String title, String content, String yes, String no) {
        super((Activity) sonderBaseActivity);
        this.sonderBaseActivity = sonderBaseActivity;
        LayoutInflater.from(context).inflate(R.layout.dialog_custom, contentContainer);


        ((TextView) findViewById(R.id.textViewTitle)).setText(title);
        ((TextView) findViewById(R.id.textViewContent)).setText(content);

        ((TextView) findViewById(R.id.textViewCancel)).setText(no);
        ((TextView) findViewById(R.id.textViewConfirm)).setText(yes);


        ((TextView) findViewById(R.id.textViewCancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

                if (null != dialogCallBackListener) {
                    dialogCallBackListener.onDone(false);
                }

            }
        });

        ((TextView) findViewById(R.id.textViewConfirm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

                if (null != dialogCallBackListener) {
                    dialogCallBackListener.onDone(true);
                }
            }
        });

        findViewById(R.id.viewRoot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
//                if (null != dialogCallBackListener) {
//                    dialogCallBackListener.onDone(false);
//                }

            }
        });

    }

    public DialogCallBackListener getDialogCallBackListener() {
        return dialogCallBackListener;
    }

    public void setDialogCallBackListener(DialogCallBackListener dialogCallBackListener) {
        this.dialogCallBackListener = dialogCallBackListener;
    }
}
