package com.sonder.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.sonder.android.R;

/**
 * Created by leocx on 2016/10/26.
 */

public class ProcessingDialog extends MyBasePicker implements View.OnClickListener {


    public ProcessingDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_processing, contentContainer);
        initLayout();

        Animation inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        inAnimation.setDuration(200);
        setInAnim(inAnimation);
        Animation outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out);
        outAnimation.setDuration(200);
        setOutAnim(outAnimation);
    }

    public void initLayout() {
    }

    @Override
    public void onClick(View v) {

    }
}
