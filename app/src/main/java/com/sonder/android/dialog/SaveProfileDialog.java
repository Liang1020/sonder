package com.sonder.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.sonder.android.R;


/**RR
 * Created by leocx on 2016/11/4.
 */

public abstract class SaveProfileDialog extends MyBasePicker implements View.OnClickListener {

    public static final int TAG_SAVE = 0x0001;
    public static final int TAG_QUIT = 0x0002;
    RelativeLayout relativeLayout_dialogSaveProfile_main;
    TextView textView_dialogSaveProfile_save;
    TextView textView_dialogSaveProfile_quit;

    public SaveProfileDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_save_profile, contentContainer);
        initLayout();
    }

    private void initLayout() {

        relativeLayout_dialogSaveProfile_main = (RelativeLayout) findViewById(R.id.relativeLayout_dialogSaveProfile_main);
        textView_dialogSaveProfile_save = (TextView) findViewById(R.id.textView_dialogSaveProfile_save);
        textView_dialogSaveProfile_quit = (TextView) findViewById(R.id.textView_dialogSaveProfile_quit);
        relativeLayout_dialogSaveProfile_main.setOnClickListener(this);
        textView_dialogSaveProfile_save.setOnClickListener(this);
        textView_dialogSaveProfile_quit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_dialogSaveProfile_save:
                onDismiss(TAG_SAVE);
                dismiss();
                break;
            case R.id.textView_dialogSaveProfile_quit:
                onDismiss(TAG_QUIT);
                dismiss();
                break;
            case R.id.relativeLayout_dialogSaveProfile_main:
                break;
        }
    }

    abstract public void onDismiss(int tag);

}
