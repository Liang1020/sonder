package com.sonder.android.dialog;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.sonder.android.App;
import com.sonder.android.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;

/**
 * Created by leocx on 2016/10/26.
 */

public class DialogRotate extends MyBasePicker {


    private Activity activity;
    private String url;
    private String resultUrl;


    private TextView textViewYes;
    private ImageView imageViewRotate;
    private ImageView imageViewTaked;
    private View viewCover;
    private int degree = 0;

    BaseTask xtask;

    private OnClickOkListener onClickOkListener;

    public DialogRotate(final Activity activity, final String url) {
        super(activity);
        this.activity = activity;
        this.url = url;
        LayoutInflater.from(context).inflate(R.layout.dialog_picture_rotate, contentContainer);

        this.imageViewRotate = (ImageView) findViewById(R.id.imageViewRotate);
        this.textViewYes = (TextView) findViewById(R.id.textViewYes);
        this.imageViewTaked = (ImageView) findViewById(R.id.imageViewTaked);
        this.viewCover = findViewById(R.id.viewCover);


        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageViewTaked.setImageBitmap(bitmap);

                viewCover.setVisibility(View.GONE);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                viewCover.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                viewCover.setVisibility(View.VISIBLE);

            }
        };

        imageViewTaked.setTag(target);


        Picasso.with(activity).load(new File(url)).error(R.drawable.icon_alarm).into(target);


        findViewById(R.id.textViewYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onClickOkListener) {

                    if (StringUtils.isEmpty(resultUrl)) {
                        onClickOkListener.onClickOk(0, url);
                    } else {
                        onClickOkListener.onClickOk(degree % 360, resultUrl);
                    }


                }
                dismiss();

            }
        });

        findViewById(R.id.imageViewRotate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Target target = new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                        imageViewTaked.setImageBitmap(bitmap);


                        BaseTask.resetTastk(xtask);
                        xtask = new BaseTask(activity, new NetCallBack() {

                            @Override
                            public void onCanCell(BaseTask baseTask) {
                                viewCover.setVisibility(View.GONE);
                            }

                            @Override
                            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                                NetResult netResult = null;

                                try {
                                    File file = new File(url);
                                    String newUrl = new String(url);

                                    resultUrl = newUrl.replace(file.getName(), String.valueOf(System.currentTimeMillis()) + "_r" + ".png");

                                    FileOutputStream fout = new FileOutputStream(url);
                                    BufferedOutputStream bfout=new BufferedOutputStream(fout);
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 75, bfout);


                                    fout.flush();
                                    bfout.flush();

                                    fout.close();
                                    bfout.close();

                                    netResult = new NetResult();
                                    netResult.setTag(newUrl);
                                    LogUtil.i(App.TAG, "save new url:" + resultUrl);
                                } catch (Exception e) {
                                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                                }
                                return netResult;
                            }

                            @Override
                            public void onFinish(NetResult result, BaseTask baseTask) {
                                viewCover.setVisibility(View.GONE);
                                if (null != result) {
                                    resultUrl = (String) result.getTag();
                                }

                            }
                        });

                        xtask.execute(new HashMap<String, String>());

                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        viewCover.setVisibility(View.GONE);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                        viewCover.setVisibility(View.VISIBLE);

                    }
                };

                viewCover.setVisibility(View.VISIBLE);

                imageViewTaked.setTag(target);
                degree = (degree + 90) % 360;
                Picasso.with(activity).load(new File(url)).rotate(degree).into(target);


            }
        });
        findViewById(R.id.imageViewBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public static interface OnClickOkListener {
        public void onClickOk(int degred, String newUrl);
    }


    public OnClickOkListener getOnClickOkListener() {
        return onClickOkListener;
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.onClickOkListener = onClickOkListener;
    }
}
