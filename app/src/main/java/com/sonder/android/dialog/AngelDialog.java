package com.sonder.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.LogUtil;
import com.sonder.android.App;
import com.sonder.android.R;

/**
 * Created by leocx on 2016/10/26.
 */

public class AngelDialog extends MyBasePicker implements View.OnClickListener {

    private ImageView imageView_dialogAngel_close;
    private OnGuidDdialogClickListener onGuidDdialogClickListener;

    public AngelDialog(Context context) {
        super(context);
        setDismissable(true);
        LayoutInflater.from(context).inflate(R.layout.dialog_angle, contentContainer);
        initLayout();
    }

    public void initLayout() {
        imageView_dialogAngel_close = (ImageView) findViewById(R.id.imageView_dialogAngel_close);
        imageView_dialogAngel_close.setOnClickListener(this);

        findViewById(R.id.linearLayoutCheckIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(null!=onGuidDdialogClickListener){
                    onGuidDdialogClickListener.onClickCheck();
                }

            }
        });

        findViewById(R.id.linearLayoutWorkwith).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(null!=onGuidDdialogClickListener){
                    onGuidDdialogClickListener.onClickWorkWith();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_dialogAngel_close:
                dismiss();
                LogUtil.v(App.TAG, "click");
                break;
        }

    }

    public static  interface OnGuidDdialogClickListener{

        public void onClickCheck();
        public void onClickWorkWith();
    }

    public OnGuidDdialogClickListener getOnGuidDdialogClickListener() {
        return onGuidDdialogClickListener;
    }

    public void setOnGuidDdialogClickListener(OnGuidDdialogClickListener onGuidDdialogClickListener) {
        this.onGuidDdialogClickListener = onGuidDdialogClickListener;
    }
}
