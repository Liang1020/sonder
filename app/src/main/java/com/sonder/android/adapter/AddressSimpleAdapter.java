package com.sonder.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sonder.android.R;
import com.sonder.android.domain.SimpleAddrss;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/7.
 */

public class AddressSimpleAdapter extends BaseAdapter {


    private List<SimpleAddrss> addresses;
    private Context context;
    private int selectIndex = -1;
    private LayoutInflater layoutInflater;

    public AddressSimpleAdapter(Context context, List<SimpleAddrss> addresses) {
        this.addresses = addresses;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        return addresses.size();
    }

    @Override
    public Object getItem(int position) {
        return addresses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_address, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        SimpleAddrss addres = addresses.get(position);


        if (selectIndex == position) {
            viewHolder.textViewAddrssLabel.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
            viewHolder.textViewAddrssLabelLong.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
        } else {
            viewHolder.textViewAddrssLabel.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));
            viewHolder.textViewAddrssLabelLong.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));
        }

//        String adminArea = addres.getAdminArea();
//        String subAdminArea = addres.getSubAdminArea();
//        String lity = addres.getLocality();
//        String sublity = addres.getSubLocality();

        String label = addres.getAddress();

        String[] ads = label.split(",");

        if (ads.length > 0) {
            viewHolder.textViewAddrssLabel.setText(ads[0].replace(",",""));
            viewHolder.textViewAddrssLabelLong.setText(label.replace(ads[0]+",", ""));
        } else {
            viewHolder.textViewAddrssLabel.setText(label);
        }


        return convertView;
    }


    public SimpleAddrss getSelectAddrss() {

        if (-1 != selectIndex) {
            return addresses.get(selectIndex);
        }

        return null;

    }


    public void select(int postion) {
        this.selectIndex = postion;
        notifyDataSetChanged();
    }

    class ViewHolder {


        @BindView(R.id.textViewAddrssLabel)
        TextView textViewAddrssLabel;

        @BindView(R.id.textViewAddrssLabelLong)
        TextView textViewAddrssLabelLong;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }


    }

}
