package com.sonder.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.common.util.Tool;
import com.common.view.RecyleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * @author xiaanming
 */
public class MyImgAdapter extends PagerAdapter {


    private String[] imgs;
    private ArrayList<RecyleImageView> mImageViews;
    private Context context;
    private LayoutInflater layoutinflater;
    private Point p;

    private OnInterfaceClickListener onInterfaceClickListener;

    public MyImgAdapter(Context context, final String[] imgs) {
        this.context = context;
        this.layoutinflater = LayoutInflater.from(context);
        this.imgs = imgs;
        this.p = Tool.getDisplayMetrics(context);
        this.mImageViews = new ArrayList<>();

        for (final String img : imgs) {

            RecyleImageView imageView = new RecyleImageView(context);
            Picasso.with(context).load(img).config(Bitmap.Config.RGB_565).into(imageView);
            this.mImageViews.add(imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(null!=onInterfaceClickListener){
                        onInterfaceClickListener.onClickImgUrl(img);
                    }
                }
            });
        }

    }


    @Override
    public int getCount() {
        return imgs.length;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {

        ((ViewPager) container).removeView(mImageViews.get(position));

    }

    /**
     * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
     */
    @Override
    public Object instantiateItem(View container, int position) {
        ((ViewPager) container).addView(mImageViews.get(position), 0);

        return mImageViews.get(position);
    }



    public static interface OnInterfaceClickListener{

        public void onClickImgUrl(String url);
    }

    public OnInterfaceClickListener getOnInterfaceClickListener() {
        return onInterfaceClickListener;
    }

    public void setOnInterfaceClickListener(OnInterfaceClickListener onInterfaceClickListener) {
        this.onInterfaceClickListener = onInterfaceClickListener;
    }
}
