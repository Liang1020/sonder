package com.sonder.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.util.Constant;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.RecyleImageView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.domain.Feed;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/12/26.
 */

public class NewsAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Feed> feeds;
    private Point point;
    private LinearLayout.LayoutParams lpp;
    private SimpleDateFormat sdf;

    public NewsAdapter(Context context, ArrayList<Feed> feeds) {
        layoutInflater = LayoutInflater.from(context);

        this.context = context;
        this.feeds = feeds;
        this.point = Tool.getDisplayMetrics(context);
        this.lpp = new LinearLayout.LayoutParams(point.x, point.x);
        this.sdf = new SimpleDateFormat("dd MMM,yyyy");
    }


    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public Object getItem(int position) {
        return feeds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderNews viewHolderNews = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_news, null);
            viewHolderNews = new ViewHolderNews(convertView);
            convertView.setTag(viewHolderNews);
        } else {
            viewHolderNews = (ViewHolderNews) convertView.getTag();
        }


        final Feed feed = feeds.get(position);

        viewHolderNews.textViewLine1.setText(feed.getTitle());
        viewHolderNews.textViewLine2.setText(feed.getContent());

        final TextView tvContent = viewHolderNews.textViewLine2;
        final TextView tvReadMore = viewHolderNews.textViewReadMore;
        final TextView textViewTime2 = viewHolderNews.timeAgo2;
        final TextView textViewTime1 = viewHolderNews.timeAgo;

        final String type = feed.getArtcileType();

        switch (type) {
            case "1":
                viewHolderNews.textViewReadMore.setText(R.string.news_read_more);
                break;
            case "2":
                viewHolderNews.textViewReadMore.setText(R.string.news_jump_video);
                break;
            case "3":
                viewHolderNews.textViewReadMore.setText(R.string.news_jump_link);
                break;
        }

        viewHolderNews.textViewReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (type) {
                    case "1": {
                        int ml = tvContent.getMaxLines();
                        if (ml == 3) {

                            tvContent.setMaxLines(100);
                            tvReadMore.setText(context.getResources().getString(R.string.news_read_less));

                            textViewTime1.setVisibility(View.INVISIBLE);
                            textViewTime2.setVisibility(View.VISIBLE);

                        } else {
                            tvContent.setMaxLines(3);
                            tvReadMore.setText(context.getResources().getString(R.string.news_read_more));

                            textViewTime1.setVisibility(View.VISIBLE);
                            textViewTime2.setVisibility(View.INVISIBLE);
                        }

                    }
                    break;

                    case "2":
                    case "3":
                        Tool.startUrl(context, feed.getContent());
                        break;
                }


            }
        });


        // "created_at": "2016-10-03 12:00:00",

        try {
//            viewHolderNews.timeAgo.setReferenceTime(Constant.SDFD4.parse(feed.getCreated()).getTime());

            Date date = Constant.SDFD4.parse(feed.getCreated());

            String time = sdf.format(date);
            viewHolderNews.timeAgo.setText(time);
            viewHolderNews.timeAgo2.setText(time);


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.TAG, "parse crate time error newsadater:" + e.getLocalizedMessage());
        }

//        News list 的ui修改一下，然后news有个字段是type，如果type＝video link，type＝link  按前两个那么显示，跳转链接


        String imgUrl = feed.getImageUrl();
        viewHolderNews.textViewLineSource.setText(feed.getAuthor());
        viewHolderNews.rcyleImageViewNewsImg.setLayoutParams(this.lpp);

        if (StringUtils.isEmpty(imgUrl)) {
            Picasso.with(App.getApp()).load(R.drawable.icon_sonder).resize(400, 400).centerCrop().into(viewHolderNews.rcyleImageViewNewsImg);
        } else {
            Picasso.with(App.getApp()).load(imgUrl).config(Bitmap.Config.RGB_565).resize(400, 400).centerCrop().into(viewHolderNews.rcyleImageViewNewsImg);
        }


        return convertView;
    }

    public static class ViewHolderNews {


        @BindView(R.id.rcyleImageViewNewsImg)
        RecyleImageView rcyleImageViewNewsImg;

        @BindView(R.id.textViewLine1)
        TextView textViewLine1;

        @BindView(R.id.textViewLineSource)
        TextView textViewLineSource;

        @BindView(R.id.textViewLine2)
        TextView textViewLine2;

        @BindView(R.id.timeAgo)
        RelativeTimeTextView timeAgo;

        @BindView(R.id.textViewCount)
        TextView textViewCount;

        @BindView(R.id.timeAgo2)
        RelativeTimeTextView timeAgo2;

        @BindView(R.id.textViewReadMore)
        TextView textViewReadMore;


        public ViewHolderNews(View view) {
            ButterKnife.bind(this, view);
        }

    }

}
