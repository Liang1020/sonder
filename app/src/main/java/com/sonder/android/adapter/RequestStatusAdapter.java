package com.sonder.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.StringUtils;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.domain.RequestStatus;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.view.CircleImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/10.
 */

public class RequestStatusAdapter extends BaseAdapter {

    private Context context;
    private JSONArray requestStatuses;
    private LayoutInflater layoutInflater;
    private String ssc;
    private String lo;

    public RequestStatusAdapter(Context context, JSONArray jsonArray, String lo, String ssc) {

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.requestStatuses = jsonArray;
        this.ssc = ssc;
        this.lo = lo;

    }

    public void reloadData(JSONArray array) {
        this.requestStatuses = array;
        notifyDataSetChanged();
    }

    public void addPost(JSONObject jsonObject) {
        if(null==requestStatuses){
            requestStatuses=new JSONArray();
        }
        requestStatuses.put(jsonObject);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return requestStatuses.length();
    }

    @Override
    public Object getItem(int position) {


        try {
            return requestStatuses.getJSONObject(position);
        } catch (Exception e) {
        }

        return null;

    }

    @Override
    public long getItemId(int position) {


        return position;
    }


    @Override
    public int getViewTypeCount() {

        return 20;
    }


    @Override
    public int getItemViewType(int position) {
//        "1 = Free Text , 2 = Image , 3 = PDF , 4 = LO Assigned"
        int ptype = 0;
        try {
            ptype = requestStatuses.getJSONObject(position).getInt("post_type");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ptype;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderBase viewHolder = null;


        int type = getItemViewType(position);
        JSONObject jo = new JSONObject();


        try {
            jo = requestStatuses.getJSONObject(position);
        } catch (Exception e) {
            LogUtil.e(App.TAG, "get status item error");
        }


        if (null == convertView) {
            switch (type) {
                case RequestStatus.TYPE_PDF: {
                    convertView = layoutInflater.inflate(R.layout.item_status_file, null);
                    viewHolder = new ViewHolderFile(convertView);
                    convertView.setTag(viewHolder);
                }
                break;

                case RequestStatus.TYPE_IMG: {
                    convertView = layoutInflater.inflate(R.layout.item_status_img, null);
                    viewHolder = new ViewHolderImg(convertView);
                    convertView.setTag(viewHolder);
                }
                break;


                case RequestStatus.TYPE_TEXT: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }


                case RequestStatus.TYPE_LO_ASSIGNED: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }

                case RequestStatus.TYPE_STU_CRATE: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }


                case RequestStatus.TYPE_CLOSE: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }

                break;

                default: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }


            }
        } else {


            switch (type) {
                case RequestStatus.TYPE_PDF: {
                    viewHolder = (ViewHolderFile) convertView.getTag();

                }
                break;

                case RequestStatus.TYPE_IMG: {
                    viewHolder = (ViewHolderImg) convertView.getTag();

                }
                break;

                case RequestStatus.TYPE_TEXT: {
                    viewHolder = (ViewHolderText) convertView.getTag();

                }
                break;

                case RequestStatus.TYPE_LO_ASSIGNED: {
                    viewHolder = (ViewHolderText) convertView.getTag();
                }
                break;
                case RequestStatus.TYPE_STU_CRATE: {
                    viewHolder = (ViewHolderText) convertView.getTag();
                }
                break;

                case RequestStatus.TYPE_CLOSE: {
                    viewHolder = (ViewHolderText) convertView.getTag();
                }

                break;
                default: {
                    convertView = layoutInflater.inflate(R.layout.item_status_text, null);
                    viewHolder = new ViewHolderText(convertView);
                    convertView.setTag(viewHolder);
                }

            }

        }


        switch (type) {
            case RequestStatus.TYPE_PDF: {
                ViewHolderFile viewHolderFile = (ViewHolderFile) viewHolder;
                viewHolderFile.textViewTitle.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));

                try {
                    viewHolderFile.textViewSize.setText(int2M(jo.getLong("attachment_size")) + " M");
                } catch (JSONException e) {
                    e.printStackTrace();
                    viewHolderFile.textViewSize.setVisibility(View.INVISIBLE);
                    LogUtil.e(App.TAG, " attchment size error:" + e.getLocalizedMessage());
                }

            }
            break;

            case RequestStatus.TYPE_IMG: {
                ViewHolderImg viewHolderImg = (ViewHolderImg) viewHolder;
                viewHolderImg.textViewTime.setText(JsonHelper.getStringFromJson("created_at", jo));

            }
            break;

            case RequestStatus.TYPE_TEXT: {
                ViewHolderText vt = ((ViewHolderText) viewHolder);
                vt.textViewLabel.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));
//                vt.textViewTime.setText(jo.getString("created_at"));
                vt.textViewTime.setText(JsonHelper.getStringFromJson("created_at", jo));

            }
            break;

            case RequestStatus.TYPE_LO_ASSIGNED: {
                ViewHolderText vt = ((ViewHolderText) viewHolder);
                vt.textViewLabel.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));
            }
            break;

            case RequestStatus.TYPE_STU_CRATE: {
                ViewHolderText vt = ((ViewHolderText) viewHolder);
                vt.textViewLabel.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));
            }
            break;

            case RequestStatus.TYPE_CLOSE: {
                viewHolder = (ViewHolderText) convertView.getTag();
                ((ViewHolderText) viewHolder).textViewLabel.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));
            }
            break;

            default: {
                viewHolder = (ViewHolderText) convertView.getTag();
                ((ViewHolderText) viewHolder).textViewLabel.setText(Html.fromHtml(JsonHelper.getStringFromJson("content", jo)));
            }

        }


        //=============================================================================

//        String userLabel = "";
//
//        if (!StringUtils.isEmpty(ssc)) {
//            userLabel += context.getResources().getString(R.string.ssc_user_label) + ssc;
//        }
//
//        if (!StringUtils.isEmpty(lo)) {
//            userLabel += " " + context.getResources().getString(R.string.lo_user_label) + lo;
//        }
//
////        userLabel+=context.getResources().getString(R.string.studeng_user_label)+jo.getString();
//
//
        String authoType = JsonHelper.getStringFromJson("author_type", jo);
//
//        if ("student".equalsIgnoreCase(authoType)) {
//            userLabel += " " + context.getResources().getString(R.string.studeng_user_label) + JsonHelper.getStringFromJson("author", jo);
//        }


        viewHolder.textViewAutor.setText(authoType + ":" + JsonHelper.getStringFromJson("author", jo));

//        LogUtil.e(App.TAG, "request status adater:" + type);

        try {

            int h = 150;
            int w = 150;

            String headerImg = jo.getString("author_avatar");

//            LogUtil.i(App.TAG, "header img:" + headerImg);
            if (!StringUtils.isEmpty(headerImg)) {

                Picasso.with(App.getApp()).load(headerImg).
                        placeholder(R.drawable.icon_default_header).
                        config(Bitmap.Config.RGB_565).error(R.drawable.icon_default_header).resize(h, w).centerCrop().
                        into(viewHolder.imageViewHeader);
            } else {
                Picasso.with(App.getApp()).load(R.drawable.icon_default_header).resize(h, w).centerCrop().
                        into(viewHolder.imageViewHeader);
            }


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.TAG, "errors:" + e.getLocalizedMessage());
        }


        return convertView;
    }


    public class ViewHolderText extends ViewHolderBase {

        @BindView(R.id.textViewLabel)
        TextView textViewLabel;

        public ViewHolderText(View view) {
            ButterKnife.bind(this, view);

            this.imageViewHeader = (CircleImageView) view.findViewById(R.id.imageViewHeader);
            this.textViewTime = (TextView) view.findViewById(R.id.textViewTime);
            this.textViewAutor = (TextView) view.findViewById(R.id.textViewAutor);
        }
    }


    public class ViewHolderImg extends ViewHolderBase {


        @BindView(R.id.imageViewImg)
        ImageView imageViewImg;


        public ViewHolderImg(View view) {
            ButterKnife.bind(this, view);
            this.imageViewHeader = (CircleImageView) view.findViewById(R.id.imageViewHeader);
            this.textViewTime = (TextView) view.findViewById(R.id.textViewTime);
            this.textViewAutor = (TextView) view.findViewById(R.id.textViewAutor);
        }
    }

    public class ViewHolderFile extends ViewHolderBase {


        @BindView(R.id.imageViewFile)
        ImageView imageViewFile;

        @BindView(R.id.textViewSize)
        TextView textViewSize;

        @BindView(R.id.textViewTitle)
        TextView textViewTitle;


        public ViewHolderFile(View view) {
            ButterKnife.bind(this, view);
            this.imageViewHeader = (CircleImageView) view.findViewById(R.id.imageViewHeader);
            this.textViewTime = (TextView) view.findViewById(R.id.textViewTime);
            this.textViewAutor = (TextView) view.findViewById(R.id.textViewAutor);
        }
    }

    public String int2M(long len) {

        double m = len * 1.0f / 1024 / 1024;
        return String.valueOf(NumberHelper.formatNumber1((m)));

    }

}
