package com.sonder.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.StringUtils;
import com.sonder.android.R;
import com.sonder.android.domain.SimpleAddrss;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/7.
 */

public class AddressSimpleAdapterWithFavor extends BaseAdapter {


    private List<SimpleAddrss> addresses;
    private Context context;
    private int selectIndex = -1;
    private LayoutInflater layoutInflater;
    private OnFavorClickListener onFavorClickListener;
    private ArrayList<SimpleAddrss> arrayListCollected;

    public AddressSimpleAdapterWithFavor(Context context, List<SimpleAddrss> addresses) {
        this.addresses = addresses;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);

    }


    public void reloadData(SimpleAddrss simpleAddrss) {
        if (null == this.arrayListCollected) {
            this.arrayListCollected = new ArrayList<>();
        }

        if (isFavor(simpleAddrss)) {
            this.arrayListCollected.remove(simpleAddrss);
        } else {
            this.arrayListCollected.add(simpleAddrss);
        }


        notifyDataSetChanged();
    }


    public void removeColelctData(SimpleAddrss simpleAddrss) {
        if (isFavor(simpleAddrss)) {
            arrayListCollected.remove(simpleAddrss);
            notifyDataSetChanged();
        }

    }

    public void addSelectedData(SimpleAddrss simpleAddrss) {
        if (null == arrayListCollected) {
            arrayListCollected = new ArrayList<>();
        }
        arrayListCollected.add(simpleAddrss);
        notifyDataSetChanged();

    }


    public boolean isFavor(SimpleAddrss simpleAddrss) {

        if (null != arrayListCollected) {

            for (SimpleAddrss sa : arrayListCollected) {

                if (simpleAddrss.equals(sa)) {

                    if (StringUtils.isEmpty(simpleAddrss.getId())) {

                        simpleAddrss.setId(sa.getId());
                    }
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public int getCount() {
        return addresses.size();
    }


    public ArrayList<SimpleAddrss> getArrayListCollected() {
        return arrayListCollected;
    }

    public void setArrayListCollected(ArrayList<SimpleAddrss> arrayListCollected) {
        this.arrayListCollected = arrayListCollected;
    }

    @Override
    public Object getItem(int position) {
        return addresses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_address_favor, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final SimpleAddrss addres = addresses.get(position);

//        if (selectIndex == position) {
//            viewHolder.textViewAddrssLabel.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
//            viewHolder.textViewAddrssLabelLong.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
//        } else {
//            viewHolder.textViewAddrssLabel.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));
//            viewHolder.textViewAddrssLabelLong.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));
//        }

        viewHolder.textViewAddrssLabel.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));
        viewHolder.textViewAddrssLabelLong.setTextColor(context.getResources().getColor(R.color.text_color_ios_back));


//        String adminArea = addres.getAdminArea();
//        String subAdminArea = addres.getSubAdminArea();
//        String lity = addres.getLocality();
//        String sublity = addres.getSubLocality();

        String label = addres.getAddress();
        String[] ads = label.split(",");

        if (ads.length > 0) {
            viewHolder.textViewAddrssLabel.setText(ads[0].replace(",", ""));
            viewHolder.textViewAddrssLabelLong.setText(label.replace(ads[0] + ",", ""));
        } else {
            viewHolder.textViewAddrssLabel.setText(label);
        }

        viewHolder.viewFavor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onFavorClickListener) {
                    onFavorClickListener.onCLickFavor(AddressSimpleAdapterWithFavor.this, addres, position, isFavor(addres));
                }
            }
        });

        if (isFavor(addres)) {
            viewHolder.imageViewFavor.setImageResource(R.drawable.icon_star_collect);
        } else {
            viewHolder.imageViewFavor.setImageResource(R.drawable.icon_star_favour);
        }


        return convertView;
    }


    public SimpleAddrss getSelectAddrss() {
        if (-1 != selectIndex) {
            return addresses.get(selectIndex);
        }
        return null;

    }


    public void select(int postion) {
        this.selectIndex = postion;
        notifyDataSetChanged();
    }

    public OnFavorClickListener getOnFavorClickListener() {
        return onFavorClickListener;
    }

    public void setOnFavorClickListener(OnFavorClickListener onFavorClickListener) {
        this.onFavorClickListener = onFavorClickListener;
    }


    public static interface OnFavorClickListener {
        public void onCLickFavor(AddressSimpleAdapterWithFavor ada, SimpleAddrss simpleAddrss, int pos, boolean isContain);
    }


    public List<SimpleAddrss> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<SimpleAddrss> addresses) {
        this.addresses = addresses;
    }

    class ViewHolder {


        @BindView(R.id.textViewAddrssLabel)
        TextView textViewAddrssLabel;

        @BindView(R.id.textViewAddrssLabelLong)
        TextView textViewAddrssLabelLong;

        @BindView(R.id.viewFavor)
        View viewFavor;

        @BindView(R.id.imageViewFavor)
        ImageView imageViewFavor;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }


}
