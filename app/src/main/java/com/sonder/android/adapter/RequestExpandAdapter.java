package com.sonder.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.net.JsonHelper;
import com.sonder.android.utils.IncidentIdFormat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/22.
 */

public class RequestExpandAdapter extends BaseExpandableListAdapter {


    private ArrayList<JSONObject> jsonObjectsGroups;
    private ArrayList<ArrayList<JSONObject>> arrayListChilds;
    private Context context;
    private LayoutInflater layoutInflater;

    public RequestExpandAdapter(Context context, ArrayList<JSONObject> groups, ArrayList<ArrayList<JSONObject>> arrayListChilds) {
        this.context = context;
        this.jsonObjectsGroups = groups;
        this.arrayListChilds = arrayListChilds;
        this.layoutInflater = LayoutInflater.from(context);

    }


    @Override
    public int getGroupCount() {
        return jsonObjectsGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arrayListChilds.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return jsonObjectsGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return arrayListChilds.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {

        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return childPosition;
    }

    @Override
    public boolean hasStableIds() {


        return false;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ViewHolderSplit viewHolderSplit = null;

        JSONObject gop = jsonObjectsGroups.get(groupPosition);

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_incident_split, null);
            viewHolderSplit = new ViewHolderSplit(convertView);
            convertView.setTag(viewHolderSplit);
        } else {
            viewHolderSplit = (ViewHolderSplit) convertView.getTag();
        }

        String types = JsonHelper.getStringFromJson("type", gop);

        if ("opens".equals(types)) {
            viewHolderSplit.imageViewSplitIcon.setImageResource(R.drawable.icon_splite_ing);
            viewHolderSplit.textViewLabelSplit.setText(R.string.apply_splite_ing);
        } else {
            viewHolderSplit.imageViewSplitIcon.setImageResource(R.drawable.icon_splite_done);
            viewHolderSplit.textViewLabelSplit.setText(R.string.apply_splite_done);
        }


        if (isExpanded) {
            viewHolderSplit.imageViewIndicator.setImageResource(R.drawable.icon_indicator_open);
        } else {
            viewHolderSplit.imageViewIndicator.setImageResource(R.drawable.icon_indicator_close);
        }


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        JSONObject jsonObject = arrayListChilds.get(groupPosition).get(childPosition);

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_incident_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        try {

            viewHolder.textViewIncidentId.setText(IncidentIdFormat.formatINcidents(jsonObject.getString("incident_id")));
            viewHolder.textViewSummry.setText(jsonObject.getString("summary"));

//            "priority": "1 = Emergency , 2 = Distress , 3 = Incident , 4 = Virtual Concierge ",


            String prioriy = "";
            switch (JsonHelper.getStringFromJson("priority", jsonObject)) {
                case "1":
                    prioriy = "Emergency";
                    break;
                case "2":
                    prioriy = "Distress";
                    break;
                case "3":
                    prioriy = "Request";
                    break;
                case "4":
                    prioriy = "Virtual Concierge";
                    break;
            }


            viewHolder.textViewPriority.setText(prioriy);

            if (jsonObject.has("has_rating")) {

                viewHolder.textViewSummry.setTextColor(Color.parseColor("#424242"));

                if (0 == jsonObject.getInt("has_rating")) {
                    viewHolder.textViewStatus.setText(R.string.apply_rate_no);
                    viewHolder.textViewStatus.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.textViewStatus.setVisibility(View.INVISIBLE);
                }

            } else {
                viewHolder.textViewStatus.setVisibility(View.INVISIBLE);
                viewHolder.textViewSummry.setTextColor(context.getResources().getColor(R.color.colorInputTitle));
            }

            String startTime = JsonHelper.getStringFromJson("start_time", jsonObject);
            viewHolder.textViewDate.setText(startTime);


        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.e(App.TAG,"parse json exception in adapter:"+e.getLocalizedMessage());
        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    static class ViewHolder {


        @BindView(R.id.textViewSummry)
        TextView textViewSummry;

//        @BindView(R.id.viewSplit)
//        View viewSplit;

        @BindView(R.id.textViewStatus)
        TextView textViewStatus;


        @BindView(R.id.textViewIncidentId)
        TextView textViewIncidentId;

        @BindView(R.id.textViewPriority)
        TextView textViewPriority;

        @BindView(R.id.textViewDate)
        TextView textViewDate;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderSplit {

        @BindView(R.id.imageViewSplitIcon)
        ImageView imageViewSplitIcon;

        @BindView(R.id.textViewLabelSplit)
        TextView textViewLabelSplit;


        @BindView(R.id.imageViewIndicator)
        ImageView imageViewIndicator;


//        @BindView(R.id.viewSplit1)
//        View viewSplit1;
//
//        @BindView(R.id.viewSplit2)
//        View viewSplit2;

        public ViewHolderSplit(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
