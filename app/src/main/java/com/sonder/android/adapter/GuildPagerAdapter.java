package com.sonder.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.common.util.SharePersistent;
import com.sonder.android.App;
import com.sonder.android.R;

import java.util.ArrayList;

public class GuildPagerAdapter extends PagerAdapter {


    private ArrayList<View> arrayListViews;

    private OnSkipOrStartClicKListener onSkipOrStartClicKListener;

    public GuildPagerAdapter(final Context context, boolean fromSetting) {
        this.arrayListViews = new ArrayList<>();

        LayoutInflater layoutInflater = LayoutInflater.from(context);

        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onSkipOrStartClicKListener) {
                    onSkipOrStartClicKListener.onClickSkip();
                }

            }
        };


        View v1 = layoutInflater.inflate(R.layout.bg_guild1, null);
        arrayListViews.add(v1);

        View v2 = layoutInflater.inflate(R.layout.bg_guild2, null);
        arrayListViews.add(v2);

        View v3 = layoutInflater.inflate(R.layout.bg_guild3, null);
        arrayListViews.add(v3);

        View v4 = layoutInflater.inflate(R.layout.bg_guild4, null);
        arrayListViews.add(v4);

        View v5 = layoutInflater.inflate(R.layout.bg_guild5, null);
        arrayListViews.add(v5);

        View v6 = layoutInflater.inflate(R.layout.bg_guild6, null);
        arrayListViews.add(v6);

        View v7 = layoutInflater.inflate(R.layout.bg_guild7, null);

        Button button = (Button) v7.findViewById(R.id.buttonEnter);

        if (fromSetting) {
            button.setText(context.getResources().getString(R.string.custom_back));
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharePersistent.saveBoolean(App.getApp(), App.KEY_GUILD, true);
                ((Activity) context).setResult(Activity.RESULT_OK);
                ((Activity) context).finish();
            }
        });
        arrayListViews.add(v7);


    }


    @Override
    public int getCount() {
        return arrayListViews.size();
    }

    // 来判断显示的是否是同一张图片，这里我们将两个参数相比较返回即可
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    // PagerAdapter只缓存三张要显示的图片，如果滑动的图片超出了缓存的范围，就会调用这个方法，将图片销毁
    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView(arrayListViews.get(position));
    }

    // 当要显示的图片可以进行缓存的时候，会调用这个方法进行显示图片的初始化，我们将要显示的ImageView加入到ViewGroup中，然后作为返回值返回即可
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        view.addView(arrayListViews.get(position));
        return arrayListViews.get(position);
    }


    public static interface OnSkipOrStartClicKListener {
        public void onClickSkip();
    }

    public OnSkipOrStartClicKListener getOnSkipOrStartClicKListener() {
        return onSkipOrStartClicKListener;
    }

    public void setOnSkipOrStartClicKListener(OnSkipOrStartClicKListener onSkipOrStartClicKListener) {
        this.onSkipOrStartClicKListener = onSkipOrStartClicKListener;
    }
}