package com.sonder.android.adapter;

import android.widget.TextView;

import com.sonder.android.view.CircleImageView;


/**
 * Created by wangzy on 2016/11/10.
 */

public class ViewHolderBase {

    CircleImageView imageViewHeader;
    TextView textViewTime;
    TextView textViewAutor;

}
