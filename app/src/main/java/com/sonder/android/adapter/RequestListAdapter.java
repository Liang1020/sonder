package com.sonder.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.adapter.CommonBaseAdapter;
import com.common.util.LogUtil;
import com.sonder.android.App;
import com.sonder.android.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wangzy on 2016/11/16.
 */

public class RequestListAdapter extends CommonBaseAdapter {

    JSONArray jsonArray;

    public RequestListAdapter(Context context, JSONArray jsonArray) {
        super(context);
        this.jsonArray = jsonArray;

    }


    @Override
    public int getCount() {
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return jsonArray.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {

        try {
            JSONObject jos = jsonArray.getJSONObject(position);


            if (jos.has("split")) {
                return TYPE_SPLIT;
            } else {
                return TYPE_NORMAL;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return TYPE_NORMAL;

    }


    @Override
    public int getViewTypeCount() {

        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder = null;
        ViewHolderSplit viewHolderSplit = null;


        int type = getItemViewType(position);
        try {

            JSONObject jsonObject = jsonArray.getJSONObject(position);


            if (null == convertView) {

                if (type == TYPE_NORMAL) {
                    convertView = layoutInflater.inflate(R.layout.item_incident_list, null);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);

                } else {
                    convertView = layoutInflater.inflate(R.layout.item_incident_split, null);
                    viewHolderSplit = new ViewHolderSplit(convertView);
                    convertView.setTag(viewHolderSplit);
                }


            } else {


                if (type == TYPE_NORMAL) {
                    viewHolder = (ViewHolder) convertView.getTag();
                } else {
                    viewHolderSplit = (ViewHolderSplit) convertView.getTag();
                }

            }

            //==========================
            if (type == TYPE_NORMAL) {

                viewHolder.textViewSummry.setText(jsonObject.getString("summary"));

                if (jsonObject.has("has_rating")) {

                    viewHolder.textViewSummry.setTextColor(Color.parseColor("#424242"));
                    if (0 == jsonObject.getInt("has_rating")) {
                        viewHolder.textViewStatus.setText(R.string.apply_rate_no);
                        viewHolder.textViewStatus.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.textViewStatus.setVisibility(View.INVISIBLE);
                    }

                } else {
                    viewHolder.textViewStatus.setVisibility(View.INVISIBLE);
                    viewHolder.textViewSummry.setTextColor(context.getResources().getColor(R.color.colorInputTitle));
                }


                if (getItemViewType(position + 1) == TYPE_SPLIT) {

                    viewHolder.viewSplit.setVisibility(View.GONE);
                } else {
                    viewHolder.viewSplit.setVisibility(View.VISIBLE);
                }

            } else {
                int stats = jsonObject.getInt("split");
                if (0 == stats) {
                    viewHolderSplit.imageViewSplitIcon.setImageResource(R.drawable.icon_splite_ing);
                    viewHolderSplit.textViewLabelSplit.setText(R.string.apply_splite_ing);
                } else {
                    viewHolderSplit.imageViewSplitIcon.setImageResource(R.drawable.icon_splite_done);
                    viewHolderSplit.textViewLabelSplit.setText(R.string.apply_splite_done);
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.TAG, " Request adapter:error:" + e.getLocalizedMessage());

        }


        return convertView;
    }


    static class ViewHolder {


        @BindView(R.id.textViewSummry)
        TextView textViewSummry;

        @BindView(R.id.viewSplit)
        View viewSplit;

        @BindView(R.id.textViewStatus)
        TextView textViewStatus;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderSplit {

        @BindView(R.id.imageViewSplitIcon)
        ImageView imageViewSplitIcon;

        @BindView(R.id.textViewLabelSplit)
        TextView textViewLabelSplit;

        @BindView(R.id.viewSplit1)
        View viewSplit1;

        @BindView(R.id.viewSplit2)
        View viewSplit2;

        public ViewHolderSplit(View view) {
            ButterKnife.bind(this, view);
        }
    }


    public int TYPE_NORMAL = 0;
    public int TYPE_SPLIT = 1;

}
