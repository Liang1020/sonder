package com.sonder.android.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.common.util.LogUtil;
import com.sonder.android.App;

import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class UpdateLocationService extends IntentService {


    public static final String ACTION_UPDTE_REGULAR = "com.sonder.android.REGULAR_UPDATE";
    public static final String ACTION_UPDTE_STAND_REQUEST = "com.sonder.android.STAND_REQUEST";
    public static final String ACTION_UPDTE_DISTRESS_REQUEST = "com.sonder.android.DISTRESS_REQUEST";
    public static final String ACTION_UPDTE_GUARDIAN_REQUEST = "com.sonder.android.GUARDIAN_REQUEST";
    public static final String ACTION_UPDTE_EMERGENCY_REQUEST = "com.sonder.android.EMERGENCY_REQUEST";

    public static final String PARAM = "param";
    public static final String PARAM_POSITIVE = "1";
    public static final String PARAM_NEGTIVE = "0";

    private static HashMap<String, UpdateLocationBundle> hashMapTask = new HashMap<>();

    public UpdateLocationService() {
        super("UpdateLocationService");

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (null == hashMapTask) {
            this.hashMapTask = new HashMap<>();
        }

        if (intent != null) {
            final String action = intent.getAction();
            final String param = intent.getStringExtra(PARAM);
            processUpdateTask(action, param);
        }
    }

    private void processUpdateTask(String action, String param) {
        UpdateLocationBundle taskBundle = hashMapTask.get(action);

        if (!PARAM_NEGTIVE.equals(param)) {//create task
            if (null == taskBundle) {
                UpdateLocationBundle updateLocationBund = new UpdateLocationBundle(action);
                hashMapTask.put(updateLocationBund.getAction(), updateLocationBund);
            }
        } else {//stop
            if (null != taskBundle) {
                LogUtil.i(App.TAG, " remove processUpdateTask:" + action);
                hashMapTask.remove(action).kilTask();
            }
        }
    }

    @Override
    public void onDestroy() {

        LogUtil.i(App.TAG,"update location service destory.-====");
        super.onDestroy();


    }

    public static void requestServiceWithActionAndParam(Context context, String action, boolean positiveOrNegtive) {
        startServiceWithParam(context, action, positiveOrNegtive ? PARAM_POSITIVE : PARAM_NEGTIVE);
    }

    public static void startUpdateLocation(Context context) {
        startServiceWithParam(context, ACTION_UPDTE_REGULAR, PARAM_POSITIVE);
    }

    public static void stopUpldateLocation(Context context) {
        startServiceWithParam(context, ACTION_UPDTE_REGULAR, PARAM_NEGTIVE);
    }

    private static void startServiceWithParam(Context context, String action, String param) {
        Intent intent = new Intent(context, UpdateLocationService.class);
        intent.setAction(action);
        intent.putExtra(PARAM, param);
        context.startService(intent);
    }

    private class UpdateLocationBundle {

        private String action;
        private Timer timer;
        private UpdateTimerTask timerTask;
        private long peroid;

        public UpdateLocationBundle(String action) {
            this.action = action;
            this.timer = new Timer();
            this.timerTask = new UpdateTimerTask(action);

            switch (action) {
                case ACTION_UPDTE_REGULAR:
                    peroid = 1 * 1000 * 60 * 60;
                    break;
                case ACTION_UPDTE_STAND_REQUEST:
                    peroid = 1 * 1000 * 60 * 10;
                    break;
                case ACTION_UPDTE_DISTRESS_REQUEST:
                    peroid = 1 * 1000 * 10;
                    break;
                case ACTION_UPDTE_GUARDIAN_REQUEST:
                    peroid = 1 * 1000 * 10;
                    break;
                case ACTION_UPDTE_EMERGENCY_REQUEST:
                    peroid = 1 * 1000 * 10;
                    break;
            }

            LogUtil.e(App.TAG, "schedule task:" + action + " with peroid:" + peroid);
            this.timer.schedule(timerTask, 0, peroid);
        }

        public String getAction() {
            return action;
        }


        public void kilTask() {
            if (null != timerTask) {
                timerTask.cancel();
            }
            if (null != timer) {
                timer.cancel();
                timer.purge();
            }

            LogUtil.i(App.TAG, "kill task:" + action);
        }

    }

    private class UpdateTimerTask extends TimerTask {

        private String action;

        public UpdateTimerTask(String action) {
            super();
            this.action = action;
        }

        @Override
        public void run() {
            LogUtil.i(App.TAG, "update location with action:" + action + (new Date()).toString());
            App.getApp().updateLocation();
        }
    }
}
