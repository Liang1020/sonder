package com.sonder.android.service;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.IntentService;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.view.WindowManager;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.domain.Angel;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by wangzy on 2017/1/3.
 */

public class AlarmIntentServce extends IntentService {


    static String name = "AlarmIntentServce";

    Handler hanlder = null;
    Dialog dialog;


    private int totalCount = 0;
    private int execCount = 0;
    private int checkDuration = 0;
    private int checkFreq = 0;
    private boolean isLastExcute = false;


    public AlarmIntentServce() {
        super(name);
    }


    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        LogUtil.i(App.TAG, "onStart=====");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i(App.TAG, "onStartCommand=====");
        return super.onStartCommand(intent, flags, startId);
    }


    private Timer timer = new Timer();
    private TimerTask timerTask = null;

    @Override
    protected void onHandleIntent(Intent intent) {
        if (null == hanlder) {
            hanlder = new Handler(getMainLooper());
        }
        isLastExcute = false;

        final Angel angel = App.getApp().getAngle();
        if (null != angel) {

            String checkin = angel.getCheckInTime();
            checkDuration = Integer.parseInt(angel.getDuration());
            checkFreq = Integer.parseInt(angel.getNotification_every());
            String checkOutTime = angel.getCheckOutTime();

            totalCount = checkDuration / checkFreq;
            initTimerWithSourceSettig(checkDuration, checkFreq, checkOutTime);
        }
    }


    private void initTimerWithSourceSettig(int checkDuration, int checkFreq, String checkOutTime) {
        if ((checkDuration > checkFreq) && (StringUtils.isEmpty(checkOutTime))) {
            if (null == timer) {
                timer = new Timer();
            }

            timerTask = new TimerTask() {
                @Override
                public void run() {
                    Angel tangle = App.getApp().getAngle();
                    if (null != tangle && isLastExcute == false) {
                        execCount++;
                        showDialog(false);
                        LogUtil.i(App.TAG, "show dialog from background");

                        if (execCount == totalCount) {
                            killTimerAndTask(timer, timerTask);
                        }
                    } else {
                        killTimerAndTask(timer, timerTask);
                    }

                }
            };

            int time = checkFreq*60*1000;
            timer.schedule(timerTask, time, time);

            final Timer terminalTimer = new Timer();
            terminalTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    showDialog(true);
                }
            }, checkDuration * 60 * 1000);


        }
    }

    private void killTimerAndTask(Timer timer, TimerTask task) {
        task.cancel();
        timer.purge();
        timer.cancel();
    }

    private void initTimerWith30Minutes() {

        if ((checkDuration > checkFreq)) {
            if (null != timer) {
                killTimerAndTask(timer, timerTask);
            }

            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    Angel tangle = App.getApp().getAngle();
                    if (null != tangle && isLastExcute == false) {
                        execCount++;
                        showDialog(false);
                        LogUtil.i(App.TAG, "show dialog from background");
                        if (execCount == totalCount) {
                            killTimerAndTask(timer, timerTask);
                        }
                    } else {
                        killTimerAndTask(timer, timerTask);
                    }

                }
            };

            int time = 30 * 60 * 1000;
            timer.schedule(timerTask, time, time);

            LogUtil.i(App.TAG, "chagne notify to every 30 minute");
        }
    }


    private boolean clickYes = false;

    private void showDialog(final boolean isLast) {
        hanlder.post(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage(R.string.check_in_check_in);
                builder.setIcon(R.drawable.icon_support_location);
                builder.setNegativeButton(R.string.common_postive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clickYes = true;
                        LogUtil.i(App.TAG, "dialog ok click");
                    }
                });
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        LogUtil.i(App.TAG, "dismiss:" + clickYes + " rcount:" + execCount);//when click
                        if (clickYes == false) {//change notify every 30 minutes
                            killTimerAndTask(timer, timerTask);
                            initTimerWith30Minutes();
                        }
                    }
                });
                dialog = builder.create();
                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.show();

                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.i(App.TAG, "play sound eceptino:" + e.getLocalizedMessage());
                }

                clickYes = false;
                isLastExcute = isLast;

                if (isLastExcute) {
                    killTimerAndTask(timer, timerTask);
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        if (null != dialog) {
            dialog.dismiss();
        }
        super.onDestroy();
    }

    public static void startService(Context context) {
        Intent intent = new Intent(context, AlarmIntentServce.class);
        context.startService(intent);
    }

    public void permission(final Dialog dialog) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivity(intent);
                return;
            } else {
                dialog.show();
            }
        } else {
            dialog.show();
        }
    }


}
