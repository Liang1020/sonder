package com.sonder.android.utils;

import com.common.util.StringUtils;

/**
 * Created by wangzy on 2016/12/9.
 */

public class IncidentIdFormat {


    public static String formatINcidents(String incidentId) {

        int idl = StringUtils.computStrlen(incidentId);

        StringBuffer pre=new StringBuffer();
        for(int i=0;i<(5-idl) && idl>=0;i++){
            pre.append("0");
        }

        return "R-"+pre.toString()+incidentId;

    }

}

