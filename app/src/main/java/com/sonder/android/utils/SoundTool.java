package com.sonder.android.utils;

import android.media.MediaPlayer;

import com.sonder.android.App;


/**
 * Created by wangzy on 16/10/27.
 */

public class SoundTool {


    static MediaPlayer mediaPlayer;


    public static void playSound(int sound) {

        if (null == mediaPlayer) {
            mediaPlayer = MediaPlayer.create(App.getApp(), sound);

        }

        mediaPlayer.start();

    }


    public static void stopPlay() {
        try {
            if (null != mediaPlayer && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();

                try {
                    mediaPlayer.prepare();
                    mediaPlayer.seekTo(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }

    }

    public static void releasePlayer() {

        if (null != mediaPlayer) {
            try {

                if(mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }

                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();

            }

        }


    }

}
