package com.sonder.android.net;

import android.content.Context;

import com.common.net.FormFile;
import com.common.net.HttpRequester;
import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.sonder.android.App;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by leocx on 2016/10/28.
 */

public class NetInterface {


    //product
    private static String base_path = "https://api.sonderaustralia.com/api/student/";

    //test
//    private static String base_path = "https://apitest.sonderaustralia.com/api/student/";


    public static NetResult updateLocaiton(JSONObject jsonObject) throws Exception {
        String path = "information/update_my_location";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;

    }

    public static NetResult endWalk() throws Exception {
        String path = "walk_with_me/end";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), new JSONObject(), base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult checkWalk() throws Exception {
        String path = "walk_with_me/walking";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWalk(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult startWalk(JSONObject jsonObject) throws Exception {
        String path = "walk_with_me/start";

        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult updateLocation(JSONObject jsonObject) throws Exception {
        String path = "information/update_my_location";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), new JSONObject(), base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult removefavorAddress(String id) throws Exception {
        String path = "favorite_address/" + id;
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), new JSONObject(), base_path + path, HttpRequester.REQUEST_DELETE);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult favorAddress(JSONObject jsonObject) throws Exception {
        String path = "favorite_address";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {

            NetResult netResult = JsonHelper.parseAddress(JsonHelper.getJsonFromInputStream(jsonInputStream));

            return netResult;
        }
        return null;
    }


    public static NetResult getFavorAddress() throws Exception {
        String path = "favorite_address";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseAddresses(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult checkOut(JSONObject jsonObject) throws Exception {
        String path = "angel/check_out";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(), jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult waittingCheckOut() throws Exception {
        String path = "angel/waiting_check_out";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseAngle(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult checkIn(JSONObject jsonObject) throws Exception {
        String path = "angel/check_in";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseAngle(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult getFeed(JSONObject jsonObject) throws Exception {
        String path = "feed";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithFeed(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult getUnpaidFeed(JSONObject jsonObject) throws Exception {
        String path = "feed/unpaid";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithFeed(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult resetPassword(JSONObject jsonObject) throws Exception {
        String path = "reset_password";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult forgotPwd(JSONObject jsonObject) throws Exception {
        String path = "forgot_password";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }

    public static NetResult index() throws Exception {
        String path = "app/index";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult rateIncident(JSONObject jsonObject) throws Exception {
        String path = "incident/rating";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult closeIncident(String id) throws Exception {
        String path = "incident/close/" + id;
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                new JSONObject(), base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult getIncidentList() throws Exception {
        String path = "incident/list";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult getStatusList(String id) throws Exception {
        String path = "incident/get/" + id;
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseWithData(JsonHelper.getJsonFromInputStream(jsonInputStream));
            return netResult;
        }
        return null;
    }


    public static NetResult createIncident(HashMap<String, String> map, FormFile[] files) throws Exception {
        String path = "incident/create";
        String jsonInputStream = createCallBackPost(map, files, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult postCreate(HashMap<String, String> map, FormFile[] files) throws Exception {
        String path = "incident/post/create";
        String jsonInputStream = createCallBackPost(map, files, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult uploadHeaderOrPassportFile(FormFile[] files) throws Exception {
        String path = "information/avatar";
        String jsonInputStream = createCallBackPost(new HashMap<String, String>(), files, base_path + path);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseCommon(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult updateProfile(JSONObject jsonObject) throws Exception {
//        String path = "information/travel";
        String path = "information/contact";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseUpdateInfo(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult updateProfileAll(JSONObject jsonObject) throws Exception {
        String path = "information/all";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseUpdateAll(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult upldateEducationInfo(JSONObject jsonObject) throws Exception {
//        String path = "information/travel";
        String path = "information/education";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseUpdateAll(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult login(JSONObject jsonObject) throws Exception {
        String path = "login";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseLogin(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getStudentInfo(JSONObject jsonObject) throws Exception {
        String path = "information/my";
        InputStream jsonInputStream = createCallBackInputStream(App.getApp().getBaseContext(),
                jsonObject, base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseStudentInfo(jsonInputStream);
            return netResult;
        }
        return null;
    }

    //-------------------------------------------------------------//
    private static InputStream createCallBackInputStream(Context context, JSONObject customizedObject, String path, String requestType)
            throws Exception {
        JSONObject commonObject = new JSONObject();

        commonObject.put("app_version", Tool.getVersionName(context));
        commonObject.put("client_type", "0");
        commonObject.put("deviceId", Tool.getImei(context));

        customizedObject.put("common", commonObject);

        String jsonRequest = customizedObject.toString();

        //=========================

        String token = null;
//        if (null != App.getApp().getUser() && !StringUtils.isEmpty(App.getApp().getApiToken())) {
        token = App.getApp().getApiToken();
//        }


        LogUtil.i(App.TAG, "path:" + path);
        LogUtil.i(App.TAG, "" + jsonRequest);
        LogUtil.i(App.TAG, "======token=======");
        LogUtil.e(App.TAG, "" + token);
        LogUtil.i(App.TAG, "======token end=======");
        InputStream jsonInputStream = HttpRequester.doHttpRequestText(context, path, jsonRequest, token, requestType);
        return jsonInputStream;
    }

    private static String createCallBackPost(HashMap<String, String> paramMap, FormFile[] files, String path)
            throws Exception {

        String token = App.getApp().getApiToken();

        LogUtil.i(App.TAG, "path:" + path);
        LogUtil.i(App.TAG, "======token=======");
        LogUtil.e(App.TAG, "" + token);
        LogUtil.i(App.TAG, "======token end=======");

        LogUtil.e(App.TAG, "=================参数列表========================");

        for (Map.Entry<String, String> entry : paramMap.entrySet()) {

            LogUtil.i(App.TAG, entry.getKey() + " = " + entry.getValue());
        }
        LogUtil.e(App.TAG, "=========================================");

        String resonse = HttpRequester.postUpload(path, paramMap, files, token);
        return resonse;
    }

}
