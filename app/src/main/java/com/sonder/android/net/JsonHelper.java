package com.sonder.android.net;

import com.common.net.NetResult;
import com.common.util.Constant;
import com.common.util.DateTool;
import com.common.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sonder.android.App;
import com.sonder.android.domain.Angel;
import com.sonder.android.domain.Feed;
import com.sonder.android.domain.Profile;
import com.sonder.android.domain.SimpleAddrss;
import com.sonder.android.domain.Student;
import com.sonder.android.domain.Walk;
import com.sonder.android.manager.DateManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by leocx on 2016/10/28.
 */

public class JsonHelper {

    public static final String TAG_CODE = "status_code";
    public static final String TAG_DATA = "data";
    public static final String TAG_MESSAGE = "message";



    public static NetResult parseWalk(String json) throws Exception {

        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);

        NetResult netResult = getNetResultNoData(jsonObject);

        if (netResult.isOk()) {
            JSONObject dataObj = (jsonObject.getJSONObject(TAG_DATA));
            Gson gson = new Gson();
            Walk walk = gson.fromJson(dataObj.toString(), Walk.class);
            netResult.setData(new Object[]{walk});
        }
        return netResult;
    }


    public static NetResult parseAddress(String json) throws Exception {

        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);

        NetResult netResult = getNetResultNoData(jsonObject);

        if (netResult.isOk()) {
            JSONObject dataObj = (jsonObject.getJSONObject(TAG_DATA));
            Gson gson = new Gson();
            SimpleAddrss simpleAddrss = gson.fromJson(dataObj.toString(), SimpleAddrss.class);
            netResult.setData(new Object[]{simpleAddrss});
        }
        return netResult;
    }


    public static NetResult parseAddresses(String json) throws Exception {

        LogUtil.i(App.TAG, "response:" + json);

        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = getNetResultNoData(jsonObject);
        if (netResult.isOk()) {

            ArrayList<SimpleAddrss> simpleAddrsses = new ArrayList<>();

            JSONArray datas = jsonObject.getJSONArray(TAG_DATA);

            if (null != datas && datas.length() > 0) {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<SimpleAddrss>>() {
                }.getType();
                simpleAddrsses = gson.fromJson(datas.toString(), type);

            }

            netResult.setData(new Object[]{simpleAddrsses});

        }

        return netResult;

    }


    public static NetResult parseAngle(String json) throws Exception {
        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = getNetResultNoData(jsonObject);
        if (netResult.isOk()) {
            JSONObject angle = jsonObject.getJSONObject(TAG_DATA);
            if (null != angle) {
                netResult.setData(new Object[]{parseAngle(angle)});
            }
        }

        return netResult;

    }

    private static Angel parseAngle(JSONObject angleObject) {

        Angel angel = new Angel();

        angel.setAngleId(getStringFromJson("angel_id", angleObject));
        angel.setStudentId(getStringFromJson("student_id", angleObject));
        angel.setStatus(getStringFromJson("status", angleObject));
        angel.setLat(getStringFromJson("location_lat", angleObject));
        angel.setLon(getStringFromJson("location_lon", angleObject));
        angel.setAddress(getStringFromJson("address", angleObject));
        angel.setDuration(getStringFromJson("duration", angleObject));


        // "2017-01-04 15:51:40",

        String beforInTime = getStringFromJson("check_in_time", angleObject);
        String timeCheckIn = DateTool.utc2Local(beforInTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

        angel.setCheckInTime(timeCheckIn);

        String beforOutTime = getStringFromJson("check_out_time", angleObject);


        String timeCheckOut = DateTool.utc2Local(beforOutTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
        angel.setCheckOutTime(timeCheckOut);


        angel.setNotification_every(getStringFromJson("notification_every", angleObject));
        angel.setCrateAt(getStringFromJson("created_at", angleObject));

        return angel;


    }


    public static NetResult parseWithFeed(String json) throws Exception {
        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = getNetResultNoData(jsonObject);

        if (netResult.isOk()) {

            ArrayList<Feed> feeds = new ArrayList<>();
            JSONArray array = jsonObject.getJSONArray(TAG_DATA);

            if (null != array && array.length() > 0) {
                for (int i = 0, isize = array.length(); i < isize; i++) {

                    JSONObject jos = array.getJSONObject(i);

                    Feed fed = new Feed();

                    fed.setArticleId(getStringFromJson("article_id", jos));
                    fed.setArtcileType(getStringFromJson("article_type", jos));
                    fed.setTitle(getStringFromJson("title", jos));
                    fed.setContent(getStringFromJson("content", jos));
                    fed.setImageUrl(getStringFromJson("image_url", jos));
                    fed.setCreated(getStringFromJson("created_at", jos));

                    fed.setAuthor(getStringFromJson("author", jos));

                    feeds.add(fed);
                }
            }

            netResult.setData(new Object[]{feeds});
        }

        return netResult;
    }


    public static NetResult parseWithData(String json) throws Exception {
        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = getNetResult(jsonObject);
        return netResult;
    }


    public static NetResult parseUpdateInfo(InputStream jsonInputStream) throws Exception {
        String result = getJsonFromInputStream(jsonInputStream);
        LogUtil.i(App.TAG, "response:" + result);
        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = getNetResult(jsonObject);
        return netResult;
    }

    public static NetResult parseCommon(InputStream jsonInputStream) throws Exception {
        String result = getJsonFromInputStream(jsonInputStream);
        LogUtil.i(App.TAG, "response:" + result);
        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = getNetResult(jsonObject);
        return netResult;
    }

    public static NetResult parseCommon(String json) throws Exception {
        LogUtil.i(App.TAG, "response:" + json);
        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = getNetResult(jsonObject);
        return netResult;
    }

    public static NetResult parseUpdateAll(InputStream jsonInputStream) throws Exception {


        String result = getJsonFromInputStream(jsonInputStream);
        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = getNetResult(jsonObject);
        JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

        Student student = parseStudent(dataObject);
        netResult.setData(new Object[]{student});

        return netResult;
    }


    public static NetResult parseStudentInfo(InputStream jsonInputStream) throws Exception {
        String result = getJsonFromInputStream(jsonInputStream);
        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = getNetResult(jsonObject);
        if (netResult.isOk()) {
            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
            Student student = parseStudent(dataObject);
            netResult.setData(new Object[]{student});
        }
        return netResult;
    }


    public static Student parseStudentInfo(String json) throws Exception {
        JSONObject jsonObject = new JSONObject(json);
        Student student = parseStudent(jsonObject);
        return student;
    }


    /**
     * @param jsonInputStream
     * @return netResult
     * data[0] String-->token
     * data[1] Student-->student info
     * @throws Exception
     */
    public static NetResult parseLogin(InputStream jsonInputStream) throws Exception {
        String result = getJsonFromInputStream(jsonInputStream);

        LogUtil.i(App.TAG, "login json return:" + result);


        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = getNetResult(jsonObject);
        if (netResult.isOk()) {
            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
            String token = getStringFromJson("token", dataObject);
            JSONObject studentObject = dataObject.getJSONObject("student");
            Student student = new Student();
            student.setStudentId(getIntFromJson("student_id", studentObject));
            student.setEmail(getStringFromJson("email", studentObject));
            student.setStatus(getIntFromJson("status", studentObject));
            student.setFullName(getStringFromJson("full_name", studentObject));
            student.setPreferredName(getStringFromJson("preferred_name", studentObject));
            student.setDob(getDateFromJson("dob", studentObject));
            student.setAddress(getStringFromJson("address", studentObject));
            student.setLastKnownLat(getLongFromJson("last_known_lat", studentObject));
            student.setLastKnownLon(getLongFromJson("last_known_lon", studentObject));
            student.setLastKnownAddress(getStringFromJson("last_known_address", studentObject));
            student.setPlanLevel(getIntFromJson("plan_level", studentObject));
            student.setPlanExpiry(getDateFromJson("plan_expiry", studentObject));
            student.setAvatar(getStringFromJson("avatar", studentObject));
            student.setPhoneNumber(getStringFromJson("phone_number", studentObject));
            student.setGender(getIntFromJson("gender", studentObject));
            student.setProfileCompletion(getIntFromJson("profile_completion", studentObject));
            student.setLastLoginTime(getDateFromJson("last_login_time", studentObject));
            student.setRegisterCity(getStringFromJson("register_city", studentObject));
            student.setEducationAgent(getStringFromJson("education_agent", studentObject));
            student.setCreatedAt(getDateFromJson("created_at", studentObject));
            student.setUpdateAt(getDateFromJson("updated_at", studentObject));
            netResult.setData(new Object[]{token, student});
        }
        return netResult;
    }


    private static Student parseStudent(JSONObject dataObject) throws JSONException {

        Student student = new Student();
        student.setStudentId(getIntFromJson("student_id", dataObject));
        student.setEmail(getStringFromJson("email", dataObject));
        student.setStatus(getIntFromJson("status", dataObject));
        student.setFullName(getStringFromJson("full_name", dataObject));
        student.setPreferredName(getStringFromJson("preferred_name", dataObject));
        student.setDob(getDayFromJson("dob", dataObject));
        student.setAddress(getStringFromJson("address", dataObject));
        student.setLastKnownLat(getLongFromJson("last_known_lat", dataObject));
        student.setLastKnownLon(getLongFromJson("last_known_lon", dataObject));
        student.setLastKnownAddress(getStringFromJson("last_known_address", dataObject));
        student.setPlanLevel(getIntFromJson("plan_level", dataObject));
        student.setPlanExpiry(getDateFromJson("plan_expiry", dataObject));
        student.setAvatar(getStringFromJson("avatar", dataObject));
        student.setPhoneNumber(getStringFromJson("phone_number", dataObject));
        student.setGender(getIntFromJson("gender", dataObject));
        student.setProfileCompletion(getIntFromJson("profile_completion", dataObject));
        student.setLastLoginTime(getDateFromJson("last_login_time", dataObject));
        student.setRegisterCity(getStringFromJson("register_city", dataObject));
        student.setEducationAgent(getStringFromJson("education_agent", dataObject));
        student.setCreatedAt(getDateFromJson("created_at", dataObject));
        student.setUpdateAt(getDateFromJson("updated_at", dataObject));

        JSONObject profileOject = dataObject.getJSONObject("profile");

        Profile profile = new Profile();

        profile.setProfileId(getIntFromJson("profile_id", profileOject));
        profile.setStudentId(getStringFromJson("student_id", profileOject));
        profile.setEducationStudentId(getStringFromJson("education_student_id", profileOject));
        profile.setEducationInstitutionName(getStringFromJson("education_institution_name", profileOject));
        profile.setEducationInstitutionCampusName(getStringFromJson("education_institution_campus_name", profileOject));
        profile.setEducationInstitutionCampusPhoneNumber(getStringFromJson("education_institution_campus_phone_number", profileOject));
        profile.setEducationInstitutionCampusAddress(getStringFromJson("education_institution_campus_address", profileOject));
        profile.setPrimaryContactName(getStringFromJson("primary_contact_name", profileOject));
        profile.setPrimaryContactPhone(getStringFromJson("primary_contact_phone", profileOject));
        profile.setSecondaryContactName(getStringFromJson("secondary_contact_name", profileOject));
        profile.setSecondaryContactPhone(getStringFromJson("secondary_contact_phone", profileOject));
        profile.setMedicalInsuranceProvider(getStringFromJson("medical_insurance_provider", profileOject));
        profile.setMedicalInsuranceMemberNumber(getStringFromJson("medical_insurance_member_number", profileOject));
        profile.setMedicalExistingConditions(getStringFromJson("medical_existing_conditions", profileOject));
        profile.setMedicalHistory(getStringFromJson("medical_history", profileOject));
        profile.setMedicalAllergies(getStringFromJson("medical_allergies", profileOject));
        profile.setMedicalCurrentMedication(getStringFromJson("medical_current_medication", profileOject));
        profile.setTravelInsuranceProvider(getStringFromJson("travel_insurance_provider", profileOject));
        profile.setTravelInsuranceMemberNumber(getStringFromJson("travel_insurance_member_number", profileOject));
        profile.setTravelPassportPhotoUrl(getStringFromJson("travel_passport_photo_url", profileOject));
        profile.setTravelDriverLicenseNumber(getStringFromJson("travel_driver_license_number", profileOject));
        profile.setTravelVisaType(getStringFromJson("travel_visa_type", profileOject));

        profile.setTravelVisaStartDate(getDateFromJson("travel_visa_start_date", profileOject));
        profile.setTravelVisaEndDate(getDateFromJson("travel_visa_end_date", profileOject));

        profile.setCreatedAt(getDateFromJson("created_at", profileOject));
        profile.setUpdateAt(getDateFromJson("updated_at", profileOject));
        student.setProfile(profile);


        return student;
    }


    public static Date getDayFromJson(String key, JSONObject jsonObject) {
        String jsonString = getStringFromJson(key, jsonObject);
        return DateManager.getDateFromString(jsonString, Constant.STRING_DAY_FORMAT2);

    }

    public static Date getDateFromJson(String key, JSONObject jsonObject) {
        String jsonString = getStringFromJson(key, jsonObject);

        return DateManager.getDateFromString(jsonString, Constant.STRING_DAY_FORMAT2);

    }

    public static long getLongFromJson(String key, JSONObject jsonObject) {
        long result = 0;
        try {
            result = Long.parseLong(getStringFromJson(key, jsonObject));
        } catch (Exception e) {
        }
        return result;
    }

    public static int getIntFromJson(String key, JSONObject jsonObject) {
        int result = 0;
        try {
            result = Integer.parseInt(getStringFromJson(key, jsonObject));
        } catch (Exception e) {
        }
        return result;
    }

    public static String getStringFromJson(String key, JSONObject jsonObject) {
        if (jsonObject.has(key)) {
            String result = null;
            try {
                result = jsonObject.getString(key);
            } catch (JSONException e) {
                LogUtil.v(App.TAG, e.toString());
            }
            if ("null".equals(result)) {
                return "";
            } else {
                return result;
            }
        } else {
            return "";
        }
    }

    public static String getJsonFromInputStream(InputStream ins) {
        if (null != ins) {
            StringBuffer sbf = new StringBuffer();
            BufferedInputStream bfris = new BufferedInputStream(ins);
            byte[] buffer = new byte[512];
            int ret = -1;
            try {
                while ((ret = bfris.read(buffer)) != -1) {
                    sbf.append(new String(buffer, 0, ret));
                }
                bfris.close();
                ins.close();
                return sbf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static NetResult getNetResult(JSONObject jsonObject) {
        NetResult netResult = new NetResult();
        netResult.setCode(getStringFromJson(TAG_CODE, jsonObject));
        netResult.setMessage(getStringFromJson(TAG_MESSAGE, jsonObject));

        Object[] data = new Object[1];

        try {

            if (jsonObject.has(TAG_DATA)) {
                JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
                data[0] = dataObject;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        netResult.setData(data);
        return netResult;
    }


    public static NetResult getNetResultNoData(JSONObject jsonObject) {
        NetResult netResult = new NetResult();
        netResult.setCode(getStringFromJson(TAG_CODE, jsonObject));
        netResult.setMessage(getStringFromJson(TAG_MESSAGE, jsonObject));

        return netResult;
    }

}
