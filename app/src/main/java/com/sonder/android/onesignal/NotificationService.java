package com.sonder.android.onesignal;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.common.util.LogUtil;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;
import com.sonder.android.App;
import com.sonder.android.R;
import com.sonder.android.activity.SplashActivity;
import com.sonder.android.activity.SupportDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by wangzy on 16/10/10.
 */

public class NotificationService extends NotificationExtenderService {


    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult notification) {


        String title = notification.payload.title;
        String body = notification.payload.body;

        LogUtil.i(App.TAG, "push info:" + title + " -" + body);
        LogUtil.e(App.TAG, "receive background service...");

//        try {
//
//            if (!body.contains("{")) {
//
//                notification(getBaseContext(), title, body);
//
//            } else {
//
//                JSONObject jsonObject = new JSONObject(body);
//
//                Iterator<String> keys = jsonObject.keys();
//                String k = keys.next();
//
//                body = jsonObject.getString(k);
//
//                notification(getBaseContext(), title, body);
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            LogUtil.i(App.TAG, "deal push error:" + e.getLocalizedMessage());
//        }



        return false;
    }


    private void notification(Context context, String title, String alert) {

//      if (App.getApp().getFlagLogout() || null == App.getApp().getUser()) {
//         return;
//      }

        Intent i = null;


        if (App.getApp().appVisible) {
            return;
        } else {
            i = new Intent(context, SplashActivity.class);
        }

        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent pi = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB && android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0,i, 0);
            Notification notify2 = new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // 设置状态栏中的小图片，尺寸一般建议在24×24，这个图片同样也是在下拉状态栏中所显示
                    // ，如果在那里需要更换更大的图片，可以使用setLargeIcon(Bitmap
                    // icon)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setTicker(alert)// 设置在status
                    .setContentTitle(title)// 设置在下拉status
                    // bar后Activity，本例子中的NotififyMessage的TextView中显示的标题
                    .setContentText(alert)// TextView中显示的详细内容
                    .setContentIntent(pi) // 关联PendingIntent
                    .setNumber(1) // 在TextView的右方显示的数字，可放大图片看，在最右侧。这个number同时也起到一个序列号的左右，如果多个触发多个通知（同一ID），可以指定显示哪一个。

                    .getNotification(); // 需要注意build()是在API level
            // 16及之后增加的，在API11中可以使用getNotificatin()来代替
            notify2.flags |= Notification.FLAG_AUTO_CANCEL;
            nm.notify(R.string.app_name, notify2);

        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0,i, 0);
            // 通过Notification.Builder来创建通知，注意API Level
            // API16之后才支持
            Notification notify3 = null;

            notify3 = new Notification.Builder(context)
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setTicker(alert)
                    .setContentTitle(title)
                    .setContentText(alert)
                    .setContentIntent(pi).setNumber(1).build();


            // 需要注意build()是在API
            // level16及之后增加的，API11可以使用getNotificatin()来替代
            notify3.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
            nm.notify(R.string.app_name, notify3);// 步骤4：通过通知管理器来发起通知。如果id不同，则每click，在status哪里增加一个提示

        }

    }

    private int getNotificationIcon() {

        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//      return whiteIcon ? R.drawable.icon_tip_push_s : R.mipmap.ic_launcher;
//        Notification.DEFAULT_SOUND

        return whiteIcon ? R.drawable.ic_stat_onesignal_default : R.mipmap.ic_launcher;
    }
}
