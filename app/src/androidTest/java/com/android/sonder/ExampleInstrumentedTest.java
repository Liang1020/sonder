package com.android.sonder;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.common.util.LogUtil;
import com.sonder.android.App;
import com.sonder.android.service.AlarmIntentServce;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;

import static com.baidu.location.h.j.l;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.android.sonder", appContext.getPackageName());
    }


    @Test
    public void testSomesing() throws Exception {
        LogUtil.i(App.TAG,"=======");
//
//        Locale[] locales = Locale.getAvailableLocales();
//
//        LogUtil.i(App.TAG,"locals:"+locales.length);
//
//
//        assertEquals("a", "a");

        Context appContext = InstrumentationRegistry.getTargetContext();
        AlarmIntentServce.startService(appContext);
    }
}
