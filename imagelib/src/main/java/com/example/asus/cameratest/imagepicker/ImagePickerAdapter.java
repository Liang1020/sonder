package com.example.asus.cameratest.imagepicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.asus.cameratest.R;
import com.example.asus.cameratest.camera.Camera2Activity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_TAKE_PHOTOS;
import static com.example.asus.cameratest.imagepicker.ImagePicker.TAG;


/**
 * Created by ASUS on 2016/11/3.
 */

public class ImagePickerAdapter extends RecyclerView.Adapter {
    private static final int TYPE_CAMERA = 0;
    private static final int TYPE_IMAGE = 1;

    private ImagePicker imagePicker;
    private LayoutInflater mInflater;
    private Context mContext;
    private List<String> mData;
    private int mItemLayoutId;
    private String mDirPath;

    private int count = 0;

    public ArrayList<String> mSelectedImage = new ArrayList<>();

    private OnImageSelectedCallBack onImageSelectedCallBack;

    public ImagePickerAdapter(Context context, List<String> mData, int itemLayoutId,
                            String dirPath){
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
        this.mItemLayoutId = itemLayoutId;

        this.mData = mData;
        this.mDirPath = dirPath;

        imagePicker = ImagePicker.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_CAMERA){
            return new CameraViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_camera, null));
        }else{
            return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_image, null));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(isCamera(position)){
            ImageView mImageView =  ((CameraViewHolder) holder).getView(R.id.imageViewItem);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent((Activity) mContext, Camera2Activity.class);  // jump to camera activity
                    ((Activity) mContext).startActivityForResult(intent, IMAGE_TAKE_PHOTOS);
                }
            });

        }else{
            int pos = position;
            if(imagePicker.getMode() == ImagePicker.Mode.CAMERA_PICKER){
                pos = position - 1;
                if(position == mData.size() -1 ){
                    pos = position ;
                }
            }

            final String url = mDirPath + "/" + mData.get(pos);
            final ImageView mImageView =  ((ImageViewHolder) holder).getView(R.id.imageViewItem);

            final ImageButton mSelect = ((ImageViewHolder) holder).getView(R.id.imageButtonItem);
            ///////
            if(imagePicker.getMaxImageSize() == 1){
                mSelect.setVisibility(View.VISIBLE);
            }else{
                mSelect.setVisibility(View.VISIBLE);
            }

            final Picasso picasso = Picasso.with(mContext);
            picasso.load(new File(url))
                    .centerCrop()
                    .resize(300, 400)
                    .config(Bitmap.Config.RGB_565)
                    .into(mImageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.e("Picasso error","onError");
                        }
                    });

            picasso.setLoggingEnabled(true);
            picasso.setIndicatorsEnabled(true);
    ;

            mImageView.setColorFilter(null);

            mImageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    imagePicker.setSelectedImages(mSelectedImage);

                    if (mSelectedImage.contains(url)) {
                        count--;
                        Log.i(TAG, count+"");
                        mSelectedImage.remove(url);
                        mSelect.setImageResource(R.drawable.ic_picture_unselected);
                        mImageView.setColorFilter(null);
                        if(onImageSelectedCallBack != null){
                            onImageSelectedCallBack.onUnselectedImage(position);
                        }
                    } else {
                        count++;
                        Log.i(TAG, count+"");
                        if(count > imagePicker.getMaxImageSize()){
                            count = imagePicker.getMaxImageSize();
                            return;
                        }
                        mSelectedImage.add(url);
                        mSelect.setImageResource(R.drawable.ic_pictures_selected);
                        mImageView.setColorFilter(Color.parseColor("#77000000"));
                        if(onImageSelectedCallBack != null){
                            onImageSelectedCallBack.onSelectedImage(position);
                        }
                    }

                }
            });

            if (mSelectedImage.contains(url))
            {
                mSelect.setImageResource(R.drawable.ic_pictures_selected);
                mImageView.setColorFilter(Color.parseColor("#77000000"));
            }
        }
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position){
        return isCamera(position)?TYPE_CAMERA:TYPE_IMAGE;
    }

    public boolean isCamera(int position){
        switch (imagePicker.getMode()){
            case CAMERA_PICKER:
                return position == 0;
            case PICKER_ONLY:
                return false;
            default:
                return position == 0;
        }
    }

    class CameraViewHolder extends RecyclerView.ViewHolder {
        private SparseArray<View> mViews;
        private View root;

        CameraViewHolder(View itemView) {
            super(itemView);
            this.root = itemView;
            this.mViews = new SparseArray<>();
        }

        public <T extends View> T getView(int viewId)
        {
            View view = mViews.get(viewId);
            if (view == null)
            {
                view = root.findViewById(viewId);
                mViews.put(viewId, view);
            }
            return (T) view;
        }
    }


    class ImageViewHolder extends RecyclerView.ViewHolder {
        private SparseArray<View> mViews;
        private View root;

        ImageViewHolder(View itemView) {
            super(itemView);
            this.root = itemView;
            this.mViews = new SparseArray<>();

        }

        public <T extends View> T getView(int viewId)
        {
            View view = mViews.get(viewId);
            if (view == null)
            {
                view = root.findViewById(viewId);
                mViews.put(viewId, view);
            }
            return (T) view;
        }
    }

    public interface OnImageSelectedCallBack{
        void onSelectedImage(int position);
        void onUnselectedImage(int position);
    }

    public void setOnImageSelectedCallBack(OnImageSelectedCallBack onImageSelectedCallBack){
        this.onImageSelectedCallBack = onImageSelectedCallBack;
    }

    public OnImageSelectedCallBack getOnImageSelectedCallBack(){
        return onImageSelectedCallBack;
    }
}
