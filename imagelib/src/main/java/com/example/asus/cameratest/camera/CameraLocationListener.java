package com.example.asus.cameratest.camera;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by clia on 2016/11/14.
 */

public class CameraLocationListener implements LocationListener{
    private static final String TAG = "CameraLocationListener";
    Location mLastLocation;
    boolean mValid = false;

    @Override
    public void onLocationChanged(Location newLocation) {
        if (newLocation.getLatitude() == 0.0
                && newLocation.getLongitude() == 0.0) {
            // Hack to filter out 0.0,0.0 locations
            return;
        }
        if (!mValid) {
            Log.d(TAG, "Got first location.");
        }
        mLastLocation.set(newLocation);
        Log.d(TAG, "the newLocation is " + newLocation.getLongitude() + "x" + newLocation.getLatitude());
        mValid = true;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.OUT_OF_SERVICE:
            case LocationProvider.TEMPORARILY_UNAVAILABLE: {
                mValid = false;
                break;
            }
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, " support current " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "no support current " + provider);
        mValid = false;
    }

    public Location current() {
        return mValid ? mLastLocation : null;
    }
}
