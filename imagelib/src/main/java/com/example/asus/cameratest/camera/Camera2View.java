package com.example.asus.cameratest.camera;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.view.Display;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.asus.cameratest.PreviewActivity;
import com.example.asus.cameratest.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_PREVIEW;

/**
 * Created by ASUS on 2016/10/26.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class Camera2View extends FrameLayout {
    private static final String TAG = "Camera2View";

    private static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int REQUEST_CODE = 0x100;

    /**
     * camera preview max width height
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private String mCameraId;
    private Handler mHandler;
    private HandlerThread mBackgroundThread;
    private ImageReader imageReader;
    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewBuilder;
    private CameraCaptureSession cameraCaptureSession;
    private CameraCharacteristics cameraCharacteristics;
    private Size mPreViewSize;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    private int mSensorOrientation;

    private ImageView imageViewShutter;
    //    private ImageView imageViewRetake;
    private ImageView imageViewSave;
    private ImageView imageViewCamera;
    private AutoFitTextureView autoFitTextureView;

    View rootView;
    Context context;
    private int textureWidth, textureHeight;

    private CameraLocationListener listener;
    private LocationManager locationManager;
    private CameraLocationListener listeners[] = {
            new CameraLocationListener(),
            new CameraLocationListener()
    };


    private String uri;
    private Bitmap bitmap;

    public Camera2View(Context context) {
        super(context);
        this.context = context;
        this.rootView = View.inflate(context, R.layout.view_camera2, null);

        FrameLayout.LayoutParams lpp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        addView(rootView, lpp);
    }

    public Camera2View(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.rootView = View.inflate(context, R.layout.view_camera2, null);

        FrameLayout.LayoutParams lpp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        addView(rootView, lpp);

        startBackgroundThread();
//        listener = new CameraLocationListener();
//        startRequestLocationUpdates();
        init();
    }

    public void startRequestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager =(LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1f, listeners[0]);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1F, listeners[1]);
    }

    public void stopRequestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(listeners[0]);
        locationManager.removeUpdates(listeners[1]);
    }

    public Location getCurrentLocation() {

        for (int i = 0; i < listeners.length; i++) {
            Location l = listeners[i].current();
            if (l != null) return l;
        }
        Log.d(TAG, "No location received yet.");
        return null;
    }


    public Camera2View(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void open() {
        startBackgroundThread();

//        if (0 != textureWidth && 0 != textureHeight) {
//            openCamera(textureWidth, textureHeight);
//        }
    }


    public void close() {
        closeCamera();
        stopBackgroundThread();
//        MyAnimationUtils.animationHideview(this, AnimationUtils.loadAnimation(activity, R.anim.slide_bottom_out));
    }

    private void init() {
        imageViewShutter = (ImageView) rootView.findViewById(R.id.imageViewShutter);
//        imageViewRetake = (ImageView) rootView.findViewById(R.id.imageViewRetake);
        imageViewSave = (ImageView) rootView.findViewById(R.id.imageViewSave);
        imageViewCamera = (ImageView) rootView.findViewById(R.id.imageViewCamera);
        autoFitTextureView = (AutoFitTextureView) rootView.findViewById(R.id.textureViewCamera);

        imageViewShutter.setOnClickListener(photoOnClickListener);
//        imageViewSave.setOnClickListener(saveOnClickListener);
        imageViewCamera.setOnClickListener(previewOnClickListener);
        autoFitTextureView.setSurfaceTextureListener(mSurfaceTextureListener);

        preTakePictureUI();

    }

    private void requestPermissions() {
        Activity activity = (Activity) context;
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(activity, "Camera permission is not allowed", Toast.LENGTH_SHORT).show();
                }
                activity.requestPermissions(CAMERA_PERMISSIONS, REQUEST_CODE);
            }
            return;
        }
    }

    private void startBackgroundThread() {

        if (null != mBackgroundThread) {
            return;
        }
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        try {
            if (null == mBackgroundThread) {
                return;
            }

            mBackgroundThread.quitSafely();
            mBackgroundThread.join();
            mBackgroundThread = null;
            mHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, "stopBackgroundThread error:" + e.getLocalizedMessage());
        }
    }


    private void openCamera(int width, int height) {
        Log.i("TextureView", width + "," + height);

        setupOutputs(width, height);
        configureTransform(width, height);

    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();

            if (null != cameraCaptureSession) {
                cameraCaptureSession.close();
                cameraCaptureSession = null;
            }

            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }

            if (null != imageReader) {
                imageReader.close();
                imageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    private void setupOutputs(int width, int height) {
        requestPermissions();
        Activity activity = (Activity) context;

        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        mCameraId = CameraCharacteristics.LENS_FACING_FRONT + "";
        Log.i(TAG, "CameraID:" + mCameraId);

        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            cameraCharacteristics = manager.getCameraCharacteristics(mCameraId);
            StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);


            Size[] ssize = map.getOutputSizes(SurfaceTexture.class);
            Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());


//            int xj = 0;
            for (int j = ssize.length - 1; j >= 0; j--) {

                Size s = ssize[j];
                Log.i(TAG, "OutputSizes:" + s);
//                if (s.getWidth() > MAX_PREVIEW_HEIGHT) {
//                    xj = j;
//                    largest = s;
//                    break;
//                }

            }
//            mPreViewSize = ssize[xj];

            int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();

            mSensorOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            boolean swappedDimensions = false;
            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e(TAG, "Display rotation is invalid: " + displayRotation);
            }

            Point displaySize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }


            mPreViewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, largest);

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                autoFitTextureView.setAspectRatio(
                        mPreViewSize.getWidth(), mPreViewSize.getHeight());
            } else {
                autoFitTextureView.setAspectRatio(
                        mPreViewSize.getHeight(), mPreViewSize.getWidth());
            }

            Log.i("PreviewSize", mPreViewSize.getWidth() + "," + mPreViewSize.getHeight());


            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    autoFitTextureView.setAspectRatio(
                            mPreViewSize.getHeight(), mPreViewSize.getWidth());
                }
            });

//            Log.i("PreviewSizeOptimized", mPreViewSize.getWidth()+","+mPreViewSize.getHeight());



            imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 5);

            imageReader.setOnImageAvailableListener(onImageAvaiableListener, mHandler);

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.openCamera(mCameraId, cameraOpenCallBack, mHandler);
        }catch (CameraAccessException e){
            e.printStackTrace();
        }catch (InterruptedException e){
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    private void createCameraPreviewSession() {
        try {
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            SurfaceTexture texture = autoFitTextureView.getSurfaceTexture();

            int width = mPreViewSize.getWidth();
            int height = mPreViewSize.getHeight();

            texture.setDefaultBufferSize(width, height);

            Surface surface = new Surface(texture);
            mPreviewBuilder.addTarget(surface);
            mCameraDevice.createCaptureSession(Arrays.asList(surface, imageReader.getSurface()), mSessionStateCallback, mHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = (Activity) context;
        if (null == autoFitTextureView || null == mPreViewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreViewSize.getHeight(), mPreViewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.CENTER);
            float scale = Math.max(
                    (float) viewHeight / mPreViewSize.getHeight(),
                    (float) viewWidth / mPreViewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        autoFitTextureView.setTransform(matrix);
    }

    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        List<Size> bigEnough = new ArrayList<>();
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    //third callback
    private CameraCaptureSession.CaptureCallback mSessionCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request,
                                       TotalCaptureResult result) {
            Log.i("TAG", "mSessionCaptureCallback, onCaptureCompleted");
//            if (null == mCameraDevice) {
//                return;
//            }
//
//            cameraCaptureSession = session;
//            try {
//                CaptureRequest.Builder builder = cameraCaptureSession.getDevice().createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
//                builder.addTarget(imageReader.getSurface());
//                builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
//                builder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
//                builder.set(CaptureRequest.JPEG_ORIENTATION, 90);
//                cameraCaptureSession.capture(builder.build(), mSessionCaptureCallback, mHandler);
//            } catch (CameraAccessException e) {
//                e.printStackTrace();
//            }
        }
        @Override

        public void onCaptureProgressed(CameraCaptureSession session, CaptureRequest request,
                                        CaptureResult partialResult) {
            Log.e("TAG", "mSessionCaptureCallback, onCaptureProgressed");
        }
        public void onCaptureFailed(CameraCaptureSession session, CaptureRequest request,
                                    android.hardware.camera2.CaptureFailure failure) {
            Log.e("TAG", "mSessionCaptureCallback, onCaptureFailed"+request+":"+failure);
            cameraCaptureSession=session;
            cameraCaptureSession.close();
            cameraCaptureSession=null;
            mCameraDevice.close();
            mCameraDevice = null;
            imageReader.close();
            imageReader=null;
        }
    };


    //second callback
    private CameraCaptureSession.StateCallback mSessionStateCallback = new CameraCaptureSession.StateCallback() {

        @Override
        public void onConfigured(CameraCaptureSession session) {
            cameraCaptureSession = session;
            try {
                cameraCaptureSession.setRepeatingRequest(mPreviewBuilder.build(), null, mHandler);
            } catch (CameraAccessException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
            Log.e("TAG", "CameraCaptureSession-----onConfigureFailed()");
            cameraCaptureSession=session;
            cameraCaptureSession.close();
            cameraCaptureSession=null;
            mCameraDevice.close();
            mCameraDevice = null;
            imageReader.close();
            imageReader=null;
        }
    };

    //first callback
    private CameraDevice.StateCallback cameraOpenCallBack = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice camera) {
            mCameraOpenCloseLock.release();
            mCameraDevice = camera;
            createCameraPreviewSession();
            Log.i(TAG, "Camera Opened");
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.i(TAG, "Camera Disconnected");
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
            imageReader.close();
            imageReader = null;
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.i(TAG, "Camera Open Failed");
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
            imageReader.close();
            imageReader = null;
            Activity activity = (Activity) context;
            if (null != activity) {
                activity.finish();
            }
        }
    };

    private ImageReader.OnImageAvailableListener onImageAvaiableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader imageReader) {
            mHandler.post(new ImageSaver(imageReader.acquireNextImage()));
        }
    };

    private class ImageSaver implements Runnable {
        Image reader;

        public ImageSaver(Image reader) {
            this.reader = reader;
        }

        @Override
        public void run() {
            Log.d(TAG, "save pics ...");
            String filePath = "/storage/emulated/0/DCIM/Camera/";

            File file = new File(filePath + "img_" + System.currentTimeMillis() + ".jpg");
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(file);
                ByteBuffer buffer = reader.getPlanes()[0].getBuffer();
                byte[] buff = new byte[buffer.remaining()];
                buffer.get(buff);

//                Bitmap bm = Bitmap.createBitmap(reader.getWidth(), reader.getHeight(), Bitmap.Config.RGB_565);
//                bm.copyPixelsFromBuffer(buffer);
                BitmapFactory.Options ontain = new BitmapFactory.Options();
                ontain.inSampleSize = 2;
                Bitmap bm = BitmapFactory.decodeByteArray(buff, 0, buff.length, ontain);

                bitmap = bm;
                uri = file.toString();

                outputStream.write(buff);
                Log.d(TAG, "save pic completed");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
//                if (reader != null) {
//                    reader.close();
//                }
                if (outputStream != null) {
                    try {
                        showImage(file);
                        outputStream.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int w, int h) {
            Log.e(TAG, "----------onSurfaceTextureAvailable()----------");
            textureHeight = h;
            textureWidth = w;

            openCamera(w, h);

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
            configureTransform(i, i1);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {

            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    private void showImage(final File file){

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Picasso.with(context).load(file).centerCrop().resize(mPreViewSize.getWidth(), mPreViewSize.getHeight()).config(Bitmap.Config.RGB_565)
                        .into(imageViewCamera);

                imageViewShutter.setClickable(true);

            }
        });

    }

    private void takePicture() {
        try {
            Log.d(TAG, "take photos ...");

            CaptureRequest.Builder builder = cameraCaptureSession.getDevice().createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            builder.addTarget(imageReader.getSurface());
            builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
            builder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
            builder.set(CaptureRequest.JPEG_ORIENTATION, 90);
            cameraCaptureSession.capture(builder.build(), mSessionCaptureCallback, mHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener photoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            takePicture();
            afterTakePictureUI();

        }
    };

    private View.OnClickListener saveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    private View.OnClickListener previewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (null != onCameraCallBackListener) {
                try {
                    ExifInterface exif = new ExifInterface(uri);
                    exif.getAttribute(exif.TAG_GPS_LATITUDE);
                    exif.getAttribute(exif.TAG_GPS_LONGITUDE);

                    onCameraCallBackListener.onImageInfo(bitmap, uri,  exif.getAttribute(exif.TAG_GPS_LATITUDE)+","+
                    exif.getAttribute(exif.TAG_GPS_LONGITUDE));
                    preTakePictureUI();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private void preTakePictureUI(){
        imageViewShutter.setVisibility(VISIBLE);
        imageViewCamera.setVisibility(INVISIBLE);
        imageViewShutter.setClickable(true);
    }

    private void afterTakePictureUI(){
        imageViewCamera.setVisibility(VISIBLE);
        imageViewShutter.setClickable(false);
    }

    private OnCameraCallBackListener onCameraCallBackListener;

    public interface OnCameraCallBackListener {

        void onImageInfo(Bitmap bitmap, String uri, String info);

    }

    public OnCameraCallBackListener getOnCameraCallBackListener() {
        return onCameraCallBackListener;
    }

    public void setOnCameraCallBackListener(OnCameraCallBackListener onCameraCallBackListener) {
        this.onCameraCallBackListener = onCameraCallBackListener;
    }


}
