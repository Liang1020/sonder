package com.example.asus.cameratest.imagepicker;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.cameratest.R;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_GALLERY;
import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_TAKE_PHOTOS;

public class ImageGridActivity extends AppCompatActivity {


    private ImagePicker imagePicker;

    private int mPicsSize;

    private File mImgDir;

    private List<String> mImgs;

    private RecyclerView mGirdView;
    private ImagePickerAdapter mAdapter;

    private HashSet<String> mDirPaths = new HashSet<>();

    private List<ImageSource> mImageFloders = new ArrayList<>();

    private RelativeLayout mBottomLy;

    private TextView mChooseDir;
    private ImageView mBack;
    private TextView mComplete;
    private int mImageCount = 0;

    int totalCount = 0;

    private int mScreenHeight;


    private Handler mHandler = new Handler() {

        public void handleMessage(android.os.Message msg) {

            bindData();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_grid);

        imagePicker = ImagePicker.getInstance();

        initView();
        getImages();
    }

    private void bindData(){
        if (mImgDir == null)
        {
            Toast.makeText(getApplicationContext(), "No picture scanned",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mImgs = new ArrayList(Arrays.asList(mImgDir.list()));

        Log.i("ImageFilePath", mImgDir.getAbsolutePath());

        for(int i = 0; i < mImgs.size(); i++){
            String url = mImgDir.getAbsolutePath() + "/" + mImgs.get(i);
            File file = new File(url);

            if (!file.getName().endsWith(".jpg")
                    && !file.getName().endsWith(".png")
                    && !file.getName().endsWith(".jpeg")
                    && !file.getName().contains(".")){
                Log.e("FILE NOT EXISTED", file.getAbsolutePath());
                mImgs.remove(i);
                --i;
            }
            if (file.getName().endsWith(".mp4")
                    || file.getName().endsWith(".gif")){
                Log.e("FORMAT NOT EXISTED", file.getAbsolutePath());
                mImgs.remove(i);
                --i;
            }

        }

        mAdapter = new ImagePickerAdapter(this, mImgs,
                R.layout.item_grid_image, mImgDir.getAbsolutePath());

        mAdapter.setOnImageSelectedCallBack(new ImagePickerAdapter.OnImageSelectedCallBack() {
            @Override
            public void onSelectedImage(int position) {
                mComplete.setText("Complete "+ ++mImageCount +"/"+ imagePicker.getMaxImageSize());
            }

            @Override
            public void onUnselectedImage(int position) {
                mComplete.setText("Complete "+ --mImageCount +"/"+ imagePicker.getMaxImageSize());
            }
        });
        mGirdView.setAdapter(mAdapter);

        GridLayoutManager layoutManager = new GridLayoutManager(this, imagePicker.getColumnNumber());
        mGirdView.setLayoutManager(layoutManager);

        Log.i("PicCount", totalCount + "pics" + mImgs.size());

    }

    private void getImages()
    {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED))
        {
            Toast.makeText(this, "no ecternal storage", Toast.LENGTH_SHORT).show();
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "Camera permission is not allowed", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0x111);
            }
            return;
        }

        new Thread(new Runnable()
        {
            @Override
            public void run() {

                String firstImage = null;

                Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                ContentResolver mContentResolver = ImageGridActivity.this
                        .getContentResolver();


                Cursor mCursor = mContentResolver.query(mImageUri, null,
                        MediaStore.Images.Media.MIME_TYPE + "=? or "
                                + MediaStore.Images.Media.MIME_TYPE + "=?",
                        new String[] { "image/jpeg", "image/png" },
                        MediaStore.Images.Media.DATE_MODIFIED);

                Log.e("TAG", mCursor.getCount() + "");
                while (mCursor.moveToNext())
                {
                    String path = mCursor.getString(mCursor
                            .getColumnIndex(MediaStore.Images.Media.DATA));

                    Log.e("TAG", path);

                    if (firstImage == null)
                        firstImage = path;
                    File parentFile = new File(path).getParentFile();
                    if (parentFile == null)
                        continue;
                    String dirPath = parentFile.getAbsolutePath();
                    ImageSource imageSource = null;
                    if (mDirPaths.contains(dirPath))
                    {
                        continue;
                    } else
                    {
                        mDirPaths.add(dirPath);
                        imageSource = new ImageSource();
                        imageSource.setDir(dirPath);
                        imageSource.setFirstImagePath(path);
                    }

                    int picSize = parentFile.list(new FilenameFilter()
                    {
                        @Override
                        public boolean accept(File dir, String filename)
                        {
                            if (filename.endsWith(".jpg")
                                    || filename.endsWith(".png")
                                    || filename.endsWith(".jpeg"))
                                return true;
                            return false;
                        }
                    }).length;
                    totalCount += picSize;

                    imageSource.setCount(picSize);
                    mImageFloders.add(imageSource);

                    if (picSize > mPicsSize)
                    {
                        mPicsSize = picSize;
                        mImgDir = parentFile;
                    }
                }
                mCursor.close();

                mDirPaths = null;

                mHandler.sendEmptyMessage(0x110);

            }
        }).start();

    }

    private void initView() {
        mGirdView = (RecyclerView) findViewById(R.id.gridViewImages);
        mChooseDir = (TextView) findViewById(R.id.textViewChooseDir);
        mComplete = (TextView) findViewById(R.id.textViewComplete);
        mBack = (ImageView) findViewById(R.id.imageViewBack);

        setListener();
    }

    private void setListener(){
        mChooseDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putStringArrayListExtra("uris", imagePicker.getSelectedImages());

                ImageGridActivity.this.setResult(IMAGE_GALLERY, intent);
                ImageGridActivity.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i("ImageGridActivity", resultCode +"," +requestCode);

        if(resultCode == IMAGE_TAKE_PHOTOS && requestCode == IMAGE_TAKE_PHOTOS) {

            Log.i("ImageGridActivity", resultCode +"," +requestCode+"" +data.getStringExtra("uri"));
            ImageGridActivity.this.setResult(IMAGE_TAKE_PHOTOS, data);
            ImageGridActivity.this.finish();
        }
    }

}
