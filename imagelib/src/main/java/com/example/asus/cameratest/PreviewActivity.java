package com.example.asus.cameratest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_TAKE_PHOTOS;

public class PreviewActivity extends AppCompatActivity {
    private ImageView imageView;
    private FrameLayout frameLayoutAgain;
    private FrameLayout frameLayoutDone;
    private String uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        initView();
    }

    private void initView(){
        imageView = (ImageView) findViewById(R.id.imageViewPreview);
        frameLayoutAgain = (FrameLayout) findViewById(R.id.btnAgain);
        frameLayoutDone = (FrameLayout) findViewById(R.id.btnDone);

        Display dis = getWindowManager().getDefaultDisplay();

        Point point = new Point();
        dis.getSize(point);

        int ww = point.x;
        int wh = point.y ;

        uri = getIntent().getStringExtra("uri");
        Picasso.with(getApplicationContext()).load(new File(uri)).centerInside().resize(wh,ww).config(Bitmap.Config.RGB_565)
                .into(imageView);

        frameLayoutDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("uri",uri);
                PreviewActivity.this.setResult(RESULT_OK, intent);
                PreviewActivity.this.finish();
            }
        });

        frameLayoutAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviewActivity.this.finish();
            }
        });

    }
}
