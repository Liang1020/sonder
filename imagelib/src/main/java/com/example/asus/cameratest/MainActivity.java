package com.example.asus.cameratest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.asus.cameratest.camera.Camera2Activity;
import com.example.asus.cameratest.imagepicker.ImageGridActivity;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_GALLERY;
import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_PICK;
import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_TAKE_PHOTOS;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout) findViewById(R.id.activity_main);
        button = (Button) findViewById(R.id.buttonAdd);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ImageGridActivity.class);
                startActivityForResult(intent, IMAGE_PICK);

//                Intent intent = new Intent(MainActivity.this, Camera2Activity.class);
//                startActivityForResult(intent, IMAGE_PICK);// jump to camera
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
//        camera2View.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(resultCode == IMAGE_TAKE_PHOTOS && requestCode == IMAGE_PICK){

            Log.i("MainActivity", resultCode +"," +requestCode+"" +data.getStringExtra("uri"));
            ImageView imageView = new ImageView(this);
            linearLayout.addView(imageView);
            Picasso.with(getApplicationContext()).load(new File(data.getStringExtra("uri"))).centerCrop().resize(800, 800)
                    .config(Bitmap.Config.RGB_565).error(R.drawable.ic_pictures_no)
                    .into(imageView);

        }

        if(resultCode == IMAGE_GALLERY && requestCode == IMAGE_PICK){

            List<String> images = data.getStringArrayListExtra("uris");
            if(images.isEmpty()){
                return;
            }
            for(int i=0; i<images.size(); i++){
                ImageView imageView = new ImageView(this);
                linearLayout.addView(imageView);
                Picasso.with(getApplicationContext()).load(new File(images.get(i))).centerCrop().resize(800, 800)
                        .config(Bitmap.Config.RGB_565).error(R.drawable.ic_pictures_no)
                        .into(imageView);

            }
            Log.i("Activity", resultCode +"," +requestCode+"" +data.getStringArrayListExtra("uris"));

        }

    }
}
