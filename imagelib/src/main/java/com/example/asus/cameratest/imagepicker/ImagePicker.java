package com.example.asus.cameratest.imagepicker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clia on 2016/11/9.
 */

public class ImagePicker {
    public static final String TAG = ImagePicker.class.getSimpleName();

    public static final int IMAGE_PICK = 0x101;
    public static final int IMAGE_TAKE_PHOTOS = 0x102;
    public static final int IMAGE_GALLERY = 0x103;
    public static final int IMAGE_PREVIEW = 0x104;

    public enum Mode{
        PICKER_ONLY, CAMERA_PICKER
    }

    private ArrayList<String> mSelectedImages = new ArrayList<>();
    private int maxImageSize = 9;
    private int columnNumber = 3;
    private Mode mode = Mode.PICKER_ONLY;

    private List<String> mImageFolders;

    private static ImagePicker mInstance;

    private ImagePicker() {
    }

    public static ImagePicker getInstance() {
        if (mInstance == null) {
            synchronized (ImagePicker.class) {
                if (mInstance == null) {
                    mInstance = new ImagePicker();
                }
            }
        }
        return mInstance;
    }

    public ArrayList<String> getSelectedImages() {
        return mSelectedImages;
    }

    public void setSelectedImages(ArrayList<String> mSelectedImages) {
        this.mSelectedImages = mSelectedImages;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    public int getMaxImageSize() {
        return maxImageSize;
    }

    public void setMaxImageSize(int maxImageSize) {
        this.maxImageSize = maxImageSize;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }
}
