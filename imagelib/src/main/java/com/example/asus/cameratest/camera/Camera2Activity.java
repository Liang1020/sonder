package com.example.asus.cameratest.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.asus.cameratest.PreviewActivity;
import com.example.asus.cameratest.R;
import com.example.asus.cameratest.crop.Crop;
import com.example.asus.cameratest.imagepicker.ImageGridActivity;

import java.io.File;

import static android.view.Window.FEATURE_NO_TITLE;
import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_PREVIEW;
import static com.example.asus.cameratest.imagepicker.ImagePicker.IMAGE_TAKE_PHOTOS;

public class Camera2Activity extends AppCompatActivity {
    Camera2View camera2View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(FEATURE_NO_TITLE);
        setContentView(R.layout.activity_camera2);

        camera2View = (Camera2View) findViewById(R.id.camera2View);

        camera2View.setOnCameraCallBackListener(new Camera2View.OnCameraCallBackListener() {
            @Override
            public void onImageInfo(Bitmap bitmap, String uri, String info) {
                Log.i("Camera2Activity", bitmap.toString());
                Log.i("Camera2Activity", uri);
                Log.i("Camera2Activity", info);
                Intent intent = new Intent(Camera2Activity.this, PreviewActivity.class);
                intent.putExtra("uri",uri);

                startActivityForResult(intent,IMAGE_PREVIEW);
                camera2View.close();


//                beginCrop(Uri.parse("file://"+uri));
//                Camera2Activity.this.setResult(RESULT_OK, intent);
//                Camera2Activity.this.finish();
            }

        });


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i("OnResume", "----------");
        camera2View.open();

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i("onDestroy", "-----------");
        camera2View.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i("Camera2Activity", resultCode +"," +requestCode);
        if(resultCode == RESULT_OK && requestCode == IMAGE_PREVIEW) {

            Log.i("Camera2Activity",data.getStringExtra("uri"));
            Camera2Activity.this.setResult(IMAGE_TAKE_PHOTOS, data);
            Camera2Activity.this.finish();
        }
    }
}
