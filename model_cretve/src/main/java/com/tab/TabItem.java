package com.tab;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tab.widget.CheckImageView;
import com.tab.widget.CheckTextView;

/**
 * Created by wangzy on 2016/11/3.
 */

public class TabItem {

    public View rootView;
    private CheckImageView imageView;
    private CheckTextView textView;


    public TabItem(LinearLayout linearLayout) {

        this.rootView = linearLayout;

        Object[] widgets = findImageTextViewFromContainer(linearLayout);

        this.imageView = (CheckImageView) widgets[0];
        this.textView = (CheckTextView) widgets[1];
    }


    public void check() {
        imageView.check();
        textView.check();
    }

    public void unCheck() {
        imageView.uncheck();
        textView.uncheck();
    }


    Object[] findImageTextViewFromContainer(ViewGroup view) {

        Object[] object = new Object[2];

        CheckImageView imageView = (CheckImageView) view.getChildAt(0);
        CheckTextView textView = (CheckTextView) view.getChildAt(1);
        object[0] = imageView;
        object[1] = textView;

        return object;

    }


}
